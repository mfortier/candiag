# TODO:
#	- try to remove the reliance on VPATH, as it slows things down due to the searching

# default compiler template
COMPILER_TEMPLATE 	?= compile.intel.xc50

# bring in compiler flags
include $(COMPILER_TEMPLATE)

# define specific CPP directives
CPPDEFS = 
CPPDEFS_32BIT = $(CPPDEFS) -D_PAR_NWORDIO=2
CPPDEFS_64BIT = $(CPPDEFS) -D_PAR_NWORDIO=1

# define pertinent directories
LIB64_DIR	= lib64
LIB32_DIR	= lib32
BIN_DIR		= bin
BUILD_DIR	= .

# define plotting program specific settings
PLT_LIB_DIR = $(LIB32_DIR)
PLT_CPPDEFS = $(CPPDEFS_32BIT)
PLT_FFLAGS	= $(FFLAGS_32BIT)

# define directories to search for source archive source files
VPATH = $(shell find ../lssub -type d)

# find all subroutine files in ../lssub
LIB_SOURCE = $(shell find ../lssub -type f -name '*.f' -o -name '*.c')

# create object file lists for the libraries
TMP_LIB_OBJECTS		= $(notdir $(patsubst %.c, %.o, $(patsubst %.f, %.o, $(LIB_SOURCE))))
LIB_64BIT_OBJECTS	= $(addprefix $(LIB64_DIR)/, $(TMP_LIB_OBJECTS))
LIB_32BIT_OBJECTS	= $(addprefix $(LIB32_DIR)/, $(TMP_LIB_OBJECTS))

# find all source program files in ../lspgm
DIAG_BIN_SOURCE = $(shell find ../lspgm/ -path ../lspgm/plots -prune -false -o -type f -name '*.f')

# find plotting program files in ../lspgm
PLOT_BIN_SOURCE  = $(shell find ../lspgm/plots -type f -name '*.f')
PLOT_BIN_TARGETS = $(addprefix $(BIN_DIR)/, $(notdir $(basename $(PLOT_BIN_SOURCE))))

# define target/source lists for ALL 64bit programs
#	NOTE: generally, we don't build all the targets and this is included for legacy support
ALL_DIAG_64BIT_BIN_SOURCE  = $(DIAG_BIN_SOURCE)
ALL_DIAG_64BIT_BIN_TARGETS = $(addprefix $(BIN_DIR)/, $(notdir $(basename $(ALL_DIAG_64BIT_BIN_SOURCE))))

# define sublist of 64bit targets to allow for smaller/shorter compilation by default
DEFAULT_DIAG_64BIT_PGMS=add beta bin2txt bins binsml chabin cofagg cwinds delhat div epflux_cmip6 expone fmask fmskplt frall gadd gdiv ggacof ggstat globavg globavl globavw gmlt gpxstat gsapl gsapl_lnsp gsaplt gsapres gshumh gsmslph gstracx gsub gwtqd joinrtd joinup mag2d mkwght mlt mstrfcn newnam rcopy recmax recmin relabl rmax rmerge rmin rsplit rzonavg select sellev separmc spltall spltslb sqroot square statsav sub taphi timavg timmax timmin tstep txt2bin window xfind xjoin xlin xsave xylin zonavg lpcrev2 icntrld initgs9 initss9 boxl2hr initg14
TMP_DEFAULT_DIAG_64BIT_BIN_SOURCE	= $(addsuffix .f, $(DEFAULT_DIAG_64BIT_PGMS))
DEFAULT_DIAG_64BIT_BIN_SOURCE		= $(filter $(addprefix %/, $(TMP_DEFAULT_DIAG_64BIT_BIN_SOURCE)), $(DIAG_BIN_SOURCE))
DEFAULT_DIAG_64BIT_BIN_TARGETS		= $(addprefix $(BIN_DIR)/, $(notdir $(basename $(DEFAULT_DIAG_64BIT_BIN_SOURCE))))

# define sublist 32bit target list 
#	NOTE: we only need a subset of these, and we append "_float1" to differentiate between the 64bit counter parts
DEFAULT_DIAG_32BIT_PGMS = unpakrs rcopy newnam murge pakrs pakgcm9 perturb select separmc rstime joinup pakgcm8 pakrs2 joinpio sub paktnd perturb_winds ggstat gcmrcpy lpcrev2 icntrld initgs9 initss9 boxl2hr initg14
TMP_DEFAULT_DIAG_32BIT_BIN_SOURCE	= $(addsuffix .f, $(DEFAULT_DIAG_32BIT_PGMS))
DEFAULT_DIAG_32BIT_BIN_SOURCE		= $(filter $(addprefix %/, $(TMP_DEFAULT_DIAG_32BIT_BIN_SOURCE)), $(DIAG_BIN_SOURCE))
DEFAULT_DIAG_32BIT_BIN_TARGETS		= $(addprefix $(BIN_DIR)/, $(addsuffix _float1, $(notdir $(basename $(DEFAULT_DIAG_32BIT_BIN_SOURCE)))))

.PHONY: all_diag_bins default_bins folders clean plot_pgms dummy 
.DEFAULT_GOAL := default_bins

# create function to create rules diagnostic binaries (not ploting pgms)
#	arg1 : target name
#	arg2 : program source file
#	arg3 : compilation flags
#	arg4 : lib directory
#
define DIAG_BIN_RULE
$(BIN_DIR)/$(1): $(2) $(4)/libcandiag.a $(4)/diag_sizes.o
	$(FC) $(3) -o $$@ $$^ $(MOD_FLAG) $(4)
endef

# create function to create rules for plotting programs
#	arg1 : target name
#	arg2 : program source file
#
define PLOT_BIN_RULE
$(BIN_DIR)/$(1): $(2) $(PLT_LIB_DIR)/libcandiag.a $(PLT_LIB_DIR)/diag_sizes.o
	$(FC) $(FFLAGS) -o $$@ $$^ $(MOD_FLAG) $(PLT_LIB_DIR) $(PLT_LDFLAGS)
endef

# create folders
folders:
	mkdir -p $(BIN_DIR)
	mkdir -p $(LIB64_DIR)
	mkdir -p $(LIB32_DIR)

#================
# 64 Bit targets
#================
# generic lib object rules (for subroutines)
$(LIB64_DIR)/%.o : $(notdir %.f)
	$(FC) $(FFLAGS_64BIT) -c $< -o $@
$(LIB64_DIR)/%.o : $(notdir %.c)
	$(CC) $(CFLAGS) -c $< -o $@

# subroutine library
$(LIB64_DIR)/libcandiag.a: $(LIB_64BIT_OBJECTS) 
	ar rc $@ $^

# sizes module
$(LIB64_DIR)/diag_sizes.o: ../diag_sizes.f90
	$(FC) $(FFLAGS_64BIT) $(CPPDEFS_64BIT) -fpp -c $< -o $@ $(MOD_FLAG) $(LIB64_DIR)

# build rules for ALL non-plotting 64bit targets
$(foreach bin_source,$(ALL_DIAG_64BIT_BIN_SOURCE),$(eval $(call DIAG_BIN_RULE,$(notdir $(basename $(bin_source))),$(bin_source),$(FFLAGS_64BIT),$(LIB64_DIR))))

#================
# 32 Bit targets
#================
# generic lib object rule (for subroutines)
$(LIB32_DIR)/%.o : $(notdir %.f)
	$(FC) $(FFLAGS_32BIT) -c $< -o $@
$(LIB32_DIR)/%.o : $(notdir %.c)
	$(CC) $(CFLAGS) -c $< -o $@

# subroutine library
$(LIB32_DIR)/libcandiag.a: $(LIB_32BIT_OBJECTS)
	ar rc $@ $^

# sizes module
$(LIB32_DIR)/diag_sizes.o: ../diag_sizes.f90
	$(FC) $(FFLAGS_32BIT) $(CPPDEFS_32BIT) -fpp -c $< -o $@ $(MOD_FLAG) $(LIB32_DIR)

# here we only create rules for the default non-plotting targets, but it could easily be expanded
$(foreach bin_source,$(DEFAULT_DIAG_32BIT_BIN_SOURCE),$(eval $(call DIAG_BIN_RULE,$(notdir $(basename $(bin_source)))_float1,$(bin_source),$(FFLAGS_32BIT),$(LIB32_DIR))))

# create rules for plotting programs, which are always in 32bit mode
$(foreach bin_source,$(PLOT_BIN_SOURCE),$(eval $(call PLOT_BIN_RULE,$(notdir $(basename $(bin_source))),$(bin_source))))

# build the plotting programs
plot_pgms: folders $(PLOT_BIN_TARGETS) 

# ALL binaries (excluding plotting programs)
all_diag_bins: folders $(ALL_DIAG_64BIT_BIN_TARGETS) $(DEFAULT_DIAG_32BIT_BIN_TARGETS)

# DEFAULT binaries only
default_bins: folders $(DEFAULT_DIAG_64BIT_BIN_TARGETS) $(DEFAULT_DIAG_32BIT_BIN_TARGETS)

clean:
	rm -rf lib* bin

dummy: 
	@echo $(DEFAULT_DIAG_64BIT_BIN_SOURCE)
