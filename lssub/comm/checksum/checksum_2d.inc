! This included file is intended to prevent duplicated code that would need to be present for each kind of 2d array

  integer :: is, ie, js, je
  logical :: indices_present

  indices_present = present(is_in) .or. present(ie_in) .or. &
                    present(js_in) .or. present(js_in)


  if (present(halo) .and. indices_present) &
    error stop "Checksumming accepts only index range OR halo size, and both were supplied"

  is = LBOUND(array,1)
  ie = UBOUND(array,1)
  js = LBOUND(array,2)
  je = UBOUND(array,2)

  if (present(is_in)) is = is_in
  if (present(ie_in)) ie = ie_in
  if (present(js_in)) js = js_in
  if (present(je_in)) je = je_in

  if (present(halo)) then
    is = is + halo
    ie = ie - halo
    js = js + halo
    je = je - halo
  endif

  chksum = SUM(bitcount(array(is:ie,js:je)))