! This included file is intended to prevent duplicated code that would need to be present for each kind of 3d array

  integer :: is, ie, js, je, ks, ke
  logical :: indices_present

  indices_present = present(is_in) .or. present(ie_in) .or. &
                    present(js_in) .or. present(js_in) .or. &
                    present(ks_in) .or. present(ks_in)


  if (present(halo) .and. indices_present) &
    error stop "Checksumming accepts only index range OR halo size, and both were supplied"

  is = LBOUND(array,1)
  ie = UBOUND(array,1)
  js = LBOUND(array,2)
  je = UBOUND(array,2)
  ks = LBOUND(array,3)
  ke = UBOUND(array,3)

  if (present(is_in)) is = is_in
  if (present(ie_in)) ie = ie_in
  if (present(js_in)) js = js_in
  if (present(je_in)) je = je_in
  if (present(ks_in)) ks = ks_in
  if (present(ke_in)) ke = ke_in

  if (present(halo)) then
    is = is + halo
    ie = ie - halo
    js = js + halo
    je = je - halo
    ks = ks + halo
    ke = ke - halo
  endif

  chksum = SUM(bitcount(array(is:ie,js:je,ks:ke)))