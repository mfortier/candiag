! This included file is intended to prevent duplicated code that would need to be present for each kind of 1d array

  integer :: is, ie
  logical :: indices_present

  indices_present = present(is_in) .or. present(ie_in)

  if (present(halo) .and. indices_present) &
    error stop "Checksumming accepts only index range OR halo size, and both were supplied"

  is = LBOUND(array,1)
  ie = UBOUND(array,1)

  if (present(is_in)) is = is_in
  if (present(ie_in)) ie = ie_in

  if (present(halo)) then
    is = is + halo
    ie = ie - halo
  endif

  chksum = SUM(bitcount(array(is:ie)))