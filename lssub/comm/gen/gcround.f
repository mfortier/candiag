      SUBROUTINE GCROUND (GCROW,IS,IF)
C 
C     * JUL 25/84 - R.LAPRISE.
C     * ROUND VALUES OF GCROW TO NEAREST ELEMENT OF SET (-1., 0., 1.).
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GCROW(1) 
C---------------------------------------------------------------------- 
      DO 100 I=IS,IF
      GC=0.E0 
      IF(GCROW(I).LT.-0.5E0)GC=-1.E0 
      IF(GCROW(I).GT.+0.5E0)GC=+1.E0 
      GCROW(I)=GC 
  100 CONTINUE
      RETURN
      END 
