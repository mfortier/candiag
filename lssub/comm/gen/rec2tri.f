      SUBROUTINE REC2TRI(TRI,REC,LA,LSR,LM)
C 
C     * NOV 03/03 - M.LAZARE. NEW ROUTINE TO TAKE SPECTRAL DATA 
C     *                       IN "RECTANGULAR" ORDER (IE 1,LM,2,LM-1,...)
C     *                       AND CONVERT TO THE USUAL "TRIANGULAR" ORDER.
C 
      COMPLEX TRI(LA)
      COMPLEX REC(LA) 
      INTEGER LSR(2,LM+1)
C---------------------------------------------------------------------
      LMHALF=LM/2
      LAC=0
      DO M=1,LMHALF

        MVAL=M
        NL=LSR(1,MVAL)
        NR=LSR(1,MVAL+1)-1
        DO N=NL,NR
          LAC=LAC+1
          TRI(N)=REC(LAC)
        ENDDO
C
        MVAL=LM-M+1
        NL=LSR(1,MVAL)
        NR=LSR(1,MVAL+1)-1
        DO N=NL,NR
          LAC=LAC+1
          TRI(N)=REC(LAC)
        ENDDO

      ENDDO
C
      RETURN
      END
