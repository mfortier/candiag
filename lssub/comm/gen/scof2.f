      SUBROUTINE SCOF2(SC,LR,LM,KIND) 
C 
C     *****   OCT 1975  -  JOHN D. HENDERSON  ****
C     * ZERO OR DOUBLE THE COMPLEX SPECTRAL COEFFICIENTS IN SC(LR,LM) 
C     * DEPENDING ON THE VALUE OF KIND. 
C 
C     * LEVEL 2,SC
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX SC(LR,1)
C-----------------------------------------------------------------------
C 
C     * IF KIND=0 SET ALL OF SC TO (0.,0.). 
C 
      IF(KIND.NE.0) GO TO 30
      DO 20 M=1,LM
      DO 20 N=1,LR
   20 SC(N,M)=(0.E0,0.E0) 
C 
C     * IF KIND=2 DOUBLE ALL OF SC. 
C 
   30 IF(KIND.NE.2) GO TO 99
      DO 40 M=1,LM
      DO 40 N=1,LR
   40 SC(N,M)=SC(N,M)+SC(N,M) 
C 
   99 RETURN
      END 
