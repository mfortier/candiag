      SUBROUTINE CF1CI64(CF,IEEEF,NF)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * APR 15/92 - J. STACEY - A SLIGHTLY MODIFIED VERSION OF "CFCI64",
C     *                         WHICH WILL COMPILE ON BOTH 32 AND 64 BIT
C     *                         MACHINES (BUT OBVIOUSLY WILL GIVE USABLE
C     *                         RESULTS ONLY ON CRAYS).
C     *
C     * CONVERT FLOATING POINT, CRAY TO IEEE 64-BIT
C     *
C     *
C     * INPUT : CF      CRAY FLOATING POINT NUMBERS
C     *         NF      NUMBER OF ELEMENTS IN CF
C     * OUTPUT: IEEEF   IEEE FLOATING POINT NUMBERS
C     *
C     * FORMAT :
C     *           SIGN  EXPONENT  MANTISSA
C     * IEEE :     1      11        52
C     * CRAY :     1      15        48
C 
      IMPLICIT   INTEGER(A-Z)
      INTEGER    IEEEF(1),CF(1)
      LOGICAL    ERROR
      PARAMETER (CEXPBIAS=16384)   ! CRAY EXPONENT BIAS (40000(OCTAL))
      PARAMETER (IEXPBIAS=1023)    ! IEEE EXPONENT BIAS
C-------------------------------------------------------------------------------

      MBSE0 = 0
      MBSE63 = 63
      MASK1 = IBSET(MBSE0,MBSE63)
      MASK2 = IBITS(-1,0,15)
      MBITM1 = -1
      MBIT0 = 0
      MBIT52 = 52
      MASK3 = IBITS(MBITM1,MBIT0,MBIT52)
      MASK4 = IBITS(-1,0,11)

C     * SET SIGN BIT, EXPONENT AND MANTISSA IN ONE VECTOR LOOP

      DO 10 I=1,NF
          MSHFM48 = -48
          MSHF52 = 52
          MSHF5 = 5
          IEEEF(I) = IOR(IOR(
     1            (IAND(CF(I),MASK1))
     2                  ,
     3            (ISHFT((IAND(ISHFT(CF(I),MSHFM48),MASK2)-
     4               CEXPBIAS+IEXPBIAS-1),MSHF52) )             )
     5                  ,
     6            (IAND(ISHFT(CF(I),MSHF5),MASK3))
     7                  )
10    CONTINUE

C     * HANDLE 0.0, UNDERFLOW AND OVERFLOW :
C     *
C     * IF THE EXPONENT FIELD GOES NEGATIVE THEN THE CRAY NUMBER WAS
C     * EITHER 0.0 OR TOO SMALL TO REPRESENT IN IEEE, IN EITHER CASE
C     * SET THE IEEE RESULT TO ALL 0'S WHICH IS THE IEEE REPRESENTATION 
C     * FOR 0.0.  IF THE EXPONENT FIELD OVERFLOWS, CALL ABORT.

      ERROR = .FALSE.
      DO 20 I=1,NF
          MSHFM48 = -48
          IF ((IAND(ISHFT(CF(I),MSHFM48),MASK2)-CEXPBIAS+IEXPBIAS-1)
     1           .LT.0)
     1           IEEEF(I)=0
          MSHFM48 = -48
          IF ((IAND(ISHFT(CF(I),MSHFM48),MASK2)-CEXPBIAS+IEXPBIAS-1)
     1           .GT.MASK4)  THEN
            ERROR = .TRUE.
          END IF
20    CONTINUE
 
      IF (.NOT. ERROR) RETURN
      WRITE(6,6100)
 6100 FORMAT('0***ERROR*** CF1CI64:CRAY EXPONENT OUT OF RANGE.')
      CALL ABORT
      END
