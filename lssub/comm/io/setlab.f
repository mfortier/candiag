      SUBROUTINE SETLAB(IBUF,I1,I2,I3,I4,I5,I6,I7,I8) 
C 
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * JAN 30/78 - J.D.HENDERSON 
C     * THIS ROUTINE FILLS THE DIAGNOSTIC OUTPUT LABEL IBUF(8)
C     * FROM THE EIGHT WORDS I1,I2,...I8. 
C     * IBUF(N) IS NOT SET IF I(N) IS -1. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER IBUF(8) 
C-------------------------------------------------------------------- 
C 
      IF(I1.NE.-1) IBUF(1)=I1 
      IF(I2.NE.-1) IBUF(2)=I2 
      IF(I3.NE.-1) IBUF(3)=I3 
      IF(I4.NE.-1) IBUF(4)=I4 
      IF(I5.NE.-1) IBUF(5)=I5 
      IF(I6.NE.-1) IBUF(6)=I6 
      IF(I7.NE.-1) IBUF(7)=I7 
      IF(I8.NE.-1) IBUF(8)=I8 
C 
      RETURN
      END 
