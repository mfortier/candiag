      SUBROUTINE PUTFLD2(NF,G,IBUF,MAXPK)
C             
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * APR 21/92 - J.STACEY - REVISED TO IMPLEMENT PORTABILITY ACROSS
C     *                        BOTH 32- AND 64-BIT MACHINES.  
C     * JAN 18/80 - J.D.HENDERSON      
C
C     * WRITES FIELD G TO FILE NF.
C     * ALL LABEL WORDS IN IBUF(1-8) MUST BE PRESET.
C     * MAXPK IS THE MAXIMUM LENGTH OF A PACKED FIELD. 
C                                                     
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON /MACHTYP/ MACHINE,INTSIZE
      DIMENSION G(1)                                      
C                                                   
      INTEGER JBUF(8),IBUF(8)                      
C-------------------------------------------------------------------- 
C                                                                    
C     * PRESERVE IBUF ARRAY IN JBUF...
C                                                                   
      DO 10 I= 1,8
        JBUF(I)=IBUF(I)
  10  CONTINUE
C                                                                  
C     * FIRST PACK THE FIELD INTO IBUF.                           
C                                                                
      CALL RECPK2(IBUF,MAXPK,G)                                 
C                                                              
C     * WRITE THE RECORD TO FILE NF.                          
C                                                            
      CALL RECPUT(NF,IBUF)                                 
C                                                          
C     * RESTORE IBUF ARRAY FROM JBUF...
C                                                         
      DO 20 I= 1,8
        IBUF(I)=JBUF(I)
  20  CONTINUE
C                                                        
      RETURN                                            
      END                                              
