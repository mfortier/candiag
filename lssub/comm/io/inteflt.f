      INTEGER FUNCTION INTEFLT(IDUMMY)
C
C     * MAY 05/08 - F.MAJAESS (ENSURE BNUM(1) IS INITIALIZED)
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * A.J. STACEY - AUG  6 1992 - USE DIMENSION STATEMENT TO DECLARE ANUM 
C     *                             AND BNUM.  THIS TRACKS THE SIZE OF A FLOATING
C     *                             POINT VARIABLE BETTER THAN A REAL DECLARATION,
C     *                             ESPECIALLY WHEN "-R8" OPTION IS SPECTIFIED.
C     * A.J. STACEY - MAR 31 1992 - THIS ROUTINE CHECKS IF INTEGER WORD SIZE
C     *                             IS THE SAME AS THE REAL WORD SIZE.  THIS
C     *                             IS SUBTLY DIFFERENT FROM THE OUTPUT OF
C     *                             "ME32O64".
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION    ANUM(1),BNUM(1)
      INTEGER      INUM,JNUM
      EQUIVALENCE (ANUM,INUM),(BNUM,JNUM)
C-------------------------------------------------------------------------------
      BNUM(1) =-DSQRT(2.0D0) + 1.0D0
      ANUM(1) = DSQRT(2.0D0)
      JNUM    = INUM
      IF (ANUM(1) .NE. BNUM(1)) THEN
       INTEFLT = 2
      ELSE
       INTEFLT = 1
      ENDIF
      RETURN
      END
