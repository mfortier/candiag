      SUBROUTINE BF2BI64(BF,IEEEF,NF)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * JUL 27/92 - J. STACEY - CORRECTED NORMALIZATION OF IBM NUMBERS.
C     * APR 15/92 - J. STACEY -
C
C     * CONVERT FLOATING POINT: IBM 32-BIT TO IEEE 64-BIT.  THIS ROUTINE
C     * IMPLICITLY ASSUMES THAT BOTH "BF" AND "IEEEF" ARE 32-BITS IN NATIVE
C     * FORMAT.
C
      IMPLICIT INTEGER (A-Z)
      INTEGER      BF(1),IEEEF(1)
      DIMENSION    ZERO(1)
      INTEGER      IZERO
      EQUIVALENCE (ZERO,IZERO)
      DATA         ZERO/0.0E0/
      INTEGER      IEXP(15),IMAN(15)       !IEEE NORMALIZATION FACTORS.
      DATA         IEXP/-4,-3,-3,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1/
      DATA         IMAN/ 0,-1,-1,-2,-2,-2,-2,-3,-3,-3,-3,-3,-3,-3,-3/
C------------------------------------------------------------------------
      MBSE0 = 0
      MBSE31 = 31
      MASK1        = IBSET(MBSE0,MBSE31)  !1 BIT MASK FOR SIGN BIT.
      MASK2        = IBITS(-1,0,7)        !7 BIT MASK FOR IBM EXPONENT.
      MASK3        = IBITS(-1,0,20)  !20 BIT MASK FOR IEEE MANTISSA.
      MASK4        = IBITS(-1,0,4)   !4 BIT MASK FOR FIRST HEX DIGIT.
      MASK5        = IBITS(-1,0,11)  !11 BIT MASK FOR IEEE EXPONENT.
      MASK6        = IBITS(-1,0,3)   !3 BIT MASK FOR L.S.B. IEEE MANT.
      K            = 1
      DO 100 I=1,2*NF-1,2
        IF (BF(K) .NE. IZERO) THEN
            MSHFM20 = -20
            J = IAND(ISHFT(BF(K),MSHFM20),MASK4)
            MSHFM24 = -24
            MSHF20 = 20
            IEEEF(I)  = IOR(IOR(
     1        IAND(BF(K),MASK1),
     2        ISHFT(IAND(4*(IAND(ISHFT(BF(K),MSHFM24),MASK2)-64)
     3              +1023+IEXP(J),MASK5),MSHF20)
     4                     ),
     5        IAND(ISHFT(BF(K),IMAN(J)),MASK3)
     6                 )
            MSHF29 = 29
            IEEEF(I+1)  = ISHFT(IAND(BF(K),MASK6),MSHF29)
        ELSE
            IEEEF(I)    = 0
            IEEEF(I+1)  = 0
        END IF
        K   = K + 1
  100 CONTINUE
      RETURN
      END
