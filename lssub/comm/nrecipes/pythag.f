      FUNCTION PYTHAG(A,B)
C
C     THIS SUBROUTINE IS TAKEN DIRECTLY FROM NUMERICAL RECIPES WITHOUT ANY CHANGES.
C     IT IS CALLED FROM TQLI1.
C
C     JUN 28/96 - SLAVA KHARIN
C
C  (C) COPR. 1986-92 NUMERICAL RECIPES SOFTWARE #=!E=#,)]UBCJ.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL A,B,PYTHAG
      REAL ABSA,ABSB
      ABSA=ABS(A)
      ABSB=ABS(B)
      IF(ABSA.GT.ABSB)THEN
        PYTHAG=ABSA*SQRT(1.E0+(ABSB/ABSA)**2)
      ELSE
        IF(ABSB.EQ.0.E0)THEN
          PYTHAG=0.E0
        ELSE
          PYTHAG=ABSB*SQRT(1.E0+(ABSA/ABSB)**2)
        ENDIF
      ENDIF
      RETURN
      END
