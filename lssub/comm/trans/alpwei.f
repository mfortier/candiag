      SUBROUTINE ALPWEI (ALP,DALP,DELALP, WL,WOSL,LSR,LM) 
C 
C     * JUL 14/92 - E. CHAN (ADD REAL*8 DECLARATIONS)
C     * JUL 21/83 - R.LAPRISE.
C     * NORMALIZE THE LEGENDRE POLYNOMIALS AND THEIR DERIVATIVES
C     * BY THE GAUSSIAN WEIGTHS AS REQUIRED FOR THE DIRECT
C     * TRANSFORM FROM FOURIER TO SPECTRAL. 
C     * DELALP ALSO INCLUDES THE FACTOR -0.5 REQUIRED FOR THE 
C     * DIVERGENCE TENDENCY TERM INVOLVING THE KINETIC ENERGY.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 ALP(1),DALP(1),DELALP(1) 
      REAL*8 WL,WOSL 
      INTEGER LSR(2,1)
C-----------------------------------------------------------------------
      NR=LSR(2,LM+1)-1
      DO 100 I=1,NR 
         ALP(I)=   ALP(I)*WL
        DALP(I)=  DALP(I)*WOSL
      DELALP(I)=DELALP(I)*WOSL*(-0.5E0) 
  100 CONTINUE
      RETURN
C-----------------------------------------------------------------------
      END 
