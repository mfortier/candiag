      SUBROUTINE  GINTC2(VAL,G,NI,NJ,FI,FJ) 
C 
C     *****   JAN 1975  -  JOHN D. HENDERSON  ****
C     * CUBIC INTERPOLATION AT POINT (FI,FJ) IN GRID G(NI,NJ).
C     * THE GRID MUST BE AT LEAST 4 BY 4 POINTS.
C     * IF (FI,FJ) IS OUTSIDE THE GRID AN EXTRAPOLATION IS DONE.
C 
C     * LEVEL 2,G 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION G(NI,1),AP(4),AQ(4),A(4)
C-----------------------------------------------------------------------
      SIXTH=1.E0/6.E0 
C 
C     * CALCULATE X-PARAMETERS AND Y-PARAMETERS.
C 
      I=INT(FI) 
      IF(I.GT.NI-2) I=NI-2
      IF(I.LT.2)    I=2 
      IM2=I-2 
      P=FI-FLOAT(I) 
      AP(1)=SIXTH*(-P*(P-1.E0)*(P-2.E0))
      AP(2)=  0.5E0*(   (P-1.E0)*(P+1.E0)*(P-2.E0)) 
      AP(3)=  0.5E0*(-P*(P+1.E0)*(P-2.E0))
      AP(4)=SIXTH*( P*(P+1.E0)*(P-1.E0))
C 
      J=INT(FJ) 
      IF(J.GT.NJ-2) J=NJ-2
      IF(J.LT.2)    J=2 
      JM2=J-2 
      Q=FJ-FLOAT(J) 
      AQ(1)=SIXTH*(-Q*(Q-1.E0)*(Q-2.E0))
      AQ(2)=  0.5E0*(   (Q-1.E0)*(Q+1.E0)*(Q-2.E0)) 
      AQ(3)=  0.5E0*(-Q*(Q+1.E0)*(Q-2.E0))
      AQ(4)=SIXTH*( Q*(Q+1.E0)*(Q-1.E0))
C 
C     * INTERPOLATE IN EACH ROW THEN IN THE RESULTING COL FOR VAL.
C 
      DO 20 L=1,4 
      A(L)=0.E0 
      DO 20 K=1,4 
   20 A(L)=A(L)+AP(K)*G(IM2+K,JM2+L)
      VAL=AQ(1)*A(1)+AQ(2)*A(2)+AQ(3)*A(3)+AQ(4)*A(4) 
C 
      RETURN
      END 
