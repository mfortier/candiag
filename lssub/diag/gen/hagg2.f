      SUBROUTINE HAGG2(GG,GGH,NLONG,NLATH,NHEM) 
C 
C     *****   NOV 1975  -  JOHN D. HENDERSON  ****
C     * TRANSFERS ONE HEMISPHERIC GAUSSIAN GRID GG(NLONG,NLATH) 
C     * TO THE CORRESPONDING HEMISPHERE OF THE FULL GAUSSIAN GRID 
C     * GG(NLONG,NLATH*2).
C 
C     * INPUT GRID IS N HEM IF NHEM=1, S HEM IF NHEM=2. 
C     * IF NHEM=0 THE INPUT GRID IS TAKEN TO BE GLOBAL, AND BOTH
C     *  HEMISPHERES ARE TRANSFERRED TO GG. 
C 
C     * LEVEL 2,GG,GGH
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION GG(NLONG,1),GGH(NLONG,1)
C 
C-----------------------------------------------------------------------
C 
      JMAX=NLATH
      IF(NHEM.EQ.0) JMAX=NLATH*2
C 
      DO 30 J=1,JMAX
      JT=J
      IF(NHEM.EQ.1) JT=NLATH+J
C 
      DO 20 N=1,NLONG 
   20 GG(N,JT)=GGH(N,J) 
C 
   30 CONTINUE
C 
      RETURN
      END 
