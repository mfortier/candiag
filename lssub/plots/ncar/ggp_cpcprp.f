      SUBROUTINE GGP_CPCPRP(ZREG, MREG, NREG, 
     +     PROJ, RWRK, LRWK, IWRK, LIWK,
     +     FLOWN, FHIGHN, FINC, DRAWCONT, 
     +     PUB, NZERO, SPVAL, SUBAREA,
     +     MAP)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE

C     
C     This routine initializes Conpack and sets necessary contour parameters.
C     
      INTEGER MREG, NREG
      REAL ZREG(MREG, NREG)

      CHARACTER*(*) PROJ

C Workspace arrays for NCARG routines.
      INTEGER LRWK, LIWK
      REAL RWRK(LRWK)
      INTEGER IWRK(LIWK)
      
      REAL FLOWN, FHIGHN, FINC
      
      LOGICAL DRAWCONT
      LOGICAL PUB
      LOGICAL NZERO
      REAL SPVAL
      LOGICAL SUBAREA
      LOGICAL MAP

      INTEGER I, ITMP
      REAL CLV,TOL
      REAL VPL, VPR, VPB, VPT
      INTEGER LASTC
      REAL R1MACH
      EXTERNAL R1MACH
      CHARACTER*4 CLVFMT
      CHARACTER*15 COORD
      INTEGER NL0,NINTS,MI
      REAL ZA, ZB, ZC, ZD, ZE, ZF, ZG, ZH, LZZ
      REAL IXA,IXB,IYA,IYB
      INTEGER LTYPE
      REAL XC,XD,YC,YD
      LOGICAL LMAJEVEN

      COMMON /GGP_MAP_LIMITS/ RLATMN,RLATMX,RLONMN,RLONMX,DGRW
      REAL RLATMN(2), RLATMX(2), RLONMN(2), RLONMX(2),DGRW
      REAL LAT1,LON1,LAT2,LON2,LAT3,LON3,LAT4,LON4
      REAL LATMIN,LATMAX,LONMIN,LONMAX

      INTEGER IGGP_CPAREA_SIZE
      PARAMETER (IGGP_CPAREA_SIZE=4000000)
      INTEGER IGGP_CPAREA(IGGP_CPAREA_SIZE)
      COMMON /GGP_AREA/IGGP_CPAREA

      EXTERNAL GGP_PATTERN

      TOL=R1MACH(3)

      CALL GETSET(ZA,ZB,ZC,ZD,ZE,ZF,ZG,ZH,LZZ) 
C     COMMENT:  FOR CE, RLONMN IS XC1 AND RLATMX IS YCN.
C               BUT FOR ST, RLONMN IS OFTEN .GT. XC1.
C               If the first subscript of the data array is a linear
C               function of the longitude and the second is a linear
C               function of the latitude, then one can transform all
C               graphics output onto a map background created by calls
C               to routines in the utility package Ezmap just by
C               setting 'MAP' = 1, 'XC1' = minimum longitude, 'XCM' =
C               maximum longitude, 'YC1' = minimum latitude, and 'YCN'
C               = maximum latitude.  Also, the parameter 'SET' must be
C               given the value 0 in order to prevent Conpack from
C               calling SET and thereby overriding the call done by
C               Ezmap.
      IF(MAP) THEN
         CALL CPSETI('SET - DO-SET-CALL FLAG', 0)
      ELSE
         CALL CPSETI('SET - DO-SET-CALL FLAG', 1)
      ENDIF
      IF (PROJ .EQ. 'ST') THEN
C Set Conpack parameters.
        CALL CPSETI('MAP - MAPPING FLAG', 4) 
        CALL CPSETR('XC1', ZE)
        CALL CPSETR('XCM', ZF)
        CALL CPSETR('YC1', ZG)
        CALL CPSETR('YCN', ZH)
      ELSE 
         IF(MAP) THEN
            CALL CPSETI('MAP - MAPPING FLAG', 1)
            CALL CPSETR('XC1', RLONMN(1))
            CALL CPSETR('XCM', RLONMX(1))
            CALL CPSETR('YC1', RLATMN(1))
            CALL CPSETR('YCN', RLATMX(1))
         ENDIF
      ENDIF
C   * Set special value, Refer to NCARv3.2 Conpack manual Cp3.6
      CALL CPSETR('SPV - Special Value',SPVAL)
      CALL CPSETR('ORV - OUT OF RANGE VALUE', 1.E12)
      
C     Set contour label positioning scheme.
      CALL CPSETI('LLP - LINE LABEL POSITIONING FLAG', -3)
      CALL CPSETR('RC1',.10)
      CALL CPSETR('RC2',.32)
      CALL CPSETR('RC3',.05)
      CALL CPSETI('RWC',1000)
      CALL CPSETI('RWG',10000)
      CALL CPSETR('PC1',1.0)
      CALL CPSETR('PC2',5.0)
      CALL CPSETR('PC3',60.0)
      CALL CPSETR('PC4',0.05)
      CALL CPSETR('PC5',0.15)
      CALL CPSETR('PC6',0.3)
      CALL CPSETR('PW1',2.0)
      CALL CPSETR('PW2',1.0)
      CALL CPSETR('PW3',1.0)
      CALL CPSETR('PW4',1.0)
C Smooth and interpolate points for contours.
      CALL CPSETI('PIC - POINT INTERPOLATION CONTOUR FLAG', -3)

C This won't work if FINC is zero
C Do operation on each contour level and label.
      CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', ITMP)
      IF (.NOT.DRAWCONT) THEN
         DO I = 1, ITMP
            CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
            CALL CPSETI('CLU - CONTOUR LEVEL USE FLAGS', 0) 
         ENDDO
      ENDIF

C Initialize Conpack.
      CALL CPRECT(ZREG, MREG, MREG, NREG, RWRK, LRWK, IWRK, LIWK)

C Add contours to area map
      CALL CPCLAM(ZREG, RWRK, IWRK, IGGP_CPAREA)

      RETURN
      END
