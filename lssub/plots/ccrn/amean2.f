      FUNCTION AMEAN2(A,M,N,IPER,JPER)
  
C     * ROUTINES BORROWED FROM J.WALMSLEY, JAN 15 1985. 
C     * ARITHMETIC MEAN OF FIELD, A.
C     * IPER,JPER  = 0  NON-PERIODIC BOUNDARY CONDITIONS
C     *            1  PERIODIC BOUNDARY CONDITIONS (I.E., LAST ROW OR 
C     *                  COLUMN OMITTED IN AVERAGING) 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION A(M,N)
C------------------------------------------------------------------ 
  
      ILAST=M-IPER
      JLAST=N-JPER
      ABAR=0.E0 
      DO 10 J=1,JLAST 
      DO 10 I=1,ILAST 
      ABAR=ABAR+A(I,J)
   10 CONTINUE
      AMEAN2=ABAR/(ILAST*JLAST) 
      RETURN
      END 
