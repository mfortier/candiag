      PROGRAM GSAPL_LNSP
C     PROGRAM  GSAPL_LNSP (GSFLD,       GSLNSP,       GPFLD,       INPUT,       J2
C    1                                                        OUTPUT,   )       J2
C    2          TAPE1=GSFLD, TAPE2=GSLNSP, TAPE3=GPFLD, TAPE5=INPUT,
C    3                                                  TAPE6=OUTPUT)
C     ---------------------------------------------------------------           J2
C                                                                               J2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2
C     JAN 12/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     FEB 29/88 - R.LAPRISE.
C                                                                               J2
CGSAPL   - INTERPOLATES FROM SIGMA/HYBRID LEVELS TO NPL PRES. LEVELS    2  1 C  J1
C                                                                               J3
CAUTHOR  - R. LAPRISE                                                           J3
C                                                                               J3
CPURPOSE - INTERPOLATES FROM ETA (SIGMA/HYBRID) LEVELS TO NPL                   J3
C          PRESSURE LEVELS. THE INTERPOLATION IS LINEAR IN LN(SIGMA).           J3
C          EXTRAPOLATION UP AND DOWN IS BY LAPSE RATES SPECIFIED BY             J3
C          THE USER.                                                            J3
C                                                                               J3
CINPUT FILES...                                                                 J3
C                                                                               J3
C      GSFLD  = SETS OF ETA (SIGMA/HYBRID) LEVEL GRID DATA.                     J3
C      GSLNSP = SERIES OF GRIDS OF LN(SF PRES).                                 J3
C                                                                               J3
COUTPUT FILE...                                                                 J3
C                                                                               J3
C      GPFLD  = SETS OF PRESSURE LEVEL GRID DATA.                               J3
C
CINPUT PARAMETERS...
C                                                                               J5
C      NPL    = NUMBER OF REQUESTED PRESSURE LEVELS, (MAX $L$).                 J5
C      RLUP   = LAPSE RATE USED TO EXTRAPOLATE UPWARDS.                         J5
C      RLDN   = LAPSE RATE USED TO EXTRAPOLATE DOWNWARDS.                       J5
C      ICOORD = 4H SIG/ 4H ETA FOR SIGMA/ETA VERTICAL COORDINATES.              J5
C      PTOIT  = PRESSURE (PA) AT THE LID OF MODEL.                              J5
C      PR     = PRESSURE LEVELS (MB)                                            J5
C                                                                               J5
CEXAMPLE OF INPUT CARDS...                                                      J5
C                                                                               J5
C*GSAPL.      5        0.        0.  SIG        0.                              J5
C*100  300  500  850 1000                                                       J5
C----------------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK

      INTEGER LEV(100),LEVP(100),KBUF(8)

      REAL ETA  (100),A    (100),B      (100)
      REAL SIG  (100),FSIG (100),DFLNSIG(101),DLNSIG (100)
      REAL PR   (100),PRLOG(100)

      COMMON/BLANCK/F(1871328)
      COMMON/ICOM  /IBUF(8),IDAT(37056)

      DATA MAXX/37056/, MAXL/1871328/, MAXLEV/100/
C---------------------------------------------------------------------
      NFIL=5
      CALL JCLPNT(NFIL,1,2,3,5,6)
      DO 110 N=1,3
  110 REWIND N

C     * READ THE CONTROL CARDS.

      READ(5,5010,END=908) NPL,RLUP,RLDN,ICOORD,PTOIT                           J4
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00E0)
      ELSE
        PTOIT=MAX(PTOIT, 0.100E-09)
      ENDIF
      IF(NPL.GT.MAXLEV) CALL                       XIT('GSAPL',-1)
      READ(5,5020,END=909) (LEVP(I),I=1,NPL)                                    J4

C     * DECODE LEVELS.

      CALL LVDCODE(PR,LEVP,NPL)

      WRITE(6,6010) RLUP,RLDN,ICOORD,PTOIT
      CALL WRITLEV(PR,NPL,' PR ')

      DO 114 L=2,NPL
  114 IF(PR(L).LE.PR(L-1)) CALL                    XIT('GSAPL',-2)

      DO 130 L=1,NPL
  130 PRLOG(L)=LOG(PR(L))

C     * GET ETA VALUES FROM THE GSFLD FILE.

      CALL FILEV (LEV,NSL,IBUF,1)
      IF(NSL.LT.1 .OR. NSL.GT.MAXLEV) CALL         XIT('GSAPL',-3)
      NWDS = IBUF(5)*IBUF(6)
      IF((MAX(NSL,NPL)+1)*NWDS.GT.MAXL) CALL      XIT('GSAPL',-4)
      DO 116 I=1,8
  116 KBUF(I)=IBUF(I)
      CALL LVDCODE(ETA,LEV,NSL)
      DO 118 L=1,NSL
  118 ETA(L)=ETA(L)*0.001E0

C     * EVALUATE THE PARAMETERS OF THE ETA VERTICAL DISCRETIZATION.

      CALL COORDAB (A,B, NSL,ETA, ICOORD,PTOIT)
C---------------------------------------------------------------------
C     * GET NEXT SET FROM FILE GSFLD.

      NSETS=0
      N=NWDS+1
  150 CALL GETSET2 (1,F(N),LEV,NSL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      IF(.NOT.OK)THEN
        WRITE(6,6030) NSETS
        IF(NSETS.EQ.0)THEN
          CALL                                     XIT('GSAPL',-5)
        ELSE
          CALL                                     XIT('GSAPL',0)
        ENDIF
      ENDIF
      CALL CMPLBL (0,IBUF,0,KBUF,OK)
      IF(.NOT.OK) CALL                             XIT('GSAPL',-6)
      NAME=IBUF(3)
      NPACK=IBUF(8)

C     * GET LN(SF PRES) FOR THIS STEP, PUT AT BEGINNING OF COMMON BLOCK.

      NST= IBUF(2)
      CALL GETFLD2 (2, F ,NC4TO8("GRID"),NST,NC4TO8("LNSP"),1,
     +                                           IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
cccc      IF(.NOT.OK) CALL                             XIT('GSAPL',-7)
      IF(.NOT.OK) then
         if(nsets.eq.0) then
            CALL                             XIT('GSAPL',-7)
         else
            CALL                                     XIT('GSAPL',0)
         endif
      endif

      CALL CMPLBL (0,IBUF,0,KBUF,OK)
      IF(.NOT.OK) CALL                             XIT('GSAPL',-8)

C     * INTERPOLATE IN-PLACE FROM ETA TO PRESSURE.

      CALL EAPL  (F(N), NWDS,PRLOG,NPL, F(N),SIG,NSL ,F(1),RLUP,RLDN,
     1            A,B, NSL+1,FSIG,DFLNSIG,DLNSIG)

C     * WRITE THE PRESSURE LEVEL GRIDS ONTO FILE 3.

      IBUF(3)=NAME
      IBUF(8)=NPACK
      CALL PUTSET2 (3,F(N),LEVP,NPL,IBUF,MAXX)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      NSETS=NSETS+1
      GO TO 150

C     * E.O.F. ON INPUT.

  908 CALL                                         XIT('GSAPL',-9)
  909 CALL                                         XIT('GSAPL',-10)
C---------------------------------------------------------------------
 5010 FORMAT(10X,I5,2E10.0,1X,A4,E10.0)                                         J4
 5020 FORMAT(16I5)                                                              J4
 6010 FORMAT(' RLUP,RLDN = ',2F6.2,', ICOORD=',1X,A4,
     1       ', P.LID (PA)=',E10.3)
 6030 FORMAT('0 GSAPL INTERPOLATED',I5,' SETS OF ',A4)
 6035 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
      SUBROUTINE LVACODE (ICODE,IBUF4,NLEV) 
C
C     * AUG 2000  - J. SCINOCCA/SLAVA KHARIN
C     *             MODIFY TO NEW HIGH PRECISION LEVEL SCHEME
C     * FEB 22/91 - E.CHAN. 
C     * 
C     * CONVERTS CODED LEVEL INFORMATION IN CCRN STANDARD LABELS
C     * GENERATED BY SUBROUTINE LVCODE INTO ALTERNATE CODE THAT 
C     * IS MONOTONICALLY INCREASING (A REQUIREMENT OF SOME PROGRAMS,
C     * E.G. SELECT, SELECT2, ETC.).
C     * 
C     * IBUF4 = CODED LABEL FOR LEVEL IN THE FOLLOWING FORMS: 
C     *  1)  -XAAA = A.AA E-X          IF IBUF4(L) < 0
C     *  2)   AAAA = AAAA              IF 0 <= IBUF4(L) <= 99100
C     *  3)  99AAA = A.AA E+1          IF IBUF4(L) .GT. 99100
C
C     * ICODE = ALTERNATE CODE: 
C     *  1) IF IBUF4(L) < 0           -XAAA -> -XCCC (CCC = 1000 - AAA)
C     *  2) IF IBUF4(L) .GT. 99100    99AAA ->   AAA
C     *  3) IF 0 <= IBUF4(L) <= 99100  AAAA ->  AAAA*10

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER IBUF4(NLEV), ICODE(NLEV)
C-----------------------------------------------------------------------
      DO 100 L = 1, NLEV
        IF (IBUF4(L) .LT. 0) THEN 
          IX = IBUF4(L) / 1000
          ICODE(L) =  (2*IX-1)*1000 - IBUF4(L)
        ELSEIF (IBUF4(L) .GE. 99101 .AND. IBUF4(L) .LE. 99999) THEN
          ICODE(L) = (IBUF4(L)-99000)
        ELSE
          ICODE(L) = IBUF4(L)*10
        ENDIF 
  100 CONTINUE
C-----------------------------------------------------------------------
      RETURN
      END 
      SUBROUTINE LVCODE (IBUF4,ETA,NLEV) 
C     *
C     * MAR  2007  - J. SCINOCCA/SLAVA KHARIN
C     *              MODIFY TO NEW HIGH PRECISION LEVEL SCHEME
C     *              BETWEEN 10 AND 100HPA
C
C     * JAN 22/93 - E.CHAN. 
C     * 
C     * GENERATES CODED LEVEL INFORMATION TO BE INCLUDED IN CCRN
C     * STANDARD LABELS.
C     * 
C     * ETA = ETA COORDINATE OR PRESSURE IN MB DIVIDED BY 1000. 
C     * IBUF4 = CODED LABEL FOR LEVEL IN THE FOLLOWING FORMS: 
C     *   1)  IF ETA <  .0100          A.AA E-X   ->  -XAAA
C     *   2)  IF .0100 <= ETA <= 0.100 AND
C     *           IF C. NE. 0          A.BC E+1   ->  99ABC  
C     *           IF C. EQ. 0          A.BC E+1   ->     AB  
C     *   3)    OTHERWISE                  AAAA   ->   AAAA
C     * 
C     * USE SUBROUTINE LVDCODE FOR REVERSE OPERATION. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION IBUF4(NLEV), ETA(NLEV) 
      CHARACTER STRING*9
C-----------------------------------------------------------------------
      DO 100 L = 1, NLEV
C
C       * CONVERT TO VERTICAL COORDINATE (PRESSURE IN MB OR ETA*1000).
C

        VC =  1000. * ETA(L) 
C
        IF (VC .GT. 10.05 .AND. VC .LT. 99.95 ) THEN 
C
C         * CASE 1: 10.0 < VC <  100.
C

          WRITE (STRING,2000) VC 
          READ  (STRING,2005) ID3
          IF (ID3.EQ.0) THEN
             IBUF4(L) = INT(VC + 0.5E0)
          ELSE
             READ  (STRING,2010) IAAA, IXP1
             IBUF4(L) = 99000 + IAAA
          ENDIF

        ELSEIF (VC .GT. 99.95) THEN
C         * CASE 2: 1001. <= VC <=  1099.

          IBUF4(L) = INT(VC + .5)

C 
        ELSEIF ( VC .GT. 1.E-9 ) THEN
C
C         * CASE 3: 10 > VC > 1E-9
C
C         * THE INTEGER EXPONENT IX AND MANTISSA IAAA ARE GENERATED 
C         * FROM THE REAL NUMBER VC BY FIRST WRITING VC AS CHARACTER 
C         * DATA INTO STRING. IX AND IAAA ARE THEN EXTRACTED FROM 
C         * THE APPROPRIATE LOCATIONS IN STRING.
C 
          WRITE (STRING,2000) VC 
          READ  (STRING,2010) IAAA, IXP1
          IX = IXP1 - 1 
          IBUF4(L) = IX*1000 - IAAA 
C 
        ELSE
C
C         CASE 4: 1E-9 >= VC >= 1E-11
C 
          IBUF4(L) = -9000 - INT( VC*1.E11 + .5 )
C 
        ENDIF 
C
  100 CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
 2000 FORMAT (E9.3) 
 2005 FORMAT (4X,I1,4X)
 2010 FORMAT (2X,I3,1X,I3)
      END 
      SUBROUTINE LVDCODE (VC,IBUF4,NLEV)
C
C     * MAR  2007 - J. SCINOCCA/SLAVA KHARIN
C     *              MODIFY TO NEW HIGH PRECISION LEVEL SCHEME
C     *              BETWEEN 10 AND 100HPA
C     *
C     * FEB 28/91 - E.CHAN. 
C     * 
C     * DECODES LEVEL INFORMATION IN CCRN STANDARD LABELS GENERATED 
C     * BY SUBROUTINE LVCODE. 
C     * 
C     * VC = VERTICAL COORDINATE: PRESSURE IN MB OR (ETA*1000)
C     * IBUF4 = CODED LABEL FOR LEVEL IN THE FOLLOWING FORMS: 
C     *   1)  -XAAA = A.AA E-X     IF IBUF4(L) .LT. 0
C     *   2)   AAAA = AAAA         IF 0 <= IBUF4(L) <= 99100
C     *   3)  99AAA = A.AA E+1     IF IBUF4(L) .GT. 99100
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION IBUF4(NLEV), VC(NLEV) 
      CHARACTER STRING*8
C-----------------------------------------------------------------------
  
C     * RECONSTITUTE THE REAL NUMBER VC FROM THE INTEGER EXPONENT IX AND
C     * MANTISSA IAAA BY FIRST WRITING IX AND IAAA INTO STRING AS CHARACTER 
C     * DATA. THE NUMBER IS THEN READ BACK INTO VC IN E-FORMAT. 
  
      DO 100 L = 1, NLEV
        IF (IBUF4(L) .LT. 0) THEN 
          IX = IBUF4(L) / 1000
          IAAA = 1000*IX - IBUF4(L) 
          WRITE (STRING,1000) IAAA, IX-2
          READ  (STRING,1010) VC(L) 
        ELSEIF (IBUF4(L) .GE. 99101 .AND. IBUF4(L) .LE. 99999) THEN
          IAAA  = (IBUF4(L)-99000)
          VC(L) = FLOAT(IAAA)/10.E0
        ELSE
          VC(L) = FLOAT(IBUF4(L)) 
        ENDIF 
  100 CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
 1000 FORMAT (I3,'.E',I3.2) 
 1010 FORMAT (E8.0) 
      END 

