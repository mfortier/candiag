      PROGRAM SPLOOK
C     PROGRAM SPLOOK (OUTSP,       INPUT,       OUTPUT,                 )       A2
C    1          TAPE1=OUTSP, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------                         A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     FEB 15/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 12/94 - F.MAJAESS (CORRECT "SKIP" OPTION )                            
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)               
C     AUG 07/90 - F.MAJAESS (ADJUST THE CODES FOR THE MODIFICATIONS MADE        
C                            TO THE CALLED ROUTINES)                           
C     NOV 29/83 - R.LAPRISE, J.D.HENDERSON. 
C                                                                               A2
CSPLOOK  - PRINTS REQUESTED SPECTRAL FIELDS FROM A FILE                 1  0 C GA1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON,R.LAPRISE                                              A3
C                                                                               A3
CPURPOSE - PRINTS SPECTRAL COEFFICIENTS FOR FIELDS IN A FILE AS                 A3
C          REQUESTED ON CARDS.                                                  A3
C          NOTE - FIELDS MUST BE REQUESTED ON INPUT CARDS IN THE                A3
C                 SAME ORDER AS THEY OCCUR IN THE FILE.                         A3
C                 AND, SPLOOK ONLY PRINTS AN INFORMATIVE MESSAGE                A3
C                 WHEN INVOKED WITH FOURIER INPUT FIELDS.                       A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      OUTSP = FILE CONTAINING SPHERICAL HARMONIC FIELDS TO BE                  A3
C              SELECTED FOR PRINTING.                                           A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C      NSTEP,NAME,LEVEL = WORDS 2,3,4 OF THE SPECTRAL LABEL REQUESTED           A5
C                         IF NAME=4HNEXT THE NEXT FIELD IS PRINTED              A5
C                         IF NAME=4HSKIP THE NEXT FIELD IS SKIPPED              A5
C      NPR,MPR          = NUMBER OF WAVES PRINTED IN N,M DIRECTIONS             A5
C      KIND             = REQUESTED COEFFICIENTS OUTPUT FORMAT CONTROL          A5
C                       =1, (REAL,IMAGINARY)                  FORMAT            A5
C                       =2, (AMPLITUDE, PHASE(DEG))           FORMAT            A5
C                       =3, 2-D SPECTRAL AMPLITUDE (M, (N-M)) FORMAT            A5
C                       =4, 2-D SPECTRAL AMPLITUDE (M, N)     FORMAT            A5
C                       NOTE - FOR KIND=3 OR 4, WHAT YOU SEE IS                 A5
C                              INT(SCALE*AMPLITUDE) IN I6 FORMAT.               A5
C      SCALE            = SCALE FACTOR USED ONLY IF KIND IS 3 OR 4.             A5
C                         IF(SCALE.EQ.0.) SCALE=1.0                             A5
C                                                                               A5
CEXAMPLE OF INPUT CARD...                                                       A5
C                                                                               A5
C*  SPLOOK        25 VORT    3   12   12    1        0.                         A5
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LMTP1,
     &                       SIZES_LRTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXX = 2*(SIZES_LA+SIZES_LMTP1)*SIZES_NWORDIO 

      COMPLEX SP2 
      COMMON/BLANCK/ SP2(SIZES_LA+SIZES_LMTP1) 
C 
      LOGICAL OK
      INTEGER IA(21), LSR(2,SIZES_LMTP1+1)
      REAL A(SIZES_LRTP1),PH(SIZES_LRTP1) 
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
  
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
      REWIND 1
C 
C     * READ A CARD IDENTIFYING THE FIELD TO BE PRINTED.
C 
  110 READ(5,5010,END=900) NSTEP,NAME,LEVEL,NPR,MPR,KIND, SCALE                 A4
      IF(NAME.EQ.NC4TO8("SKIP"))THEN
C       READ(1,END=901) 
        CALL FBUFFIN(1,IBUF,MAXX,KK,LEN)
        IF (KK.GE.0) GO TO 901
        GO TO 110 
      ENDIF 
      IF(SCALE.EQ.0.E0) SCALE=1.E0
C 
C     * FIND THE REQUESTED SPECTRAL OR FOURIER FIELD. 
C 
  160 CALL GETFLD2(1,SP2,NC4TO8("SPEC"),NSTEP,NAME,LEVEL,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
                  WRITE(6,6020) NSTEP,NAME,LEVEL
                  CALL                             XIT('SPLOOK',-101) 
      ENDIF 
      LRLMT=IBUF(7) 
      IF(LRLMT.LT.100) LRLMT=1000*IBUF(5)+10*IBUF(6)
      CALL DIMGT(LSR,LA,LR,LM,KTR,LRLMT)
C 
C     * PRINT THE COEFF AND GO BACK FOR THE NEXT FIELD. 
C 
      IF(KIND.LE.1)CALL PRST2  (SP2,LSR,LM,NPR,MPR) 
      IF(KIND.EQ.2)CALL PRPOL2 (SP2,LSR,LM,LR,NPR,MPR,
     +                            NC4TO8(" DEG"),A,PH)
      IF(KIND.EQ.3)CALL PRTODS2(SP2,LSR,LM,LR,NPR,MPR,
     +                             SCALE,NAME,A,PH,IA)
      IF(KIND.GE.4)CALL PRNMS2 (SP2,LSR,LM,LR,NPR,MPR,
     +                             SCALE,NAME,A,PH,IA)
      CALL PRTLAB (IBUF)
      GO TO 110 
C 
C     * E.O.F. ON INPUT.
C 
  900 CALL                                         XIT('SPLOOK',0)
  901 CALL                                         XIT('SPLOOK',-102) 
C---------------------------------------------------------------------
 5010 FORMAT(10X,I10,1X,A4,4I5,F10.0)                                           A4
 6020 FORMAT('0..EOF LOOKING FOR',I10,2X,A4,I6)
      END
