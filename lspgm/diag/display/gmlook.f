      PROGRAM GMLOOK
C     PROGRAM GMLOOK (GG,       MASK,       INPUT,       OUTPUT,        )       A2
C    1          TAPE1=GG, TAPE2=MASK, TAPE5=INPUT, TAPE6=OUTPUT)
C     ----------------------------------------------------------                A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JAN 12/94 - F.MAJAESS (CORRECT "SKIP" OPTION )                            
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     NOV 24/83 - B.DUGAS.                                                      
C     SEP 08/80 - J.D.HENDERSON 
C                                                                               A2
CGMLOOK  - MAPS SELECTED GRIDS FROM A GAUSSIAN GRID FILE (MASKED)       2  0 C  A1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON                                                        A3
C                                                                               A3
CPURPOSE - DRAWS A PRINTER CONTOUR MAP OF EACH GRID IN A FILE THAT IS           A3
C          REQUESTED ON CARDS.  CONTOURS ARE SUPPRESSED  WHEREVER THE           A3
C          MASK VALUE IS NOT ZERO.                                              A3
C          NOTE - THE MASK FILE CAN BE CREATED WITH PROGRAM FMASK.              A3
C                 GRIDS MUST BE  REQUESTED ON INPUT CARDS IN THE SAME           A3
C                 ORDER AS THEY APPEAR ON THE FILE  SINCE THE FILE IS           A3
C                 SCANNED ONLY ONCE.                                            A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      GG   = FILE OF GRIDS                                                     A3
C                                                                               A3
C      MASK = INPUT FILE OF ONE RECORD HAVING THE SAME SIZE AS THE              A3
C             GRIDS TO BE MAPPED AND CONTAINING A FLOATING POINT                A3
C             MASK (0. OR 1.).                                                  A3
C             (CONTOURS ARE SUPPRESSED WHEREVER MASK VALUE IS NOT ZERO)         A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C      NSTEP,NAME,LEVEL = STEP,NAME,LEVEL FROM THE LABEL                        A5
C                         IF NAME=4HNEXT THE NEXT GRID ON THE FILE IS MAPPED.   A5
C                         IF NAME=4HSKIP THE NEXT GRID ON THE FILE IS SKIPPED.  A5
C                         (IN THIS CASE NO LABEL CARD IS READ).                 A5
C                         IF NSTEP = -1 THE STEP NUMBER IS NOT CHECKED.         A5
C      CINT,SCAL        = CONTOUR INTERVAL AND SCALING FACTOR FOR FCONW2.       A5
C      MS               = MAP SIZE CONTROL (NO MAP IF MS=0).                    A5
C                                                                               A5
C      THE CODE FOR THIS PARAMETER IS AS FOLLOWS...                             A5
C                                                                               A5
C                         MS=1 : 1 INCH PER GRID POINT,                         A5
C                            2 : 2  "                                           A5
C                            3 : 3  "                                           A5
C                         MS=21: 1/20 000 000 FOR POLAR STEREO MAP,             A5
C                            22: 1/40 000 000  "                                A5
C                            23: 1/60 000 000  "                                A5
C                            ...                                                A5
C                            30: 1/200 000 000 "                                A5
C                         MS=31: 1/30 000 000  "                                A5
C                            32: 1/60 000 000  "                                A5
C                            ...                                                A5
C                            40: 1/300 000 000 "                                A5
C                                                                               A5
C      (IW,JW)          = LOWER LEFT CORNER OF WINDOW                           A5
C                         IF IW=0 THE WHOLE MAP IS CONTOURED.                   A5
C      (LL,MM)          = WIDTH AND HEIGHT OF WINDOW.                           A5
C      LABEL            = 80 CHARACTER LABEL PRINTED AFTER THE MAP.             A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C*GMLOOK           6  PHI  500     1.2E2   1.02E-1   22    1    1   65   52     A5
C*        500 MB GEOPOTENTIAL                                                   A5
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/GG(SIZES_LONP1xLAT),GMSK(SIZES_LONP1xLAT) 
C 
      LOGICAL LBL,OK
      INTEGER LABEL(8),KBUF(8)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * GET THE MASK FIELD FROM FILE 2. 
C 
      CALL GETFLD2(2,GMSK,NC4TO8("GRID"),-1,NC4TO8("FMSK"),-1,
     +                                           IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
                  CALL                             XIT('GMLOOK',-101) 
      ENDIF 
      WRITE(6,6025) IBUF
      DO 105 I=1,8
  105 KBUF(I)=IBUF(I) 
C 
C     * READ A CARD IDENTIFYING THE FIELD TO BE CONTOURED.
C     * IF NAME=4HSKIP THE NEXT RECORD ON THE FILE IS SKIPPED.
C     * IF NAME=4HRWND THE FILE IS REWOUND. 
C     * IF NAME=4HSTOP THE PROGRAM STOPS HERE.
C 
  110 READ(5,5010,END=900) NSTEP,NAME,LEVEL,CINT,SCAL,MS,IW,JW,LL,MM            A4
      IF(NAME.EQ.NC4TO8("SKIP"))THEN
C       READ(1,END=904) 
        CALL FBUFFIN(1,IBUF,MAXX,KK,LEN)
        IF (KK.GE.0) GO TO 904
        GO TO 110 
      ENDIF 
      IF(NAME.EQ.NC4TO8("RWND"))THEN
        REWIND 1
        GO TO 110 
      ENDIF 
      IF(NAME.EQ.NC4TO8("STOP")) CALL              XIT('GMLOOK',1)
C 
C     * READ THE LABEL TO BE PRINTED UNDER THE MAP. IF THIS IS MISSING
C     * THE PROGRAM WILL STOP AFTER THE MAP IS DRAWN. 
C 
      READ(5,5012,END=122) LABEL                                                A4
      LBL=.TRUE.
      GO TO 123 
  122 LBL=.FALSE. 
  123 CONTINUE
C 
C     * FIND THE REQUESTED FIELD. 
C 
      CALL GETFLD2(1,GG ,NC4TO8("GRID"),NSTEP,NAME,LEVEL,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
                  WRITE(6,6020) NSTEP,NAME,LEVEL
                  CALL                             XIT('GMLOOK',-102) 
      ENDIF 
      LX=IBUF(5)
      LY=IBUF(6)
C 
C     * THE FIELD AND MASK MUST BE THE SAME KIND AND SIZE.
C 
      CALL CMPLBL(0,IBUF,0,KBUF,OK) 
      IF(.NOT.OK)THEN 
        WRITE(6,6025) IBUF,KBUF 
        CALL                                       XIT('GMLOOK',-103) 
      ENDIF 
C 
C     *  MAP  THE FIELD AND GO BACK FOR THE NEXT ONE. 
C 
      IF(IW.GT.0) GO TO 230 
      IW=1
      JW=1
      LL=LX-1 
      MM=LY-1 
  230 CALL FCONM2(GG, CINT,SCAL,LX,LY,IW,JW,LL,MM,MS,GMSK)
C 
C     * WRITE IBUF AND LABEL UNDER THE MAP. STOP IF LABEL MISSING.
C 
      WRITE(6,6030) IBUF
      IF(.NOT.LBL) CALL                            XIT('GGLOOK',2)
      WRITE(6,6040) LABEL 
      GO TO 110 
C 
C     * E.O.F. ON INPUT.
C 
  900 CALL                                         XIT('GMLOOK',0)
C 
C     * E.O.F. ON FILE GG.
C 
  904 CALL                                         XIT('GMLOOK',-104) 
C---------------------------------------------------------------------
 5010 FORMAT(10X,I10,1X,A4, I5,2E10.0,5I5)                                      A4
 5012 FORMAT(10A8)                                                              A4
 6020 FORMAT('0..EOF LOOKING FOR',I10,2X,A4,I10)
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT('0', 8X,'    STEP  NAME     LEVEL  LX  LY  KHEM NPACK',
     1 /' ',2X, A4,I10,2X,A4,I10,2I4,2I6)
 6040 FORMAT('+',48X,10A8)
      END
