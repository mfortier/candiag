      PROGRAM INVNTRY 
C     PROGRAM INVNTRY (IN,       INPUT,       OUTPUT,                   )       A2
C    1           TAPE1=IN, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -----------------------------------------------                           A2
C                                                                               A2
C     NOV 23/10 - F.MAJAESS (FORMAT STATEMENT ADJUSTMENTS)                      A2
C     MAY 26/10 - F.MAJAESS (REVISED TO SUPPORT 4321X2161 DIMENSION AND         
C                            TO OUTPUT EXPECTED FILE SIZE)                      
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       
C     JUL 30/92 - E. CHAN  (MODIFY LENGTH OF PACKED ARRAY RETURNED BY           
C                           THE REVISED LBLCHK TO REFLECT 64-BIT WORDS)         
C     FEB 20/92 - E. CHAN  (TREAT SUPERLABELS AS ASCII INSTEAD OF AS            
C                           HOLLERITH LITERALS)                                
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)              
C     DEC 07/83 - B.DUGAS.                                                      
C     NOV 23/82 - R.LAPRISE.
C                                                                               A2
CINVNTRY - LISTS FIELD LABELS FROM A DIAGNOSTICS FILE                   1  0    A1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON                                                        A3
C                                                                               A3
CPURPOSE - PRINTS EACH RECORD LABEL IN FILE IN ALONG WITH THE LENGTH            A3
C          OF THE PACKED DATA FIELD.                                            A3
C          NOTE - THE FIRST 64 CHARACTERS OF SUPER LABELS ARE PRINTED           A3
C                 AS WELL AS THE TOTAL FILE LENGTH (INCLUDING THE LABELS).      A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      IN = FILE FOR WHICH ALL LABELS ARE TO BE PRINTED.                        A3
C-------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


C     * MAXIMUM 2-D GRID SIZE: 4321 X 2161 --> IJM

      PARAMETER (IM=4321,JM=2161,IJM=IM*JM,IJMV=IJM*SIZES_NWORDIO)

      CHARACTER*64 SLABL
      LOGICAL OK
C 
      COMMON/ICOM/IBUF(8),IDAT(IJMV)
      EQUIVALENCE (SLABL,IDAT)
C 
      COMMON /MACHTYP/ MACHINE,INTSIZE
      DATA LA/IJMV/
C     DATA ILABL/4HLABL/
C-------------------------------------------------------------------- 
      ILABL=NC4TO8("LABL")
      NFF=2 
      CALL JCLPNT(NFF,1,6)
      REWIND 1
      LHEAD = 8 * MACHINE
C 
      NR=0
      NW=0
      NB=0
  110 CALL RECGET(1, -1,-1,-1,-1, IBUF,LA,OK) 
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('INVNTRY',-101)
        ELSE
C         WRITE(6,6005)NW 
          WRITE(6,6025)NB
          CALL                                     XIT('INVNTRY',0) 
        ENDIF 
      ENDIF 
      NR=NR+1 
      CALL LBLCHK(LR,NWDS,NPACK,IBUF) 
      LF=(LR-LHEAD)/MACHINE 
C     NW=NW+LR
      IF ( INTSIZE .EQ. 1 ) THEN
       NB=NB+(LR*(8*INTSIZE/MACHINE))+16
      ELSE
       NB=NB+(LR*(8/INTSIZE))+16
      ENDIF
      IF(IBUF(1).NE.ILABL)THEN
        WRITE(6,6010) NR,IBUF,LF
      ELSE
        WRITE(6,6012) NR,IBUF,SLABL
      ENDIF 
      GO TO 110 
C-------------------------------------------------------------------- 
 6005 FORMAT('0FILE LENGTH (INCLUDING LABELS) IS',I8,' WORDS')
 6025 FORMAT('0EXPECTED FILE LENGTH (IN BYTES) IS:',I12)
 6010 FORMAT(I7,1X,A4,I10,2X,A4,I10,2I6,I9,I3,2X,
     1       'DATA WRDS=',I6)
 6012 FORMAT(I7,1X,A4,I10,2X,A4,I10,2I6,I9,I3,'  =',A64)
      END
