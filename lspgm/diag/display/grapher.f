      PROGRAM GRAPHER 
C     PROGRAM GRAPHER (C1,       C2,       C3,       C4,       INPUT,           A2
C    1                                                         OUTPUT,  )       A2
C    2          TAPE11=C1,TAPE12=C2,TAPE13=C3,TAPE14=C4, TAPE5=INPUT, 
C    3                                                   TAPE6=OUTPUT)
C     ----------------------------------------------------------------          A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JUL 22/92 - E. CHAN  (REPLACE UNFORMATTED I/O WITH I/O ROUTINES)          
C     MAR 16/92 - E. CHAN  (MODIFY FOR CCRN DATA IN 2-RECORD FORMAT)            
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)               
C     MAY 11/83 - R.LAPRISE.                                                    
C     FEB 02/79 - J.D.HENDERSON 
C                                                                               A2
CGRAPHER - DRAWS LINE PRINTER GRAPH OF UP TO 4 CURVES                   4  0 C  A1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON                                                        A3
C                                                                               A3
CPURPOSE - DRAWS UP TO FOUR GRAPHS ON THE PRINTER FROM DATA                     A3
C          LOCATED IN UP TO FOUR FILES.                                         A3
C          NOTE - MAX GRAPH LENGTH IS "IJ" POINTS.                              A3
C                                                                               A3
CINPUT FILES...                                                                 A3
C                                                                               A3
C      C1       = FILE CONTAINING ONE RECORD TO BE GRAPHED                      A3
C      C2,C3,C4 = OPTIONAL ADDITIONAL CURVES.                                   A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C      LI        = LENGTH OF GRAPH (MAX "IJ")                                   A5
C      NC        = NUMBER OF CURVES (MAX 4)                                     A5
C      INCR      = GRAPH INCREMENT                                              A5
C      GMIN,GMAX = GRAPH LIMITS (IF BOTH OF THESE ARE THE SAME                  A5
C                  THE PROGRAM PICKS THE MIN AND MAX VALUES IN THE              A5
C                  DATA AS THE GRAPH LIMITS)                                    A5
C                                                                               A5
CEXAMPLE OF INPUT CARD...                                                       A5
C                                                                               A5
C* GRAPHER  120    3    1        0.        0.                                   A5
C-----------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      REAL G(SIZES_LONP1xLAT,4) 
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
C 
      DATA LMAX/SIZES_LONP1xLATxNWORDIO/ 
C---------------------------------------------------------------------- 
      NFF=6 
      CALL JCLPNT(NFF,11,12,13,14,5,6)
C     * READ GRAPH CONTROLS FROM A CARD.
C 
      READ(5,5010,END=901) LI,NC,INCR,GMIN,GMAX                                 A4
      WRITE(6,6010) LI,NC,INCR,GMIN,GMAX
      OK=.TRUE. 
      IF(LI.LE.0.OR.LI.GT.LMAX.OR.NC.LE.0.OR.NC.GT.4) OK=.FALSE.
      IF(.NOT.OK) CALL                             XIT('GRAPHER',-101)
C 
C     * GET ONE CURVE OF THE PLOT FROM EACH OF THE FILES. 
C 
      DO 210 N=1,NC 
      NF=N+10 
      REWIND NF 
      CALL GETFLD2 (NF,G(1,N),-1,0,NC4TO8("NEXT"),0,IBUF,LMAX,OK)
      IF (.NOT.OK) CALL                            XIT('GRAPHER',-1)
      WRITE(6,6025) IBUF
  210 CONTINUE
C 
C     * DRAW THE GRAPH. 
C 
      CALL SPLAT2(G,LMAX,NC,LI,INCR,GMIN,GMAX)
C 
      CALL                                         XIT('GRAPHER',0) 
C 
C     * E.O.F. ON INPUT.
C 
  901 CALL                                         XIT('GRAPHER',-2)
C 
C     * E.O.F. ON FILE NUMBER NF. 
C 
  902 CALL                                         XIT('GRAPHER',-N-110) 
C---------------------------------------------------------------------- 
 5010 FORMAT(10X,3I5,2E10.0)                                                    A4
 6010 FORMAT(' LI,NC,INCR,GMIN,GMAX =',3I6,2E12.4)
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
