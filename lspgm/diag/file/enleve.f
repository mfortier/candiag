      PROGRAM ENLEVE
C     PROGRAM ENLEVE (IN,       OUT,       INPUT,       OUTPUT,         )       B2
C    1          TAPE1=IN, TAPE2=OUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     ---------------------------------------------------------                 B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     JAN 12/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII AND             
C                           REPLACE BUFFERED I/O)                               
C     NOV 07/83 - R.LAPRISE.                                                    
C                                                                               B2
CENLEVE  - DELETES UP TO FOUR VARIABLES FROM A FILE                     1  1 C  B1
C                                                                               B3
CAUTHOR  - R.LAPRISE.                                                           B3
C                                                                               B3
CPURPOSE - DELETES RECORDS FROM FILE IN FOR ALL TIMESTEPS AND LEVELS            B3
C          BETWEEN REQUESTED INTERVALS THEN PUTS THE RESULT ON FILE OUT.        B3
C          NOTE - STEP NUMBERS ARE ASSUMED TO BE INCREASING.                    B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      IN  = FILE TO BE COPIED.                                                 B3
C            (THIS FILE MUST BE ORDERED BY TIMESTEP NUMBER)                     B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      OUT = FILE COPIED FROM IN WITH DELETIONS.                                B3
C 
CINPUT PARAMETERS...
C                                                                               B5
C     NT1,NT2         = TIMESTEP INTERVAL TO BE DELETED, (INCLUSIVE).           B5
C     INT             = DATA IS DELETED EVERY INT STEPS FROM STEP NT1.          B5
C     LV1,LV2         = LEVEL INTERVAL TO BE DELETED.                           B5
C     NAME1,...,NAME4 = NAMES TO BE DELETED.                                    B5
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C*ENLEVE STEP           0        24    6 LEVS  100  950 NAME VORT  DIV          B5
C-------------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/ICOM/IBUF(8),F(SIZES_BLONP1xBLATxNWORDIO) 
C 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE CONTROL CARD.
C     * NT1,NT2 = TIMESTEP INTERVAL TO BE DELETED.
C     * INT =   DATA IS DELETED EVERY INT STEPS FROM STEP NT1.
C     * LV1,LV2 = LEVEL INTERVAL TO BE DELETED. 
C     * NAME1 TO NAME4 = NAMES TO BE DELETED. 
C 
      READ(5,5010,END=901) NT1,NT2,INT,LEV1,LEV2,NAME1,NAME2,NAME3,NAME4        B4
      IF(INT.LE.0) INT=1
      WRITE(6,6010) NT1,NT2,INT 
      WRITE(6,6020) LEV1,LEV2 
      WRITE(6,6030) NAME1,NAME2,NAME3,NAME4 
C
C     * ENSURE LEVELS ARE MONOTONICALLY INCREASING.
C 
      CALL LVACODE(LV1,LEV1,1)
      CALL LVACODE(LV2,LEV2,1)
C 
C     * READ NEXT RECORD FROM FILE 1. 
C     * STOP AT THE END OF THE FILE.
C 
      NREC=0
      NCOPY=0
      MAXLEN=MAXX+8
  150 CALL FBUFFIN(1,IBUF(1),MAXLEN,K,LEN)
      NDEL=NREC-NCOPY 
      IF(K.EQ.0)THEN
        WRITE(6,6040) NREC,NCOPY,NDEL 
        IF(NREC.EQ.0)THEN 
          CALL                                     XIT('ENLEVE',-1) 
        ELSE
          CALL                                     XIT('ENLEVE',0)
        ENDIF 
      ENDIF 
      NREC=NREC+1 
      N=IBUF(2) 
      CALL LVACODE(L,IBUF(4),1)
C 
C     * IF TIMESTEP AND LEVEL NUMBERS ARE NOT WITHIN THE REQUESTED
C     * INTERVALS, COPY THIS RECORD TO FILE 2.
C 
      IF(N.LT.NT1.OR.N.GT.NT2) GO TO 650
      IF(MOD(N-NT1,INT).NE.0)  GO TO 650
      IF(L.LT.LV1.OR.L.GT.LV2) GO TO 650
C 
C     * IF TIME AND LEVEL ARE OK, DELETE ONLY CORRECT NAMES.
C     * NAME OF 4H ALL DELETES ALL VARIABLES. 
C 
      IF(NAME1.EQ.NC4TO8(" ALL"))  GO TO 150
      IF(NAME1.EQ.IBUF(3)) GO TO 150
      IF(NAME2.EQ.IBUF(3)) GO TO 150
      IF(NAME3.EQ.IBUF(3)) GO TO 150
      IF(NAME4.EQ.IBUF(3)) GO TO 150
C 
C     * COPY THIS RECORD TO FILE 2. 
C 
  650 CALL FBUFOUT(2,IBUF,LEN,K)
      NCOPY=NCOPY+1 
      GO TO 150 
C 
C     * E.O.F. ON INPUT.
C 
  901 CALL                                         XIT('ENLEVE',-2) 
C---------------------------------------------------------------------
 5010 FORMAT(10X,5X,2I10,I5,5X,2I5,5X,4(1X,A4))                                 B4
 6010 FORMAT('0DELETE TIMES ',I10,'  TO',I10,' IN STEPS OF',I6)
 6020 FORMAT(' DELETE LEVELS',I6,'  TO',I6)
 6030 FORMAT(' VARIABLE NAMES =',4(2X,A4))
 6040 FORMAT('0RECORDS IN =',I6,'  COPIED =',I6,'  DELETED =',I6)
      END
