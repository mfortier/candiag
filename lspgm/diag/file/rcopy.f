      PROGRAM RCOPY
C     PROGRAM RCOPY(XIN,      XOUT,      YOUT,      INPUT,      OUTPUT,  )      B2
C    1        TAPE1=XIN,TAPE2=XOUT,TAPE3=YOUT,TAPE5=INPUT,TAPE6=OUTPUT)
C     ---------------------------------------------------------                 B2
C                                                                               B2
C     NOV 02/04 - S.KHARIN (OPTIONALLY SAVE COMPLEMENTARY RECORDS IN            B2
C                           THRID FILE.)                                        B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     FEB 19/93 - F.MAJAESS (CORRECT FBUFOUT CALL)
C     JAN / 93  - G.J.B.                                                        B2
C                                                                               B2
CRCOPY   - COPY RECORDS FROM RECORD NUMBERS N1 TO N2  INCLUSIVE         1  1 C  B1
C                                                                               B3
CAUTHOR  - G.J.B. FOLLOWING XLIN                                                B3
C                                                                               B3
CPURPOSE - COPY RECORDS BY RECORD NUMBER                                        B3
C          NOTE - XIN CAN BE REAL OR COMPLEX.                                   B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      XIN  = INPUT FILE (REAL OR COMPLEX)                                      B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      XOUT = RECORDS FROM RECORD NUMBERS N1 TO N2                              B3
C      YOUT = (OPTIONAL) RECORDS FROM RECORD NUMBERS 1..N1-1 TO N2+1..EOF       B3
C
CINPUT PARAMETERS...
C                                                                               B5
C      N1     = FIRST RECORD TO BE COPIED                                       B5
C      N2     = LAST RECORD TO BE COPIED                                        B5
C      NEWNAM = NEW NAME FOR OUTPUT LABEL                                       B5
C               (BLANK KEEPS OLD NAME)                                          B5
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C* RCOPY          10        23 NAME                                             B5
C-----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: MAXX = 10+SIZES_BLONP1xBLATxNWORDIO 
      COMMON/ICOM/IBUF(MAXX)
C---------------------------------------------------------------------
      NFF=5
      CALL JCLPNT(NFF,1,2,3,5,6)
      NFF=NFF-2
      REWIND 1
      REWIND 2
      IF (NFF.EQ.3) THEN
         REWIND 3
      ENDIF
C
C     * READ N1,N2 AND NEWNAM FROM CARD.
C
      READ(5,5010,END=903) N1,N2,NEWNAM                                         B4
      IF ( NEWNAM.EQ.NC4TO8("    ")) THEN
        WRITE(6,6007) N1,N2
      ELSE
        WRITE(6,6009) N1,N2,NEWNAM
      ENDIF
C
C     * READ THE NEXT GRID FROM FILE XIN.
C
      NR=0
      NRCOPY=0
      NRLEFT=0
  150 CALL FBUFFIN(1,IBUF,MAXX,K,LEN)
      IF(K.GE.0) GO TO 900
      IF(NR.EQ.0) WRITE(6,6020) (IBUF(K),K=1,8)
C
      NR =NR+1
C
C     * IF RECORD NUMBER NR IN RANGE N1,N2 THEN WRITE OUT THE RECORD
C     * CHANGE THE NAME IF NEWNAM IS NON-BLANK.
C     * OTHERWISE READ NEXT RECORD PROVIDED N2 IS NOT REACHED.
C
      IF(NR.GE.N1.AND.NR.LE.N2)THEN
         IF(NRCOPY.EQ.0) WRITE(6,6023)
         IF(NEWNAM.NE.NC4TO8("    ")) IBUF(3)=NEWNAM
         IF(NRCOPY.EQ.0.OR.NRCOPY.EQ.(N2-N1))
     +                     WRITE(6,6025) (IBUF(K),K=1,8)
         CALL FBUFOUT(2,IBUF,LEN,K)
         NRCOPY=NRCOPY+1
      ELSE IF (NFF.EQ.3) THEN
         IF(NEWNAM.NE.NC4TO8("    ")) IBUF(3)=NEWNAM
         CALL FBUFOUT(3,IBUF,LEN,K)
         NRLEFT=NRLEFT+1
      ENDIF
C
      IF(NR.LT.N2.OR.NFF.EQ.3) GO TO 150
C
  900 IF(NR.EQ.0.OR.NRCOPY.EQ.0) CALL              XIT(' RCOPY',-1)
      IF(NRCOPY.LT.(N2-N1)) WRITE(6,6025) (IBUF(K),K=1,8)
      WRITE(6,6010) NR,NRCOPY
      IF (NFF.EQ.3) WRITE(6,6015) NRLEFT
      CALL                                         XIT(' RCOPY',0)
C
C     * E.O.F. ON INPUT.
C
  903 CALL                                         XIT('RCOPY',-2)
C---------------------------------------------------------------------
 5010 FORMAT(10X,2I10,1X,A4)                                                    B4
 6007 FORMAT('0 RCOPY RECORDS FROM N1 =',I10, ' TO N2 =',I10)
 6009 FORMAT('0 RCOPY RECORDS FROM N1 =',I10, ' TO N2 =',I10,
     +       ', NEWNAM = ',A4)
 6010 FORMAT('0 RCOPY READ',I6,' AND COPIED',I6,
     *       ' RECORDS INTO 1ST OUTPUT FILE')
 6015 FORMAT('0                  AND COPIED',I6,
     *       ' RECORDS INTO 2ND OUTPUT FILE')
 6020 FORMAT(' FIRST RECORD READ... ',/,' ',A4,I10,2X,A4,I10,4I6)
 6023 FORMAT(' COPIED INTO 1ST OUTPUT FILE...')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
