      PROGRAM BINACH2 
C     PROGRAM BINACH2 (BIN,        CHAR,       OUTPUT,                  )       B2
C    1          TAPE11=BIN, TAPE12=CHAR, TAPE6=OUTPUT)
C     ------------------------------------------------                          B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     DEC 18/02 - F.MAJAESS (REVISE FOR 128-CHARACTERS/LINE IN "CHAR" RECORDS)  B2
C     MAR 05/02 - F.MAJAESS    (REVISED TO HANDLE "CHAR" KIND RECORDS)          
C     JUL 10/92 - E. CHAN      (WRITE OUT SUPERLABEL AS CHARACTER DATA)         
C     JAN 29/92 - E. CHAN      (CONVERT HOLLERITH LITERALS TO ASCII)            
C     OCT 04/89 - F. MAJAESS - BASED ON BINACH PROGRAM                          
C                                                                               B2
CBINACH2 - CONVERT A STANDARD CCRN FILE TO CHARACTER FORM                       B1
C          (NO CALL TO JCLPNT)                                          1  1    B1
C                                                                               B3
CAUTHORS - R.LAPRISE, S.J.LAMBERT, B.DUGAS                                      B3
C                                                                               B3
CPURPOSE - CONVERT A STANDARD CCRN FILE FROM BINARY TO CHARACTER FORM.          B3
C          THIS PROGRAM IS USED MAINLY TO CREATE A CHARACTER FILE WITH A        B3
C          TRANSPORTABLE CHARACTER FORMAT - SUCH AS A VMS STANDARD ASCII        B3
C          FILE HAVING FIXED LENGTH RECORDS WITH 80 CHARACTERS/RECORD           B3
C          AND 400 RECORDS/BLOCK. THE JCL FOR THIS:                             B3
C          ASSIGN(DN=...,FD=VMS,CS=AS,RF=F,CV=ON,MBS=32000,RS=80,               B3
C                                                       LM=60000,A=FT12)        B3
C          NOTE - FORTRAN TAPE UNITS 6, 11 AND 12 ASSIGNMENTS HAS TO BE         B3
C                 DONE PRIOR TO RUNNING BINACH2.                                B3
C                 AN EOF MARK IS WRITTEN AT THE END OF UNIT # 12.               B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      BIN = BINARY (PACKED) STANDARD CCRN FILE.                                B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      CHAR = CODED CHARACTER FILE.                                             B3
C-------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/LCM/F(SIZES_BLONP1xBLAT) 
C 
      COMMON/BUFCOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
C
      CHARACTER*80 LABEL
      CHARACTER*8  CBUF(1)
      EQUIVALENCE (LABEL,IDAT),(CBUF,IDAT)
C 
      LOGICAL OK
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C-----------------------------------------------------------------------
      REWIND 11 
      NRECS=0 
      NCOPY=0 
C 
C     * READ IN BINARY RECORD.
C 
100   CALL GETFLD2(11,F,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6000) NRECS,NCOPY 
        IF(NRECS.EQ.0)THEN
          CALL                                     XIT('BINACH2',-1)
        ELSE
          ENDFILE(12) 
          CALL                                     XIT('BINACH2',0) 
        ENDIF 
      ENDIF 
      NWDS=IBUF(5)*IBUF(6)
      IF(IBUF(1).EQ.NC4TO8("SPEC").OR.
     +   IBUF(1).EQ.NC4TO8("FOUR")    ) NWDS=NWDS*2
C 
C     * WRITE OUT CHARACTER RECORD. 
C 
      IBUF(8)=1 
      IF(IBUF(1).NE.NC4TO8("LABL").AND.IBUF(1).NE.NC4TO8("CHAR"))THEN
        WRITE(12,6010) IBUF,(F(I),I=1,NWDS) 
        NCOPY=NCOPY+INT((NWDS/5.E0)+0.5E0)+1
      ELSE
        IF(IBUF(1).EQ.NC4TO8("CHAR"))THEN
         WRITE(12,6040) IBUF,(CBUF(I),I=1,NWDS)
        ELSE
         WRITE(12,6030) IBUF,LABEL
        ENDIF
        NCOPY=NCOPY+2 
      ENDIF 
      NRECS=NRECS+1 
      GO TO 100 
C-----------------------------------------------------------------------
 6000 FORMAT('0 BINACH2 CONVERTED ',I10,' INTO ',I10,' RECORDS.')
 6010 FORMAT(1X,A4,I10,1X,A4,5I10,10X,/(1P6E22.15))
 6030 FORMAT(1X,A4,I10,1X,A4,5I10,10X,/,A80)
 6040 FORMAT(1X,A4,I10,1X,A4,5I10,10X,/,(16A8))
      END
