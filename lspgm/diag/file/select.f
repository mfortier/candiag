      PROGRAM SELECT
C     PROGRAM SELECT (IN,        S1,       ...,           S86,                  B2
C    1                                      INPUT,        OUTPUT,       )       B2
C    2          TAPE1=IN, TAPE11=S1,       ...,    TAPE96=S86,
C    3                               TAPE5 =INPUT, TAPE6 =OUTPUT)
C     -----------------------------------------------------------               B2
C                                                                               B2
C     NOV 23/09 - S.KHARIN (OUTPUT "NAME" OF FIELDS WITH NO MATCHING REQUESTED  B2
C                           FOUND RECORDS)                                      B2
C     NOV 03/09 - S.KHARIN (ADJUST 6040 FORMAT)                                 
C     JUL 06/09 - F.MAJAESS (CHECK PROPER HANDLING AND POSSIBLY MASSAGE NT1/NT2
C                            ENTERED VALUES.
C                            SWITCH "JCLPNT1" REFERENCES TO "JCLPNT")
C     APR 29/09 - S.KHARIN (TRY TO SELECT ALL VARIABLES BEFORE ABORTING.
C                           TREAT NT2=999999999 AS +INFINITY.
C                           ADD OPTION TO SCAN SCAN FILE UNTIL THE END.
C                           ADD OPTION FOR KEEPING SUPERLABELS).
C     MAR 02/09 - S.KHARIN (INCREASE NUMBER OF OUTPUT FILES TO 86)
C     MAR 17/04 - S.KHARIN,F.MAJAESS (WHEN REMOVING OUTPUT FILE/LINK VIA SYSTEM
C                                     CALL "rm", ENSURE THE ASSOCIATED UNIT
C                                     NUMBER IS CLOSED BEFORE THE ISSUANCE OF
C                                     THE SYSTEM CALL, AND THEN IF REQUIRED,
C                                     RE-OPEN THE SAME UNIT NUMBER AFTERWARDS.)
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     MAR 26/02 - S.KHARIN (INCREASE NUMBER OF SELECTED VARIABLE TO 18 BY
C                           READING THE NAMES FROM THE 2ND INPUT CARD;
C                          'IN' MAY BE A DIRECTORY WITH SPLITTED VARIABLES;
C                          ABORT IF OUTPUT FILES DON'T MATCH THE INPUT CARD;
C                          ABORT IF OUTPUT FILES ARE EMPTY;
C                          THE NAME ' ALL' FOR SELECTING ALL VARIABLES IS NOT
C                          ALLOWED IN SPLIT-ALL APPROACH)
C     MAR 28/01 - F.MAJAESS(PROTECT AGAINST THE SPECIFIED TIMESTEP RANGE TO BE
C                           OUTSIDE THE TIMESTEP RANGE IN THE INPUT FILE).
C     JAN 06/92 - E. CHAN  (ADD CALLS TO LVACODE TO ENSURE MONOTONICALLY
C                           INCREASING VERTICAL COORDINATE)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII AND
C                           CALL TO CHCONV WITH READ OF INTERNAL FILE)
C     APR 28/88 - FM. (ALLOW SPECIFYING A TIMESTEP INCREMENT VALUE
C                      ("INT") OF UP TO 10 DIGITS OR 9 DIGITS AND
C                      A NEGATIVE SIGN).
C     MAY 13/83 - RL.
C                                                                               B2
CSELECT  - SELECTS/MAKE SYMLINKS TO UP TO 86 VARIABLES FROM A FILE/DIR  1 86 C  B1
C                                                                               B3
CAUTHOR  - J.D.HENDERSON                                                        B3
C                                                                               B3
CPURPOSE - SELECTS/MAKES SYMBOLIC LINKS TO UP TO 86 NAMED VARIABLES,            B3
C          AT EQUAL TIMESTEP INTERVALS BETWEEN TWO SPECIFIED TIMESTEPS          B3
C          AND BETWEEN TWO SPECIFIED LEVELS FROM A FILE OR DIRECTORY WHERE      B3
C          SPLITTED VARIABLE ARE LOCATED.                                       B3
C          NOTE - STEP NUMBERS IN FILE 'IN' ARE ASSUMED TO BE INCREASING.       B3
C                 THERE MUST BE A VALID VARIABLE NAME SPECIFIED FOR EACH        B3
C                 OUTPUT FILE. OTHERWISE, THE PROGRAM WILL ABORT.               B3
C                 THE VARIABLE NAME '4H ALL' IS NOT ALLOWED WHEN THE FIRST      B3
C                 ARGUMENT IS A DIRECTORY WITH SPLITTED FILES.                  B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      IN = FILE WITH RECORDS TO BE SELECTED, OR DIRECTORY WITH VARIABLES,      B3
C           ONE PER FILE.                                                       B3
C                                                                               B3
COUTPUT FILES...                                                                B3
C                                                                               B3
C      S1,...,S86 = SELECTED VARIABLES.                                         B3
C
CINPUT PARAMETERS...
C                                                                               B5
C      NT1,NT2   = TIMESTEP RANGE (INCLUSIVE)                                   B5
C                  IF NT1<0 THEN SCAN THE FILE UNTIL EOF AND                    B5
C                  SELECT ALL TIMESTEPS IN THE RANGE |NT1|,...,NT2.             B5
C                  IF NT2=999999999, TREAT IT AS NT2=+INFINITY.                 B5
C      ANT1,ANT2 = TWO 5-CHARACTER STRINGS USED TO COMPUTE THE TIMESTEP         B5
C                  INCREMENT 'INT'.                                             B5
C                  IF ANT1=0 OR EMPTY, ANT2 IS IGNORED AND 'INT' IS SET TO 1.   B5
C                  IF ANT2 CONTAINS CHARACTERS OTHER THAN (0..9), IT IS         B5
C                  IGNORED AND 'INT' IS CALCULATED FROM ANT1 ONLY.              B5
C                  OTHERWISE 'INT' IS CALCULATED FROM 10-CHARACTER STRING       B5
C                  ANT1 & ANT2. FOR EXAMPLE,                                    B5
C                  IF ANT1,ANT2='          ' THEN INT=1,                        B5
C                  IF ANT1,ANT2='    0     ' THEN INT=1,                        B5
C                  IF ANT1,ANT2='        12' THEN INT=1,                        B5
C                  IF ANT1,ANT2='0000000012' THEN INT=1,                        B5
C                  IF ANT1,ANT2='    1     ' THEN INT=1,                        B5
C                  IF ANT1,ANT2='   12     ' THEN INT=12,                       B5
C                  IF ANT1,ANT2='   12    0' THEN INT=12,                       B5
C                  IF ANT1,ANT2='  -12     ' THEN INT=-12,                      B5
C                  IF ANT1,ANT2='   1200000' THEN INT=120000,                   B5
C                  IF ANT1,ANT2='    -10000' THEN INT=-10000.                   B5
C                                                                               B5
C                  INT  >0, RECORDS ARE SELECTED IN THE INTERVAL NT1 TO NT2     B5
C                           BY INCREMENT INT STARTING FROM STEP NT1, THAT IS,   B5
C                           NT1, NT1+INT, NT1+2*INT, ..., NT2-MOD(NT2,INT).     B5
C                       <0  RECORDS ARE SELECTED IN THE INTERVAL NT1 TO NT2     B5
C                           BY INCREMENT ABS(INT) STARTING THE FIRST TIME STEP  B5
C                           IN INPUT FILE THAT IS LARGER THAN, OR EQUAL TO NT1, B5
C                           THAT IS, NT1*, NT1*+ABS(INT), NT1*+2*ABS(INT), ..., B5
C                           WHERE NT1* IS THE FIRST STEP THAT >=NT1.            B5
C      LV1,LV2   = LEVEL RANGE (INCLUSIVE)                                      B5
C      NAME1     = FIRST VARIABLE TO BE SELECTED FOR FILE S1.                   B5
C                = ' ALL' TO SELECT ALL VARIABLES FROM THE INPUT FILE.          B5
C                = 'LALL' THE SAME AS ABOVE BUT ALSO TO KEEP SUPERLABELS.       B5
C                  THE NAMES ' ALL'/'LALL' ARE NOT ALLOWED IF THE FIRST         B5
C                  ARGUMENT IS A DIRECTORY WITH SPLITTED FILES.                 B5
C      NAME2     = SECOND VARIABLE TO BE SELECTED FOR FILE S2.                  B5
C        ...                                                                    B5
C      NAME86    = FOURTH VARIABLE TO BE SELECTED FOR FILE S86.                 B5
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C*SELECT.  STEP         1 999999999    1       500  500 NAME TEMP  PHI          B5
C(SELECT TEMP AND PHI AT LEVEL 500, ALL TIME STEPS)                             B5
C                                                                               B5
C*SELECT.  STEP         1 999999999    1       500  500 NAME  ALL               B5
C(SELECT ALL VARIABLES AT LEVEL 500, ALL TIME STEPS)                            B5
C                                                                               B5
C*SELECT.  STEP         1 999999999    1     -9001 1000 NAME TEMP  PHI VORT  DIVB5
C(SELECT TEMP,PHI,VORT,DIV ALL LEVELS, ALL TIME STEPS)                          B5
C                                                                               B5
C*SELECT.  STEP      7901      8812  100     -9001 1000 NAME   ST               B5
C(SELECT ST, 79 TO 88, JANUARY ONLY, ASSUMING MONTHLY DATA AND TIME STAMP YYMM) B5
C                                                                               B5
C*SELECT.  STEP    -10100 999123124   1000000-9001 1000 NAME  ALL               B5
C(SELECT ALL VARIABLES FOR JANUARY, ASSUMING TIME STAMP YYMMHHDD, BY SCANNING   B5
C THE INPUT FILE UNTIL THE END)                                                 B5
C                                                                               B5
C*SELECT.  STEP         1 999999999    1     -9001 1000 NAME X001 X002 X003 X004B5
C*SELECT.  X005 X006 X007 X008 X009 X010 X011 X012 X013 X014 X015 X016 X017 X018B5
C(SELECT 18 VARIABLES X001,...,X018, ALL TIME STEPS AND ALL LEVELS)             B5
C                                                                               B5
C----------------------------------------------------------------------------
C
C     * NFOUTMX - MAXIMAL NUMBER OF OUTPUT FILES
C

      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER (NFOUTMX=86)
      LOGICAL OK,NEGINT,FIRST,STAT,LSPL,LINF
      CHARACTER NUM(10)*1,ANT1*5,ANT2*5,CNT1*1
      INTEGER*8 NT81,NT82,I109
C
C     * ITAB  - NAME,NTMIN,NTMAX,LEVMIN,LEVMAX FROM .VARTABLE
C     * NROUT - THE NUMBER OF RECORDS SELECTED FOR EACH OUTPUT FILE
C     * FNAM  - SPLITTED VARIABLE FILE NAMES
C     * LSEL  - LOGICAL SWITCH FOR SELECTING A SUBSET OR MAKING A SYM LINK
C
      INTEGER ITAB(5),NAME(NFOUTMX),NROUT(NFOUTMX)
      CHARACTER*4 FNAM(NFOUTMX),LABL
      LOGICAL LSEL(NFOUTMX),LMISS
      CHARACTER*128 ARG(90),DIRNAM

C     * ARG - ARRAY WITH COMMAND LINE ARGUMENTS

      COMMON /JCLPNTA/ ARG

C     * BEFORE JCLPNT:
C     *  LDIR   = .TRUE.,  TEST WHETHER THE FIRST ARGUMENT IS A DIRECTORY.
C     *         = .FALSE., DONT RUN THE TEST.
C     * AFTER JCLPNT:
C     *  LDIR   = .TRUE.,  IF THE FIRST ARGUMENT IS A DIRECTORY,
C     *           .FALSE., OTHERWISE.
C     *  DIRNAM = THE DIRECTORY NAME.
C     *  LENDIR = NUMBER OF CHARACTERS IN THE DIRECTORY NAME.

      LOGICAL LDIR,LEOF
      COMMON /JCLPNTD/ LDIR,LENDIR,DIRNAM
      COMMON /MACHTYP/ MACHINE,INTSIZE

      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      DATA LA/SIZES_BLONP1xBLATxNWORDIO/
      DATA NUM /'0','1','2','3','4','5','6','7','8','9'/
      DATA LSEL/NFOUTMX*.TRUE./,LMISS/.FALSE./,LINF/.FALSE./,
     +     LSPL/.FALSE./
      DATA I109/9999999999/

C---------------------------------------------------------------------
C
C     * SET LDIR=..TRUE. TO PERFORM THE TEST ON THE FIRST ARGUMENT
C     * AFTER JCLPNT, LDIR=.FALSE. IF THE 1ST ARGUMENT IS A FILE, AND
C                      LDIR=.TRUE.  IF THE 1ST ARGUMENT IS A DIRECTORY.
C
      LDIR=.TRUE.
      NF=89
      CALL JCLPNT(NF,1,11,12,13,14,
     +                  15,16,17,18,19,20,21,22,23,24,25,26,27,28,
     +                  29,30,31,32,33,34,35,36,37,38,39,40,41,42,
     +                  43,44,45,46,47,48,49,50,51,52,53,54,55,56,
     +                  57,58,59,60,61,62,63,64,65,66,67,68,69,70,
     +                  71,72,73,74,75,76,77,78,79,80,81,82,83,84,
     +                  85,86,87,88,89,90,91,92,93,94,95,96,5,6)
      NFOUT=NF-3
      IF (NFOUT.LT.1.OR.NFOUT.GT.NFOUTMX) CALL     XIT('SELECT',-1)
C
C     * PRINT THE TYPE (FILE/DIR) OF THE FIRST ARGUMENT
C
      IF (LDIR) THEN
        WRITE(6,6005) DIRNAM
      ELSE
        WRITE(6,6006) ARG(1)
      ENDIF

C
C     * REWIND FILES
C
      IF (.NOT.LDIR) REWIND 1
      DO I=1,NFOUT
        REWIND 10+I
      ENDDO
C
C     * READ THE 1ST INPUT CARD
C
      READ(5,5010,END=900)CNT1,NT81,NT82,ANT1,ANT2,LV1,LV2,                     B4
     1                    (NAME(I),I=1,MIN(4,NFOUT))                            B4

C
C     * MODIFY POSSIBLE '9999999999' ENTERED NT1/NT2� VALUE TO '999999999'
C
      IF (IABS(NT81).EQ.I109) THEN
       IF(NT81.LT.0) THEN
          NT81=-999999999
       ELSE
          NT81=999999999
       ENDIF
       WRITE(6,6009) 'NT1',NT81
      ENDIF
      IF (IABS(NT82).EQ.I109) THEN
       IF(NT82.LT.0) THEN
          NT82=-999999999
       ELSE
          NT82=999999999
       ENDIF
       WRITE(6,6009) 'NT2',NT82
      ENDIF
C
C     * CHECK FOR PROPER HANDLING OF NT1/NT2 ENTERED VALUES WHEN RUNNING IN
C     * 32-BITS INTEGER MODE.

      IF(MACHINE.EQ.2) THEN
       RLARGE=(2.**31)-1.
       IF (IABS(NT81).GT.RLARGE.OR.IABS(NT82).GT.RLARGE) THEN
         WRITE(6,6008)NT81,NT82
         CALL                                      XIT('SELECT',-2)
       ENDIF
      ENDIF

      NT1=NT81
      NT2=NT82
C
C     * READ ADDITIONAL CARDS, IF NECESSARY
C
      NREAD=4
 100  CONTINUE
      IF (NFOUT.GT.NREAD) THEN
        READ(5,5011,END=901)(NAME(I),I=NREAD+1,MIN(NFOUT,NREAD+14))             B4
        NREAD=NREAD+14
        GOTO 100
      ENDIF
C
C     * VERIFY THAT THERE IS A NON-BLANCK NAME FOR EACH OUTPUT FILE
C
      DO I=1,NFOUT
        IF (NAME(I).EQ.NC4TO8("    ")) THEN
          WRITE(6,6007)
          CALL                                     XIT('SELECT',-3)
        ENDIF
      ENDDO
C
C     * CONVERT CNT1 TO THE SIGN
C
      IF(NT1.GT.999999999.AND.CNT1.EQ.'-')NT1=-NT1
C
C     * IF NT2=999999999, TREAT IT AS +INFINITY
C
      IF(NT2.EQ.999999999)THEN
        LINF=.TRUE.
        WRITE(6,*)'THE LAST TIME STEP NT2 IS TREATED AS +INFINITY.'
      ENDIF
C
C     * SET READ-UNTIL-EOF SWITCH
C
      LEOF=.FALSE.
      IF (NT1.LT.0)THEN
        NT1=-NT1
        LEOF=.TRUE.
        WRITE(6,'(A)')' READ UNTIL EOF.'
      ENDIF
C
C     * SET SUPERLABEL SWITCH
C
      IF (NAME(1).EQ.NC4TO8("LALL")) THEN
        NAME(1)=NC4TO8(" ALL")
        LSPL=.TRUE.
        WRITE(6,'(A)')' KEEP SUPERLABELS'
      ENDIF
C
C     * ABORT IF NAME(1)=4H ALL AND LDIR=.TRUE.
C     * (THE NAME 4H ALL IS NOT ALLOWED IN SPLIT-ALL MODE)
C
      IF (LDIR.AND.(NAME(1).EQ.NC4TO8(" ALL")))
     1   CALL                                      XIT('SELECT',-4)
C
C     * CONVERT ANT1 AND CHECK IF ANT2 IS TO BE USED IN ASSIGNING A VALUE
C     * TO 'INT'.
C
      IF (ANT1(5:5).EQ.'-') THEN
        INT=0
        NEGINT=.TRUE.
      ELSE
        READ(ANT1,'(I5)') INT
        IF (INT.LT.0) THEN
          NEGINT=.TRUE.
          INT=-INT
        ELSE
          NEGINT=.FALSE.
          IF (INT.EQ.0) THEN
            INT=1
            GO TO 140
          ENDIF
        ENDIF
      ENDIF
C
C     * CHECK AND ADD TO INT ANT2 IF IT IS A 5 DIGITS NUMBER CONSISTING
C     * OF (0-9).
C
      DO 125 J=1,5
        DO 115 I=1,10
          IF (ANT2(J:J).EQ.NUM(I)) GO TO 125
 115    CONTINUE
        GO TO 140
 125  CONTINUE
      READ(ANT2,'(I5)') INTP
      INT = 100000*INT + INTP
      IF (INT.EQ.0) INT=1
C
 140  WRITE(6,6010) NT1,NT2,INT
      WRITE(6,6020) LV1,LV2
      WRITE(6,6030) (NAME(I),I=1,NFOUT)
C
C     * CONVERT LV1 AND LV2 INTO CODED LEVELS THAT ARE MONOTONICALLY INCREASING.
C     * THIS IS REQUIRED FOR HANDLING VERTICAL COORDINATES (IBUF(4)) ABOVE 1 MB.
C
      CALL LVACODE(LV1,LV1,1)
      CALL LVACODE(LV2,LV2,1)

      IF (LDIR) GOTO 200
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * CASE 1: THE FIRST COMMAND LINE ARGUMENT IS A FILE.
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      NRIN=0
      DO I=1,NFOUT
        NROUT(I)=0
      ENDDO
      FIRST=.TRUE.
C
C     * READ RECORD BY RECORD FROM FILE 1.
C     * STOP AT EOF OR IF STEP NUMBER EXCEEDS NT2.
C
 150  CALL RECGET(1, -1,-1,-1,-1, IBUF,LA,OK)
      IF (.NOT.OK .AND. NRIN.EQ.0) THEN
C
C       * INPUT FILE IS EMPTY. ABORT.
C
        WRITE(6,6060)
        CALL                                       XIT('SELECT',-5)
      ENDIF
      NT=IBUF(2)
      IF (.NOT.OK .OR. (NT.GT.NT2.AND..NOT.LINF.AND..NOT.LEOF)) THEN
        WRITE(6,6040) NRIN,(NROUT(I),I=1,NFOUT)
        DO I=1,NFOUT
          IF (NROUT(I).EQ.0) THEN
            WRITE(6,6070)NAME(I)
            LMISS=.TRUE.
          ENDIF
        ENDDO
C
C       * ABORT IF SOME OUTPUT FILES ARE EMPTY
C
        IF(LMISS)CALL                              XIT('SELECT',-6)
C
C       * NORMAL EXIT
C
        CALL PRTLAB(IBUF)
        CALL                                       XIT('SELECT', 0)
      ENDIF
      CALL LVACODE(L,IBUF(4),1)
      NRIN=NRIN+1
      IF (LSPL.AND.(IBUF(3).EQ.NC4TO8("LABL").OR.
     +              IBUF(3).EQ.NC4TO8("CHAR")))THEN
        CALL RECPUT(11,IBUF)
        GOTO 150
      ENDIF
C
C     * IF TIMESTEP AND LEVEL NUMBERS ARE NOT WITHIN THE REQUESTED
C     * INTERVALS, GO BACK AND READ THE NEXT RECORD.
C
      IF (NT.LT.NT1.OR.(NT.GT.NT2.AND..NOT.LINF).OR.
     +     L.LT.LV1.OR.L.GT.LV2) GO TO 150
      DO I=1,NFOUT

C       * IF NAME IS REQUESTED OR THE FIRST NAME IS 4H ALL, WRITE OUT

        IF (NAME(I).EQ.IBUF(3).OR.(I.EQ.1.AND.
     +                             NAME(1).EQ.NC4TO8(" ALL")) ) THEN
          IF (NEGINT.AND.FIRST) THEN
            NT1=NT
            WRITE(6,6050) NT1
            FIRST=.FALSE.
          ENDIF
          IF (MOD(NT-NT1,INT).NE.0) GO TO 160
          CALL RECPUT(10+I,IBUF)
          NROUT(I)=NROUT(I)+1
          IF(NROUT(I).EQ.1)CALL PRTLAB(IBUF)
        ENDIF
 160    CONTINUE
      ENDDO
      GO TO 150
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * CASE 2: THE FIRST ARGUMENT IS A DIRECTORY (SPLIT-ALL MODE).
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
 200  CONTINUE
C
C     * CONVERT NAMES TO CHARACTER STRINGS AND STRIP OUT THE LEADING SPACES
C
      DO I=1,NFOUT
        WRITE(LABL,'(A4)')NAME(I)
        NSP=0
        DO J=1,3
          IF (LABL(J:J).EQ.' ') NSP=NSP+1
        ENDDO
        FNAM(I)=LABL((NSP+1):4)
      ENDDO
      LENARG1=LEN_TRIM(ARG(1))
C
C     * ATTEMPT TO OPEN THE FILE '.VARTABLE'.
C     * DO NOT ABORT IF IT DOES NOT EXIST.
C
      OPEN(1,FILE=DIRNAM(1:LENDIR)//'/.VARTABLE',
     1     STATUS='OLD',FORM='FORMATTED',ERR=220)
      REWIND 1
C
C     * CHECK THE TIME STEP AND LEVEL RANGES OBTAINED FROM .VARTABLE
C
      NFOUND=0
 205  READ(1,5020,END=215)(ITAB(J),J=1,5)
      CALL LVACODE(ITAB(4),ITAB(4),1)
      CALL LVACODE(ITAB(5),ITAB(5),1)
      DO I=1,NFOUT
        IF (ITAB(1).EQ.NAME(I)) THEN
C
C         * SET LSEL=.FALSE. IF THE TIME AND LEVEL RANGES READ FROM .VARTABLE
C         * ARE WITHIN THE REQUESTED RANGES READ FROM THE INPUT CARD
C
          IF ( INT.EQ.1 .AND.
     1         NT1.LE.ITAB(2).AND.((NT2.GE.ITAB(3).OR.LINF)).AND.
     2         LV1.LE.ITAB(4).AND.LV2.GE.ITAB(5)) LSEL(I)=.FALSE.
          NFOUND=NFOUND+1
C
C         * PROCEED IF ALL VARIABLES ARE FOUND, OTHERWISE READ NEXT ENTRY.
C
          IF (NFOUND.EQ.NFOUT) GO TO 220
          GOTO 205
        ENDIF
      ENDDO
      GOTO 205
 215  WRITE(6,6035)
 220  CONTINUE
C
C     * DO FOR EACH VARIABLE
C
      DO I=1,NFOUT
        IF (LSEL(I)) THEN
C
C         * SELECT FROM A SPLITTED FILE...
C
          WRITE(6,6090) ARG(1)(1:LENARG1),FNAM(I)
C
C         * REMOVE THE OUTPUT FILE FIRST TO ENSURE THAT DATA ARE NOT
C         * OVERWRITTEN IN CASE IT IS A LINK.
C         *
C         * ENSURE THE CLOSURE OF THE ASSOCIATED UNIT NUMBER BEFORE
C         * AND THE RE-OPENNING OF IT AFTER THE SYSTEM CALL.
C         * OTHERWISE, THE FILE UNIT ASSOCIATION GETS MESSED UP AND
C         * THE FILE REMOVAL TO GET DEFERRED LEADING TO THE
C         * DISAPPEARANCE OF THE FILE TO WHICH THE OUTPUT DATA IS
C         * WRITTEN TO.
C
          ITNF=10+I
          INQUIRE(ITNF,OPENED=STAT)
          IF (STAT) THEN
           CLOSE (ITNF)
          ENDIF
          CALL SYSTEM(' \rm -f '//ARG(I+1))
          OPEN(ITNF,FILE=ARG(I+1),FORM='UNFORMATTED')
          REWIND ITNF
C
C         * OPEN OUTPUT FILE
C
          OPEN(50+I,FILE=DIRNAM(1:LENDIR)//'/'//FNAM(I),
     1         STATUS='OLD', FORM='UNFORMATTED',ERR=260)
          REWIND 50+I
C
C         * READ THE NEXT RECORD
C
          NRIN=0
          NROUT(I)=0
          FIRST=.TRUE.
 250      CALL RECGET(50+I, -1,-1,-1,-1, IBUF,LA,OK)
          IF (.NOT.OK .AND. NRIN.EQ.0) THEN
C
C           * INPUT FILE IS EMPTY.
C
            WRITE(6,6060)
            GOTO 260
          ENDIF
          NT=IBUF(2)
          IF (.NOT.OK.OR.((NT.GT.NT2.AND..NOT.LINF).AND..NOT.LEOF))THEN
            WRITE(6,6040) NRIN,NROUT(I)
            IF (NROUT(I).EQ.0) THEN
C
C             * OUTPUT FILE IS EMPTY
C
              WRITE(6,6070)
              GOTO 260
            ENDIF
            GO TO 270
          ENDIF
          CALL LVACODE(L,IBUF(4),1)
          NRIN=NRIN+1
C
C         * IF TIMESTEP AND LEVEL NUMBERS ARE NOT WITHIN THE REQUESTED
C         * INTERVALS, GO BACK AND READ THE NEXT RECORD.
C
          IF (NT.LT.NT1.OR.(NT.GT.NT2.AND..NOT.LINF).OR.
     +         L.LT.LV1.OR.L.GT.LV2) GO TO 250
          IF (NEGINT.AND.FIRST) THEN
            NT1=NT
            WRITE(6,6050)NT1
            FIRST=.FALSE.
          ENDIF
          IF (MOD(NT-NT1,INT).NE.0) GO TO 250
          CALL RECPUT(10+I,IBUF)
          NROUT(I)=NROUT(I)+1
          GO TO 250
 260      LMISS=.TRUE.
          WRITE(6,6080) ARG(1)(1:LENARG1),FNAM(I)
        ELSE
C
C         * MAKE A SYMLINK TO A SPLITTED FILE...
C         *
C         * ENSURE THE CLOSURE OF THE ASSOCIATED UNIT NUMBER ...
C
          WRITE(6,6095) ARG(1)(1:LENARG1),FNAM(I)
          ITNF=10+I
          INQUIRE(ITNF,OPENED=STAT)
          IF (STAT) THEN
           CLOSE (ITNF)
          ENDIF
          CALL SYSTEM(' \rm -f '//ARG(I+1)//
     1         '; \ln -s '//DIRNAM(1:LENDIR)//'/'//
     2         FNAM(I)//' '//ARG(I+1))
        ENDIF
 270    CONTINUE
      ENDDO
      IF(LMISS)CALL                                XIT('SELECT',-7)

C     * NORMAL EXIT

      CALL                                         XIT('SELECT',0)
C
C     * E.O.F. ON INPUT.
C
 900  CALL                                         XIT('SELECT',-8)
 901  CALL                                         XIT('SELECT',-9)
C---------------------------------------------------------------------
 5010 FORMAT(10X,4X,A1,2I10,2A5,2I5,5X,4(1X,A4))                                B4
 5011 FORMAT(10X,14(1X,A4))                                                     B4
 5020 FORMAT(A4,4(1X,I10))
 6005 FORMAT(' FIRST ARGUMENT IS A DIRECTORY = ',A)
 6006 FORMAT(' FIRST ARGUMENT IS A FILE = ',A)
 6007 FORMAT(' COMMAND LINE ARGUMENTS AND VARIABLE NAMES DON''T MATCH.')
 6008 FORMAT(' SORRY, VALUES ENTERED FOR NT1,NT2 (=',I10,',',I10,
     +                       ') ARE TOO LARGE FOR 32-BITS INTEGER MODE')
 6009 FORMAT(' ',A3,' ENTERED VALUE HAS BEEN RESET TO ',I10)
 6010 FORMAT(' SELECT TIMES ',I10,'  TO',I10,' IN STEPS OF',I10)
 6020 FORMAT(' SELECT LEVELS',I10,'  TO',I10)
 6030 FORMAT(' VARIABLE NAMES =',20(1X,A4)/4(17X,20(1X,A4)/))
 6035 FORMAT(' SOME VARIABLES ARE NOT FOUND IN .VARTABLE!')
 6040 FORMAT(' RECORDS IN  =',I6,/,' RECORDS OUT =',20I6/4(14X,20I6/))
 6050 FORMAT(' NEGATIVE INT, NT1 RESET TO',I10)
 6060 FORMAT(' INPUT FILE IS EMPTY')
 6065 FORMAT(' NO RECORDS SELECTED')
 6070 FORMAT(' NO RECORDS IN THE INPUT FILE FALLS WITHIN',
     +       ' THE SPECIFIED TIMESTEP OR LEVEL RANGE FOR NAME=',A4)
 6080 FORMAT(' SPLITTED FILE ',A,'/',A,' DOES NOT EXIST.')
 6090 FORMAT(' SELECT FROM FILE ',A,'/',A)
 6095 FORMAT(' MAKE SYM LINK TO ',A,'/',A)
      END
