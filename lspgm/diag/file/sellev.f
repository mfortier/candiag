      PROGRAM SELLEV
C     PROGRAM SELLEV (IN,       OUT,      INPUT,        OUTPUT,         )       B2
C    1          TAPE1=IN, TAPE2=OUT,TAPE5=INPUT, TAPE6 =OUTPUT)
C     -----------------------------------------------------------               B2
C                                                                               B2
C     FEB 06/19 - F.MAJAESS (REVISED TO COPE WITH TRIGGERED EOF ERROR WHEN      B2
C                            READING COMPLETELY FILLED LAST INPUT LINE)         B2
C     OCT 27/09 - S.KHARIN - ORIGINAL VERSION                                   B2
C                                                                               B2
CSELLEV  - SELECTS SPECIFIED LEVELS FROM A FILE.                        1  1 C  B1
C                                                                               B3
CAUTHOR  - S. KHARIN                                                            B3
C                                                                               B3
CPURPOSE - SELECTS SPECIFIED LEVELS FROM A FILE.                                B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      IN = FILE CONTAINING GRID OR SPECTRAL FIELDS.                            B3
C                                                                               B3
COUTPUT FILES...                                                                B3
C                                                                               B3
C     OUT = OUTPUT FILE CONTAINING SELECTED LEVELS.                             B3
C                                                                               B3
CCARD READ...                                                                   B3
C                                                                               B3
C     READ(5,5010,END=900) NLEV,(LEV(I),I=NLEV)                                 B3
C                                                                               B3
C                                                                               B5
CINPUT PARAMETERS...                                                            B3
C                                                                               B3
C(I1) LSPL = 1, KEEP SUPERLABELS. OTHERWISE, SKIP SUPERLABELS.                  B3
C(I4) NLEV = NUMBER OF LEVELS TO BE SELECTED.                                   B3
C(I5) LEVS = LEVELS TO BE SELECTED.                                             B3
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C*SELLEV     13   10   20   30   50   70  100  150  200  250  300  400  500  700B5
C(SELECT 13 LEVELS. SKIP SUPERLABELS)                                           B5
C                                                                               B5
C*SELLEV  1  17   10   20   30   50   70  100  150  200  250  300  400  500  600B5
C*SELLEV    700  850  925 1000                                                  B5
C(17 STANDARD WMO LEVELS. KEEP SUPERLABELS)                                     B5
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      LOGICAL OK
C
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      INTEGER LEV(200),ISTATS
C
      DATA LA,MAXL/SIZES_BLONP1xBLATxNWORDIO,200/
C---------------------------------------------------------------------
      NF=4
      CALL JCLPNT(NF,1,2,5,6)
      REWIND 1
      REWIND 2

C     * INITIALIZE TO AID IN DETECTING POTENTIAL PROBLEMS

      DO L=1,MAXL
       LEV(L)=-99999
      ENDDO
      NLEV=-99999
C
C     * READ INPUT PARAMETERS FROM CARD
C
      READ(5,5010,IOSTAT=ISTATS) LSPL,NLEV,(LEV(L),L=1,NLEV)                    B4
      IF ( ISTATS .GT. 0 ) THEN
C      * INPUT ERROR CASE
       GO TO 910
      ELSE
       IF ( ISTATS .LT. 0 ) THEN
C       *  EOF CASE - FORCE AN ABORT IF, NO VALID VALUE IN NLEV,
C       *             OR LAST "NLEV" LEV VALUE STILL UNCHANGED
        IF ( NLEV .LE. 0 ) GO TO 900
        IF ( LEV(NLEV) .EQ. -99999 ) GO TO 900
       ENDIF
      ENDIF
      IF (NLEV.GT.MAXL) CALL                       XIT('SELLEV',-1)
C
C     * WRITE OUT THE INPUT PARAMETERS TO STANDARD OUTPUT
C
      WRITE(6,6010) LSPL,NLEV,(LEV(I),I=1,NLEV)
C
C     * READ NEXT RECORD
C
      NRIN=0
      NROUT=0
 100  CONTINUE
      CALL RECGET(1,-1,-1,-1,-1,IBUF,LA,OK)
      IF (.NOT.OK) THEN
         IF(NRIN.EQ.0) CALL                        XIT('SELLEV',-2)
         CALL PRTLAB(IBUF)
         WRITE(6,6020) NRIN,NROUT
         CALL                                      XIT('SELLEV',0)
      ENDIF
      IF(IBUF(1).EQ.4HLABL.OR.IBUF(1).EQ.4HCHAR)THEN
        IF(LSPL.EQ.1)CALL RECPUT(2,IBUF)
        GOTO 100
      ENDIF
      NRIN=NRIN+1
      IF (NRIN.EQ.1) CALL PRTLAB(IBUF)
C
C     * WRITE RECORD OUT
C
      DO I=1,NLEV
         IF (IBUF(4) .EQ. LEV(I)) THEN
            CALL RECPUT(2,IBUF)
            NROUT=NROUT+1
            GO TO 110
         END IF
      END DO
 110  CONTINUE
      GO TO 100
C     * EOF CASE
 900  CALL                                         XIT('SELLEV',-3)
C     * INPUT READ ERROR CASE
 910  CALL                                         XIT('SELLEV',-4)
C---------------------------------------------------------------------
 5010 FORMAT (10X,I1,I4,13I5,1000(/10X,14I5))                                   B4
 6010 FORMAT (' INPUT PARAMETERS: LSPL=',I1,' NLEV=',I5/1000(14I5/))
 6020 FORMAT (1X,I10,' RECORDS PROCESSED.'
     1       /1X,I10,' RECORDS WRITTEN OUT.')
      END
