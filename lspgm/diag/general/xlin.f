      PROGRAM XLIN
C     PROGRAM XLIN (XIN,       XOUT,       INPUT,       OUTPUT,         )       C2
C    1        TAPE1=XIN, TAPE2=XOUT, TAPE5=INPUT, TAPE6=OUTPUT)
C     ---------------------------------------------------------                 C2
C                                                                               C2
C     APR 26/10 - S.KHARIN (CORRECT AN EXAMPLE ENTRY)                           C2
C     FEB 24/10 - S.KHARIN (ADD "LSPL" FOR SUPERLABELS HANDLING CONTROL)        
C     MAY 16/09 - S.KHARIN (ADD OPTION FOR DIVISION BY A CONSTANT)              
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     MAY 14/83 - R.LAPRISE.
C     NOV 27/80 - J.D.HENDERSON
C                                                                               C2
CXLIN    - LINEAR OPERATION ON ONE FILE Y=A*X+B OR Y=(X/A)+B            1  1 C  C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - FILE COMPUTATION  XOUT = A*XIN + B OR XOUT = (XIN/A) + B.            C3
C          NOTE - XIN CAN BE REAL OR COMPLEX.                                   C3
C                 B MUST BE 0.0 FOR COMPLEX FILES.                              C3
C                 LDIV INPUT PARAMETER CONTROLS WHETHER TO MULTIPLY             C3
C                      OR DIVIDE BY A.                                          C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      XIN  = INPUT FILE (REAL OR COMPLEX)                                      C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      XOUT = A*XIN+B (OR (XIN/A) + B)                                          C3
C
CINPUT PARAMETERS...
C                                                                               C5
C      A      = MULTIPLYING OR DIVIDING FACTOR                                  C5
C      B      = ADDITIVE CONSTANT                                               C5
C      NEWNAM = NEW NAME FOR OUTPUT LABEL                                       C5
C               (BLANK KEEPS OLD NAME)                                          C5
C      LDIV   = 1, DIVIDE BY A. OTHERWISE, MULTIPLY BY A.                       C5
C      LSPL   = 1, SKIP SUPERLABELS FROM OUTPUT FILE.                           C5
C                  OTHERWISE, PROPAGATE SUPERLABELS FROM INPUT FILE TO          C5
C                  OUTPUT FILE.                                                 C5
C                                                                               C5
CEXAMPLE OF INPUT CARD...                                                       C5
C                                                                               C5
C*XLIN          2.E1      1.E0 NAME                                             C5
C (XOUT=20.*XIN+1, NEW NAME IS 'NAME')                                          C5
C                                                                               C5
C*XLIN     9.80616E0      0.E0         1                                        C5
C (XOUT=XIN/9.80616)                                                            C5
C                                                                               C5
C*XLIN           1.0   -273.16              1                                   C5
C (SUBTRACT 273.16 AND SKIP SUPERLABELS)                                        C5
C-----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/G(SIZES_BLONP1xBLAT)
C
      LOGICAL OK,SPEC
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * READ A,B AND NEWNAM FROM CARD.
C
      READ(5,5010,END=903) A,B,NEWNAM,LDIV,LSPL                                 C4

      IF(LDIV.NE.1)THEN
        WRITE(6,6007) A,B,NEWNAM,LDIV,LSPL
      ELSE
        IF(A.EQ.0.E0)CALL                          XIT('XLIN',-1)
        WRITE(6,6008) A,1.E0/A,B,NEWNAM,LDIV,LSPL
        A=1.E0/A
      ENDIF
C
C     * READ THE NEXT GRID FROM FILE XIN.
C
      NR=0
  150 CALL GETFLD2(1,G,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0) CALL                           XIT('XLIN',-2)
        WRITE(6,6025) IBUF
        WRITE(6,6010) NR
        CALL                                       XIT('XLIN',0)
      ENDIF
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C
C     * DETERMINE SIZE OF THE FIELD.
C     * STOP IF FIELD IS COMPLEX AND B IS NOT 0.
C
      KIND=IBUF(1)
      IF(KIND.EQ.NC4TO8("LABL") .OR.KIND.EQ.NC4TO8("CHAR")) THEN
        IF(LSPL.NE.1)CALL PUTFLD2(2,G,IBUF,MAXX)
        GOTO 150
      ENDIF
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC.AND.B.NE.0.E0) CALL                  XIT('XLIN',-3)
C
C     * SET G = A*G + B.
C
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
      DO 210 I=1,NWDS
  210 G(I)=A*G(I)+B
C
C     * SAVE ON FILE XOUT.
C     * CHANGE THE NAME IF NEWNAM IS NON-BLANK.
C
      IF(NEWNAM.NE.NC4TO8("    ")) IBUF(3)=NEWNAM
      CALL PUTFLD2(2,G,IBUF,MAXX)
      NR=NR+1
      GO TO 150
C
C     * E.O.F. ON INPUT.
C
  903 CALL                                         XIT('XLIN',-4)
C---------------------------------------------------------------------
 5010 FORMAT(10X,2E10.0,1X,A4,2I5)                                              C4
 6007 FORMAT('0 XLIN A,B =',1P2E12.4,' NEWNAM=',A4,
     1     ' LDIV=',I5,' LSPL=',I5)
 6008 FORMAT('0 XLIN A = 1.E0 /',1P1E12.4,' =',1P1E12.4,' B =',1P1E12.4,
     1     ' NEWNAM=',A4,' LDIV=',I5,' LSPL=',I5)
 6010 FORMAT('0 XLIN READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
