C     PROGRAM TIMMAX (X1,       MAX,                                            C2
C    1                X2,       ...,       X12,                 OUTPUT, )       C2
C    2          TAPE1=X1, TAPE2=MAX, 
C    3          TAPE2=X2,       ...,TAPE12=X12,           TAPE6=OUTPUT)         C2
C     ----------------------------------------------------------------          C2
C                                                                               C2
C     SEP 16/13 - S.KHARIN (ADD UP TO 12 OPTIONAL ADDITIONAL INPUT FILES)       C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       
C     AUG 21/00 - F.MAJAESS (ENSURE FIELD DIMENSION CHECK VERSUS "MAXRSZ")      
C     JUL 05/99 - F.MAJAESS (REVISED TO OPERATE ON REAL AND IMAGINARY PARTS OF
C                            THE COMPLEX FIELD AS A PAIR INSTEAD OF SEPARATELY)
C     JUL 16/98 - D.LIU
C                                                                               C2
CTIMMAX  - CALCULATES TIME INTERVAL MAXIMUM FOR EACH DATA POINT.       12  1    C1
C                                                                               C3
CAUTHOR  - D. LIU                                                               C3
C                                                                               C3
CPURPOSE - CALCULATES THE MAXIMUM VALUE FOR EACH DATA POINT IN THE PERIOD       C3
C          SPANNED BY THE INPUT FILE. FOR COMPLEX FIELDS, THE OUTPUT CONTAINS   C3
C          CORRESPONDING REAL AND IMAGINARY PARTS OF THE COMPLEX NUMBER WITH    C3
C          THE LARGEST AMPLITUDE FOR THE PERIOD.                                C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C        X1 = SERIES OF MULTI-LEVEL SETS, (DATA MAY BE REAL OR COMPLEX)         C3
C             ALL RECORDS MUST HAVE THE SAME KIND, NAME AND DIMENSIONS.         C3
C   X2...X12= (OPTIONAL) ADDITIONAL TIME SERIES OF MULTI-LEVEL SETS.            C3
C             TIME MEAN OF ALL INPUT FILES IS COMPUTED.                         C3
C             THEY MUST BE SPECIFIED AFTER OUTPUT MAX FILE ON THE COMMAND LINE. C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C     MAX = MAXIMUM FOR EACH DATA POINT FOR THE PERIOD OF THE INPUT.            C3
C--------------------------------------------------------------------------
C
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVxBLONP1xBLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,SPEC
      INTEGER LEV(SIZES_MAXLEV),JBUF(8)
      COMMON/BLANCK/X(SIZES_BLONP1xBLAT),Y(SIZES_MAXLEVxBLONP1xBLAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C
C     * IUA/IUST  - ARRAY KEEPING TRACK OF UNITS ASSIGNED/STATUS.
C
      INTEGER IUA(100),IUST(100)
      COMMON /JCLPNTU/ IUA,IUST

      DATA MAXX,MAXG,MAXL / SIZES_BLONP1xBLATxNWORDIO,
     & SIZES_MAXLEVxBLONP1xBLAT,SIZES_MAXLEV/
      DATA MAXRSZ /SIZES_BLONP1xBLAT/
C---------------------------------------------------------------------
      NF=14
      CALL JCLPNT(NF,11,2,12,13,14,15,16,17,18,19,20,21,22,6)
      IF (NF.LT.3) CALL                            XIT('TIMMAX',-1)
      REWIND 11
      REWIND 2
      NINP=1
      DO I=2,12
        IF (IUST(I+10).EQ.1) THEN
          NINP=NINP+1
        ENDIF
      ENDDO
      WRITE(6,*)'NUMBER OF INPUT FILES=',NINP

C     * FIND THE NUMBER OF LEVELS, TYPE OF DATA, ETC...

      CALL FILEV(LEV,NLEV,IBUF,11)
      IF (NLEV.EQ.0) THEN
         WRITE(6,6010)
         CALL                                      XIT('TIMMAX',-2)
      ENDIF
      IF(NLEV.GT.MAXL) CALL                        XIT('TIMMAX',-3)
      NWDS=IBUF(5)*IBUF(6)
      KIND=IBUF(1)
      IF (KIND.EQ.NC4TO8("TIME")) CALL             XIT('TIMMAX',-4)
      NAME=IBUF(3)
      NPACK=MIN(2,IBUF(8))
      SPEC=(KIND.EQ.NC4TO8("FOUR").OR.KIND.EQ.NC4TO8("SPEC"))
      IF (SPEC) NWDS=NWDS*2
      IF (NWDS.GT.MAXRSZ.OR.NWDS*NLEV.GT.MAXG)
     +     CALL                                    XIT('TIMMAX',-5)
      WRITE(6,6020) NAME,NLEV,(LEV(L),L=1,NLEV)
      DO I=1,8
        JBUF(I)=IBUF(I)
      ENDDO
      CALL PRTLAB (IBUF)
C
C     * PROCESS ALL INPUT FILES.
C
      NSETS=0
      DO NF=1,NINP
        NSETSNF=0
C
C       * TIMESTEP AND LEVEL LOOPS.
C
 200    CONTINUE
        DO L=1,NLEV
          IW=(L-1)*NWDS
C
C         * GET THE NEXT FIELD FROM FILE 11 AND CHECK THE LABEL.
C
          CALL GETFLD2(NF+10,X,-1,-1,-1,LEV(L),IBUF,MAXX,OK)
          IF (.NOT.OK) THEN
            IF (L.EQ.1) GO TO 300
C
C           * (MULTI-LEVEL) SET IN FILE NF IS NOT COMPLETE. ABORT.
C
            CALL PRTLAB (IBUF)
            WRITE(6,6030) NAME,LEV(L)
            CALL                                   XIT('TIMMAX',-6)
          ENDIF
C
C         * MAKE SURE THAT ALL RECORDS HAVE THE SAME NAME, KIND AND DIMENSIONS
C
          CALL CMPLBL(0,IBUF,0,JBUF,OK)
          IF (.NOT.OK .OR. IBUF(3).NE.JBUF(3)) THEN
            CALL PRTLAB (IBUF)
            CALL PRTLAB (JBUF)
            CALL                                   XIT('TIMMAX',-7)
          ENDIF
          NPACK=MIN(NPACK,IBUF(8))
          IF(NSETS.EQ.0)THEN
            DO I=1,NWDS
              Y(IW+I)=X(I)
            ENDDO
          ELSE
            IF (SPEC) THEN
              DO I=1,NWDS,2
                IF (ABS(CMPLX(X(I)   ,X(I+1) )).GT.
     +              ABS(CMPLX(Y(I+IW),Y(I+1+IW)))) THEN
                  Y(I  +IW)=X(I)
                  Y(I+1+IW)=X(I+1)
                ENDIF
              ENDDO
            ELSE
              DO I=1,NWDS
                IF (X(I).GT.Y(I+IW)) Y(I+IW)=X(I)
              ENDDO
            ENDIF
          ENDIF
        ENDDO                   ! L=1,NLEV
        NSETS=NSETS+1
        NSETSNF=NSETSNF+1
        GO TO 200
 300    CONTINUE
        WRITE(6,6040) NSETSNF,NAME,NF
        IF (NSETSNF.EQ.0) THEN
          WRITE(6,6050)
          CALL                                     XIT('TIMMAX',-8)
        ENDIF
      ENDDO                     ! NF=11,NINP+10
C
C     * SAVE THE MAXIMUM VALUES
C
      DO L=1,NLEV
        IW=(L-1)*NWDS
        IBUF(2)=NSETS
        IBUF(4)=LEV(L)
        IBUF(8)=NPACK
C
C       * PUT THE TIME MAXIMUM TO FILE 2 (PACKED AT HIGHEST 2:1).
C
        CALL PUTFLD2(2,Y(IW+1),IBUF,MAXX)
      ENDDO
C
C     * NORMAL EXIT.
C
      WRITE(6,6060) NSETS,NAME
      IF(SPEC) WRITE(6,6070)
      CALL                                         XIT('TIMMAX',0)

C---------------------------------------------------------------------
 6010 FORMAT('0..TIMMAX INPUT FILE IS EMPTY')
 6020 FORMAT('0NAME =',A4/'0NLEVS =',I5/
     1       '0LEVELS = ',15I6/100(10X,15I6/))
 6030 FORMAT('0..TIMMAX INPUT ERROR - NAME,L=',2X,A4,I5)
 6040 FORMAT('0TIMMAX PROCESSED ',I10,' SETS OF ',A4,' IN FILE ',I5)
 6050 FORMAT('0..TIMMAX TIME SERIES NAMES INCORRECT OR EMPTY FILE ',I5)
 6060 FORMAT('0TIMMAX PROCESSED ',I10,' SETS OF ',A4,' IN TOTAL')
 6070 FORMAT(/,' FOR COMPLEX FIELDS, THE OUTPUT CONTAINS ',
     1     'THE CORRESPONDING REAL',/,' AND IMAGINARY PARTS ',
     2     'OF THE COMPLEX NUMBER WITH THE LARGEST ',/,
     3     ' AMPLITUDE FOR THE PERIOD.')
      END
