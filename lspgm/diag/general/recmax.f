      PROGRAM RECMAX
C     PROGRAM RECMAX (X,      Y,      INPUT,      OUTPUT,               )       C2
C    1          TAPE1=X,TAPE2=Y,TAPE5=INPUT,TAPE6=OUTPUT)
C     ------------------------------------------------                          C2
C                                                                               C2
C     SEP 23/02 - S. KHARIN                                                     C2
C                                                                               C2
CRECMAX  - FIND MAXIMA OVER GIVEN NUMBER OF RECORDS.                    1  1C   C1
C                                                                               C3
CAUTHOR  - S. KHARIN                                                            C3
C                                                                               C3
CPURPOSE - FIND MAXIMA OVER GIVEN NUMBER OF RECORDS.                            C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      X = INPUT DATA IN CCRN FORMAT                                            C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      Z = OUTPUT FILE WITH MAXIMA.                                             C3
C                                                                               C3
CCARD READ...                                                                   C3
C                                                                               C3
C     READ(5,'(10X,I10)',END=900) NREC                                          C3
C                                                                               C3
CINPUT PARAMETERS...                                                            C3
C                                                                               C3
C     NREC = CHUNK LENGTH.                                                      C3
C          = 0, FIND MAXIMA OVER ALL INPUT RECORDS.                             C3
C          = NEGATIVE, DO NOT SAVE THE LAST INCOMPLETE CHUNK.                   C3
C                                                                               C5
CEXAMPLE OF INPUT CARD...                                                       C5
C                                                                               C5
C*RECMAX    4                                                                   C5
C---------------------------------------------------------------------------
C

      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      REAL  A(SIZES_BLONP1xBLAT), B(SIZES_BLONP1xBLAT)
      INTEGER NREC(700)
      LOGICAL OK,COMPLETE
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/,COMPLETE/.FALSE./,
     & SPVAL/1.E+38/,SPVALT/1.E+32/ 

C---------------------------------------------------------------------
      NF = 4
      CALL JCLPNT(NF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * READ INPUT PARAMETERS FROM CARD
C
      READ(5,5010,END=900) (NREC(I),I=1,7)                                      C4
      I0=7
      DO K=1,100
        READ(5,5010,END=901) (NREC(I),I=I0+1,I0+7)
        I0=I0+7
      ENDDO
 901  CONTINUE
      WRITE(6,*) 'READ IN', I0,' INPUT PARAMETERS.'
      DO IREC=1,I0
        IF(NREC(IREC).EQ.0)GOTO 902
      ENDDO
 902  IREC=IREC-1
      WRITE(6,*) 'USING', IREC,' NONZERO INPUT PARAMETERS.'
      IF (NREC(1).LT.0) THEN
        COMPLETE=.TRUE.
        DO I=1,IREC
          NREC(I)=ABS(NREC(I))
        ENDDO
      ENDIF
      WRITE(6,*)' CHUNK LENGTH(S) =',(NREC(I),I=1,IREC)
      WRITE(6,*)' SAVE ONLY COMPLETE BINS =',COMPLETE
      IF (IREC.GT.1)THEN
        LREC=0
        DO I=1,IREC
          LREC=LREC+NREC(I)
        ENDDO
        WRITE(6,*)' SUM OF ALL CHUNKS =',LREC
      ENDIF

      NIN=0
      NOUT=0
      IR=0
 100  CONTINUE
C
C     * LOOP OVER ALL RECORDS OF X.
C
      CALL GETFLD2(1,B,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
        IF(NIN.EQ.0) CALL                          XIT('RECMAX',-1)
        CALL PRTLAB(IBUF) 
        WRITE(6,6010) NIN,NOUT
        CALL                                       XIT('RECMAX',0)
      ENDIF
      NIN=NIN+1
      
      IF (NIN.EQ.1) THEN
        CALL PRTLAB(IBUF) 
        NLON=IBUF(5)
        NLAT=IBUF(6)
        NWDS=NLON*NLAT
        IF (KIND.EQ.4HSPEC.OR.KIND.EQ.4HFOUR)CALL  XIT('RECMAX',-2)
      ENDIF
      DO I=1,NWDS
        IF(ABS(B(I)-SPVAL).LT.SPVALT)THEN
          B(I)=SPVAL
        ENDIF
      ENDDO
C
C     * FIND MAX OF NREC RECORDS
C     
      IR=IR+1
      IF(IR.GT.IREC)IR=1
      NR=1
 110  CONTINUE
      CALL GETFLD2(1,A,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
        IF (NREC(IR).NE.0) THEN
          WRITE(6,*)'***WARNING: LAST CHUNK IS INCOMPLETE: NR=',NR
          IF (COMPLETE) WRITE(6,*)'***WARNING: SKIP IT.'
        ENDIF
C        IBUF(3)=4H MAX
        IF(.NOT.COMPLETE) THEN
          CALL PUTFLD2(2,B,IBUF,MAXX)
          NOUT=NOUT+1
        ENDIF
        CALL PRTLAB(IBUF) 
        WRITE(6,6010) NIN,NOUT
        CALL                                       XIT('RECMAX',0)
      ENDIF
      NIN=NIN+1
      NR=NR+1
      DO I=1,NWDS
        IF (ABS(A(I)-SPVAL).GT.SPVALT)THEN
          IF (ABS(B(I)-SPVAL).LT.SPVALT.OR.A(I).GT.B(I))B(I)=A(I)
        ENDIF
      ENDDO
      IF (NR.LT.NREC(IR)) GOTO 110
C
C     * SAVE 
C
C      IBUF(3)=4H MAX
      CALL PUTFLD2(2,B,IBUF,MAXX)
      NOUT=NOUT+1
      
      GOTO 100
 900  CALL                                         XIT('RECMAX',-1)

C---------------------------------------------------------------------
 5010 FORMAT(10X,8I10)
 6010 FORMAT('0',I10,' RECORD(S) PROCESSED.'/
     1       '0',I10,' RECORD(S) WRITTEN OUT.')
      END
