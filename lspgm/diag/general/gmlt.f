      PROGRAM GMLT
C     PROGRAM GMLT (X,       Y,       Z,       OUTPUT,                  )       C2
C    1        TAPE1=X, TAPE2=Y, TAPE3=Z, TAPE6=OUTPUT)
C     ------------------------------------------------                          C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     JUL 22/86 - B.DUGAS                                                       
C                                                                               C2
CGMLT    - MULTIPLIES FIRST FILE BY FIRST RECORD OF SECOND FILE         2  1    C1
C                                                                               C3
CAUTHOR  - B.DUGAS                                                              C3
C                                                                               C3
CPURPOSE - FILE  ARITHMATIC  PROGRAM  THAT  MULTIPLIES  THE  FIRST INPUT        C3
C          FILE (X)  BY  THE FIRST RECORD  OF  THE SECOND INPUT FILE (Y)        C3
C          AND PUTS THE RESULTS ON FILE Z. (Z = X * (FIRST RECORD OF) Y)        C3
C          NOTE - NO TYPE CHECKING IS DONE EXCEPT FOR THE REQUIREMENT           C3
C                 THAT THE TWO FILES HAVE SAME TYPE OF RECORDS.                 C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      X = ANY DATA FILE WITH CCRN LABEL                                        C3
C      Y = ANY DATA FILE WITH CCRN LABEL                                        C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      Z = OUTPUT FILE WHOSE CONTENT IS: X * (FIRST REC. OF) Y                  C3
C---------------------------------------------------------------------------
C 
  
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/ A(SIZES_LONP1xLAT), B(SIZES_LONP1xLAT) 
      CHARACTER*4 CTYP
      INTEGER TYPE
      LOGICAL OK
  
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
  
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
  
C---------------------------------------------------------------------
      NF = 4
      CALL JCLPNT(NF,1,2,3,6) 
      REWIND 1
      REWIND 2
      REWIND 3
  
C     * READ THE FIRST RECORD OF Y. 
  
      CALL GETFLD2(2,B,-1,-1,-1,-1,IBUF,MAXX,OK) 
  
      IF (.NOT.OK) CALL                            XIT('GMLT',-1) 
  
      TYPE = IBUF(1)
      ILG  = IBUF(5)
      ILAT = IBUF(6)
      NWDS = ILG*ILAT 
      WRITE(CTYP,0001) TYPE 
      IF (CTYP.EQ.'SPEC' .OR. CTYP.EQ.'FOUR') 
     1    NWDS = NWDS*2 
  
C     * READ THE NEXT FIELD.
  
      NR = 0
  100 CALL GETFLD2(1,A,TYPE,-1,-1,-1,IBUF,MAXX,OK) 
  
          IF (.NOT.OK)                                         THEN 
              IF(NR.EQ.0) CALL                     XIT('GMLT',-2) 
              WRITE(6,6010) NR
              CALL                                 XIT('GMLT',0)
          END IF
  
          IF (NR.EQ.0) WRITE(6,6025) IBUF 
  
C         * MAKE SURE THAT A AND B ARE THE SAME SIZE. 
  
          IF (IBUF(5).NE.ILG .OR. IBUF(6).NE.ILAT)             THEN 
              WRITE(6,6025) IBUF
              CALL                                 XIT('GMLT',-3) 
          END IF
  
C         * MULTIPLY THE FIELDS.
  
          DO 200 I=1,NWDS 
              A(I) = A(I)*B(I)
  200     CONTINUE
  
C         * SAVE THE RESULT ON FILE C.
  
          CALL PUTFLD2(3,A,IBUF,MAXX)
          IF (NR.EQ.0) WRITE(6,6025) IBUF 
  
      NR = NR+1 
      GOTO 100
  
C---------------------------------------------------------------------
 0001 FORMAT(A4)
 6010 FORMAT('0',I6,' RECORDS PROCESSED.')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
