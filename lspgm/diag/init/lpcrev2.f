      PROGRAM LPCREV2
C     PROGRAM LPCREV2 (GLC,        FCAN,          LNZ0,         AVW,            I2
C    1                 AIW,        LAIMAX,        LAIMIN,       CMASS,          I2
C    2                             ROOT,          GC,           LICN,           I2
C    3                                                          OUTPUT, )       I2
C    4          TAPE12=GLC, TAPE13=FCAN,   TAPE14=LNZ0,  TAPE15=AVW,
C    5          TAPE16=AIW, TAPE17=LAIMAX, TAPE18=LAIMIN,TAPE19=CMASS,
C    6                      TAPE20=ROOT,   TAPE21=GC,    TAPE22=LICN,
C    7                                                    TAPE6=OUTPUT)
C     ----------------------------------------------------------------          I2
C                                                                               I2
C     SEP 08/2014 - M.LAZARE. LIKE LPCRREV BUT:                                 I2
C                             - NORMALIZATION AT BEGINNING CORRECTED.           I2
C                             - "FUDGING" AROUND ANTARCTIC NOW REMOVED          I2
C                               SINCE ED NOW HAS IT REPRESENTED PROPERLY        I2
C                               FOR WATER BODY (CLASS #20) AND GLACIER          I2
C                               (CLASS #21).                                    I2
C                             - NEW GLC2000 LOOKUP TABLES.                      I2
C                             - INPUT FILE IS NOW 4320X2160, SO PARAMETER       I2
C                               STATEMENT CHANGED.                              I2
C                             - "GC" IS NOW READ IN INSTEAD OF CALCULATED.      I2
C                             - LICN NOW OUTPUT ALSO.                           I2
C                             - "PCNT" -> "FRAC" SINCE ARE REALLY FRACTIONS.    I2
C                                                                               I2
CLPCREV2 - PRODUCES FIELDS FOR CLASS FROM GLC2000 INPUT DATA            1 11    I1
C                                                                               I3
CAUTHORS - M.LAZARE                                                             I3
C                                                                               I3
CPURPOSE - FROM GLC2000 INPUT, USING LOOK-UP TABLES, GENERATE VARIOUS           I3
C          FIELDS FOR CLASS.                                                    I3
C                                                                               I3
CINPUT FILE ...                                                                 I3
C                                                                               I3
C      GLC   = GLC DATA                                                         I3
C                                                                               I3
COUTPUT FILES...                                                                I3
C                                                                               I3
C      FCAN    = FRACTION OF CANOPY IN EACH OF 5 CLASS TYPES                    I3
C      LNZ0    = LN OF SURFACE ROUGHNESS                                        I3
C      AVW     = VISIBLE ALBEDO                                                 I3
C      AIW     = NEAR-IR ALBEDO                                                 I3
C      LAIMAX  = MAXIMUM LEAF AREA INDEX                                        I3
C      LAIMIN  = MINIMUM LEAF AREA INDEX                                        I3
C      CMASS   = CANOPY MASS                                                    I3
C      ROOT    = ROOTING DEPTH                                                  I3
C      GC      = MASK                                                           I3
C      LICN    = FRACTION OF LAND ICE (GLACIER)                                 I3
C------------------------------------------------------------------------
C
      PARAMETER(LON=4320, LAT=2160, IGRID=LON*LAT, IGRIDP=IGRID,
     1          NCLASSV=18, NCLASST=22, ICAN=4, ICANP1=ICAN+1)

      real, dimension (:),   allocatable :: GC, FRAC, LICN, FRACSUM
      real, dimension (:,:), allocatable :: FCAN, LNZ0, AVW, AIW, 
     &                                      LAIMAX, LAIMIN, CMASS, ZROOT

C
      COMMON/ICOM/IBUF(8),IDAT(IGRIDP)
C
      LOGICAL OK
C
C     * LOOK-UP TABLE FOR DECIDING UPON VEGETATION TYPES.
C     * VEGETATION CLASSES ARE DEFINED AS FOLLOWS:
C     *      CLASS 1: TALL CONIFEROUS.
C     *      CLASS 2: TALL BROADLEAF.
C     *      CLASS 3: ARABLE AND CROPS.
C     *      CLASS 4: GRASS, SWAMP AND TUNDRA (I.E. OTHER).
C     *      CLASS 5: URBAN
C     *      CLASS 6: LAKE
C     *      CLASS 7: OCEAN
C     *      CLASS 8: BARE
C
      REAL FRACN  (5,NCLASSV)
      REAL HYDCNTL(8,NCLASST)
      DATA HYDCNTL /
     1      0.0,   1.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
     2      0.0,   1.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
     3      0.0,   0.6,   0.0,   0.2,   0.0,   0.1,   0.0,   0.1,   
     4      1.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,
     5      0.8,   0.0,   0.0,   0.1,   0.0,   0.0,   0.0,   0.1, 
     6      0.4,   0.5,   0.0,   0.1,   0.0,   0.0,   0.0,   0.0,
     7      0.0,   0.5,   0.0,   0.0,   0.0,   0.5,   0.0,   0.0, 
     8      0.0,   0.5,   0.0,   0.0,   0.0,   0.0,   0.5,   0.0,
     9      0.0,   0.6,   0.0,   0.2,   0.0,   0.0,   0.0,   0.2,        
     A      0.2,   0.2,   0.0,   0.3,   0.0,   0.0,   0.0,   0.3,
     B      0.0,   0.6,   0.0,   0.2,   0.0,   0.1,   0.0,   0.1,  
     C      0.0,   0.4,   0.0,   0.3,   0.0,   0.0,   0.0,   0.3,
     D      0.0,   0.0,   0.0,   0.7,   0.0,   0.0,   0.0,   0.3,     
     E      0.0,   0.1,   0.0,   0.1,   0.0,   0.0,   0.0,   0.8,
     F      0.0,   0.5,   0.0,   0.3,   0.0,   0.1,   0.0,   0.1,    
     G      0.0,   0.0,   0.5,   0.4,   0.0,   0.0,   0.0,   0.1,
     H      0.0,   0.2,   0.5,   0.2,   0.0,   0.0,   0.0,   0.1,   
     I      0.0,   0.1,   0.5,   0.3,   0.0,   0.0,   0.0,   0.1,
     J      0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0,
     K      0.0,   0.0,   0.0,   0.0,   0.0,   1.0,   0.0,   0.0,
     L      0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   0.0,   1.0,
     M      0.0,   0.0,   0.0,   0.0,   1.0,   0.0,   0.0,   0.0/
C
C     * LOOK-UP TABLES FOR VEGETATION CHARACTERISTICS.
C
      REAL ALBVIS(5,NCLASST)
      DATA ALBVIS /
     1      0.00,   3.00,   0.00,   0.00,   0.00,
     2      0.00,   5.00,   0.00,   0.00,   0.00,
     3      0.00,   5.00,   0.00,   6.00,   0.00,
     4      3.00,   0.00,   0.00,   0.00,   0.00,
     5      3.00,   0.00,   0.00,   3.00,   0.00,
     6      3.00,   5.00,   0.00,   6.00,   0.00,
     7      0.00,   3.00,   0.00,   0.00,   0.00,
     8      0.00,   3.00,   0.00,   0.00,   0.00,
     9      0.00,   3.00,   0.00,   3.00,   0.00,
     A      3.00,   3.00,   0.00,   3.00,   0.00,
     B      0.00,   3.00,   0.00,   6.00,   0.00,
     C      0.00,   5.00,   0.00,   6.00,   0.00,
     D      0.00,   0.00,   0.00,   6.00,   0.00,
     E      0.00,   5.00,   0.00,   5.00,   0.00,
     F      0.00,   3.00,   0.00,   3.00,   0.00,
     G      0.00,   0.00,   6.00,   6.00,   0.00,
     H      0.00,   5.00,   6.00,   6.00,   0.00,
     I      0.00,   5.00,   6.00,   6.00,   0.00,
     J      0.00,   0.00,   0.00,   0.00,   0.00,
     K      0.00,   0.00,   0.00,   0.00,   0.00,
     L      0.00,   0.00,   0.00,   0.00,   0.00,
     M      0.00,   0.00,   0.00,   0.00,   9.00/

      REAL ALBNIR(5,NCLASST)
      DATA ALBNIR /
     1      0.00,   23.0,   0.00,   0.00,   0.00,
     2      0.00,   29.0,   0.00,   0.00,   0.00,
     3      0.00,   29.0,   0.00,   34.0,   0.00,
     4      19.0,   0.00,   0.00,   0.00,   0.00,
     5      19.0,   0.00,   0.00,   19.0,   0.00,
     6      19.0,   29.0,   0.00,   34.0,   0.00,
     7      0.00,   23.0,   0.00,   0.00,   0.00,
     8      0.00,   23.0,   0.00,   0.00,   0.00,
     9      0.00,   19.0,   0.00,   19.0,   0.00,
     A      19.0,   19.0,   0.00,   19.0,   0.00,
     B      0.00,   19.0,   0.00,   34.0,   0.00,
     C      0.00,   29.0,   0.00,   34.0,   0.00,
     D      0.00,   0.00,   0.00,   34.0,   0.00,
     E      0.00,   29.0,   0.00,   29.0,   0.00,
     F      0.00,   19.0,   0.00,   19.0,   0.00,
     G      0.00,   0.00,   34.0,   34.0,   0.00,
     H      0.00,   29.0,   34.0,   34.0,   0.00,
     I      0.00,   29.0,   34.0,   34.0,   0.00,
     J      0.00,   0.00,   0.00,   0.00,   0.00,
     K      0.00,   0.00,   0.00,   0.00,   0.00,
     L      0.00,   0.00,   0.00,   0.00,   0.00,
     M      0.00,   0.00,   0.00,   0.00,   15.0/

      REAL ROUGH (5,NCLASST)
      DATA ROUGH /
     1      0.00,   3.50,   0.00,   0.00,   0.00,
     2      0.00,   2.00,   0.00,   0.00,   0.00,
     3      0.00,   0.80,   0.00,   0.02,   0.00,
     4      1.50,   0.00,   0.00,   0.00,   0.00,
     5      1.00,   0.00,   0.00,   0.02,   0.00,
     6      1.50,   2.00,   0.00,   0.02,   0.00,
     7      0.00,   3.50,   0.00,   0.00,   0.00,
     8      0.00,   3.50,   0.00,   0.00,   0.00,
     9      0.00,   0.30,   0.00,   0.02,   0.00,
     A      1.50,   0.30,   0.00,   0.02,   0.00,
     B      0.00,   0.05,   0.00,   0.02,   0.00,
     C      0.00,   0.05,   0.00,   0.02,   0.00,
     D      0.00,   0.00,   0.00,   0.02,   0.00,
     E      0.00,   0.15,   0.00,   0.02,   0.00,
     F      0.00,   0.30,   0.00,   0.02,   0.00,
     G      0.00,   0.00,   0.10,   0.02,   0.00,
     H      0.00,   1.50,   0.20,   0.25,   0.00,
     I      0.00,   1.50,   0.20,   0.25,   0.00,
     J      0.00,   0.00,   0.00,   0.00,   0.00,
     K      0.00,   0.00,   0.00,   0.00,   0.00,
     L      0.00,   0.00,   0.00,   0.00,   0.00,
     M      0.00,   0.00,   0.00,   0.00,   1.35/
C
      REAL LAMAX (4,NCLASSV)
      DATA LAMAX /
     1      0.00,   10.0,   0.00,   0.00,
     2      0.00,   6.00,   0.00,   0.00,
     3      0.00,   4.00,   0.00,   3.00,
     4      2.00,   0.00,   0.00,   0.00,
     5      2.00,   0.00,   0.00,   1.00,
     6      5.00,   6.00,   0.00,   3.00,
     7      0.00,   10.0,   0.00,   0.00,
     8      0.00,   10.0,   0.00,   0.00,
     9      0.00,   2.30,   0.00,   1.00,
     A      2.00,   2.30,   0.00,   1.00,
     B      0.00,   2.00,   0.00,   3.00,
     C      0.00,   2.00,   0.00,   1.00,
     D      0.00,   0.00,   0.00,   1.00,
     E      0.00,   1.00,   0.00,   1.00,
     F      0.00,   2.30,   0.00,   1.00,
     G      0.00,   0.00,   4.00,   3.00,
     H      0.00,   2.00,   1.00,   2.50,
     I      0.00,   2.00,   1.00,   1.00/
C
      REAL LAMIN (4,NCLASSV)
      DATA LAMIN /
     1      0.00,   10.0,   0.00,   0.00,
     2      0.00,   0.50,   0.00,   0.00,
     3      0.00,   0.50,   0.00,   3.00,
     4      1.60,   0.00,   0.00,   0.00,
     5      0.50,   0.00,   0.00,   1.00,
     6      4.00,   0.50,   0.00,   3.00,
     7      0.00,   10.0,   0.00,   0.00,
     8      0.00,   10.0,   0.00,   0.00,
     9      0.00,   1.60,   0.00,   1.00,
     A      1.60,   1.60,   0.00,   1.00,
     B      0.00,   2.00,   0.00,   3.00,
     C      0.00,   0.50,   0.00,   1.00,
     D      0.00,   0.00,   0.00,   1.00,
     E      0.00,   0.50,   0.00,   1.00,
     F      0.00,   1.60,   0.00,   1.00,
     G      0.00,   0.00,   0.00,   3.00,
     H      0.00,   0.50,   0.00,   2.50,
     I      0.00,   0.50,   0.00,   1.00/
C
      REAL CMASSX(4,NCLASSV)
      DATA CMASSX /
     1      0.00,   50.0,   0.00,   0.00,
     2      0.00,   20.0,   0.00,   0.00,
     3      0.00,   15.0,   0.00,   1.50,
     4      12.0,   0.00,   0.00,   0.00,
     5      15.0,   0.00,   0.00,   0.20,
     6      25.0,   20.0,   0.00,   1.50,
     7      0.00,   50.0,   0.00,   0.00,
     8      0.00,   50.0,   0.00,   0.00,
     9      0.00,   7.00,   0.00,   0.20,
     A      12.0,   7.00,   0.00,   0.20,
     B      0.00,   2.00,   0.00,   1.50,
     C      0.00,   2.00,   0.00,   1.00,
     D      0.00,   0.00,   0.00,   1.00,
     E      0.00,   2.00,   0.00,   0.20,
     F      0.00,   7.00,   0.00,   0.20,
     G      0.00,   0.00,   2.00,   1.50,
     H      0.00,   15.0,   2.00,   2.00,
     I      0.00,   15.0,   2.00,   1.00/
C
      REAL ZROOTX(4,NCLASSV)
      DATA ZROOTX /
     1      0.00,   5.00,   0.00,   0.00,
     2      0.00,   2.00,   0.00,   0.00,
     3      0.00,   5.00,   0.00,   1.20,
     4      1.00,   0.00,   0.00,   0.00,
     5      1.00,   0.00,   0.00,   0.20,
     6      1.00,   2.00,   0.00,   1.20,
     7      0.00,   5.00,   0.00,   0.00,
     8      0.00,   5.00,   0.00,   0.00,
     9      0.00,   0.30,   0.00,   0.20,
     A      1.00,   0.30,   0.00,   0.20,
     B      0.00,   0.20,   0.00,   1.20,
     C      0.00,   1.00,   0.00,   1.00,
     D      0.00,   0.00,   0.00,   1.00,
     E      0.00,   0.20,   0.00,   0.20,
     F      0.00,   0.30,   0.00,   0.20,
     G      0.00,   0.00,   1.20,   1.20,
     H      0.00,   1.00,   2.00,   1.00,
     I      0.00,   1.00,   2.00,   1.00/
C
      DATA MAXX/IGRIDP/



C=====================================================================
C     * HERE ARE THE GLC2000 CLASSES:
C
C  1   "Tree Cover, broadleaved, evergreen"
C  2   "Tree Cover, broadleaved, deciduous, closed"
C  3   "Tree Cover, broadleaved, deciduous, open"
C  4   "Tree Cover, needle-leaved, evergreen"
C  5   "Tree Cover, needle-leaved, deciduous"
C  6   "Tree Cover, mixed leaf type"
C  7   "Tree Cover, regularly flooded, fresh water"
C  8   "Tree Cover, regularly flooded, saline water"
C  9   "Mosaic: Tree Cover / Other natural vegetation"
C 10   "Tree Cover, burnt"
C 11   "Shrub Cover, closed-open, evergreen"
C 12   "Shrub Cover, closed-open, deciduous"
C 13   "Herbaceous Cover, closed-open"
C 14   "Sparse herbaceous or sparse shrub cover"
C 15   "Regularly flooded shrub and/or herbaceous cover"
C 16   "Cultivated and managed areas"
C 17   "Mosaic: Cropland / Tree Cover / Other natural vegetation"
C 18   "Mosaic: Cropland / Shrub and/or grass cover"
C--------------- End of Vegetative Classes (NCLASSV) ----------------
C 19   "Bare Areas"
C 20   "Water Bodies"
C 21   "Snow and Ice"
C 22   "Artificial surfaces and associated areas"
C====================================================================

      ! Allocate large arrays
      allocate(GC(IGRID), FRAC(IGRID), LICN(IGRID), FRACSUM(IGRID))
      allocate(   FCAN(IGRID,ICANP1),   LNZ0(IGRID,ICANP1), 
     &             AVW(IGRID,ICANP1),    AIW(IGRID,ICANP1),
     &          LAIMAX(IGRID,ICAN  ), LAIMIN(IGRID,ICAN  ),
     &           CMASS(IGRID,ICAN  ),  ZROOT(IGRID,ICAN  ))

      NLU=12
      CALL JCLPNT(NLU,12,13,14,15,16,17,18,19,20,21,22,6)
      DO I=12,22
       REWIND I
      ENDDO
C
C     * READ THE LAND-WATER MASK.
C
      CALL GETFLD2(12,GC  ,NC4TO8("GRID"),0,NC4TO8("  GC"),1,
     1             IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('LPCREV2',-1)
      WRITE(6,6025) IBUF
      NWDS=IBUF(5)*IBUF(6)
      REWIND 12
C
C     * INITIALIZE OUTPUT ARRAYS.
C
      DO 125 IC=1,ICAN
      DO 125 I=1,NWDS
        FCAN  (I,IC)=0.
        LNZ0  (I,IC)=0.
        AVW   (I,IC)=0.
        AIW   (I,IC)=0.
        LAIMAX(I,IC)=0.
        LAIMIN(I,IC)=0.
        CMASS (I,IC)=0.
        ZROOT (I,IC)=0.
  125 CONTINUE
C
      IC=ICANP1
      DO 130 I=1,NWDS
        FCAN  (I,IC)=0.
        AVW   (I,IC)=0.
        LNZ0  (I,IC)=0.
        AIW   (I,IC)=0.
  130 CONTINUE
C
      DO 135 I=1,NWDS
        FRACSUM(I)  =0.
  135 CONTINUE
C
C     * NORMALIZE FRACTION DATA TO REMOVE LAKE AND WATER FRACTIONS.
C
      DO N=1,NCLASSV
       FNORM     =1./(1.-HYDCNTL(6,N)-HYDCNTL(7,N))
       FRACN(1,N)=HYDCNTL(1,N)*FNORM
       FRACN(2,N)=HYDCNTL(2,N)*FNORM
       FRACN(3,N)=HYDCNTL(3,N)*FNORM
       FRACN(4,N)=HYDCNTL(4,N)*FNORM
       FRACN(5,N)=HYDCNTL(5,N)*FNORM
       FBARE     =HYDCNTL(8,N)*FNORM
       FSUM=FRACN(1,N)+FRACN(2,N)+FRACN(3,N)+FRACN(4,N)+FRACN(5,N)+FBARE
       IF(FSUM.GT.1.000001 .OR. FSUM.LE.0.999999) THEN
          WRITE(6,6150) N,FNORM,FSUM
          CALL                                     XIT('LPCREV2',-1)
       ENDIF
      ENDDO
C
      DO N=1,NCLASSV
C
C       * PRE-CALCULATION CHECK. 
C       * ABORT IF MIS-MATCH BETWEEN FRACTIONS AND VEGETATION
C       * CHARACTERISTICS (SHOULD BE BE ZERO OR BOTH NON-ZERO).
C
        DO IC=1,ICAN
          IF( (FRACN(IC,N).NE.0. .AND. ALBVIS(IC,N).EQ.0.) .OR.
     1        (FRACN(IC,N).EQ.0. .AND. ALBVIS(IC,N).NE.0.) ) THEN
            PRINT *, 'N,IC,FRACN,ALBVIS =',N,IC,FRACN(IC,N),ALBVIS(IC,N)
            CALL                                   XIT('LPCREV2',-2)
          ENDIF
C
          IF( (FRACN(IC,N).NE.0. .AND. ALBNIR(IC,N).EQ.0.) .OR.
     1        (FRACN(IC,N).EQ.0. .AND. ALBNIR(IC,N).NE.0.) ) THEN
            PRINT *, 'N,IC,FRACN,ALBNIR =',N,IC,FRACN(IC,N),ALBNIR(IC,N)
            CALL                                   XIT('LPCREV2',-3)
          ENDIF
C
          IF( (FRACN(IC,N).NE.0. .AND.  ROUGH(IC,N).EQ.0.) .OR.
     1        (FRACN(IC,N).EQ.0. .AND.  ROUGH(IC,N).NE.0.) ) THEN
            PRINT *, 'N,IC,FRACN,ROUGH  =',N,IC,FRACN(IC,N),ROUGH (IC,N)
            CALL                                   XIT('LPCREV2',-4)
          ENDIF
C
          IF( (FRACN(IC,N).NE.0. .AND. LAMAX(IC,N).EQ.0.) .OR.
     1        (FRACN(IC,N).EQ.0. .AND. LAMAX(IC,N).NE.0.) ) THEN
            PRINT *, 'N,IC,FRACN,LAMAX  =',N,IC,FRACN(IC,N),LAMAX (IC,N)
            CALL                                   XIT('LPCREV2',-5)
          ENDIF
C
          IF( (FRACN(IC,N).EQ.0. .AND. LAMIN(IC,N).NE.0.) ) THEN
            PRINT *, 'N,IC,FRACN,LAMIN  =',N,IC,FRACN(IC,N),LAMIN (IC,N)
            CALL                                   XIT('LPCREV2',-6)
          ENDIF
C
          IF( (FRACN(IC,N).NE.0. .AND. CMASSX(IC,N).EQ.0.) .OR.
     1        (FRACN(IC,N).EQ.0. .AND. CMASSX(IC,N).NE.0.) ) THEN
            PRINT *, 'N,IC,FRACN,CMASSX =',N,IC,FRACN(IC,N),CMASSX(IC,N)
            CALL                                   XIT('LPCREV2',-7)
          ENDIF
C
          IF( (FRACN(IC,N).NE.0. .AND. ZROOTX(IC,N).EQ.0.) .OR.
     1        (FRACN(IC,N).EQ.0. .AND. ZROOTX(IC,N).NE.0.) ) THEN
            PRINT *, 'N,IC,FRACN,ZROOTX =',N,IC,FRACN(IC,N),ZROOTX(IC,N)
            CALL                                   XIT('LPCREV2',-8)
          ENDIF
        ENDDO
      ENDDO
C
      REWIND 12
      DO 150 N=1,NCLASSV
        CALL GETFLD2(12,FRAC, -1 ,-1,-1,-1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('LPCREV2',-9-N)
        WRITE(6,6025) IBUF
C
        DO I=1,NWDS
         IF(GC(I).EQ.-1.)                                          THEN
          FRACSUM(I)=FRACSUM(I)+FRAC(I)
C
C         * LOOP OVER CANOPY TYPES AND DATA POINTS.
C
          DO IC=1,ICAN
           IF(FRACN(IC,N).GT.0.)               THEN
             RLOGZ0=LOG(ROUGH(IC,N))
           ELSE
             RLOGZ0=0.
           ENDIF  
C
           FCAN  (I,IC)=FCAN  (I,IC)+FRAC(I)*FRACN(IC,N)
           AVW   (I,IC)=AVW   (I,IC)+FRAC(I)*FRACN(IC,N)*ALBVIS(IC,N)
           AIW   (I,IC)=AIW   (I,IC)+FRAC(I)*FRACN(IC,N)*ALBNIR(IC,N)
           LNZ0  (I,IC)=LNZ0  (I,IC)+FRAC(I)*FRACN(IC,N)*RLOGZ0
           LAIMAX(I,IC)=LAIMAX(I,IC)+FRAC(I)*FRACN(IC,N)*LAMAX (IC,N)
           LAIMIN(I,IC)=LAIMIN(I,IC)+FRAC(I)*FRACN(IC,N)*LAMIN (IC,N)
           CMASS (I,IC)=CMASS (I,IC)+FRAC(I)*FRACN(IC,N)*CMASSX(IC,N)
           ZROOT (I,IC)=ZROOT (I,IC)+FRAC(I)*FRACN(IC,N)*ZROOTX(IC,N)
           FCAN  (I,IC)=MIN(FCAN(I,IC),1.)
          ENDDO
         ELSE
C
C         * DEFAULT VALUE OF LNZ0 OVER OPEN WATER.
C
          DO IC=1,ICAN
           LNZ0  (I,IC)=-8.11
          ENDDO
         ENDIF
        ENDDO
  150 CONTINUE    
C
      DEGHY=180./(REAL(LAT))
      DEGHYST=DEGHY*0.5E0
C
      DO 170 N=NCLASSV+1,NCLASST
        CALL GETFLD2(12,FRAC, -1 ,-1,-1,-1,IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('LPCREV2',-30-N)
        WRITE(6,6025) IBUF
C
        DO 160 I=1,NWDS
C
C         * STORE GLACIER FRACTION FOR EVENTUAL OUTPUT.
C
          IF(N.EQ.21)                                            THEN
            LICN(I)=FRAC(I)
          ENDIF
C
C         * STORE URBAN AREAL PERCENTAGE,VISIBLE AND NEAR-IR
C         * ALBEDOES AND LN(ROUGHNESS) LOOK-UP TABLE VALUES IN
C         * "ICANP1" COLUMN OF CORRESPONDING ARRAYS.
C
          IF(N.EQ.22)                                            THEN
            IF(GC(I).EQ.-1.)                                  THEN          
              RLOGZ0         =LOG(ROUGH(ICANP1,N))
              FCAN (I,ICANP1)=FRAC(I)
              AVW  (I,ICANP1)=FRAC(I)*ALBVIS(ICANP1,N)
              AIW  (I,ICANP1)=FRAC(I)*ALBNIR(ICANP1,N)
              LNZ0 (I,ICANP1)=FRAC(I)*RLOGZ0
            ELSE
C
C             * DEFAULT VALUE OF LNZ0 OVER OPEN WATER.
C
              LNZ0  (I,ICANP1)=-8.11
            ENDIF
          ENDIF

          FRACSUM(I)=FRACSUM(I)+FRAC(I)
  160   CONTINUE
  170 CONTINUE
C
      DO 175 I=1,NWDS
       IF(GC(I).EQ.-1.)                                          THEN
        IF(FRACSUM(I).GT.1.000001 .OR. FRACSUM(I).LE.0.999999) THEN
          WRITE(6,6175) I,GC(I),FRACSUM(I)
          CALL                                     XIT('LPCREV2',-50)
        ENDIF
       ENDIF
  175 CONTINUE
C
C     * SAVE THE RESULTS.
C
      IBUF(2)=0
C
      DO 800 IC=1,ICANP1
        IBUF(4)=IC
C
        IBUF(3)=NC4TO8("FCAN")
        CALL PUTFLD2(13,FCAN  (1,IC),IBUF,MAXX)
        WRITE(6,6025) IBUF
C
        IBUF(3)=NC4TO8("LNZ0")
        CALL PUTFLD2(14,LNZ0  (1,IC),IBUF,MAXX)
        WRITE(6,6025) IBUF
C
        IBUF(3)=NC4TO8(" AVW")
        CALL PUTFLD2(15,AVW   (1,IC),IBUF,MAXX)
        WRITE(6,6025) IBUF
C
        IBUF(3)=NC4TO8(" AIW")
        CALL PUTFLD2(16,AIW   (1,IC),IBUF,MAXX)
        WRITE(6,6025) IBUF
C
C       * NO URBAN CLASS FIELDS FOR ENSUING FIELDS.
C
        IF(IC.EQ.ICANP1) GO TO 800
C
        IBUF(3)=NC4TO8("LAMX")
        CALL PUTFLD2(17,LAIMAX(1,IC),IBUF,MAXX)
        WRITE(6,6025) IBUF
C
        IBUF(3)=NC4TO8("LAMN")
        CALL PUTFLD2(18,LAIMIN(1,IC),IBUF,MAXX)
        WRITE(6,6025) IBUF
C
        IBUF(3)=NC4TO8("CMAS")
        CALL PUTFLD2(19,CMASS (1,IC),IBUF,MAXX)
        WRITE(6,6025) IBUF
C
        IBUF(3)=NC4TO8("ROOT")
        CALL PUTFLD2(20,ZROOT (1,IC),IBUF,MAXX)
        WRITE(6,6025) IBUF
  800 CONTINUE
C
      IBUF(3)=NC4TO8("  GC")
      IBUF(4)=1
      CALL PUTFLD2(21,GC,IBUF,MAXX)
      WRITE(6,6025) IBUF
C
      IBUF(3)=NC4TO8("LICN")
      IBUF(4)=1
      CALL PUTFLD2(22,LICN,IBUF,MAXX)
      WRITE(6,6025) IBUF

      CALL                                         XIT('LPCREV2',0)
C---------------------------------------------------------------------
 6025 FORMAT(' ',A4,I10,2X,A4,5I10)
 6150 FORMAT('0INITIAL: N,FNORM,FRACSUM ',I5,2F12.5)
 6175 FORMAT('0INITIAL: I,GC,FRACSUM ',I5,2F12.5)
      END
