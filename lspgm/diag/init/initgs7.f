      PROGRAM INITGS7
C     PROGRAM INITGS7 (ICTL,       GPINIT,       GSINIT,       OUTPUT,  )       I2
C    1           TAPE1=ICTL, TAPE2=GPINIT, TAPE3=GSINIT, TAPE6=OUTPUT)
C     ----------------------------------------------------------------          I2
C                                                                               I2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       I2
C     JAN 19/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL AND CALL SIGLAB2)     
C     JAN 29/92 - E. CHAN  (PREVIOUS ROUTINE INITGSH)                         
C                                                                               I2
CINITGS7 - INTERPOLATES SIGMA LEVEL GRIDS FROM PRESS. LEVELS FOR GCM7   2  1    I1
C                                                                               I3
CAUTHOR  - M.LAZARE                                                             I3
C                                                                               I3
CPURPOSE - CONVERTS A PRESSURE LEVEL GAUSSIAN GRID DATASET OF INITIAL           I3
C          MODEL VARIABLES (SFPR,PHI,TEMP,U,V,Q) TO A SIGMA/HYBRID LEVEL        I3
C          DATASET (LNSP,TEMP,U,V,ES) SUITABLE FOR SPECTRAL ANALYSIS.           I3
C          NOTE - IT CALLS PTSH WHICH CONVERTS ES=T-TD TO MODEL MOISTURE        I3
C                 VARIABLE 'MOIST' WHICH CONTROLS THE CHOICE OF MOISTURE        I3
C                 VARIABLE... 4HT-TD/4H  TD/4H   Q/4HSQRT/4HRLNQ  FOR           I3
C                 DEW POINT DEPRESSION/DEW POINT/SPEC.HUM./Q**0.5/-LN(Q)**-1    I3
C                                                                               I3
CINPUT FILES...                                                                 I3
C                                                                               I3
C      ICTL   = INITIALIZATION CONTROL DATASET (SEE ICNTRLH).                   I3
C      GPINIT = PRESSURE LEVEL GAUSSIAN GRIDS. ALL EXCEPT PMSL MUST             I3
C               BE AVAILABLE ON THE SAME PRESSURE LEVELS. NOTE THAT             I3
C               U,V MUST BE SCALED (MODEL) WIND COMPONENTS.                     I3
C               (I.E. (U,V)*COS(LAT)/A, WHERE A = EARTH RADIUS).                I3
C                                                                               I3
C                 NAME   VARIABLE          UNITS                                I3
C                                                                               I3
C                 PMSL   SURFACE PRESSURE  MB                                   I3
C                  PHI   GEOPOTENTIAL      (M/SEC)**2                           I3
C                 TEMP   TEMPERATURE       DEG K                                I3
C                    U   WIND COMPONENT    1./SEC                               I3
C                    V   WIND COMPONENT    1./SEC                               I3
C                   ES   DEW-POINT DEP.    DEG K                                I3
C                                                                               I3
COUTPUT FILE...                                                                 I3
C                                                                               I3
C      GSINIT = SIGMA/HYBRID LEVEL GAUSSIAN GRIDS OF...                         I3
C                                                                               I3
C                 NAME   VARIABLE          UNITS     LEVELS                     I3
C                                                                               I3
C                 LNSP   LN(SF.PRES.)      PA           1   SFC                 I3
C                 TEMP   TEMPERATURE       DEG K     ILEV   SH                  I3
C                    U   WIND COMPONENT   1./SEC     ILEV   SG                  I3
C                    V   WIND COMPONENT   1./SEC     ILEV   SG                  I3
C                   ES   MOISTURE VARIABLE           ILEV   SH                  I3
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVxLONP1xLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMMON/ICOM/ IBUF(8), IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/COM1/ U (SIZES_MAXLEVxLONP1xLAT), GZS (SIZES_LONP1xLAT)
      COMMON/COM2/ V (SIZES_MAXLEVxLONP1xLAT), SP  (SIZES_LONP1xLAT)
      REAL         T (SIZES_MAXLEVxLONP1xLAT), 
     &             ES(SIZES_MAXLEVxLONP1xLAT)
      EQUIVALENCE  (U(1),T(1)), (V(1),ES(1))
  
      LOGICAL OK
      CHARACTER*4 STRING
  
      INTEGER KBUF(8), LP(SIZES_MAXLEV), LG(SIZES_MAXLEV), 
     & LH(SIZES_MAXLEV)
  
      REAL AG(SIZES_MAXLEV), BG(SIZES_MAXLEV), AH(SIZES_MAXLEV), 
     & BH(SIZES_MAXLEV), SG(SIZES_MAXLEV)
      REAL SH(SIZES_MAXLEV), PR(SIZES_MAXLEV), SIG(SIZES_MAXLEV), 
     & PRLOG(SIZES_MAXLEV) 
      REAL FPCOL(SIZES_MAXLEV), DFDLNP(SIZES_MAXLEV+1), 
     & DLNP(SIZES_MAXLEV) 
  
      DATA IJ/SIZES_LONP1xLATxNWORDIO/, 
     & IJK/SIZES_MAXLEVxLONP1xLAT/, 
     & MAXLEV/SIZES_MAXLEV/
C-----------------------------------------------------------------------
      NFIL=4
      CALL JCLPNT(NFIL,1,2,3,6)

C     * GET THE PRESSURE LEVEL VALUES AND THE ANALYSIS GRID SIZE
C     * FROM THE TEMPERATURE FIELDS IN FILE GPINIT.

      REWIND 2
      CALL FIND(2,NC4TO8("GRID"),-1,NC4TO8("TEMP"),-1,OK)
      IF(.NOT.OK) CALL                             XIT('INITGS7',-1)
      CALL FILEV(LP ,NPL,KBUF,-2)
      IF((NPL.LT.1).OR.(NPL.GT.MAXLEV)) CALL       XIT('INITGS7',-2)
      CALL LVDCODE(PR,LP,NPL)
      ILG =KBUF(5)-1
      ILAT=KBUF(6)
      WRITE(6,6005) NPL,(PR(L),L=1,NPL)

C     * READ MODEL SIGMA VALUES FROM THE CONTROL FILE.

      REWIND 1
      READ(1,END=903) LABL,NSL,(SG(L),L=1,NSL)
     1                        ,(SH(L),L=1,NSL),LAY,ICOORD,PTOIT,MOIST
      IF((NSL.LT.1).OR.(NSL.GT.MAXLEV)) CALL       XIT('INITGS7',-3)
      CALL WRITLEV(SG,NSL,' SG ')
      CALL WRITLEV(SH,NSL,' SH ')
      WRITE(6,6016) LAY,ICOORD,PTOIT,MOIST
C
      CALL SIGLAB2 (LG,LH, SG,SH, NSL)
      CALL COORDAB (AG,BG, NSL,SG, ICOORD,PTOIT)
      CALL COORDAB (AH,BH, NSL,SH, ICOORD,PTOIT)

C     * STOP IF THERE IS NOT ENOUGH SPACE.

      LEN = ILAT*(ILG+1)
      IF(    LEN.GT.IJ ) CALL                      XIT('INITGS7',-4)
      IF(NPL*LEN.GT.IJK) CALL                      XIT('INITGS7',-5)
      IF(NSL*LEN.GT.IJK) CALL                      XIT('INITGS7',-6)

C     * PERFORM THE VERTICAL INTERPOLATION, WITH CONVERSION OF ES.

      CALL PTSH   (1,2,3,T,U,V,ES,GZS,SP,LEN,NPL,PR,NSL,
     1             AG,BG,AH,BH,NPL+1,FPCOL,DFDLNP ,DLNP ,
     2             LP,LG,LH,SIG,PRLOG, IBUF,IJ+8,MOIST)

      CALL                                         XIT('INITGS7',0)

C     * E.O.F. ON FILE ICTL.

  903 CALL                                         XIT('INITGS7',-7)
C-----------------------------------------------------------------------
 6005 FORMAT('0  PRES LEVELS',I5,20F5.0/('0',18X,20F5.0))
 6010 FORMAT(I6,A4,/,(10X,10F6.3))
 6016 FORMAT(' LAY=',I5,', COORD=',A4,', P.LID=',F10.3,' (PA)',
     1       ', MOIST=',A4)
      END
