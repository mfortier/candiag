      PROGRAM GCMPARM                                                                                                               
C     PROGRAM GCMPARM (PARAMS,       INPUT,       OUTPUT,               )       I2                                                  
C    1           TAPE1=PARAMS, TAPE5=INPUT, TAPE6=OUTPUT)                                                                           
C     ---------------------------------------------------                       I2                                                  
C
C     FEB 20/18 - M. LAZARE.    - UPWARDLY-COMPATIBLE UPGRADE TO SUPPORT        I2
C                                 IOZTYP=6 (CMIP6).                             I2
C     MAY 13/11 - M. LAZARE.    - UPWARDLY-COMPATIBLE UPGRADE TO SUPPORT   
C                                 IOZTYP=5 (RANDEL/CMAM DATASET WITH       
C                                 LEVOZ=45)                                
C     OCT 14/09 - M. LAZARE.    - UPWARDLY-COMPATIBLE UPGRADE TO SUPPORT                                                          
C                                 IOZTYP=4 (DAVE RANDEL DATASET WITH                                                              
C                                 LEVOZ=24)                                                                                       
C     FEB 08/07 - M. LAZARE.    - UPWARDLY-COMPATIBLE UPGRADE TO SUPPORT                                                            
C                                 IOZTYP=3 (F&K/HALOE FROM RPN WITH                                                                 
C                                 LEVOZ=28)                                                                                         
C     DEC 11/03 - L. SOLHEIM    - LMT NOW READ IN INSTEAD ON LRLMT                                                                  
C                               - RENAMED NNODE TO NNODE_A                                                                          
C                               - REORDERED INPUT PARAMETER LIST AND CHANGED                                                        
C                                 I5 FORMAT TO I6                                                                                   
C                               - MOVED CALCULATION OF LATOTAL TO PARMS MODULE                                                      
C     DEC 10/03 - M. LAZARE.    - ADD SUPPORT FOR ADVECTED/NON-ADVECTED                                                             
C                                 TRACER FORMULATION (NTRAC,ITRACA REPLACE                                                          
C                                 ITRAC AND NTRACN CALCULATED).                                                                     
C                               - MOVE OUT NINTSW TO PARMS13B DECK (HARD-                                                           
C                                 CODED) SO THAT THIS PROGRAM IS NOW GENERIC.                                                       
C     NOV 26/03 - L. SOLHEIM    - MODIFIED TO REFLECT USE OF PARMS MODULE                                                           
C                                 WHICH CONSOLODATES ALL PARMSUB PARAMETERS                                                         
C                                 INTO ONE PLACE AND CALCULATES SOME OF THESE                                                       
C                                 PARAMETERS THERE.                                                                                 
C     NOV 06/03 - M. LAZARE.    - ADD CALCULATION OF LA,LM,LATOTAL,                                                                 
C                                 LMTOTAL VALUES FOR MPI OVER M-SPACE, AND                                                          
C                                 REMOVE "KM1" (REPLACED BY LM+1) IN COM13B.                                                        
C                                 ALSO "IRAM" NOW LATOTAL+LMTOTAL TO WORK                                                           
C                                 WITH TRANSFORMS DONE OVER ALL TRIANGLE.                                                           
C     OCT 29/03 - M. LAZARE.    - CHANGE/SIMPLIFY TRANSFORM SIZES IN                                                                
C                                 ACCORDANCE WITH NEW MODEL RESTRUCTURING.                                                          
C                                 THIS INCLUDES THE ADDITION OF "ILH" AND                                                           
C                                 "ILHD" BEING WRITTEN OUT AS PART OF                                                               
C                                 FOURIER ARRAY SIZES.                                                                              
C     OCT 08/03 - M. LAZARE.    - ILG,ILGD VALUES DEPENDENT ON LON,LOND,                                                            
C                                 LONSL,LONSLD.                                                                                     
C     JUN 26/03 - M. LAZARE.    NEW VERSION FOR IBM (MODEL VERSION GCM13B):                                                         
C                               - REMOVE (UNUSED) DEFINITIONS FOR:                                                                  
C                                 LM1,G1,G2,P0,P1,P2,LJ,L1J,LL,TENER,                                                               
C                                 ILG,ILGD,NADVSL,NTOTSL,NTASKS,IDM.                                                                
C                               - CHANGE READ-IN PARAMETERS:                                                                        
C                                 LONSL,LONSLD,LON,LOND,NNODE INSTEAD OF                                                            
C                                 ILGSL,ILGSLD,NLATJ,NLATJD,MACHINE.                                                                
C                                 ALSO:  NLAT,NLATD READ INSTEAD OF                                                                 
C                                 ILAT,ILATD.                                                                                       
C                                 ILGSL,ILGSLD,ILAT,ILATD,NLATJ,NLATJD,                                                             
C                                 LONTP,LONTD,ILGTP,ILGTD,NTASKT,NTASKTD,                                                           
C                                 NTASKP AND NTASKD CALCULATED INSIDE.                                                              
C                               - CODE RESTRUCTURED IN FUNCTIONAL SECTIONS.                                                         
C     JUN 21/03 - M. LAZARE.    NEW VERSION FOR IBM (MODEL VERSION GCM13A):                                                         
C                               - "NENDJ" -> NTASK.                                                                                 
C                               - ENSURE CONSISTENCY BETWEEN VARIOUS GRID                                                           
C                                 SIZES FOR BOTH PHYSICS AND DYNAMICS.                                                              
C                               - REFERENCES TO "NPPS", "NPVG", "NPSF"                                                              
C                                 REMOVED ("NPGG" HAS TO BE KEPT FOR I/O).                                                          
C                               - REFERENCES TO "IL", "IL1", "IS", "DL",                                                            
C                                 "DS" REMOVED IN CONJUNCTION WITH CHANGES                                                          
C                                 TO LSMOD ROUTINES.                                                                                
C                               - PROPER NAMING CONVENTION FOR                                                                      
C                                 ILGSL/ILGTD/ILG AND ILGSLD/ILGDTD/ILGD.                                                           
C                               - REORGANIZATION OF S/L STUFF.                                                                      
C                               - IP0J NOW LONSL*ILAT+1.                                                                            
C                               - "IDBL" REFERENCES REMOVED (ALWAYS TRUE).                                                          
C                               - DP0J ADDED (LONSLD*ILATD+1).                                                                      
C                               - MULTI-LEVEL "PAK" DIMENSIONS REMOVED.                                                             
C     OCT 12/01 - M. LAZARE.    NEW VERSION FOR MODEL VERSION GCM13A.                                                               
C                               REMOVE 5*ILG WORK FIELDS FOR CLASS AND                                                              
C                               ADD 1*ILG*IGND.                                                                                     
C     OCT  7/99 - J. SCINOCCA - PREVIOUS VERSION GCMPRDA FOR GCM13.                                                                 
C                                                                               I2                                                  
CGCMPARM - COMPUTES ARRAY DIMENSIONS FOR GCM13B AND GCM15B              0  1 C  I1                                                  
C                                                                               I3                                                  
CAUTHOR  - M.LAZARE/L.SOLHEIM                                                   I3                                                  
C                                                                               I3                                                  
CPURPOSE - COMPUTES AND WRITES OUT TO PARAMS FILE THE PARMSUB INPUT CARD        I3                                                  
C          IMAGES FOR THE AUTOMATIC DIMENSIONING OF ARRAYS IN THE G.C.M.        I3                                                  
C                                                                               I3                                                  
COUTPUT FILE...                                                                 I3                                                  
C                                                                               I3                                                  
C      PARAMS = CARD IMAGE FILE CONTAINING THE REPLACEMENT VALUES FOR           I3                                                  
C               PARMSUB TO PROCESS THE GCM VERSION-13 ARRAY DIMENSIONS.         I3                                                  
C                                                                                                                                   
CINPUT PARAMETERS...                                                                                                                
C                                                                               I5                                                  
C      ILEV  = NUMBER OF MODEL    LEVELS                                        I5                                                  
C      LEVS  = NUMBER OF MOISTURE LEVELS                                        I5                                                  
C      LONSLD= NUMBER OF LONGITUDES         ON THE DYNAMICS GRID                I5                                                  
C      NLATD = NUMBER OF GAUSSIAN LATITUDES ON THE DYNAMICS GRID                I5                                                  
C      LMT   = SPECTRAL TRUNCATION WAVENUMBER                                   I5                                                  
C      NTRAC = NUMBER OF TRACERS IN MODEL (IF NOT USED, THEN GCMPARM            I5                                                  
C              COMMENTS OUT DIMENSION STATEMENTS FOR TRACER-RELATED             I5                                                  
C              ARRAYS).                                                         I5                                                  
C      ITRACA= NUMBER OF ADVECTED TRACERS. IF NEGATIVE, THE TRACERS ARE         I5                                                  
C              BEING ADVECTED SEMI-LAGRANGIAN INSTEAD OF SPECTRAL, AND          I5                                                  
C              VARIOUS DIMENSION STATEMENTS ARE COMMENTED OUT OR ACTIVATED      I5                                                  
C              IN EITHER CASE DEPENDING ON THE CHOICE ($CSP$ OR $CSL$).         I5                                                  
C              AS WELL, THE REMAINDER OF NTRAC AND NTRACA=ABS(ITRACA),          I5                                                  
C              DEFINED AS NTRACN, IS CALCULATED AND ALSO IS USED TO DEFINE      I5                                                  
C              OR COMMENT OUT VARIOUS ARRAYS ($CSN$).                           I5                                                  
C      IOZTYP= SWITCH FOR INPUT OZONE DISTRIBUTION                              I5                                                  
C               (IOZTYP=1 => OLD JAPANESE DATASET WITH LEVOZ=37,                I5                                                  
C                IOZTYP=2 => NEW AMIP2    DATASET WITH LEVOZ=59)                I5                                                  
C                IOZTYP=3 => F&K/HALOE    DATASET WITH LEVOZ=28)                I5                                                  
C                IOZTYP=4 => RANDEL       DATASET WITH LEVOZ=24)                I5                                                  
C                IOZTYP=5 => RANDEL/CMAM  DATASET WITH LEVOZ=45)                I5                                                  
C                IOZTYP=6 => CMIP6        DATASET WITH LEVOZ=49)                I5                                                  
C    NNODE_A = NUMBER OF SMP NODES USED TO RUN ATM (MPI FOR NNODE_A>1).         I5                                                  
C      LOND  = NUMBER OF GRID POINTS PER SLICE FOR DYNAMICS CALCULATION.        I5                                                  
C      LONSL = NUMBER OF LONGITUDES         ON THE PHYSICS GRID                 I5                                                  
C      NLAT  = NUMBER OF GAUSSIAN LATITUDES ON THE PHYSICS GRID                 I5                                                  
C      LON   = NUMBER OF GRID POINTS PER SLICE FOR PHYSICS CALCULATION.         I5                                                  
C                                                                               I5                                                  
CEXAMPLE OF INPUT CARD...                                                       I5                                                  
C                                                                               I5                                                  
C   GCMPARM    35    35    47   144    72   576    96    48   384               I5                                                  
C   GCMPARM    17    12     2     1                                             I5                                                  
C----------------------------------------------------------------------------                                                       
                                                                                                                                    
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)                                                                                        
C                                                                                                                                   
      INTEGER*4 II                                                                                                                  
C                                                                                                                                   
      CHARACTER*128 FILNAM                                                                                                          
                                                                                                                                    
C====================================================================                                                               
C     * OPEN UNITS, READ AND PRINTOUT INPUT DATA.                                                                                   
C                                                                                                                                   
      II=1                                                                                                                          
      CALL GETARG(II,FILNAM)                                                                                                        
      OPEN(1,FILE=FILNAM)                                                                                                           
C                                                                                                                                   
      READ(5,'(10X,9I6)',END=901)                                                                                                   
     +  ILEV,LEVS,LMT,LONSLD,NLATD,LOND,LONSL,NLAT,LON                                                                              
      READ(5,'(10X,4I6)',END=902) NTRAC,ITRACA,IOZTYP,NNODE_A                                                                       
C                                                                                                                                   
      WRITE(6,'(A,3I6)')'    ILEV,LEVS,LMT: ',ILEV,LEVS,LMT                                                                         
      WRITE(6,'(A,3I6)')'LONSLD,NLATD,LOND: ',LONSLD,NLATD,LOND                                                                     
      WRITE(6,'(A,3I6)')'LONSL, NLAT, LON : ',LONSL,NLAT,LON                                                                        
      WRITE(6,'(A,2I6)')'     NTRAC,ITRACA: ',NTRAC,ITRACA                                                                          
      WRITE(6,'(A,2I6)')'   IOZTYP,NNODE_A: ',IOZTYP,NNODE_A                                                                        
C======================================================================                                                             
C     * ALL CHECKS ON VARIABLE CONSISTENCY ARE NOW DONE IN GCM                                                                      
C                                                                                                                                   
      IF (NNODE_A.EQ.0) NNODE_A=1                                                                                                   
      WRITE(1,'(A8,I5,",")')'NNODE_A=',NNODE_A                                                                                      
C                                                                                                                                   
      NTRAC =MAX(NTRAC,0)                                                                                                           
      NTRACA=MAX(ITRACA,0)                                                                                                          
      IF(ITRACA.GE.0)       THEN                                                                                                    
        NTRACN=NTRAC-NTRACA                                                                                                         
      ELSE                                                                                                                          
        NTRACN=0                                                                                                                    
      ENDIF                                                                                                                         
C                                                                                                                                   
      WRITE(1,'(A8,I5,",")')'  NTRAC=',NTRAC                                                                                        
      WRITE(1,'(A8,I5,",")')' NTRACA=',NTRACA                                                                                       
      WRITE(1,'(A8,I5,",")')' NTRACN=',NTRACN                                                                                       
C======================================================================                                                             
C     * OZONE LEVELS.                                                                                                               
C     * FIRST CONSISTENCY CHECK.                                                                                                    
C                                                                                                                                   
      IF(IOZTYP.LT.1.OR.IOZTYP.GT.6) CALL          XIT('GCMPARM',-1)                                                                
C                                                                                                                                   
      IF(IOZTYP.EQ.1)          THEN                                                                                                 
         LEVOZ=37                                                                                                                   
      ELSE IF(IOZTYP.EQ.2)     THEN                                                                                                 
         LEVOZ=59                                                                                                                   
      ELSE IF(IOZTYP.EQ.3)     THEN                                                                                                 
         LEVOZ=28                                                                                                                   
      ELSE IF(IOZTYP.EQ.4)     THEN                                                                                                 
         LEVOZ=24                                                                                                                   
      ELSE IF(IOZTYP.EQ.5)     THEN                                                                                                 
         LEVOZ=45                                                                                                                
      ELSE IF(IOZTYP.EQ.6)     THEN                                                                                                 
         LEVOZ=49                  
      ELSE                                                                                                                          
         CALL                                      XIT('GCMPARM',-2)                                                                
      ENDIF                                                                                                                         
      WRITE(1,'(A8,I5,",")')' IOZTYP=',IOZTYP                                                                                       
      WRITE(1,'(A8,I5,",")')'  LEVOZ=',LEVOZ                                                                                        
C=======================================================================                                                            
C     * FULL GRID LATITUDE INFORMATION.                                                                                             
C                                                                                                                                   
      WRITE(1,'(A8,I5,",")')'  LONSL=',LONSL                                                                                        
      WRITE(1,'(A8,I5,",")')' LONSLD=',LONSLD                                                                                       
      WRITE(1,'(A8,I5,",")')'   NLAT=',NLAT                                                                                         
      WRITE(1,'(A8,I5,",")')'  NLATD=',NLATD                                                                                        
C=======================================================================                                                            
C     * DERIVED RUN-TIME CONFIGURATION PARAMETERS BASED ON INPUT.                                                                   
C     * "NTASK" REFERS TO THE RESULTING NUMBER OF ITTERATIONS IN THE                                                                
C     * LATITUDE LOOP ON EACH NODE, BASED ON NUMBER OF NODES AND                                                                    
C     * SPECIFIED CHUNK SIZE (FOR EACH OF PHYSICS,DYNAMICS).                                                                        
C     * THE ABOVE APPLIES TO BOTH THE DYNAMICS (SUBSCRIPT "D") AND                                                                  
C     * PHYSICS (SUBSCRIPT "P") GRIDS.                                                                                              
C                                                                                                                                   
      WRITE(1,'(A8,I5,",")')'    LON=',LON                                                                                          
      WRITE(1,'(A8,I5,",")')'   LOND=',LOND                                                                                         
C                                                                                                                                   
      NLATJ =MAX(LON /LONSL ,1)                                                                                                     
      NLATJD=MAX(LOND/LONSLD,1)                                                                                                     
      ILGSL =LONSL +2                                                                                                               
      ILGSLD=LONSLD+2                                                                                                               
      ILG_TP=ILGSL*NLATJ+1                                                                                                          
      ILG_TD=ILGSLD*NLATJD+1                                                                                                        
C                                                                                                                                   
C     * VALUES FOR ILG,ILGD ARE DEPENDENT ON WHETHER WE ARE RUNNING                                                                 
C     * WITH SUB-LATITUDES PER TASK OR NOT. IF NO SUBLATITUDES, THE                                                                 
C     * CODE CAN BE MORE EFFICIENT RUNNING WITH JUST ONE OPENMP LOOP                                                                
C     * FOR EACH OF THE PHYSICS/DYNAMICS; TO DO SO, HOWEVER, REQUIRES                                                               
C     * CHANGING THE DEFINITION OF ILG TO BE CONSISTENT WITH ILG_TP                                                                 
C     * AND SIMILARILY FOR DYNAMICS.                                                                                                
C                                                                                                                                   
      IF(LON.GE.LONSL) THEN                                                                                                         
        ILG=ILG_TP                                                                                                                  
      ELSE                                                                                                                          
        ILG=LON+1                                                                                                                   
      ENDIF                                                                                                                         
      WRITE(1,'(A8,I5,",")')'      I=',ILG                                                                                          
C                                                                                                                                   
      IF(LOND.GE.LONSLD) THEN                                                                                                       
        ILGD=ILG_TD                                                                                                                 
      ELSE                                                                                                                          
        ILGD=LOND+1                                                                                                                 
      ENDIF                                                                                                                         
      WRITE(1,'(A8,I5,",")')'      D=',ILGD                                                                                         
C=======================================================================                                                            
C     * LEVEL AND SPECTRAL SPACE SIZES.                                                                                             
C                                                                                                                                   
      WRITE(1,'(A8,I5,",")')'      L=',ILEV                                                                                         
      WRITE(1,'(A8,I5,",")')'      S=',LEVS                                                                                         
      WRITE(1,'(A8,I5,",")')'LMTOTAL=',LMT+1                                                                                        
C=======================================================================                                                            
C     * CONDITIONAL COMMENT CARDS                                                                                                   
C                                                                                                                                   
      IF(NTRAC.EQ.0) THEN                                                                                                           
        WRITE(1,'(A)')'    COM=C ***,'                                                                                              
        WRITE(1,'(A)')'    CSL=C ***,'                                                                                              
        WRITE(1,'(A)')'    CSP=C ***,'                                                                                              
        WRITE(1,'(A)')'    CSN=C ***,'                                                                                              
      ELSE                                                                                                                          
        WRITE(1,'(A)')'    COM=     ,'                                                                                              
C                                                                                                                                   
        IF(ITRACA.GT.0) THEN                                                                                                        
          WRITE(1,'(A)')'    CSL=C ***,'                                                                                            
          WRITE(1,'(A)')'    CSP=     ,'                                                                                            
        ELSE IF(ITRACA.LT.0) THEN                                                                                                   
          WRITE(1,'(A)')'    CSL=     ,'                                                                                            
          WRITE(1,'(A)')'    CSP=C ***,'                                                                                            
        ELSE                                                                                                                        
          WRITE(1,'(A)')'    CSL=C ***,'                                                                                            
          WRITE(1,'(A)')'    CSP=C ***,'                                                                                            
        ENDIF                                                                                                                       
C                                                                                                                                   
        IF(ITRACA.LT.0 .OR. NTRACN.EQ.0) THEN                                                                                       
          WRITE(1,'(A)')'    CSN=C ***,'                                                                                            
        ELSE                                                                                                                        
          WRITE(1,'(A)')'    CSN=     ,'                                                                                            
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * NORMAL TERMINATION.                                                                                                         
C                                                                                                                                   
      STOP                                                                                                                          
C                                                                                                                                   
C     * E.O.F. ON INPUT.                                                                                                            
C                                                                                                                                   
  901 CALL                                         XIT('GCMPARM',-3)                                                                
  902 CALL                                         XIT('GCMPARM',-4)                                                                
C======================================================================                                                             
      END                                                                                                                           
