      PROGRAM TSGGFIL
C     PROGRAM TSGGFIL (INFIL,       OUTFIL,       OUTPUT,               )       H2
C    1           TAPE1=INFIL, TAPE2=OUTFIL, TAPE6=OUTPUT)
C     ---------------------------------------------------                       H2
C                                                                               H2
C     FEB 22/07 - B.DUGAS.                                                      H2
C                                                                               H2
CTSGGFIL  - FILLS GRID ARRAY WITH TIMESERIES VALUE FOR THAT TIME        1  1    H1
C                                                                               H3
CAUTHOR  - GJB                                                                  H3
C                                                                               H3
CPURPOSE - FILLS AN OUTPUT GRID  ARRAY EVERYWHERE WITH THE VALUE                H3
C          AT EACH TIME OF A SINGLE TIMESERIES ARRAY X(I,J,T) = TS(T)           H3
C                                                                               H3
CINPUT FILE...                                                                  H3
C                                                                               H3
C      INFIL  = A TIME SERIES FILE WITH A  STANDARD TIME SERIES LABEL           H3
C                                                                               H3
C                  IBOUT(1) = TIME                                              H3
C                  IBOUT(2) = LOCATION IN LATLON FORMAT OR LINEAR POSITION      H3
C                             IN THE ORIGINAL SPECTRAL ARRAY.                   H3
C                  IBOUT(3) = NAME                                              H3
C                  IBOUT(4) = LEVEL                                             H3
C                  IBOUT(5) = LENGTH OF SERIES.                                 H3
C                  IBOUT(6) = 1                                                 H3
C                  IBOUT(7) = DIMENSION OF GRID, KHEM AND KIND OF DATA          H3
C                             (IN THE FORMAT CCCRRRKQ OR LRLMTQ)                H3
C                             WHERE Q IS IKIND (SHOULD BE GRID HERE)            H3
C                  IBOUT(8) = PACKING DENSITY                                   H3
C                                                                               H3
COUTPUT FILE...                                                                 H3
C                                                                               H3
C      OUTFIL = A TIME SERIES OF GRIDS WITH THE TIMESERIES VALUE AT             H3
C               ALL GRID POINTS                                                 H3
C --------------------------------------------------------------------------------
      use diag_sizes, only : SIZES_LMTP1,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      INTEGER LSR(2,SIZES_LMTP1+1)
      LOGICAL OK
      CHARACTER STRING*4
      COMMON/BLANCK/ TS(SIZES_LONP1xLAT), G(SIZES_LONP1xLAT)
      COMMON /ICOM/ IBUF(8),DAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/JCOM/ IBOUT(8),IDOUT(SIZES_LONP1xLATxNWORDIO)
C
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NF = 3
      CALL JCLPNT(NF,1,2,6)
      REWIND 1
      REWIND 2
C
C     * CHECK THE INPUT FILE
C
      CALL GETFLD2(1,TS,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         CALL                                      XIT('TSGGFIL',-1)
      ENDIF
      WRITE(6,6010)
      CALL PRTLAB (IBUF)

C     * GET INFO FROM TS LABEL ABOUT SIZE OF GRID AND SO FORTH
C
      CALL T2LBL(IBUF, IBOUT, LSR)
          IF (IBOUT(1) .NE. 4HGRID) THEN
          CALL                                     XIT('TSGGFIL',-2)
      ENDIF
C    
      NCOLS    = IBOUT(5)
      NROWS    = IBOUT(6)
      NWDS     = NROWS*NCOLS

      LENGTH   = IBUF(5)
      WRITE (6,6020) LENGTH
      WRITE (6,6030) NWDS
C-----------------------------------------------------------------------
C
        DO 200 J=1,LENGTH
        IBOUT(2)=J
C
            DO 210 I=1,NWDS
            G(I)=TS(J)
  210       CONTINUE
C
       CALL PUTFLD2(2,G,IBOUT,MAXX)
  200  CONTINUE
C
           WRITE(6,6060)
           CALL PRTLAB (IBOUT)
           CALL                                    XIT('TSGGFIL', 0)

C-----------------------------------------------------------------------

 6010 FORMAT('  INPUT TIMESERIES:')
 6020 FORMAT('  TIME SERIES IS OF LENGTH ',I10,'.')
 6030 FORMAT('  SIZE OF GRID ',I10)
 6060 FORMAT('  LAST RECORD CREATED:')
      END
