      PROGRAM TFILTER
C     PROGRAM TFILTER (INFIL,       GAINSQ,       OUTFIL,       INPUT,          H2
C    1                                                          OUTPUT, )       H2
C    2           TAPE1=INFIL, TAPE2=GAINSQ, TAPE3=OUTFIL, TAPE5=INPUT,
C    3                                                    TAPE6=OUTPUT)
C     -----------------------------------------------------------------         H2
C                                                                               H2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       H2
C     FEB 14/02 - F. MAJAESS(INCREASE NUMBER OF LEVELS WHICH CAN BE HANDLED AND 
C                            ENSURE THEY ARE IN HARMONY WITH THOSE SUPPORTED IN 
C                            "FILES" SUBROUTINE. ALSO, ENSURE PROGRAM STOPS IN  
C                            THE MULTI-LEVELS CASE)                             
C     FEB 09/98 - F. MAJAESS(IMPLEMENT MOVING AVERAGE OPTION)                   
C     JAN 09/98 - F. ZWIERS (UPDATE DOCUMENTATION, MINOR CHANGE IN THE          
C                            IMPLEMENTATION OF THE HANNING TAPER,               
C                            SIMPLIFICATION OF THE COMPUTATION OF THE           
C                            FILTER'S GAIN, REMOVAL OF THE SEQUENTIAL           
C                            MOVING AVERAGE FILTER OPTION BECAUSE IT            
C                            WAS FOUND TO HAVE POOR CUTOFF PROPERTIES           
C                            AND UNEVEN GAIN IN THE FREQUENCY BAND PASSED).     
C     DEC 23/93 - J. FYFE (FIX CONFLICT IN "K" INPUT PARAMETER VS. "K"
C                          ARGUMENT IN FBUFFIN CALL)
C     JUL 22/92 - E. CHAN  (REPLACE UNFORMATTED I/O WITH I/O ROUTINES)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     OCT 31/85 - F. ZWIERS, N.E. SARGENT, B.DUGAS.
C                                                                               H2
CTFILTER - TIME FILTER A SET OF SERIES BY CONVOLUTION FILTERS.          1  2 C  H1
C                                                                               H3
CAUTHORS - F. ZWIERS, N.E. SARGENT AND B. DUGAS                                 H3
C                                                                               H3
CPURPOSE - DEPENDING ON THE INPUT CARD VALUES, APPLY A GRID POINT TIME FILTER   H3
C          OR CALCULATE MOVING AVERAGE OF A TIME SERIES OF FIELDS.              H3
C                                                                               H3
C          THE FILTER APPLIED IS AN IDEAL LOW-PASS FILTER WITH CUTOFF           H3
C          FREQUENCY F (CYCLES PER UNIT TIME) MODIFIED BY A HANNING WINDOW.     H3
C          SEE 'THE SPECTRAL ANALYSIS OF TIME SERIES',                          H3
C          L.H. KOOPMANS, ACADEMIC PRESS, 1974.                                 H3
C                                                                               H3
C                                                                               H3
CINPUT FILE...                                                                  H3
C                                                                               H3
C      INFIL  = A TIME SERIES OF FIELDS.                                        H3
C                                                                               H3
COUTPUT FILES...                                                                H3
C                                                                               H3
C      GAINSQ = THIS PROGRAM WRITES ONE RECORD WITH IBUF(1) = 4HZONL WHICH      H3
C               CONTAINS THE SQUARED GAIN OF THE FILTER APPLIED TO INFIL.       H3
C                                                                               H3
C      OUTFIL = IF FILTER <= 0, THEN THIS FILE CONTAINS THE FILTERED TIME       H3
C                               SERIES WHICH IS 2*K SHORTER THAN THE "INFIL"    H3
C                               INPUT TIME SERIES.                              H3
C
CINPUT PARAMETERS...
C                                                                               H5
C       ABS(F) = IS THE CUTOFF FREQUENCY IN CYCLES PER OBSERVING TIME           H5
C                (IGNORED IF K < 0 )                                            H5
C              > 0, CHOOSES A  LOW-PASS FILTER                                  H5
C              < 0, CHOOSES A HIGH-PASS FILTER                                  H5
C                                                                               H5
C       K      > 0, CHOOSES A SYMMETRIC FILTER WITH 2*K+1 WEIGHTS THAT          H5
C                   IS CONSTRUCTED BY                                           H5
C                      (A) TRUNCATING THE WEIGHTS OF THE IDEAL                  H5
C                          HIGH OR LOW PASS FILTER AT LAGS +/- K, AND           H5
C                      (B) APPLYING A HANNING TAPER TO INHIBIT VARIANCE         H5
C                          LEAKAGE THROUGH SIDE LOBES.                          H5
C                   A LARGE VALUE OF K WILL PRODUCE A FILTER WITH A SHARP       H5
C                   CUTOFF AT FREQUENCY F, AND A SMALL VALUE OF K RESULTS       H5
C                   IN A FILTER WITH A GRADUAL CUTOFF AT FREQUENCY F.           H5
C                                                                               H5
C                   A PRACTICAL RULE OF THUMB IS TO SELECT K TO BE 5% OR        H5
C                   10% OF THE LENGTH OF THE TIME SERIES.  IF YOU PLAN TO       H5
C                   COMPARE TWO FILTERED TIME SERIES, MAKE SURE TO USE          H5
C                   THE SAME K FOR BOTH. FOR MANY PURPOSES, K=10 OR 15          H5
C                   WORKS VERY WELL.                                            H5
C                                                                               H5
C       K      < 0, CHOOSES A MOVING AVERAGE OF LENGTH 2*K+1 ON THE INPUT       H5
C                   DATA IN "INFIL" FILE.                                       H5
C                                                                               H5
C                                                                               H5
C              THE SIZE OF K IS RESTRICTED BY THE NUMBER OF RECORDS IN INFIL    H5
C              WHICH CAN BE STORED IN MEMORY SIMULTANEOUSLY AND THE LENGTH OF   H5
C              THE ARRAY USED TO STORE THE WEIGHTS.                             H5
C                                                                               H5
C              BASED ON X-DIMENSION OF 4307520 THAT SUPPORTS K <= 10 FOR T213   H5
C              GRID; THE MAXIMUM ALLOWED K IS :                                 H5
C                   FOR T21 = 4255 (SPEC), 1034 (GRID) AND                      H5
C                   FOR T32 = 1919 (SPEC),  462 (GRID) AND                      H5
C                   FOR T48 =  878 (SPEC),  205 (GRID) AND                      H5
C                   FOR T64 =  501 (SPEC),  115 (GRID) AND                      H5
C                   FOR T96 =  226 (SPEC),   51 (GRID) AND                      H5
C                   FOR T213=   46 (SPEC),   10 (GRID).                         H5
C                                                                               H5
C       FILTER <= 0, APPLY FILTER TO DATA IN INFIL.                             H5
C              >  0, COMPUTE AND DISPLAY THE SQUARED GAIN OF THE FILTER ONLY.   H5
C                    THIS OPTION IS USEFUL WHEN CHOOSING FILTER PARAMETERS.     H5
C                                                                               H5
C       NOTE - THE PROGRAM COMPUTES AND DISPLAYS THE SQUARED GAIN OF THE        H5
C              FILTER RESULTING FROM THE CHOICE OF F AND K. IT ALSO OUTPUTS     H5
C              A RECORD WITH IBUF(1)=4HZONL CONTAINING THE COMPUTED SQUARED     H5
C              GAIN WHICH CAN BE PLOTTED BY 'CRVPLOT'.                          H5
C                                                                               H5
CEXAMPLE OF INPUT CARD...                                                       H5
C                                                                               H5
C*TFILTER.      -0.2        20              0                                   H5
C----------------------------------------------------------------------------
C
C "IMAXK" IS THE MAXIMUM K FOR T21 (SPEC) BASED ON "IXDIM=(2*10+1)*641*320"
C DIMENSION FOR "X" WHICH CAN SUPPORT UP TO K=10, FOR T213 (GRID).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER (IYDIM=641*320,IYDIMV=2*IYDIM)
      PARAMETER (IXDIM=(2*10+1)*IYDIM)
      PARAMETER (IMAXK=((IXDIM/(22*23))-1)/2)
      PARAMETER (IWDIM=(2*IMAXK)+1)

C     * "MAXUN"/"IOFFST" MUST BE SET TO SAME CORRESPONDING VALUE IN
C     * "FILES" SUBROUTINE.

      PARAMETER (MAXUN=40,IOFFST=10)

C     * "IXWDIM" = IWDIM*8 (WHERE "8" IS FOR THE 8-WORD LABEL).

      PARAMETER (IXWDIM=IWDIM*8)

      DIMENSION    W(IWDIM),IX(IXWDIM),X(IXDIM),Y(IYDIM)
      REAL         L0
      INTEGER      ADD   ,BASE   ,FILTER   ,LEV(MAXUN),
     1             FOUR  ,ZONL   ,SPEC     ,IW(IWDIM)
      LOGICAL      OK    ,LOPASS
      COMMON/ICOM/ IBUF(8),IDAT(IYDIMV)
      COMMON /MACHTYP/ MACHINE,INTSIZE
      DATA         MAXX /IYDIMV/,MAXXR /IYDIM/    ,MAXK /IMAXK/,
     1             NW   /IXDIM/ ,TWOPI /6.2831853E0/
C     DATA         FOUR  /4HFOUR/   ,ZONL /4HZONL/, SPEC /4HSPEC/
C----------------------------------------------------------------------------
      FOUR=NC4TO8("FOUR")
      ZONL=NC4TO8("ZONL")
      SPEC=NC4TO8("SPEC")
C
      NF = 5
      CALL JCLPNT (NF,1,2,3,5,6)
      REWIND 1
      IF (NF.GT.2) REWIND 3
C
C     * POSSIBLY ADJUST "MAXX" VALUE BASED ON THE "INTSIZE" VALUE
C     * COMPUTED IN "MACHTYP" COMMON BLOCK VIA THE CALL TO "JCLPNT".
C
      MAXX=MAXXR*INTSIZE
C
C     * GET AND CHECK THE TFILTER CARD PARAMETERS.
C
     
      READ (5,5010,END=900) F,K,FILTER                                          H4

      IF (K.EQ.0 .OR. ABS(K).GT.MAXK)                          THEN
        WRITE(6,6065) K
        CALL                                       XIT('TFILTER',-1)
      ENDIF

      NWGHTS = 2*ABS(K) + 1

C     * COMPUTE FILTER WEIGHTS, DISPLAY AND OUTPUT SQUARED GAIN

      IF (K.GT.0)                                              THEN

        WRITE(6,6050) F,K,FILTER

        IF (F.LT.0.0E0)                                          THEN
          LOPASS = .FALSE.
          F      = ABS(F)
        ELSE
          LOPASS = .TRUE.
        ENDIF

        IF (F.EQ.0.0E0 .OR. F.GE.0.5E0 )                           THEN
          WRITE(6,6060)
          CALL                                     XIT('TFILTER',-2)
        ENDIF

        L0 = TWOPI*F

        CALL FWGHTS (L0,K,W,LOPASS,SUMW)

      ELSE

C       * MOVING AVERAGE CASE SETUP

        WRITE(6,6055) K,FILTER

        K      = ABS(K)

        L0     = 2*TWOPI/NWGHTS
        FAC    = 1.E0/NWGHTS

        DO I = 1,NWGHTS
         W(I) = FAC
        ENDDO

      ENDIF

      CALL GAINSQ (L0,K,W,X,Y,NFREQ)

      IF (NFREQ.GT.MAXXR)                                      THEN
        WRITE(6,6150)
        CALL                                       XIT('TFILTER',-3)
      ENDIF

      CALL SETLAB (IBUF,ZONL,1,NC4TO8("TFUN"),1,NFREQ,1,0,1)
      CALL PUTFLD2(2,Y,IBUF,MAXX)

      IF (FILTER.GT.0)                                         THEN
        CALL                                       XIT('TFILTER',0)
      ENDIF

C--------------------------------------------------------------------------

C     * CHECK INFIL

      CALL FBUFFIN(1,IBUF,-8,KOK,LEN)
      IF (KOK.GE.0) GOTO 910
      REWIND 1

      NWDS     = IBUF(5)*IBUF(6)
      IF (IBUF(1).EQ.SPEC .OR. IBUF(1).EQ.FOUR)
     1    NWDS = NWDS*2
      MAXK     = MIN(MAXK,(NW/NWDS - 1)/2)
C
      IF (K.GT.MAXK .OR. NWDS.GT.MAXXR)                         THEN
          WRITE(6,6150)
          CALL                                     XIT('TFILTER',-4)
      ENDIF
      CALL FILEV (LEV,NLEV,IBUF,1)
      IF ((NLEV.LT.1).OR.(NLEV.GT.MAXUN)) CALL     XIT('TFILTER',-5)

C     * OPEN "NLEV" SCRATCH UNIT FILE PAIRS.

      CALL FILES (+1,NLEV)

C     * LOOP THROUGH ALL LEVELS

      DO 525 LV=1,NLEV
          IF (NLEV.EQ.1)                                       THEN
              REWIND 1
              NFO = 3
          ELSE
              NFI = IOFFST+LV
              NFO = NFI+MAXUN
              REWIND NFI
              REWIND NFO
          ENDIF

C         * LOAD UP MEMORY WITH THE FIRST 2*K + 1 RECORDS FROM
C         * LEV(LV) AND INITIALISE MEMORY POINTER VECTOR IW.

          NOUT   = 0

          DO 150 I=1,NWGHTS
              IW(I) = I
  150     CONTINUE

          IF (LV.EQ.1)                                         THEN

              DO 200 I=1,NWGHTS

                  ADD = (IW(I)-1)*NWDS + 1
                  CALL GETFLD2 (1,X(ADD),-1,-1,-1,LEV(1),IBUF,MAXX,OK)
                  IF (.NOT.OK)                                 THEN
                      WRITE (6,6160) NWGHTS
                      CALL                         XIT('TFILTER',-6)
                  ENDIF

                  BASE = (IW(I)-1)*8
                  DO 175 J=1,8
                      IX(BASE+J) = IBUF(J)
  175             CONTINUE

                  DO 200 L=2,NLEV
                      NFT = IOFFST+L
                      CALL RECGET (1,-1,-1,-1,LEV(L),IBUF,MAXX,OK)
                      IF (.NOT.OK) THEN
                       WRITE(6,6600) LEV(L)
                       CALL                        XIT('TFILTER',-7)
                      ENDIF
                      CALL RECPUT (NFT,IBUF)
  200         CONTINUE

          ELSE

              DO 225 I=1,NWGHTS
                  ADD = (IW(I)-1)*NWDS + 1
                  CALL GETFLD2(NFI,X(ADD),-1,-1,-1,LEV(LV),IBUF,MAXX,OK)
                  BASE = (IW(I)-1)*8
                  DO 225 J=1,8
                      IX(BASE+J) = IBUF(J)
  225         CONTINUE

          ENDIF

C         * X(IW(K)) HOLDS NWGHTS RECORDS, THE CENTER OF WHICH IS
C         * IW(K+1) (IN A CIRCULAR FASHION). IX(IW(K)) HOLDS THE
C         * CORRESPONDING LABELS.

C                           * ========= *
C                           * TIME LOOP *
C                           * ========= *

C         * APPLY FILTER OR MOVING AVERAGE CALCULATION TO CURRENT SET 
C         * OF NWGHTS RECORDS.  
C         * OUTPUT TO BE PUT IN Y.

  250     BASE       = (IW(1)-1)*NWDS

              WEIGHT = W(1)
              DO 275 J=1,NWDS
                  Y(J) = X(BASE+J)*WEIGHT
  275         CONTINUE

              DO 300 I=2,NWGHTS
                  BASE   = (IW(I)-1)*NWDS
                  WEIGHT = W(I)
                  DO 300 J=1,NWDS
                      Y(J) = Y(J)+WEIGHT*X(BASE+J)
  300         CONTINUE

C             * USE SAME LABEL AS FOR X.

              BASE = (IW(K+1)-1)*8
              DO 350 I=1,8
                  IBUF(I)=IX(BASE+I)
  350         CONTINUE
              CALL PUTFLD2(NFO,Y,IBUF,MAXX)
              NOUT=NOUT+1

C             * SHUFFLE RECORD POINTERS.

              IFIRST     = IW(1)
              DO 400 L=1,NWGHTS-1
                  IW(L)  = IW(L+1)
  400         CONTINUE
              IW(NWGHTS) = IFIRST

C             * GET A NEW FIELD FROM LEV(LV) AND
C             * PUT IT IN POSITION IW(NWGHTS).

              ADD  = (IW(NWGHTS)-1)*NWDS+1
              BASE = (IW(NWGHTS)-1)*8

              IF (LV.EQ.1)                                     THEN

                  CALL GETFLD2 (1,X(ADD),-1,-1,-1,LEV(1),IBUF,MAXX,OK)
                  IF (.NOT.OK) GOTO 525
                  DO 450 I=1,8
                      IX(BASE+I) = IBUF(I)
  450             CONTINUE
                  DO 475 L=2,NLEV
                      NFT = IOFFST+L
                      CALL RECGET (1,-1,-1,-1,LEV(L),IBUF,MAXX,OK)
                      IF (.NOT.OK) THEN
                       WRITE(6,6600) LEV(L)
                       CALL                        XIT('TFILTER',-8)
                      ENDIF
                      CALL RECPUT (NFT,IBUF)
  475             CONTINUE

              ELSE

                  CALL GETFLD2(NFI,X(ADD),-1,-1,-1,LEV(LV),IBUF,MAXX,OK)
                  IF (.NOT.OK) GOTO 525
                  DO 500 I=1,8
                      IX(BASE+I) = IBUF(I)
  500             CONTINUE

              ENDIF

          GOTO 250

  525 CONTINUE

C     * IF THERE ARE MORE THAN ONE LEVEL,
C     * MERGE THEM ALL ONTO ONE FILE AND
C     * RELEASE THE SCRATCH FILES.

      IF (NLEV.GT.1)                                           THEN

          DO LV=1,NLEV
           NF = IOFFST+MAXUN+LV
           REWIND NF
          ENDDO

  600     CONTINUE
              DO 650 LV=1,NLEV
                  NF = IOFFST+MAXUN+LV
                  CALL RECGET(NF,-1,-1,-1,LEV(LV),IBUF,MAXX,OK)
                  IF (.NOT.OK)                                 THEN
                      WRITE(6,6500) NOUT

C                     * RELEASE "NLEV" SCRATCH FILE UNIT PAIRS.

                      CALL FILES (-1,NLEV)

                      CALL                         XIT('TFILTER',0)
                  ENDIF
                  CALL RECPUT(3,IBUF)
  650         CONTINUE
          GOTO 600

      ELSE

          WRITE(6,6500) NOUT
          CALL                                     XIT('TFILTER',0)

      ENDIF

C     * EOF ON UNIT 5.

  900 WRITE(6,6030)
      CALL                                         XIT('TFILTER',-9)

C     * PREMATURE EOF ON UNIT 1 (INFIL).

  910 WRITE(6,6110)
      CALL                                         XIT('TFILTER',-10)
C-----------------------------------------------------------------------------

 5010 FORMAT(10X,E10.0,5X,I5,10X,I5)                                            H4
 6030 FORMAT('0THE TFILTER CARD IS MISSING.')
 6050 FORMAT('0TFILTER       F',E13.4,'    K',I5,'    FILTER',I2)
 6055 FORMAT('0TFILTER       K',I5,'    FILTER',I2)
 6060 FORMAT('0TFILTER PARAMETER OUT OF RANGE.')
 6065 FORMAT('0TFILTER PARAMETER K=',I5,', INVALID OR OUT OF RANGE.')
 6070 FORMAT('0SQUARED GAIN OF FILTER'/
     1       '0 FREQUENCY     GAIN**2')
 6080 FORMAT(1X,F10.4,E12.4)
 6110 FORMAT('0THE INPUT TIME SERIES IS EMPTY.')
 6130 FORMAT(1X,A4,I10,1X,A4,5I10)
 6150 FORMAT('0INSUFFICIENT MEMORY TO APPLY THE REQUESTED FILTER.')
 6160 FORMAT('0INSUFFICIENT DATA ON INFIL TO BEGIN FILTERING.'/
     1       ' NEED AT LEAST ',I5,' RECORDS IN INFIL.')
 6500 FORMAT('0PRODUCED ',I10,' SETS.')
 6600 FORMAT('0TFILTER - PROBLEM AT LEVEL:',I3,'.')
      END
