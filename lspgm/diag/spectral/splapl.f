      PROGRAM SPLAPL
C     PROGRAM SPLAPL (IN,       OUT,       OUTPUT,                      )       E2
C    1          TAPE1=IN, TAPE2=OUT, TAPE6=OUTPUT)
C     --------------------------------------------                              E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     FEB 15/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     JUL 22/86 - B.DUGAS.                                                      
C                                                                               E2
CSPLAPL  - CALCULATES LAPLACIAN OF SPECTRAL INPUT FILE                  1  1    E1
C                                                                               E3
CAUTHOR - B.DUGAS                                                               E3
C                                                                               E3
CPURPOSE - CALCULATES THE LAPLACIAN IN FILE (OUT) OF THE INPUT FILE (IN)        E3
C          BY MULTIPLYING IT BY (-N(N+1)/A**2).                                 E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C    IN  =  CCRN FORMAT SPECTRAL FILE                                           E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C    OUT = FILE CONTAINING THE LAPLACIAN OF THE INPUT FILE                      E3
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXX = 2*(SIZES_LA+SIZES_LMTP1)*SIZES_NWORDIO 

      DIMENSION ALPHA(2*(SIZES_LA+SIZES_LMTP1)), 
     & G(2*(SIZES_LA+SIZES_LMTP1))
      CHARACTER*4 NAME
  
      LOGICAL OK
      INTEGER LSR(2,SIZES_LMTP1+1)
  
      COMMON /ICOM/ IBUF(8), IDAT(MAXX) 
      DATA NAME / 'VORT' /, CONST / -2.464E-14 /
  
C-----------------------------------------------------------------------
      NF = 2
      CALL JCLPNT(NF,1,2,6) 
      REWIND 1
      REWIND 2
  
C     * READ RECORDS FROM INPUT.
  
      NR=0
  100 CALL GETFLD2(1,G,'SPEC',-1,-1,-1,IBUF,MAXX,OK) 
  
          IF (.NOT.OK)                                         THEN 
              IF (NR.EQ.0) CALL                    XIT(' SPLAPL',-1)
              WRITE(6,6020) NR
              CALL                                 XIT(' SPLAPL',0) 
          END IF
  
C         * COMPUTE THE ARRAY C-ALPHA(N,M) - ONLY IF NR=0.
  
          IF (NR.EQ.0)                                         THEN 
              LRLMT=IBUF(7) 
              IF(LRLMT.LT.100) 
     1              CALL FXLRLMT (LRLMT,IBUF(5),IBUF(6),0)
              CALL DIMGT(LSR,LA,LR,LM,KTR,LRLMT)
              CALL CALPHA(ALPHA,LSR,LM,LA)
              DO 200 L=1,2*LA-1,2 
                  ALPHA(L) = ALPHA(L)*CONST 
  200         CONTINUE
          ELSE
              IF (IBUF(7).NE.LRLMT) CALL           XIT(' SPLAPL',-2)
          END IF
  
C         * MULTIPLY G BY ALPHA.
  
          DO 300 I=1,2*LA-1,2 
              G(I)   = G(I)  *ALPHA(I)
              G(I+1) = G(I+1)*ALPHA(I)
  300     CONTINUE
  
C         * SAVE ON FILE OUT. 
  
          READ(NAME,0001) IBUF(3) 
          CALL PUTFLD2(2,G,IBUF,MAXX)
          IF (NR.EQ.0) CALL PRTLAB (IBUF)
  
      NR = NR+1 
      GOTO 100
  
C-----------------------------------------------------------------------
 0001 FORMAT(A4)
 6020 FORMAT('0SPLAPL  PRODUCED',I6,' RECORDS.')
      END
