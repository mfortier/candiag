      PROGRAM SPZDEV
C     PROGRAM SPZDEV (SPIN,       SPOUT,       INPUT,       OUTPUT,     )       E2
C    1          TAPE1=SPIN, TAPE2=SPOUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------------             E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     AUG 05/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/83 - R.LAPRISE.                                                    
C     DEC  3/80 - J.D.HENDERSON 
C     SEP   /79 - S. LAMBERT
C                                                                               E2
CSPZDEV  - COMPUTES SPECTRAL DEVIATION FROM ZONAL MEAN                  1  1   GE1
C                                                                               E3
CAUTHOR  - S.LAMBERT                                                            E3
C                                                                               E3
CPURPOSE - COMPUTES THE SPHERICAL HARMONIC COEFFICIENTS OF THE DEVIATION        E3
C          OF A FIELD FROM ITS ZONAL MEAN                                       E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      SPIN  = GLOBAL SPECTRAL FIELDS.                                          E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      SPOUT = GLOBAL SPECTRAL FIELDS OF DEVIATIONS FROM THE ZONAL MEAN.        E3
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXX = 2*(SIZES_LA+SIZES_LMTP1)*SIZES_NWORDIO 

      COMMON/BLANCK/F(2*(SIZES_LA+SIZES_LMTP1)) 
C 
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
      NRECS=0 
  100 CALL GETFLD2(1,F,NC4TO8("SPEC"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NRECS.EQ.0) CALL                        XIT('SPZDEV',-1) 
        WRITE(6,6020) NRECS 
        CALL                                       XIT('SPZDEV',0)
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
      IF(IBUF(7).LE.99999) THEN
       LR=2*(IBUF(7)/1000) 
      ELSE
       LR=2*(IBUF(7)/10000) 
      ENDIF
      DO 200 I=1,LR 
200   F(I)=0.E0 
      IBUF(3)=NC4TO8("ZDEV")
      CALL PUTFLD2(2,F,IBUF,MAXX)
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
      NRECS=NRECS+1 
      GO TO 100 
C-----------------------------------------------------------------------
6020  FORMAT('0SPZDEV READ',I6,' RECORDS')
6030  FORMAT(1X,A4,I10,2X,A4,I10,4I6)
      END
