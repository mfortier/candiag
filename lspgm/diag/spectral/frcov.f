      PROGRAM FRCOV 
C     PROGRAM FRCOV (FX,       FY,       COV,      INPUT,      OUTPUT,  )       E2
C    1        TAPE11=FX,TAPE12=FY,TAPE13=COV,TAPE5=INPUT,TAPE6=OUTPUT)
C     ----------------------------------------------------------------          E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 11/83 - R.LAPRISE.                                                   
C     DEC 04/80 - J.D.HENDERSON 
C     OCT   /79 - S.LAMBERT 
C                                                                               E2
CFRCOV   - COMPUTES FOURIER COVARIANCE                                  1  1    E1
C                                                                               E3
CAUTHOR  - S.LAMBERT                                                            E3
C                                                                               E3
CPURPOSE - COMPUTES FOURIER COVARIANCES FOR M=0,...,(LM-1) OF THE FORM:         E3
C            FX(M)*FY(-M) + FX(-M)*FY(M)   (IF M.NE.0)                          E3
C            FX(0) + FY(0)                 (IF M.EQ.0)                          E3
C          AT EACH OF THE LATITUDES CONTAINED IN FX(LM,NLAT) AND FY(LM,NLAT).   E3
C          THE COMPUTED COVARIANCES ARE WRITTEN TO THE OUTPUT FILE IN THE       E3
C          ARRAY COV(LM,NLAT).                                                  E3
C                                                                               E3
CINPUT FILES...                                                                 E3
C                                                                               E3
C      FX  = FIRST INPUT  FILE OF FOURIER COEFF                                 E3
C      FY  = SECOND INPUT FILE OF FOURIER COEFF                                 E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      COV = OUTPUT FILE OF COVARIANCES                                         E3
C---------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LAT,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXX = 2*(SIZES_LMTP1+1)*SIZES_LAT*SIZES_NWORDIO 

      COMPLEX F,G,H 
      COMMON/BLANCK/F((SIZES_LMTP1+1)*SIZES_LAT),
     & G((SIZES_LMTP1+1)*SIZES_LAT),H((SIZES_LMTP1+1)*SIZES_LAT) 
C 
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(MAXX) 
C-----------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,11,12,13,6) 
      DO 50 N=11,13 
50    REWIND N
C 
C     * READ INPUT FILES
C 
      NRECS=0 
   70 CALL GETFLD2(11,F,NC4TO8("FOUR"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NRECS.EQ.0)THEN
          CALL                                     XIT('FRCOV',-1)
        ELSE
          WRITE(6,6020) NRECS 
          CALL                                     XIT('FRCOV',0) 
        ENDIF 
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
      CALL GETFLD2(12,G,NC4TO8("FOUR"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('FRCOV',-2)
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
      NWRDS=IBUF(5)*IBUF(6) 
      LM=IBUF(5)
C 
C     * COMPUTE FOURIER COVARIANCES 
C 
      DO 100 I=1,NWRDS
      Q=2.E0*(REAL(F(I))*REAL(G(I))+ IMAG(F(I))* IMAG(G(I)))
100   H(I)=CMPLX(Q,0.E0)
C 
C     * TREATMENT FOR SPECIAL CASE M=0
C 
      DO 200 I=1,NWRDS,LM 
200   H(I)=H(I)*0.5E0 
      IBUF(3)=NC4TO8("FCOV")
      IBUF(8)=1 
      CALL PUTFLD2(13,H,IBUF,MAXX) 
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
      NRECS=NRECS+1 
      GO TO 70
C-----------------------------------------------------------------------
 6020 FORMAT(' ',I6,' RECORDS READ')
 6030 FORMAT(1X,A4,I10,2X,A4,I10,4I6)
      END
