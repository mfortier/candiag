      PROGRAM O2AGRID
C     PROGRAM O2AGRID(FIELDO,         LONO,         LATO,         DAO,          D2
C    1                               BETAO,       FIELDA,       OUTPUT, )       D2
C    2          TAPE1=FIELDO,   TAPE2=LONO,   TAPE3=LATO,   TAPE4=DAO,  
C    3                         TAPE7=BETAO, TAPE8=FIELDA, TAPE6=OUTPUT)
C     -----------------------------------------------------------------         D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     APR 04/2002 - B.MIVILLE (REVISED FOR NEW DATA DESCRIPTION FORMAT)         D2
C     JUN 06/2001 - B.MIVILLE - ORIGINAL VERSION                       
C                                                                               D2
CO2AGRID - INTERPOLATION FROM OCEAN GRID TO ATMOSPHERE GRID             5  1    D1
C                                                                               D3
CAUTHOR  - B. MIVILLE                                                           D3
C                                                                               D3
CPURPOSE - INTERPOLATION FROM OCEAN GRID TO ATMOSPHERE GRID                     D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C   FIELDO = OCEAN INPUT FIELD                                                  D3
C   LONO   = OCEAN GRID LONGITUDES                                              D3
C   LATO   = OCEAN GRID LATITUDES                                               D3
C   DAO    = AREA OF OCEAN GRID CELL                                            D3
C   BETAO  = BETA OF OCEAN GRID CELL                                            D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C   FIELDA = ATMOSPHERE INTERPOLATED OUTPUT FIELD                               D3
C                                                                               D3
CINPUT PARAMETERS...                                                            D5
C                                                                               D5
C   NLONA = NUMBER OF LONGITUDE GRID POINTS IN THE ATMOSPHERE INCLUDING CYCLIC  D5
C   NLATA = NUMBER OF LATITUDE GRID POINTS IN THE ATMOSPHERE                    D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C* O2AGRID        97        48                                                  D5
C-------------------------------------------------------------------------------
C
C     * MAXIMUM 2-D GRID SIZE 388X194 -> 75272
C     * MAXIMUM 2-D GRID SIZE (I+2)*(J+2)=IM*JM --> IJM
C
      use diag_sizes, only : SIZES_NWORDIO,
     &                       SIZES_OLAT,
     &                       SIZES_OLON

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


      PARAMETER (IM=SIZES_OLON+2,JM=SIZES_OLAT+2,
     & IJM=IM*JM,IJMV=IJM*SIZES_NWORDIO)
      REAL FIELDA(IJM),ELONA(IJM),ELATA(IJM)
      REAL FIELDO(IJM),LONO(IJM),LATO(IJM)
      REAL DAO(IJM),BETAO(IJM),DLON
      INTEGER INDI(IJM),INDJ(IJM)
      INTEGER NLONO,NLATO,NLONA,NLATA
      INTEGER I,J,II,JJ
      INTEGER NF,NR
C
      LOGICAL OK
      INTEGER IBUF,IDAT,MAXX,IBUF3
      COMMON/ICOM/IBUF(8),IDAT(IJMV)
      DATA MAXX/IJMV/
C---------------------------------------------------------------------
C
      NF=8
      CALL JCLPNT(NF,1,2,3,4,7,8,5,6)
      REWIND 1
      REWIND 2
      REWIND 3
      REWIND 4
      REWIND 7
      REWIND 8
C
      NR=0
C
C     * READ IN ATMOSPHERE GRID SIZE FROM INPUT CARD
C
      READ(5,5001,END=900) NLONA,NLATA                                          D4
C
C     * READ OCEAN LONGITUDES AND LATITUDES
C
      CALL GETFLD2 (2,LONO,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) CALL                            XIT('O2AGRID',-1)
C
      NLONO=IBUF(5)
      NLATO=IBUF(6)
C
      CALL GETFLD2 (3,LATO,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) CALL                            XIT('O2AGRID',-2)
C
      IF((IBUF(5).NE.NLONO).OR.(IBUF(6).NE.NLATO)) 
     1   CALL                                      XIT('O2AGRID',-3)
C
C     * READ IN THE OCEAN GRID CELL AREAS
C
      CALL GETFLD2 (4,DAO,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) CALL                            XIT('O2AGRID',-4)
C
C     * CHECK IF DIMENSION OF DAO ARE THE SAME AS LATO/LONO
C
      IF((NLATO.NE.IBUF(6)).OR.(NLONO.NE.IBUF(5)))
     1 CALL                                        XIT('O2AGRID',-5)
C
C     * READ IN OCEAN BETA, SHOULD BE AT SURFACE OR FIRST LEVEL
C
      CALL GETFLD2 (7,BETAO,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) CALL                            XIT('O2AGRID',-6)
C
C     * CHECK IF DIMENSION OF BETAO ARE THE SAME AS LATO/LONO
C
      IF((NLATO.NE.IBUF(6)).OR.(NLONO.NE.IBUF(5)))
     1 CALL                                        XIT('O2AGRID',-7)
C
C     * READ IN INPUT OCEAN FIELD
C
 600  CALL GETFLD2 (1,FIELDO,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK)THEN
         IF (NR.EQ.0) CALL                         XIT('O2AGRID',-8)
         CALL                                      XIT('O2AGRID',0)
      ENDIF
C
C     * VERIFY IF THERE IS ANY SUPERLABEL OR CHAR.
C     * PROGRAM WILL EXIT IF IT ENCOUNTERS A SUPERLABEL OR CHAR..
C
      IF(IBUF(1).NE.NC4TO8("GRID"))THEN
         IF((IBUF(1).EQ.NC4TO8("LABL")).OR.
     +      (IBUF(1).EQ.NC4TO8("CHAR"))    )    THEN
            WRITE(6,6030) ' *** SUPERLABELS OR CHAR ARE NOT ALLOWED ***'
            CALL                                   XIT('O2AGRID',-9)
         ELSE
            WRITE(6,6030)' *** FIELD LABEL IS NOT GRID ***'
            CALL                                   XIT('O2AGRID',-10)
         ENDIF
      ENDIF
C
      IF(NR.EQ.0) THEN
         IBUF3=IBUF(3)
         NWRDSO=NLONO*NLATO
         NWRDSA=(NLONA+1)*(NLATA+2)
C
C        * CHECK THAT OCEAN FIELD IS SAME DIMENSION AS LONO/LATO
C
         IF((IBUF(5).NE.NLONO).OR.(IBUF(6).NE.NLATO))THEN
            WRITE(6,6030)' * NLAT/NLON OF FIELD AND GRID ARE',
     1           ' DIFFERENT *'
            WRITE(6,6015)' * RECORD NUMBER: ',NR+1
            CALL                                   XIT('O2AGRID',-11)
         ENDIF
C
C        * CALCULATE THE ATMOSPHERE LATITUDE ASSUMING THAT THE GRID IS A
C        * NORMAL GAUSSIAN GRID
C
         CALL ALATLON(NLONA,NLATA,ELONA,ELATA)
C
C        * VERIFY THAT OCEAN LONGITUDES FALL WITHIN THE ATMOSPHERE
C        * LONGITUDES RANGE PLUS ONE POINT TO THE EAST AND ONE TO THE WEST.
C        * ASSUMING UNIFORMLY SPACED LONGITUDES.
C
         IF(LONO(1).LT.ELONA(1).OR.LONO(NWRDSO).GT.ELONA(NWRDSA))THEN
            WRITE(6,6030) 'OCEAN LONGITUDES OUTSIDE ATMOSPHERE RANGE'
            WRITE(6,6020) 'LONO(1): ',LONO(1),
     1           '    LONO(NLONO): ',LONO(NWRDSO)
            CALL                                   XIT('O2AGRID',-12)
         ENDIF
C
         WRITE(6,6025)'ATMOSPHERE GRID DIMENSION: ',NLONA,NLATA
         WRITE(6,6025)'     OCEAN GRID DIMENSION: ',NLONO,NLATO
C
C        * SEARCH FOR RELATIVE POSITION OF EACH OCEAN POINT TO ATMOSPHERE POINT
C
         CALL INDEXOA(ELONA,ELATA,NLONA,NLATA,LONO,LATO,NLONO,NLATO,
     1        INDI,INDJ)
C
      ELSE
C
C        * CHECK THAT OCEAN FIELD DIMENSION DOES NOT CHANGE
C
         IF(IBUF(5).NE.NLONO.OR.IBUF(6).NE.NLATO)THEN
            WRITE(6,6030)'* NLAT/NLON OF FIELD HAS CHANGED * '
            WRITE(6,6015)'* RECORD NUMBER: ',NR+1
            CALL                                   XIT('O2AGRID',-13)
         ENDIF
C
C        * CHECK THAT LABEL HAS NOT CHANGED
C
         IF(IBUF(3).NE.IBUF3) THEN
            WRITE(6,6030) 'INPUT FIELD HAS CHANGED LABEL'
            WRITE(6,6010) IBUF(3)
            CALL                                   XIT('O2AGRID',-14)
         ENDIF
      ENDIF
C
C     * CALCULATE THE ATMOSPHERE POINTS
C
      CALL ATMOS(FIELDO,NLONO,NLATO,NLONA,NLATA,DAO,BETAO,
     1     INDI,INDJ,FIELDA)
C
C     * WRITE RESULTS ATMOSPHERE FIELD TO FILE
C
      IBUF(5)=NLONA
      IBUF(6)=NLATA
C
      CALL PUTFLD2(8,FIELDA,IBUF,MAXX)
C
      NR=NR+1
C
      GOTO 600
C
  900 IF (NR.EQ.0) CALL                            XIT('O2AGRID',-15)
C
C----------------------------------------------------------------------
 5001 FORMAT(10X,2I10)                                                          D4
 6010 FORMAT('FIELD: ',A4)
 6015 FORMAT(A,I8)
 6020 FORMAT(A,I8,A,I8)
 6025 FORMAT(A,2I8)
 6030 FORMAT(A)
      END
