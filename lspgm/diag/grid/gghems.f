      PROGRAM GGHEMS
C     PROGRAM GGHEMS (GGL,       GGN,       GGS,       OUTPUT,          )       D2
C    1          TAPE1=GGL, TAPE2=GGN, TAPE3=GGS, TAPE6=OUTPUT)
C     --------------------------------------------------------                  D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     NOV 06/95 - F.MAJAESS  (REVISE TO ALLOW NHEM=3)                          
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 11/83 - R.LAPRISE.                                                   
C     DEC 17/80 - J.D.HENDERSON 
C                                                                               D2
CGGHEMS  - SPLITS GLOBAL GAUSSIAN GRID FILE INTO HEMISPHERES            1  2   GD1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - SPLITS A FILE OF GLOBAL GAUSSIAN GRIDS OR CROSS-SECTIONS             D3
C          INTO TWO FILES OF NORTHERN AND SOUTHERN HEMISPHERES.                 D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      GGL = GLOBAL GAUSSIAN GRIDS OR CROSS-SECTIONS                            D3
C            (GAUSSIAN GRIDS MUST HAVE AN EVEN NUMBER OF ROWS)                  D3
C                                                                               D3
COUTPUT FILES...                                                                D3
C                                                                               D3
C      GGN = NORTHERN HEMISPHERE GRIDS OR CROSS-SECTIONS                        D3
C      GGS = SOUTHERN HEMISPHERE GRIDS OR CROSS-SECTIONS                        D3
C---------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/GG(SIZES_LONP1xLAT)
C 
      LOGICAL OK
      LOGICAL GRID,ZONL 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * GET THE NEXT GLOBAL GRID FROM FILE 1. 
C 
      NR=0
  150 CALL GETFLD2(1,GG,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('GGHEMS',-1) 
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('GGHEMS',0)
        ENDIF 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      IF(IBUF(7).NE.0 .AND. IBUF(7).NE.3 )THEN
        WRITE(6,6025) IBUF
        CALL                                       XIT('GGHEMS',-2) 
      ENDIF 
C 
C     * PROCESS ONLY GRIDS AND CROSS SECTIONS.
C 
      NTYPE=IBUF(1) 
      GRID=(NTYPE.EQ.NC4TO8("GRID"))
      ZONL=(NTYPE.EQ.NC4TO8("ZONL"))
      IF(.NOT.(GRID.OR.ZONL)) GO TO 150 
C 
      IF(GRID)THEN
        NLG=IBUF(5) 
        NLAT=IBUF(6)
      ENDIF 
      IF(ZONL)THEN
        NLG=1 
        NLAT=IBUF(5)
      ENDIF 
      NLATH=NLAT/2
      N=NLATH*NLG+1 
C 
C     * SAVE HEMISPHERES ON FILES 2 (N) AND 3 (S).
C 
      IF(GRID) IBUF(6)=NLATH
      IF(ZONL) IBUF(5)=NLATH
      IBUF(7)=1 
      CALL PUTFLD2(2,GG(N),IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      IBUF(7)=2 
      CALL PUTFLD2(3,GG(1),IBUF,MAXX)
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 150 
C---------------------------------------------------------------------
 6010 FORMAT( '0',I6,'  RECORDS READ')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
