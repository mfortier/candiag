      PROGRAM A2OGRID
C     PROGRAM A2OGRID(FIELDA,         LONO,        LATO,         DAO,           D2
C    1                 BETAO,       FIELDO,       INPUT,       OUTPUT,  )       D2
C    2          TAPE1=FIELDA,   TAPE2=LONO,  TAPE3=LATO,   TAPE4=DAO,
C    3           TAPE7=BETAO, TAPE8=FIELDO, TAPE5=INPUT, TAPE6=OUTPUT)
C     ----------------------------------------------------------------          D2
C                                                                               D2
C     SEP 21/09 - D. YANG - FIXED A BUG IN '6045 FORMAT' LINE.                  D2
C     MAR 26/04 - B. MIVILLE - ADDED IF STATEMENT FOR TILE OUTPUT
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     APR 04/02 - B.MIVILLE (REVISED FOR NEW DATA DESCRIPTION FORMAT)
C     JUN 05/01 - B.MIVILLE - ORIGINAL VERSION
C                                                                               D2
CA2OGRID - CONSTRAINED INTERPOLATION FROM ATMOSPHERE TO OCEAN GRID      5  2 C  D1
C                                                                               D3
CAUTHOR  - B. MIVILLE                                                           D3
C                                                                               D3
CPURPOSE - CONSTRAINED INTERPOLATION FROM ATMOSPHERE GRID TO OCEAN GRID         D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C   FIELDA = ATMOSPHERE INPUT FIELD                                             D3
C   LONO   = OCEAN GRID LONGITUDES                                              D3
C   LATO   = OCEAN GRID LATITUDES                                               D3
C   DAO    = AREA OF OCEAN GRID CELL                                            D3
C   BETAO  = BETA OF OCEAN GRID CELL FOR TRACER POINT                           D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C   FIELDO = OCEAN INTERPOLATED AND ADJUSTED OUTPUT FIELD                       D3
C
CINPUT PARAMETERS...
C                                                                               D5
C   TYPE  = DETERMINES THE TYPE OF INTERPOLATION                                D5
C           BILIN = BILINEAR INTERPOLATION                                      D5
C           TILE  = COPY ATMOSPHERE VALUE TO ALL UNDERLYING OCEAN GRID POINTS   D5
C                                                                               D5
C   CONST = DETERMINES THE TYPE OF ADJUSTMENTS                                  D5
C           CON  =  CONSERVATIVE ADJUSTMENTS                                    D5
C           WIND =  SPECIFIES THAT THE INPUT FILE IS TO BE TREATED AS WIND      D5
C                   STRESS FIELDS                                               D5
C                =  NO CONSERVATIVE ADJUSTMENTS (LEFT BLANK, DEFAULT)           D5
C    GT   = MINIMUM FLAG                                                        D5
C  RMIN   = MINIMUM VALUE ALLOWED BY THE OCEAN OUTPUT FIELD (FOLLOWS GT)        D5
C    LT   = MAXIMUM FLAG                                                        D5
C  RMAX   = MAXIMUM VALUE ALLOWED BY THE OCEAN OUTPUT FIELD (FOLLOWS LT)        D5
C                                                                               D5
CNOTES:  GT AND LT CAN BE INTERCHANGED, THAY CAN BOTH BE THERE OR ONLY ONE OF   D5
C        THEM DEPENDING ON WHAT TYPE OF CONSTRAINT YOU WANT TO DO               D5
C        RMIN HAS TO BE EQUAL SMALLER THAN THE SMALLEST INPUT VALUE             D5
C        RMAX HAS TO BE EQUAL OR BIGGER THAN THE MAXIMUM INPUT VALUE            D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C* A2OGRID     BILIN       CON   GT        0.   LT      100.                    D5
C                                                                               D5
CNOTES: THIS PROGRAM ASSUMES THAT THERE IS AN INTEGRAL NUMBER OF OCEAN GRID     D5
C       CELLS THAT COVERS AN ATMOSPHERE CELL (1:1, 1:4, ...).                   D5
C       IT ASSUMES THAT THE ATMOSPHERE GRID IS A NORMAL GAUSSIAN GRID.          D5
C       IT ONLY WORKS ON GLOBAL FIELD WITH A CYCLIC LONGITUDE VALUE.            D5
C                                                                               D5
C-------------------------------------------------------------------------------
C
C     * MAXIMUM 2-D GRID SIZE (I+2)*(J+2)=IM*JM --> IJM
C     * FOR MAX GRID SIZE OF I*J PLUS ONE MORE ON EACH SIDE.
C
      use diag_sizes, only : SIZES_NWORDIO,
     &                       SIZES_OLAT,
     &                       SIZES_OLON

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


      PARAMETER (IM=SIZES_OLON+2,
     & JM=SIZES_OLAT+2,
     & IJM=IM*JM,
     & IJMV=IJM*SIZES_NWORDIO)
      REAL FIELDA(IJM)
      REAL AFIELD(IJM),LONO(IJM),LATO(IJM)
      REAL ELONA(IJM),ELATA(IJM)
      REAL FIELDI(IJM)
      REAL DAO(IJM),BETAO(IJM)
      REAL DAA(IJM),BETAA(IJM),BETAUV(IJM)
      REAL DLON,LONAW,LONAE
      REAL EFIELDA(IJM),RMIN,RMAX,VAL1,VAL2
      CHARACTER*2 GT,LT,SP2,THR1,THR2
      CHARACTER*5 TYPE,CONST,WIND,TILE,BILIN,CON,SP5
      INTEGER INDI(IJM),INDJ(IJM)
      INTEGER INDII(IJM),INDJJ(IJM)
C
      LOGICAL OK
      INTEGER IBUF,IDAT,MAXX
      COMMON/ICOM/IBUF(8),IDAT(IJMV)
      DATA MAXX/IJMV/
C---------------------------------------------------------------------
C
      NF=8
      CALL JCLPNT(NF,1,2,3,4,7,8,5,6)
      REWIND 1
      REWIND 2
      REWIND 3
      REWIND 4
      REWIND 7
      REWIND 8
C
      NR=0
      TILE =' TILE'
      BILIN='BILIN'
      CON  ='  CON'
      GT   ='GT'
      LT   ='LT'
      SP5  ='     '
      SP2 ='  '
      WIND =' WIND'
      MMTYPE=0
      RMIN=0.E0
      RMAX=0.E0
C
C     * READ TYPE OF CALCULATION, MINIMUM AND MAXIMUM FIELD VALUE
C     * FROM INPUT CARD
C
      READ(5,5000,ERR=900) TYPE,CONST,THR1,VAL1,THR2,VAL2                       D4
      WRITE(6,6000) TYPE,CONST,THR1,VAL1,THR2,VAL2
C
C     * VERIFY THAT TYPE AND CONST ARE COMPATIBLE AND VALID
C
      IF (TYPE.EQ.TILE) THEN
         IF(CONST.NE.SP5) THEN
            WRITE(6,6040) 'TILE CAN ONLY BE BY ITSELF'
            WRITE(6,6025) 'TYPE: ', TYPE,' CONST: ',CONST
            CALL                                   XIT('A2OGRID',-1)
         ELSEIF (THR1.NE.SP2) THEN
            WRITE(6,6040) 'TILE CAN ONLY BE BY ITSELF'
            WRITE(6,6025) 'TYPE: ', TYPE,' THR1: ',THR1
            CALL                                   XIT('A2OGRID',-2)
         ELSEIF (THR2.NE.SP2) THEN
            WRITE(6,6040) 'TILE CAN ONLY BE BY ITSELF'
            WRITE(6,6025) 'TYPE: ', TYPE,' THR2: ',THR2
            CALL                                   XIT('A2OGRID',-3)
         ENDIF
      ELSE
         IF((TYPE.NE.BILIN).AND.(TYPE.NE.TILE)) THEN
            WRITE(6,6035) 'TYPE IS NOT A VALID CHOICE,   TYPE: ',TYPE
            WRITE(6,6040) 'VALID CHOICE OF TYPE: BILIN, TILE'
            CALL                                   XIT('A2OGRID',-4)
         ENDIF
C
         IF((CONST.NE.CON).AND.(CONST.NE.SP5).AND.(CONST.NE.WIND)) THEN
            WRITE(6,6035) 'CONST IS NOT A VALID CHOICE,  CONST: '
     1           ,CONST
            WRITE(6,6035) 'VALID CHOICE OF CONST: CON',
     1           ' WIND AND BLANK FOR NO CONSERVATION'
            CALL                                   XIT('A2OGRID',-5)
         ENDIF
C
         IF((CONST.EQ.WIND).AND.(TYPE.NE.BILIN)) THEN
            WRITE(6,6015) 'NOT A VALID CHOICE, CONST: ',CONST,'
     1           TYPE: ',TYPE
            WRITE(6,6040) 'WIND CAN ONLY GO WITH BILIN'
            CALL                                   XIT('A2OGRID',-6)
         ENDIF
      ENDIF
C
      IF((THR1.EQ.THR2).AND.(THR1.NE.SP2)) CALL    XIT('A2OGRID',-7)
      IF((THR1.EQ.SP2).AND.(THR2.NE.SP2)) CALL     XIT('A2OGRID',-8)
C
C     * MMTYPE = 1
C
C
      IF(THR1.EQ.GT) THEN
         RMIN=VAL1
         MMTYPE=1
      ELSEIF(THR1.EQ.LT) THEN
         RMAX=VAL1
         MMTYPE=2
      ENDIF
C
      IF(THR2.EQ.GT) THEN
         RMIN=VAL2
         MMTYPE=1
      ELSEIF(THR2.EQ.LT) THEN
         RMAX=VAL2
         MMTYPE=2
      ENDIF
C
      IF(((THR1.EQ.GT).AND.(THR2.EQ.LT)).OR.
     1     ((THR2.EQ.GT).AND.(THR1.EQ.LT))) MMTYPE=3
C
C     * READ OCEAN LONGITUDES AND LATITUDES
C     * IF THE VARIABLE TO BE CONVERTED IS ATMOSPHERIC WIND STRESS
C     * THE OCEAN LATITUDES AND LONGITUDES HAVE TO BE THE LATH AND LONH
C
      CALL GETFLD2 (2,LONO,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) CALL                            XIT('A2OGRID',-9)
C
      IF((CONST.EQ.WIND).AND.(IBUF(3).NE.NC4TO8("LONH"))) THEN
         WRITE(6,6002) 'WRONG LONGITUDES TYPE, NEED THE U,V LONH'
         CALL                                      XIT('A2OGRID',-10)
      ENDIF
C
      NLONO=IBUF(5)
      NLATO=IBUF(6)
C
      CALL GETFLD2 (3,LATO,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) CALL                            XIT('A2OGRID',-11)
C
      IF((CONST.EQ.WIND).AND.(IBUF(3).NE.NC4TO8("LATH"))) THEN
         WRITE(6,6002) 'WRONG LATITUDES TYPE, NEED THE U,V LATH'
         CALL                                      XIT('A2OGRID',-12)
      ENDIF
C
      IF((IBUF(5).NE.NLONO).OR.(IBUF(6).NE.NLATO))
     1   CALL                                      XIT('A2OGRID',-13)
C
C     * READ IN THE OCEAN GRID CELL AREAS
C
      CALL GETFLD2 (4,DAO,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) CALL                            XIT('A2OGRID',-14)
C
C     * CHECK IF DIMENSION OF DAO ARE THE SAME AS LATO/LONO
C
      IF((IBUF(5).NE.NLONO).OR.(IBUF(6).NE.NLATO))
     1     CALL                                    XIT('A2OGRID',-15)
C
C     * READ IN OCEAN BETA, SHOULD BE AT SURFACE OR FIRST LEVEL
C
      CALL GETFLD2 (7,BETAO,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) CALL                            XIT('A2OGRID',-16)
C
C     * CHECK IF DIMENSION OF BETAO ARE THE SAME AS LATO/LONO
C
      IF((IBUF(5).NE.NLONO).OR.(IBUF(6).NE.NLATO))
     1     CALL                                    XIT('A2OGRID',-17)
C
C     * READ IN ATMOSPHERE FIELD
C
 100  CALL GETFLD2 (1,FIELDA,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
         IF (NR.EQ.0)CALL                          XIT('A2OGRID',-18)
         CALL                                      XIT('A2OGRID',0)
      ENDIF
C
C     * VERIFY IF THERE IS ANY SUPERLABEL OR CHAR TYPE.
C     * PROGRAM WILL EXIT IF IT ENCOUNTERS A SUPERLABEL OR CHAR.
C
      IF(IBUF(1).NE.NC4TO8("GRID"))THEN
         IF((IBUF(1).EQ.NC4TO8("LABL")).OR.
     +      (IBUF(1).EQ.NC4TO8("CHAR")))      THEN
            WRITE(6,6040)' *** SUPERLABELS OR CHAR ARE NOT ALLOWED ***'
            CALL                                   XIT('A2OGRID',-19)
         ELSE
            WRITE(6,6040)' *** FIELD LABEL IS NOT GRID ***'
            CALL                                   XIT('A2OGRID',-20)
         ENDIF
      ENDIF
C
      IF(NR.EQ.0) THEN
         IBUF3=IBUF(3)
         NLONA=IBUF(5)
         NLATA=IBUF(6)
         NWRDSA=(NLONA+1)*(NLATA+2)
         NWRDSO=NLONO*NLATO
C
C        * CALCULATE THE ATMOSPHERE LATITUDE ASSUMING THAT THE GRID
C        * IS A NORMAL GAUSSIAN GRID
C
         CALL ALATLON(NLONA,NLATA,ELONA,ELATA)
C
C        * VERIFY THAT OCEAN LONGITUDES FALL WITHIN THE ATMOSPHERE
C        * LONGITUDES RANGE PLUS ONE POINT TO THE EAST AND ONE TO THE
C        * WEST. ASSUMING UNIFORMLY SPACED LONGITUDES.
C
         IF(LONO(1).LT.ELONA(1).OR.LONO(NWRDSO).GT.ELONA(NWRDSA))THEN
           WRITE(6,6040) 'OCEAN LONGITUDES OUTSIDE ATMOSPHERE RANGE'
           WRITE(6,6010) 'LONO(1): ',LONO(1),
     1          '   LONO(NWRDSO): ',LONO(NWRDSO)
           CALL                                    XIT('A2OGRID',-21)
         ENDIF
C
         WRITE(6,6005)'ATMOSPHERE GRID DIMENSION: ',NLONA,NLATA
         WRITE(6,6005)'     OCEAN GRID DIMENSION: ',NLONO,NLATO
C
C        * FIND BETA FOR WIND STRESS POINTS
C
C        * IT ASSUMES THAT FOR EACH OCEAN POINT THERE ARE 4 WIND STRESS
C        * POINTS LOCATED AT THE TRACERS GRID CELL CORNERS (LONH, LATH).
C        * THIS WILL MAKE SURE THAT WIND STRESS POINTS AT THE LAND/OCEAN
C        * BORDERS ARE GIVEN A BETA OF 1.

         IF(CONST.EQ.WIND) CALL BETAWS(NLONO,NLATO,BETAO,BETAUV)
C
C        * FIND RELATIVE POSITION OF ATMOSPHERE POINTS AND OCEAN POINTS
C        * VALID FOR BOTH NORMAL CENTERED AND WIND STRESS POINTS
C
         CALL INDEXAO(ELONA,ELATA,NLONA,NLATA,LONO,LATO,NLONO,NLATO,
     1                DAO,BETAO,INDI,INDJ,INDII,INDJJ,DAA,BETAA)
C
      ELSE
C
C        * MAKE SURE LABEL IS THE SAME
C
         IF(IBUF(3).NE.IBUF3)THEN
            WRITE(6,'(A)') '*** FIELD IS NOT THE SAME ***'
            CALL                                   XIT('A2OGRID',-22)
         ENDIF
C
C        * CHECK THAT ATMOSPHERE FIELD DIMENSION HAS NOT CHANGED
C
         IF((IBUF(5).NE.NLONA).OR.(IBUF(6).NE.NLATA))THEN
            WRITE(6,6040)'* DIMENSION OF FIELD HAS CHANGED * '
            WRITE(6,6020)'* RECORD NUMBER: ',NR+1
            CALL                                   XIT('A2OGRID',-23)
         ENDIF
      ENDIF
C
C     * CHECK TYPE
C
      IF(TYPE.EQ.TILE) THEN
C
C        * DO A DIRECT EQUAL COPY OF ATMOSPHERE VALUE TO OCEAN VALUE
C
         CALL TILES(FIELDA,NLONA,NLATA,INDII,INDJJ,NLONO,
     1        NLATO,BETAO,AFIELD)
C
      ELSE
C
C        * DO THE INTERPOLATION
C
         IF(CONST.EQ.WIND)THEN
            CALL BVINTRP(FIELDA,NLONA,NLATA,ELONA,ELATA,INDI,INDJ,NLONO,
     1           NLATO,LONO,LATO,DAO,BETAUV,BETAA,DAA,FIELDI,EFIELDA)
         ELSE
            CALL BVINTRP(FIELDA,NLONA,NLATA,ELONA,ELATA,INDI,INDJ,NLONO,
     1           NLATO,LONO,LATO,DAO,BETAO,BETAA,DAA,FIELDI,EFIELDA)
         ENDIF
C
C        * DO THE ADJUSTMENTS TO THE ORIGNAL ATMOSPHERE VALUES
C
         IF((CONST.NE.SP5).AND.(CONST.NE.WIND)) THEN
            CALL GRIDCON(EFIELDA,FIELDI,NLONO,NLATO,NLONA,NLATA,DAO,
     1                   BETAO,DAA,BETAA,INDII,INDJJ,AFIELD)
         ENDIF
C
C        * MAKE SURE THE MIN/MAX ARE RESPECTED
C
         IF((MMTYPE.EQ.1).OR.(MMTYPE.EQ.2).OR.
     1        (MMTYPE.EQ.3))THEN
C
C           * CHECK IF INPUT ATMOSPHERE FIELD IS WITHIN MIN/MAX
C
C           * TAKING INTO ACCOUNT THAT THERE ARE NLONA + 1 POINTS AND
C           * NLATA + 2 POINTS IN THE ATMOSPHERE POINTS
C
            DO 120 L=1,NWRDSA
               IF(BETAA(L).NE.0.E0)THEN
                  IF((EFIELDA(L).LT.RMIN).AND.(MMTYPE.EQ.1))THEN
                     WRITE(6,6035) ' INPUT FIELD LOWER THAN MIN'
                     WRITE(6,6045) ' MIN=',RMIN,'  ATMOS FIELD='
     1                    ,EFIELDA(L)
                     CALL                          XIT('A2OGRID',-24)
                  ELSEIF((EFIELDA(L).GT.RMAX).AND.(MMTYPE.EQ.2))THEN
                     WRITE(6,6035) ' INPUT FIELD GREATER THAN MAX'
                     WRITE(6,6045) ' MAX=',RMAX,'  ATMOS FIELD='
     1                    ,EFIELDA(L)
                     CALL                          XIT('A2OGRID',-25)
                  ELSEIF((EFIELDA(L).LT.RMIN).OR.(EFIELDA(L).GT.RMAX)
     1                   .AND.(MMTYPE.EQ.3))THEN
                     WRITE(6,6035) ' INPUT FIELD GREATER THAN MIN/MAX'
                     WRITE(6,6045) ' MIN=',RMIN,' MAX=',RMAX,
     1                             '  ATMOS FIELD=',EFIELDA(L)
                     CALL                          XIT('A2OGRID',-26)
                  ENDIF
               ENDIF
 120        CONTINUE
C
            CALL LIMITAO(EFIELDA,AFIELD,NLONO,NLATO,NLONA,NLATA,DAO,
     1           BETAO,DAA,INDII,INDJJ,RMIN,RMAX,MMTYPE)
         ENDIF
C
      ENDIF
C
C     * WRITE RESULTS OCEAN INTERPOLATED/ADJUSTED FIELD TO FILE
C
      IBUF(5)=NLONO
      IBUF(6)=NLATO
      IF(((CONST.EQ.SP5).OR.(CONST.EQ.WIND)).AND.(TYPE.NE.TILE))THEN
         CALL PUTFLD2(8,FIELDI,IBUF,MAXX)
      ELSE
         CALL PUTFLD2(8,AFIELD,IBUF,MAXX)
      ENDIF
C
      NR = NR + 1
C
      GOTO 100
C
 900  CALL                                         XIT('A2OGRID',-27)
C
C----------------------------------------------------------------------
 5000 FORMAT(10X,5X,A5,5X,A5,3X,A2,E10.0,3X,A2,E10.0)                           D4
 6000 FORMAT(10X,5X,A5,5X,A5,3X,A2,E12.5,3X,A2,E12.5)
 6002 FORMAT(1X)
 6005 FORMAT(A,2I5)
 6010 FORMAT(A,E12.5,A,E12.5)
 6015 FORMAT(4A)
 6020 FORMAT(A,I8)
 6025 FORMAT(A,I8,A,I8)
 6035 FORMAT(2A)
 6040 FORMAT(A)
 6045 FORMAT(A,E12.5,A,E12.5)
      END
