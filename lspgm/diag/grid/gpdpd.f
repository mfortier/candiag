      PROGRAM GPDPD 
C     PROGRAM GPDPD (GPSHUM,       GPTEMP,       GPES,      INPUT,              D2
C    1                                                      OUTPUT,     )       D2
C    2        TAPE11=GPSHUM,TAPE12=GPTEMP,TAPE13=GPES,TAPE5=INPUT,
C    3                                                TAPE6=OUTPUT) 
C     -------------------------------------------------------------             D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     JAN 15/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)               
C     MAY 11/83 - R.LAPRISE.                                                    
C     DEC 04/80 - J.D.HENDERSON 
C                                                                               D2
CGPDPD   - COMPUTES DEW POINT DEPRESSION FROM T,SHUM                    2  1    D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - CONVERTS PRESSURE LEVEL GRID FILES OF TEMPERATURE AND                D3
C          SPECIFIC HUMIDITY TO DEW POINT DEPRESSION.                           D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C      GPSHUM = PRESSURE LEVEL GRIDS OF SPECIFIC HUMIDITY.                      D3
C      GPTEMP = PRESSURE LEVEL GRIDS OF TEMPERATURE (DEG K).                    D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      GPES   = PRESSURE LEVEL GRIDS OF DEW POINT DEPRESSION.                   D3
C-------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_PLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/F(SIZES_LONP1xLAT),G(SIZES_LONP1xLAT) 
C 
      LOGICAL OK,SPEC 
      INTEGER LEV(SIZES_PLEV) 
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_LONP1xLATxNWORDIO)
      DATA A,B,EPS1,EPS2/ 21.656E0, 5423.E0, 0.622E0, 0.378E0 / 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_PLEV/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,11,12,13,6) 
      DO 110 N=11,13
  110 REWIND N
C 
C     * GET LEVELS FROM SHUM FILE. STOP IF COMPLEX. 
C 
      CALL FILEV(LEV,NLEV,IBUF,11)
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('GPDPD',-1)
      WRITE(6,6005) (LEV(L),L=1,NLEV) 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC").OR.KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC) CALL                                XIT('GPDPD',-2)
      NWDS=IBUF(5)*IBUF(6)
C 
C     * LEVEL LOOP OVER ALL MOISTURE LEVELS.
C 
      NRECS=0 
  160 DO 410 L=1,NLEV 
C 
C     * GET THE NEXT PAIR OF SHUM,T FIELDS. 
C 
      CALL GETFLD2(11,F,KIND,-1,NC4TO8("SHUM"),LEV(L),IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NRECS.EQ.0)THEN
          CALL                                     XIT('GPDPD',-3)
        ELSE
          WRITE(6,6010) NRECS 
          CALL                                     XIT('GPDPD',0) 
        ENDIF 
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
C 
      CALL GETFLD2(12,G,KIND,IBUF(2),NC4TO8("TEMP"),LEV(L),JBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6010) NRECS 
        CALL                                       XIT('GPDPD',-4)
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6025) JBUF 
C 
C     * CHECK THAT FIELDS ARE SAME SIZE.
C 
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK)THEN 
        WRITE(6,6025) IBUF,JBUF 
        CALL                                       XIT('GPDPD',-5)
      ENDIF 
C 
C     * COMPUTE DEW POINT DEPRESSION. 
C     * NOTE - IF Q IS ZERO IT IS SET TO 1.E-6. 
C 
      CALL LVDCODE(PRES,LEV(L),1)
      DO 310 I=1,NWDS 
      T=G(I)
      Q=F(I)
      IF(Q.LE.0.E0) Q=3.E-6 
      E=Q*PRES/(Q*EPS2+EPS1)
      TD=B/(A-LOG(E))
      F(I)=T-TD 
  310 CONTINUE
C 
C     * SAVE DEW POINT DEPRESSION ON FILE GPES. 
C 
      IBUF(3)=NC4TO8("  ES")
      CALL PUTFLD2(13,F,IBUF,MAXX) 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
      NRECS=NRECS+1 
  410 CONTINUE
      GO TO 160 
C---------------------------------------------------------------------
 6005 FORMAT('0LEVELS',20I6/(7X,20I6))
 6010 FORMAT(' ',I6,' RECORDS READ')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
