      PROGRAM BUDPLT6
C     PROGRAM BUDPLT6 (I1 ,       I2 ,       I3,                                A2
C                      I4 ,       I5 ,       I6,                                A2
C                      I7 ,       I8 ,       I9,                                A2
C                      I10,       I11,       I12,                               A2
C                      I13,       I14,       I15,                               A2
C                               INPUT,      OUTPUT,                     )       A2
C    1          TAPE11=I1 ,TAPE12=I2 ,TAPE13=I3,
C    2          TAPE14=I4 ,TAPE15=I5 ,TAPE16=I6,
C    2          TAPE17=I7 ,TAPE18=I8 ,TAPE19=I9,
C    2          TAPE20=I10,TAPE21=I11,TAPE22=I12,
C    2          TAPE23=I13,TAPE24=I14,TAPE25=I15,
C                         TAPE5=INPUT,TAPE6=OUTPUT)
C     ---------------------------------------------                             A2
C                                                                               A2
C     AUG 23/07 - B.MIVILLE - UPDATED LOOK OF DIAGRAM                           A2
C     MAR 30/96 - F.ZWIERS, T. GUI.                                             A2
C                                                                               A2
CBUDPLT6 - BUDGET PLOTS BASED ON 15 GLOBAVG INPUT VALUES               15  1 C  A1
C                                                                               A3
CAUTHORS - F.ZWIERS, T. GUI                                                     A3
C                                                                               A3
CPURPOSE - READ 15 CCRN STANDARD BINARY FILES OF GLOBAVG BUDGET VALUES          A3
C          AND GENERATE BUDGET PLOT.                                            A3
C                                                                               A3
CINPUT FILE...                                                                  A3  
C                                                                               A3
C      I1 ... I15 = BINARY STANDARD CCRN FILES OF GLOBAL AVERAGES.              A3
C                                                                               A3
CGRAPH DESCRIPTION                                                              A3
C                                             Gz                     Dz         A3 
C                                               \                   /           A3
C                                      * 6- box  Az  ----->----   Kz            A3
C                                                 | \            / |            A3
C                                                 |  \  Gs   Ds /  |            A3
C                                                 |   \/      \/   |            A3
C                                                 v   As -->- Ks   ^            A3
C                                                 |  /          \  |            A3
C                                                 | /            \ |            A3
C                                                At  -----> ----  Kt            A3
C                                               /                   \           A3
C                                             Gt                     Dt         A3
C                                                                               A3
CINPUT PARAMETER...                                                             A5
C                                                                               A5
C      CARD 1- MAIN CARD                                                        A5
C                                                                               A5
C      READ(5,5010) (W(J),J=1,6), IBC                                           A5
C 5010 FORMAT(10X,7I5)                                                          A5
C                                                                               A5
C      W(I)       COLOR OF THE 6 BOXES                                          A5
C                 DEFAULT IS WHITE (BLANK OR ZERO)                              A5
C                 THE COLOR OF THE FIRST BOX IS THE DEFAULT COLOR OF ALL BOXES  A5
C                 IF OTHER BOXES ARE NOT SPECIFIED.                             A5
C      IBC        BOX AROUND VALUES                                             A5
C                 = 0 NO BOX (DEFAULT)                                          A5
C                 = 1 BOX                                                       A5
C                                                                               A5
C      CARD 2- TITLE                                                            A5
C                                                                               A5
C      READ(5,5020) NTIT                                                        A5
C 5020 FORMAT(80A1)                                                             A5
C                                                                               A5
C      NTIT       TITLE OF DIAGRAM (80 CHARACTERS MAXIMUM)                      A5
C                                                                               A5
CEXAMPLE OF INPUT CARD...                                                       A5 
C*BUDPLT6    180  160  160  160  170  150    1                                  A5
C*Main Title                                                                    A5
C-------------------------------------------------------------------------------
C
C     * INTERNAL COMMENT...
C
C     THE VALUE IS DEFINED AS BELOW:
C     Az - WORD(1)
C     Kz - WORD(2)
C     At - WORD(3)
C     Kt - WORD(4)
C     As - WORD(5)
C     Ks - WORD(6)
C     Az->Kz - WORD(7)
C     Az->At - WORD(8) 
C     At->Kt - WORD(9)
C     Kt->Kz - WORD(10)
C     Az->As - WORD(11)
C     As->At - WORD(12)
C     Ks->Kt - WORD(13)
C     Ks->Kz - WORD(14)
C     As->Ks - WORD(15)
C     Gs->As - WORD(16)
C     Ks->Ds - WORD(17)
C     Gz->Az - WORD(18)     
C     Kz->Dz - WORD(19)
C     Gt->At - WORD(20)
C     Kt->Dt - WORD(21)
C     (1) TO (6) USES F4.1 FORMAT 
C     (7) TO (21) USES F5.2 FORMAT
C     
C-------------------------------------------------------------------------
C
C     * Font size for the numbers in the boxes and beside the arrows
C     * and for the characters beside the boxes and arrows  
C

      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_MAXBLONP1BLAT,
     &                       SIZES_NWORDIO
      integer, parameter :: MAXX 
     & = max(SIZES_BLONP1,SIZES_BLAT)*SIZES_NWORDIO

      PARAMETER (BOXNO=.016, ARRNO=.014, BCHAR=.020, SCHAR=.011)
      PARAMETER (TCHAR=.014)
      PARAMETER (RBM=0.40)
C
C     * Scaling Factor for Jm^-2
C
      PARAMETER (SCALE=100000.) 
C
C     * Size of each box and defining the areas of title and comment.  
C
      PARAMETER (SZX=.14, SZY=.12, Y0=.08, Y1=.08)
C
C     * Define variables of the 21-word binary data and 8-word labels.
C
      COMMON/F/F(SIZES_MAXBLONP1BLAT)
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
C
C     * Store data, position and title.
C
      REAL WORD(21), RBM
      REAL WORD1,WORD2,WORD3,WORD4,WORD5,WORD6 
      REAL LBOXPS,RBOXPS,LBOXST,RBOXST,LBOXED,RBOXED           
      CHARACTER*10 CS1,CS2,CS3,CS4,CS5,CS6
      CHARACTER*11 CS16,CS17
      CHARACTER*1 NTIT(80),NT(85)
      CHARACTER*80 TITLE 
      CHARACTER*85 MT
      CHARACTER*6 CS016,CS017
C
      LOGICAL OK,OK1,OK2,OK3,OK4,OK5,OK6,OK7,OK8
      LOGICAL OK9,OK10,OK11,OK12,OK13,OK14,OK15 
      INTEGER MO,MOM,IBC
C
C     * Array W stores hafton color values of 6 boxes read from input cards
C     * KT are the actual length of main title excluding space        
C     * on both sides. 
C
      INTEGER W(6), FILPAT(200), KT, K1, K2    
C
      REAL HSVV(3,25) 
C
      EQUIVALENCE(TITLE,NTIT),(MT,NT)
C
C     * Color value array.
C
      DATA (FILPAT(I), I=100, 199)
     +              /  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 
     2                12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 
     3                22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 
     4                32, 33, 34, 35, 36, 37,  0,  0,  0,  0, 
     5                42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 
     6                52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 
     7                62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
     8                72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 
     9                82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 
     +                92, 93, 94, 95, 96, 97, 98, 99, 100, 101 /
  
C-------------------------------------------------------------------------
C 
C    * Import data file
C 
      NFF=17
      CALL JCLPNT(NFF,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,5,6)
      CALL PSTART
      REWIND 11
      REWIND 12
      REWIND 13
      REWIND 14
      REWIND 15
      REWIND 16
      REWIND 17
      REWIND 18
      REWIND 19
      REWIND 20
      REWIND 21
      REWIND 22
      REWIND 23
      REWIND 24
      REWIND 25
      NRECS=0
C
C     * Loop for reading records and drawing graphs
C
 100  CONTINUE
C
C     * Read from input card the parameters.
C
      READ(5,5010,END=901) (W(J),J=1,6), IBC
C
C     * Make W(1) default colour
C
      IF (W(2).EQ.0) W(2)=W(1)
      IF (W(3).EQ.0) W(3)=W(1)
      IF (W(4).EQ.0) W(4)=W(1)
      IF (W(5).EQ.0) W(5)=W(1)
      IF (W(6).EQ.0) W(6)=W(1)
C
C     * COLOR 
C
      NCLRS=-6
      CALL DFCLRS(NCLRS,HSVV,W)
C
C     * Read main title
C
      READ(5,5020,END=901) NTIT 
C
C     * Take away spaces in the both sides of title
C
      CALL SHORTN(NTIT,80,.TRUE.,.TRUE.,KT) 
C
C     * Read in global averages from 15 files.  
C
      CALL GLOAVG(11,WORD(1),MAXX,OK1)
      CALL GLOAVG(12,WORD(2),MAXX,OK2)
      CALL GLOAVG(13,WORD(3),MAXX,OK3)
      CALL GLOAVG(14,WORD(4),MAXX,OK4)
      CALL GLOAVG(15,WORD(5),MAXX,OK5)
      CALL GLOAVG(16,WORD(6),MAXX,OK6)
      CALL GLOAVG(17,WORD(7),MAXX,OK7)
      CALL GLOAVG(18,WORD(8),MAXX,OK8)
      CALL GLOAVG(19,WORD(9),MAXX,OK9)
      CALL GLOAVG(20,WORD(10),MAXX,OK10)
      CALL GLOAVG(21,WORD(11),MAXX,OK11)
      CALL GLOAVG(22,WORD(12),MAXX,OK12)
      CALL GLOAVG(23,WORD(13),MAXX,OK13)
      CALL GLOAVG(24,WORD(14),MAXX,OK14)
      CALL GLOAVG(25,WORD(15),MAXX,OK15)
      OK=OK1.AND.OK2.AND.OK3.AND.OK4
     1     .AND.OK5.AND.OK6.AND.OK7.AND.OK8
     2     .AND.OK9.AND.OK10.AND.OK11.AND.OK12
     3     .AND.OK13.AND.OK14.AND.OK15
      IF(.NOT.OK)THEN
         WRITE(6,6000) NRECS
         IF(NRECS.EQ.0)THEN
            CALL                                   PXIT('BUDPLT6',-1) 
         ELSE
            CALL                                   PXIT('BUDPLT6',0) 
         ENDIF
      ENDIF
C     
C     * Compute global averages from record 16 to 21.
C
      WORD(16)=WORD(15)+WORD(12)-WORD(11)
      WORD(17)=WORD(15)-WORD(14)-WORD(13)
      WORD(18)=WORD(7)+WORD(8)+WORD(11)
      WORD(19)=WORD(7)+WORD(14)+WORD(10)
      WORD(20)=WORD(9)-WORD(8)-WORD(12) 
      WORD(21)=WORD(9)+WORD(13)-WORD(10)
C
C     * Use the Complex character set of PLOTCHAR.
C
      CALL PCSETI('CC',1)
C
C     * Define some color indices and default drawing conditions.
C
      CALL GSCR(1,1,0.,0.,0.) 
      CALL GSCR(1,0,1.,1.,1.)
      CALL GSTXCI(1) 
      CALL GSPMCI(1) 
      CALL GSPLCI(1) 
      CALL GSLN(1)
C
C     * Compute the position of left and right box in x-axis.
C
      LBOXST = SZX*0.5
      RBOXST = SZX*5.5 
      LBOXPS = SZX*1.0
      RBOXPS = SZX*6.0
      LBOXED = SZX*1.5
      RBOXED = SZX*6.5
C
C     * Drawing the color boxes
C
      MO=1 
      DO 400 J=6,2,-4  
         Y = Y0+(J-1)*(SZY)
         DO 300  I=1,2,1
            IF (I.EQ.1) X = LBOXST
            IF (I.EQ.2) X = RBOXST
            IF(W(MO).GE.100.AND.W(MO).LE.199) THEN
              MOM=W(MO)-98
            ELSE
             IF(W(MO).GE.350.AND.W(MO).LE.420) MOM=W(MO)-248
            ENDIF
            CALL GSFACI(MOM)
            MO=MO+1
            CALL DRBOX(X,Y,SZX,SZY,0) 
 300     CONTINUE
 400  CONTINUE
      Y = Y0+(4-1)*(SZY)
      DO 450 I=3,5,2
         X = (I-1)*(SZX)
         IF(W(MO).GE.100.AND.W(MO).LE.199) THEN
          MOM=W(MO)-98
         ELSE
          IF(W(MO).GE.350.AND.W(MO).LE.420) MOM=W(MO)-248
         ENDIF
         CALL GSFACI(MOM)
         MO=MO+1
         CALL DRBOX(X,Y,SZX,SZY,0) 
 450  CONTINUE
C     
C     * Draw the border lines of color squares
C 
      CALL SETUSV('LW',4000) 
      CALL GSPLCI(1)
      DO 600 J=6,2,-4 
         Y = Y0+(J-1)*(SZY)
         DO 500 I=1,2,1 
            IF (I.EQ.1) THEN
             X = LBOXST
            ELSE
             IF (I.EQ.2) X = RBOXST
            ENDIF
            CALL LINE(X,Y,X+SZX,Y) 
            CALL LINE(X,Y,X,Y+SZY)
            CALL LINE(X+SZX,Y,X+SZX,Y+SZY)
            CALL LINE(X,Y+SZY,X+SZX,Y+SZY)
 500     CONTINUE
 600  CONTINUE
      Y = Y0+(4-1)*(SZY)
      DO 650 I=3,5,2
         X = (I-1)*(SZX)
         CALL LINE(X,Y,X+SZX,Y) 
         CALL LINE(X,Y,X,Y+SZY)
         CALL LINE(X+SZX,Y,X+SZX,Y+SZY)
         CALL LINE(X,Y+SZY,X+SZX,Y+SZY)
 650  CONTINUE
C     
C     * Plot labels.
C
      CALL GSPLCI(1)
      CALL GSPMCI(1)
      CALL GSFACI(1)
      CALL GSTXCI(1) 
      CALL PCSETI('CC',1)
C     
C     * Plot the title
C
      WRITE(MT,1008) TITLE(4:KT)
      CALL SHORTN(NT,85,.TRUE.,.TRUE.,KT)
      CALL PLCHHQ(.5,.96,MT(4:KT),TCHAR,0.,0.) 
C     
C     * Plot the comment
C
      CALL PLCHHQ(.5,.07, ":F21: 10:S:5:N: Jm:S:-2",SCHAR,0.,0.)
      CALL PLCHHQ(.52,.03, ":F21:Wm:S:-2",SCHAR,0.,0.)
      CALL LINE(.42,.075,.44,.075)
      CALL LINE(.42,.055,.44,.055)
      CALL LINE(.42,.055,.42,.075)
      CALL LINE(.44,.055,.44,.075)
      CALL ARDBD4(.46,.03,.38,.03)
      IF(IBC.EQ.1) THEN
         CALL LINE(.42,.04,.44,.04)
         CALL LINE(.42,.02,.44,.02)
         CALL LINE(.42,.04,.42,.02)
         CALL LINE(.44,.04,.44,.02)
         CALL GSFACI(0)
         CALL DRBOX(.42,.02,.02,.02,0) 
      ENDIF
C     
C     * Plot the characters besides boxes.
C
      CALL PLCHHQ(LBOXST-0.04,Y0+5.5*SZY,':F21:A:B1:Z',BCHAR,0.,0.) 
      CALL PLCHHQ(6.5*SZX+0.02,Y0+5.5*SZY,':F21:K:B1:Z',BCHAR,0.,0.)
      CALL PLCHHQ(LBOXST-0.04,Y0+1.5*SZY,':F21:A:B1:T',BCHAR,0.,0.)
      CALL PLCHHQ(6.5*SZX+0.02,Y0+1.5*SZY,':F21:K:B1:T',BCHAR,0.,0.)
      CALL PLCHHQ(SZX*2-0.04,Y0+3.5*SZY,':F21:A:B1:S',BCHAR,0.,0.)
      CALL PLCHHQ(SZX*5+0.02,Y0+3.5*SZY,':F21:K:B1:S',BCHAR,0.,0.) 
C
C     * Plot the 21 word records in the graph.
C
      WORD1 = WORD(1) / SCALE
      WORD2 = WORD(2) / SCALE
      WORD3 = WORD(3) / SCALE
      WORD4 = WORD(4) / SCALE
      WORD5 = WORD(5) / SCALE
      WORD6 = WORD(6) / SCALE 
      WRITE (CS1,1006) WORD1 
      WRITE (CS2,1006) WORD2 
      WRITE (CS3,1006) WORD3 
      WRITE (CS4,1006) WORD4 
      WRITE (CS5,1006) WORD5 
      WRITE (CS6,1006) WORD6 
      CALL PLCHHQ(LBOXPS,Y0+5.5*SZY,CS1,BOXNO,0.,0.) 
      CALL PLCHHQ(RBOXPS,Y0+5.5*SZY,CS2,BOXNO,0.,0.) 
      CALL PLCHHQ(LBOXPS,Y0+1.5*SZY,CS3,BOXNO,0.,0.) 
      CALL PLCHHQ(RBOXPS,Y0+1.5*SZY,CS4,BOXNO,0.,0.) 
      CALL PLCHHQ(SZX*2.5,Y0+3.5*SZY,CS5,BOXNO,0.,0.) 
      CALL PLCHHQ(SZX*4.5,Y0+3.5*SZY,CS6,BOXNO,0.,0.) 
C
      CALL SETCBOX(RBM,IBC)
      CALL ARDBD1(LBOXED,Y0+5.5*SZY,RBOXST,Y0+5.5*SZY,WORD(7)) 
      CALL ARDBD1(LBOXPS,Y0+5*SZY,LBOXPS,Y0+2*SZY,WORD(8)) 
      CALL ARDBD1(LBOXED,Y0+1.5*SZY,RBOXST,Y0+1.5*SZY,WORD(9))
      CALL ARDBD1(RBOXPS,Y0+2*SZY,RBOXPS,Y0+5*SZY,WORD(10)) 
      CALL ARDBD1(LBOXED,Y0+5*SZY,2.0*SZX,Y0+4*SZY,WORD(11)) 
      CALL ARDBD1(2.0*SZX,Y0+3*SZY,LBOXED,Y0+2*SZY,WORD(12)) 
      CALL ARDBD1(SZX*5.0,Y0+3*SZY,RBOXST,Y0+2*SZY,WORD(13)) 
      CALL ARDBD1(SZX*5.0,Y0+4*SZY,RBOXST,Y0+5*SZY,WORD(14))
      CALL ARDBD1(SZX*3,Y0+3.5*SZY,SZX*4,Y0+3.5*SZY,WORD(15))
      CALL PCSETI ('BF - BOX FLAG',0)
      CALL PLCHHQ(SZX*2.9,4.9*SZY+Y0,':F21:G:B1:S',BCHAR,0.,0.)
      CALL PLCHHQ(SZX*4.0,4.9*SZY+Y0,':F21:D:B1:S',BCHAR,0.,0.)
C     * Adding leading zero (Will not need this in F90)
      IF(WORD(16).GT.0..AND.WORD(16).LT.1.) THEN
         WRITE(CS16,1007) WORD(16) 
      ELSEIF(WORD(16).GT.-1..AND.WORD(16).LT.0.) THEN
         WRITE(CS16,1009) WORD(16)*(-1.0)
      ELSEIF(WORD(16).EQ.0.) THEN
         WRITE(CS016,1011)
      ELSE
         WRITE(CS16,1013) WORD(16)
      ENDIF
      IF(WORD(17).GT.0..AND.WORD(17).LT.1.) THEN
         WRITE(CS17,1007) WORD(17) 
      ELSEIF(WORD(17).GT.-1..AND.WORD(17).LT.0.) THEN
         WRITE(CS17,1009) WORD(17)*(-1.0)
      ELSEIF(WORD(17).EQ.0.) THEN
         WRITE(CS017,1011)
      ELSE
         WRITE(CS17,1013) WORD(17)
      ENDIF
C     * Inner dash lines
      X1=3.3*SZX
      Y11=Y0+5*SZY
      X2=3.0*SZX
      Y2=Y0+4*SZY
      XC=(X1+X2)/2.
      YC=(Y11+Y2)/2.
      CALL ARDBD3(X1,Y11,X2,Y2) 
      CALL SETCBOX(RBM,IBC)
      IF(WORD(16).EQ.0.) THEN
         CALL PLCHHQ(XC,YC,CS016,ARRNO,0.,0.) 
      ELSE
         CALL PLCHHQ(XC,YC,CS16,ARRNO,0.,0.)
      ENDIF
      X1=4.0*SZX
      Y11=Y0+4*SZY
      X2=3.7*SZX
      Y2=Y0+5*SZY
      XC=(X1-X2)/2.+X2
      YC=(Y2-Y11)/2.+Y11
      CALL ARDBD3(X1,Y11,X2,Y2)
      IF(WORD(17).EQ.0.) THEN
         CALL PLCHHQ(XC,YC,CS017,ARRNO,0.,0.)
      ELSE
         CALL PLCHHQ(XC,YC,CS17,ARRNO,0.,0.)
      ENDIF
      CALL PCSETI ('BF - BOX FLAG',0)
C     *  UPPER LEFT DASH LINE
      CALL ARDBD3(.0,1.-Y1,LBOXST,1.-SZY-Y1)
C     * UPPER RIGHT DASH LINE
      CALL ARDBD3(6.5*SZX,1.-SZY-Y1,7*SZX,1.-Y1)
C     * LOWER LEFT DASH LINE
      CALL ARDBD3(.0,Y0,LBOXST,Y0+SZY) 
C     * LOWER RIGHT DASH LINE
      CALL ARDBD3(6.5*SZX,Y0+SZY,7.*SZX,Y0) 
      CALL SETCBOX(RBM,IBC)
      CALL ARDBD2(.0,1.-Y1,LBOXST,1.-SZY-Y1,WORD(18),'G','Z')
      CALL SETCBOX(RBM,IBC)
      CALL ARDBD2(6.5*SZX,1.-SZY-Y1,7.*SZX,1.-Y1,WORD(19),'D','Z') 
      CALL SETCBOX(RBM,IBC)
      CALL ARDBD2(.0,Y0,LBOXST,Y0+SZY,WORD(20),'G','T') 
      CALL SETCBOX(RBM,IBC)
      CALL ARDBD2(6.5*SZX,Y0+SZY,7.*SZX,Y0,WORD(21),'D','T')
C
        CALL FRAME
C
        NRECS=NRECS+1
      GO TO 100
  901 CONTINUE
      CALL                                         PXIT('BUDPLT6',0)
C-------------------------------------------------------------------------
 1006 FORMAT(':F21:',F5.1)
 1007 FORMAT(':F21:0',F3.2)
 1008 FORMAT(':F21:',A)
 1009 FORMAT(':F21:-0',F3.2)
 1011 FORMAT(':F21:0')
 1013 FORMAT(':F21:',F5.2)
 5010 FORMAT(10X,7I5)
 5020 FORMAT(80A1)
 6000 FORMAT('BUDPLT6 CONVERTED ',I5,' RECORDS') 
      END
