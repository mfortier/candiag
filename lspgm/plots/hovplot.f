      PROGRAM HOVPLOT
C     PROGRAM HOVPLOT (Z,       INPUT,       OUTPUT,                    )       A2
C    1           TAPE1=Z, TAPE5=INPUT, TAPE6=OUTPUT)
C     ----------------------------------------------                            A2
C
C     AUG 23/07 - B.MIVILLE (ARBITRARY CONTOURS, 28 COLOURS, NEW LEGEND)        A2
C     MAY 13/04 - F.MAJAESS (ENSURE "ITYPE" CHECK IS DONE AS INTEGER)           
C     MAR 21/05 - F.MAJAESS (REVISE THE LOGIC CONTROL IN READING THE RECORDS)   
C     DEC 31/97 - R. TAYLOR (CLEAN UP GGPLOT UPGRADE, FIX BUGS,TEST,TEST,TEST)
C     AUG 31/96 - T. GUI (UPDATE TO NCAR V3.2)                                  
C     JAN 06/97 - F.MAJAESS (REVISE TO ALLOW PLOTTING LONG TIME SERIES)         
C     MAY 15/96 - F.MAJAESS (REVISE TO ALLOW PLOTTING ZERO CONTOUR LINE AND     
C                            MAKE "ICOSH" SWITCH OPTIONS CONSISTENT WITH        
C                            GGPLOT)                                            
C     MAY 07/96 - F.MAJAESS (REVISE DFCLRS CALL)
C     MAR 11/96 - F.MAJAESS (ISSUE WARNING WHEN PACKING DENSITY <= 0)           
C     SEP 21/93 - M. BERKLEY (changed to save data files instead of
C                             generating meta code.)
C     SEP 21/93 - M. BERKLEY (added CHAR PLTNAM*8 definition)
C     SEP 21/93 - M. BERKLEY (changed Holleriths to strings)
C     SEP 08/93 - J.FYFE    (ADD ODD/EVEN FUNCTINALITY, INCREASE # OF PATTERNS)
C     SEP  1/92 - T. JANG   (add format to write to unit 44)
C     JUL 20/92 - T. JANG   (changed variable "MAX" to "MAXX" so not to
C                            conflict with the generic function "MAX")
C     AUG 26/91 - S. WILSON (ADD COLOUR SHADING AND CONVERT TO AUTOGRAPH)
C     DEC 31/90 - F. ZWIERS (CHANGES TO PLOT STYLE)
C     JUL 20/90 - F. MAJAESS (PUT BACK MISSING "REWIND 1" STATEMENT)
C     APR  1/90 - C.BERGSTEIN (INCLUDE FT44 (AAASOVL) TO CONTROL OVERLAYS,
C                              SET UP SHADING OPTION, ADJUST LINE WIDTHS  )
C     JAN 31/90 - F.ZWIERS (UPDATE TO NCAR V2.0)
C     APR 14/88 - F. MAJAESS (REPLACE THE CALL TO MOVLEV SUBROUTINE
C                             BY A CALL TO THE CRAY SUBROUTINE SCOPY)
C     APR 07/86 - F.ZWIERS   (HOVMUELLER DIAGRAM ONLY - READ IN A TIME
C                             SERIES OF ZONAL MEANS OR SLICES)
C                                                                               A2
CHOVPLOT - PRODUCES HOVMUELLER DIAGRAMS OF ZONAL PROFILES ON A                  A1
C          SINGLE LEVEL                                                 1  1 C  A1
C                                                                               A3
CAUTHOR  - F. ZWIERS                                                            A3
C                                                                               A3
CPURPOSE - PRODUCE HOVMUELLER DIAGRAMS OF A TIME SERIES OF ZONAL PROFILES       A3
C          ON A SINGLE LEVEL.                                                   A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      Z = FILE OF TIME SERIES OF SLICES ON A SINGLE LEVEL                      A3
C
CINPUT PARAMETERS...
C                                                                               A5
C      CARD 1                                                                   A5
C      ------                                                                   A5
C      READ(5,5010,END=112) SCAL,FLO,HI,FINC,AR,NXLAB,NYLAB,NXTIC,NYTIC,        A5
C     1                     ICOSH,MS,LHI                                        A5
C 5010 FORMAT(10X,5E10.0,1X,2I2,2I5,I1,2I2)                                     A5
C                                                                               A5
C      SCAL  = SCALING FACTOR. IF SCAL = 0. THEN SCAL,FLO,HI AND                A5
C              FINC ARE CHOSEN BY THE PROGRAM.                                  A5
C      FLO   = LOWEST VALUE TO BE CONTOURED                                     A5
C      HI    = HIGEST VALUE TO BE CONTOURED                                     A5
C      FINC  = CONTOUR INTERVAL (A MAX. OF 40 CONTOURS IS PERMITTED)            A5
C              IF <  0 WILL DO ARBITRARY CONTOUR LINES (SCALE .NE. 0) AND       A5
C                      ABS(FINC) WILL EQUAL THE NUMBER OF CONTOURS. AN EXTRA    A5
C                      INPUT CARD WILL BE READ AFTER THE SHADING ZLEV CONTOUR   A5
C                      VALUES IF PRESENT. (MAXIMUM 28 ABRBITRARY CONTOURS)      A5
C      AR    = ASPECT RATIO = LENGTH OF Y-AXIS / LENGTH OF XAXIS                A5
C              IF AR<=0 THE DEFAULT PLOT IS DRAWN WITH ASPECT RATIO             A5
C              DETERMINED FROM THE LENGTH OF RECORD AND NUMBER OF               A5
C              RECORDS                                                          A5
C      NXLAB = NO LONGER USED - TICKS AUTOMATICALLY HANDLED BY AUTOGRAPH.       A5
C              (USED TO BE NUMBER OF POINTS ON THE X-AXIS WHICH ARE LABELED)    A5
C      NYLAB = NO LONGER USED - TICKS AUTOMATICALLY HANDLED BY AUTOGRAPH.       A5
C              (USED TO BE NUMBER OF POINTS ON THE Y-AXIS WHICH ARE LABELED)    A5
C      NXTIC = NO LONGER USED - TICKS AUTOMATICALLY HANDLED BY AUTOGRAPH.       A5
C              (USED TO BE NUMBER OF TICKS BETWEEN LABELS ON THE X-AXIS)        A5
C      NYTIC = NO LONGER USED - TICKS AUTOMATICALLY HANDLED BY AUTOGRAPH.       A5
C              (USED TO BE NUMBER OF TICKS BETWEEN LABELS ON THE Y-AXIS)        A5
C              (DEFAULT IS 0)                                                   A5
C      ICOSH   CONTOUR/SHADING CONTROL                                          A5
C            =0, CONTOUR LINES                                                  A5
C            =1, CONTOUR LINES, NO ZERO LINE                                    A5
C            =2, CONTOUR LINES AND SHADING                                      A5
C            =3, CONTOUR LINES AND SHADING, NO ZERO LINE                        A5
C      MS         PUBLICATION QUALITY OPTIONS:                                  A5
C                                                                               A5
C                 IF MS.LT.0, THEN PUBLICATION QUALITY OPTION IS SET, AND       A5
C                 PLOTS HAVE: TITLE, THICK LINES, NO INFORMATION LABEL, NO      A5
C                 LEGEND, AND ALL CONTOUR LINES ARE SAME WIDTH.  LHI IS SET     A5
C                 TO -1, SO NO HIGH/LOW LABELS ARE DISPLAYED, AND MAP IS        A5
C                 CENTRED ON LON 0.                                             A5
C                                                                               A5
C                 IF MS.GE.0, THEN NORMAL QUALITY OPTION IS SET, AND PLOTS      A5
C                 HAVE THICK/THIN LINES, AND HIGH/LOW LABELS (DEPENDING UPON    A5
C                 LHI SETTING.  IN ADDITION, IF MS                              A5
C                  =6,  SMOOTH SHADING INSTEAD OF CONTOURS.  ALSO -6.           A5
C                       WITH SMOOTH SHADING, GGPLOT INTERPOLATES COLOURS ON     A5
C                       HSV COLOUR WHEEL BETWEEN LEVELS.  WITH THE FOLLOWING    A5
C                       EXAMPLE CARDS, GGPLOT WILL:                             A5
C                            IF (Z.LT.-3) SHADE=130                             A5
C                            IF (Z.GE.-1) SHADE=106                             A5
C                            IF (Z.GE.-3.AND.Z.LT.-2) THEN                      A5
C                                   SHADE INTERPOLATED BETWEEN 130 AND 100.     A5
C                            IF (Z.GE.-2.AND.Z.LT.-1) THEN                      A5
C                                   SHADE INTERPOLATED BETWEEN 100 AND 106.     A5
C                        SHADING   0   6  130  100  106                         A5
C                        VALUES        -3.000    -2.000   -1.0000               A5
C                       ONLY 100 COLOURS ARE ALLOCATED.  SMOOTH IMAGES CAN      A5
C                       BE OBTAINED BY SPECIFYING FEW LEVELS AND LETTING        A5
C                       GGPLOT INTERPOLATE OVER ENTIRE RANGE.                   A5
C                  =5,  NO INFO LABEL, NO TITLE, NO LEGEND, NO CONTOUR LABELS   A5
C                  =4,  NO INFO LABEL, NO TITLE, NO LEGEND                      A5
C                  =3,  NO INFO LABEL, NO TITLE                                 A5
C                  =2,  NO INFO LABEL                                           A5
C                  =1,  NO TITLE                                                A5
C                  =0   NORMAL INFO LABEL AND TITLE                             A5
C                                                                               A5
C      LHI   = 0, HIGHS AND LOWS LABELLED,                                      A5
C            =-1, .. NOT LABELLED.                                              A5
C                                                                               A5
C                                                                               A5
C      CARD 2                                                                   A5
C      ------                                                                   A5
C      READ(5,5020,END=122) (XLAB(I),I=1,20),XMIN,XMAX,                         A5
C     1                     (YLAB(I),I=1,20),YMIN,YMAX                          A5
C 5020 FORMAT(20A1,2E10.0,20A1,2E10.0)                                          A5
C                                                                               A5
C      XLAB  = 20 CHARACTER LABEL FOR X-AXIS.                                   A5
C      XMIN  = VALUE OF LABEL AT LEFT  HAND END OF X-AXIS                       A5
C      XMAX  = VALUE OF LABEL AT RIGHT HAND END OF X-AXIS                       A5
C              (XMIN>=XMAX TURNS OFF LABELING OF TICKS ON X-AXIS)               A5
C      YLAB  = 20 CHARACTER LABEL FOR Y-AXIS.                                   A5
C      YMIN  = VALUE OF LABEL AT LEFT  HAND END OF Y-AXIS                       A5
C      YMAX  = VALUE OF LABEL AT RIGHT HAND END OF Y-AXIS                       A5
C              (YMIN>=YMAX TURNS OFF LABELING OF TICKS ON Y-AXIS)               A5
C                                                                               A5
C      CARD 3                                                                   A5
C      ------                                                                   A5
C      READ(5,5030,END=132) (LABEL(I),I=1,80)                                   A5
C 5030 FORMAT(80A1)                                                             A5
C                                                                               A5
C      LABEL = 80 CHARACTER LABEL FOR CONTOUR MAP.                              A5
C                                                                               A5
C      CARD 4                                                                   A5
C      ------                                                                   A5
C      READ(5,5056,END=901) WHITFG, NPAT, (IPAT(I),I=1,7)                       A5
C 5056 FORMAT(10X,I1,I4,7I5)                                                    A5
C      READ(5,5055,END=901) (IPAT(I),I=22,NPAT)                                 A5
C 5055 FORMAT(15X,7I5)                                                          A5
C                                                                               A5
C      ONLY IF (ICOSH .EQ. 2 .OR. ICOSH .EQ. 3)                                 A5
C                                                                               A5
C      WHITFG     .NE. 0 IF COLOUR PLOTS ARE TO HAVE WHITE CONTOURS             A5
C      NPAT       NUMBER OF DIFFERENT RANGES TO SHADE (MAXIMUM 28 COLOURS)      A5
C      IPAT       NPAT SHADING PATTERN CODES                                    A5
C                 28 MAXIMUM CODES, 7 PER LINES                                 A5
C                                                                               A5
C      CARD 5                                                                   A5
C      ------                                                                   A5
C      ONLY IF (ICOSH .EQ. 2 .OR. ICOSH .EQ. 3)                                 A5
C                                                                               A5
C      READ(5,5057,END=901) (ZLEV(I),I=1,7)                                     A5
C 5057 FORMAT(10X,7G10.4)                                                       A5
C                                                                               A5
C      ZLEV       NPAT-1 SCALAR FIELD VALUES TO DEFINE THE SHADED RANGES.       A5
C      ZLEV(I) =  NPAT-1 DATA VALUES AT WHICH TO CHANGE SHADING PATTERNS.       A5
C                 THE FIRST IPAT PATTERN WILL SHADE THE CONTOUR LEVEL           A5
C                 WITH VALUES < ZLEV(1). THE SECOND IPAT PATTERN WILL           A5
C                 SHADE AREAS WITH VALUES BETWEEN ZLEV(1) AND ZLEV(2),          A5
C                 AND SO ON FOR THE REST OF THE ARRAY. THE LAST(NPAT)           A5
C                 IPAT PATTERN WILL SHADE THE CONTOUR LEVEL WITH                A5
C                 VALUES > ZLEV(NPAT-1).                                        A5
C                 27 MAXIMUM LEVELS, 7 PER LINES                                A5
C                                                                               A5
C      CARD 6 (CARD 4 IF ICOSH .EQ. 0)                                          A5
C      ------                                                                   A5
C      FOR ARBITRARY CONTOUR LINES OF SCALAR FIELD                              A5
C      ================================================                         A5
C                                                                               A5
C      ONLY IF FINC < 0. WORKS WITH ALL ICOSH VALUES.                           A5
C                                                                               A5
C      NPATS=-FINC                                                              A5
C                                                                               A5
C      NOTE: SCAL CAN NOT BE EQUAL TO ZERO IF YOU ARE REQUESTING ARBITRARY      A5
C            LINE CONTOURS.                                                     A5
C                                                                               A5
C      DO K=1,NPATS,7                                                           A5
C         READ(5,5027,END=920) (ZLEVS(I),I=K,K+6)                               A5
C      ENDDO                                                                    A5
C                                                                               A5
C 5027 FORMAT(10X,7E10.0)                                                       A5
C                                                                               A5
C      NPATS       EXACT NUMBER OF CONTOUR LINES (NPATS=-FINC) (MAXIMUM 28)     A5
C      ZLEVS(I) =  NPATS DATA VALUES OF THE CONTOUR LINES. (7 PER LINES)        A5
C                                                                               A5
C                                                                               A5
C* HOVPLOT        1.      -40.       40.        2.       0.9  2 5   39    1  2-1A5
C*   SEASON                1.0      40.0    LATITUDE             -90.0      90.0A5
C* SEASONAL MEAN ZONAL SURFACE PRESSURE VS TIME FOR RUN FJ (FIRST 40 SEASONS)   A5
C*        0   3  140  142  145                                                  A5
C*               0.0      10.0                                                  A5
C-----------------------------------------------------------------------------
C
C Setup "MAXSIZ" to handle up to 1000 records each with up to 360 elements.

      use diag_sizes, only : SIZES_NWORDIO,
     &                       SIZES_TSL
      integer, parameter :: MAXX = SIZES_TSL*SIZES_NWORDIO

      PARAMETER ( MAXSIZ = 1000 * SIZES_TSL )
      LOGICAL OK,TRAIL,LEAD,CLRPLT,PATPLT,NZERO
      INTEGER ADD,BASE,NPAT,IPAT(28),WHITFG,ITMP,MAXPAT,MAXCLRS
      INTEGER LXXX, LYYY, POSI
      INTEGER INZERO, MINLW, MAJLW, LOWMKR, HIMKR
      CHARACTER*1 LABEL(83),XLAB(23),YLAB(23)
      CHARACTER*100 LINFOLABEL
      CHARACTER*10 LI1,LI2,LI3,LI4
      CHARACTER*100 ILT
      CHARACTER ALABEL*83,AXLAB*23,AYLAB*23
      REAL ZLEV(28),HSVV(3,28),PT
      REAL X(2), Y(2), GRIDCO(4), XLO, XHI, YLO, YHI

      EQUIVALENCE (LABEL,ALABEL),(XLAB,AXLAB),(YLAB,AYLAB)
      EQUIVALENCE (GRIDCO(1), XLO), (GRIDCO(2), XHI)
      EQUIVALENCE (GRIDCO(3), YLO), (GRIDCO(4), YHI)

C     logical unit numbers for saving card images, data, etc.
      INTEGER LUNCARD,UNIQNUM,LUNDATAFILES(1)
      PARAMETER (LUNOFFSET=10,
     1     LUNFIELD1=LUNOFFSET+1)

      COMMON/BLANCK/ GG2(MAXSIZ),U(MAXSIZ)

      COMMON /CONCCC/MAJLW,MINLW,LOWMKR,HIMKR,INZERO

      COMMON/ICOM/IBUF(8),IDAT(MAXX)

      COMMON /FILLCO/ LAN,LSP,LPA,LCH,LDP(8,8)

      COMMON /FILLCB/ IPAT, ZLEV, NPAT, MAXPAT, MAXCLRS
C
C     * COMMON BLOCK FOR SETTING LINE THICKNESSES.
C
C Workspace arrays for NCARG routines.
      INTEGER LRWK, LIWK
      PARAMETER (LRWK=15000, LIWK=7000)
      REAL RWRK(LRWK)
      INTEGER IWRK(LIWK)
C     * Arbitrary contours lines
      INTEGER NIPATS,NPATS,IARBI
      PARAMETER(NIPATS = 28)
      REAL ZLEVS(NIPATS), CWM
      COMMON /ARBI/ ZLEVS,NPATS,IARBI, CWM
COLour CONtour plotting routine.
      INTEGER HOV_MAP_SIZE
      PARAMETER (HOV_MAP_SIZE=2000000)
      INTEGER HOV_MAP(HOV_MAP_SIZE)
      COMMON /HOV_AREA/HOV_MAP
      INTEGER NWRK, NOGRPS
      PARAMETER (NWRK=15000,NOGRPS=5)
      REAL XWRK(NWRK), YWRK(NWRK)
      INTEGER IAREA(NOGRPS), IGRP(NOGRPS)
C
      CHARACTER*4 ATYPE, CHOVP
      INTEGER*4 ITYPE, IHOVP
C ITYPE is used by all plotting programs.
      COMMON /CCCPLOT/ ITYPE
C ARG is used by LBFILL
      INTEGER ARG
      COMMON /PAT/ ARG

C     Smooth Shading
      LOGICAL SMSHAD1,SMSHAD2

C Publication common block
      LOGICAL PUB, DOTITLE, DOINFOLABEL, DOLGND, DOLABEL, SHAD
      COMMON /PPPP/ PUB, SHAD, DOTITLE, DOINFOLABEL, DOLGND, DOLABEL

      EQUIVALENCE (CHOVP,IHOVP)

C
C
C     * DECLARE NCAR PLUGINS EXTERNAL, SO CORRECT VERSION ON SGI
C
      EXTERNAL AGCHNL,AGPWRT,CPDRPL,CPCHHL,CPCHIL,CONTDF,
     1         DFNCLR,GGP_COLSHD,GGP_COLSMSHD,GGP_CONLS,
     2         GGP_ILDEF,GGP_LEGEND,GGP_PATTERN,HOV_LEGEND,
     3         LBFILL,MAPUSR,NEW_HAFTNP,PATTERN,SAMPLE,SETPAT,
     4         SP_LEGEND,STRINDEX
C
C     * DECLARE BLOCK DATAS EXTERNAL, SO INITIALIZATION HAPPENS
C
      EXTERNAL FILLCBBD
      EXTERNAL FILLPAT_BD

C---------------------------------------------------------------------
      CHOVP='HOVP'
      NF=3
      CALL JCLPNT(NF,LUNFIELD1,5,6)
      REWIND LUNFIELD1
      CALL PSTART

C
C     * SET PLOT PARAMETERS
C
C     ITYPE = 'HOVP'
      ITYPE = IHOVP
      WHITFG=0
      INZERO=-1
      ARG=0

C     * LINE LABEL SIZE
      NPLOT=0

C     * WANT TO HAVE TRAILING AND LEADING BLANKS REMOVED BY ROUTINE SHORTN.
      TRAIL= .TRUE.
      LEAD = .TRUE.
C
C     * SET THE LINE THICKNESSES.
C
      LWMJ=2000
      LWMN=1000

      LWMJP=2000
      LWMNP=2000
      CALL PCSETR('CL',.2)

C     * CHANGE THE LINE-END CHARACTER TO '!'.
      CALL AGSETC('LINE/END.', '!')

C     * MAKE THE NUMERIC LABELS BIGGER.
      CALL AGSETF('LEFT/WIDTH/MANTISSA.', 0.020)
      CALL AGSETF('LEFT/WIDTH/EXPONENT.', 0.013333)
      CALL AGSETF('BOTTOM/WIDTH/MANTISSA.', 0.020)
      CALL AGSETF('BOTTOM/WIDTH/EXPONENT.', 0.013333)

C     * MAKE THE TOP LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'T')
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.026666)

C     * MAKE THE LEFT LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'L')
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.026666)

C     * MAKE THE BOTTOM LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'B')
      CALL AGSETF('LINE/NUMBER.', -100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.026666)

C
C     * CENTER THE GRID WINDOW.
C
      CALL AGSETF('GRID/LEFT.', 0.10)
      CALL AGSETF('GRID/RIGHT.', 0.90)
      CALL AGSETF('GRID/BOTTOM.', 0.10)
      CALL AGSETF('GRID/TOP.', 0.90)

      CALL AGSETF('X/NICE.', 0.0)
      CALL AGSETF('Y/NICE.', 0.0)

C
C     * SHUT OFF INFORMATION LABEL SCALING.  AUTOGRAPH WILL ONLY BE ABLE TO
C     * PREVENT INFORMATION LABELS FROM OVERLAPPING BY MOVING THEM.
C
      CALL AGSETF('LABEL/CONTROL.', 1.0)

C
C     *  CHANGE THE MAXIMIMUM LINE LENGTH.
C
      CALL AGSETF('LINE/MAXIMUM.', 80.0)

C
C     * READ-IN DIRECTIVE CARDS.
C     * CARD 1 ****************************************

  110 READ(5,5010,END=112) SCAL,FLO,HI,FINC,AR,NXLAB,NYLAB,NXTIC,NYTIC,         
     1                     ICOSH,MS,LHI                                         
      GOTO 114
  112 IF(NPLOT.EQ.0) CALL                          PXIT('HOVPLOT',-1)
      CALL                                         PXIT('HOVPLOT',0)
  114 CONTINUE
C
C     * Check for arbitrary line contours
C
      IARBI=0
C
      IF(FINC.LT.0.) THEN
         IF(SCAL.EQ.0.) THEN
            WRITE(6,*) 'CAN NOT HAVE BOTH SCALE = 0 AND FINC < 0'
            CALL                                   PXIT('HOVPLOT',-100)
         ENDIF
         FINC=-FINC
         IARBI=INT(FINC)
      ENDIF      
C
      IF (LHI.LT.0) THEN
         IF(LHI.LT.-4) THEN
            LHI=LHI+5
            ILAB=0
         ENDIF
      ELSE
         IF(LHI.GT.4) THEN
            LHI=LHI-5
            ILAB=0
         ENDIF
      ENDIF
C
C     * CHECK FOR PUBLICATION QUALITY OPTION.
C
      NULBLL=1
      IF (MS .LT. 0) THEN
         CALL GSLWSC(2.0)
         PUB=.TRUE.
         DOTITLE=.TRUE.
         DOINFOLABEL=.FALSE.
         DOLGND=.FALSE.
         DOLABEL=.TRUE.
         ILAB=0
         LHI=-1
      ELSE
         PUB=.FALSE.
         DOTITLE=.TRUE.
         DOINFOLABEL=.TRUE.
         DOLGND=.TRUE.
         DOLABEL=.TRUE.

         IF(MS.EQ.1) THEN
            DOTITLE=.FALSE.
         ELSE IF(MS.EQ.2) THEN
            DOINFOLABEL=.FALSE.
         ELSE IF(MS.EQ.3) THEN
            DOTITLE=.FALSE.
            DOINFOLABEL=.FALSE.
         ELSE IF(MS.EQ.4) THEN
            DOTITLE=.FALSE.
            DOINFOLABEL=.FALSE.
            DOLGND=.FALSE.
         ELSE IF(MS.EQ.5) THEN
            DOTITLE=.FALSE.
            DOINFOLABEL=.FALSE.
            DOLGND=.FALSE.
            DOLABEL=.FALSE.
         ENDIF
      ENDIF
C
      WRITE(6,6050) NPLOT + 1

C     * CARD 2 ****************************************

      READ(5,5020,END=122) (XLAB(I),I=1,20),XMIN,XMAX,                          
     1                     (YLAB(I),I=1,20),YMIN,YMAX                           
      WRITE(6,5020)(XLAB(I),I=1,20),XMIN,XMAX,
     1             (YLAB(I),I=1,20),YMIN,YMAX
      GOTO 124
  122 CALL                                         PXIT('HOVPLOT',-2)
  124 CONTINUE

C     * CARD 3 ****************************************

      READ(5,5030,END=132) (LABEL(I),I=1,80)                                    
      WRITE(6,5030) (LABEL(I),I=1,80)
      GOTO 134
  132 CALL                                         PXIT('HOVPLOT',-3)
  134 CONTINUE
C
C-----------------------------------------------------------------------
C     * CONSTRUCT THE FIELD TO BE CONTOURED.
C
C     * LEARN ABOUT THE FIELD
C
      CALL GETFLD2(LUNFIELD1,GG2,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK) THEN
        CALL                                       PXIT('HOVPLOT',-101)
      ENDIF
      IF(IBUF(6).NE.1)THEN
        CALL                                       PXIT('HOVPLOT',-102)
      ENDIF
      IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)
      LY=IBUF(5)
      LX=1
      BASE=LY+1
  150 CONTINUE
C
         IF(MAXSIZ-BASE.LT.LY)THEN
         CALL                                      PXIT('HOVPLOT',-103)
         ENDIF
         CALL GETFLD2(LUNFIELD1,GG2(BASE),-1,-1,-1,-1,IBUF,MAXX,OK)
         IF(.NOT.OK) GOTO 160
         IF(IBUF(5).NE.LY.OR.IBUF(6).NE.1) THEN
           CALL                                    PXIT('HOVPLOT',-104)
         ENDIF
         IF(IBUF(8).LE.0) WRITE(6,6090) IBUF(8)
         LX=LX+1
         BASE=BASE+LY
         GOTO 150

  160 CONTINUE
      LXY=LX*LY

C
C     *  SET THE TENSION FOR THE CONTOUR LINE SMOOTHER
C
C     IF(LXY.LT.500) THEN
C        TENSN=30.0
C     ELSEIF(500.LT.LXY.AND.LXY.LT.2000) THEN
C        TENSN=39.16666667 - 0.018333333*FLOAT(LXY)
C     ELSE
C        TENSN=2.5
C     ENDIF
C     IF ( TENSN.GT.9.0) TENSN=9.0
C
C     * TRANSPOSE THE ARRAY
C

      ADD=0
      DO 175 I=1,LY
         DO 170 J=1,LX
            ADD=ADD+1
            U(ADD)=GG2((J-1)*LY+I)
  170    CONTINUE
  175 CONTINUE
C
C     * SCALE THE FIELD IF NECESSARY

      IF (SCAL.EQ.0.)   THEN
          ARIMEAN=AMEAN2(U,LX,LY,0,0)
          SMALL  =U(ISMIN(LXY,U,1))
          IF (SMALL.GT.0)   THEN
              GEOMEAN=GMEAN2(U,LX,LY,0,0)
              WRITE(6,6060) ARIMEAN,GEOMEAN
          ELSE
             WRITE(6,6060) ARIMEAN
          ENDIF
          CALL PRECON2(FLO,HI,FINC,SCAL,U,LX,LY,10)
      ENDIF
      DO 180 I=1,LXY
         U(I)=U(I)*SCAL
  180 CONTINUE

C
C     * SET THE ASPECT RATIO OF THE GRID.
      CALL PCSETI('CD', 1)
      CALL GSTXFP(-12,2)
      CALL PCSETI('OF', 1)
      CALL PCSETI('OL', 2)
C
C Set legend position horizontal or vertical
C
      POSI = 0
      IF (ABS(AR) .GT. 0.79) POSI = 1
C
      IF (AR .LE. 0.0) AR = FLOAT(LY - 1)/FLOAT(LX - 1)

      CALL AGSETF('GRID/SHAPE.', -1.0/AR)

C
C     * SET UP THE TITLE OF THE GRAPH.
C
      IF(DOTITLE) THEN
         CALL AGSETC('LABEL/NAME.', 'T')
         CALL AGSETF('LINE/NUMBER.', 100.0)
         CALL SHORTN(LABEL, 83, LEAD, TRAIL, NC)
         CALL AGSETC('LINE/TEXT.', ALABEL(4:NC)//'!')
      ENDIF

      WRITE(6, 6040) (LABEL(I),I=1,NC)

C
C     * SET UP THE X AND Y AXIS LABELS.
C
      CALL SHORTN(XLAB, 23, LEAD, TRAIL, NCX)
      CALL SHORTN(YLAB, 23, LEAD, TRAIL, NCY)
      CALL ANOTAT(AXLAB(4:NCX)//'!', AYLAB(4:NCY)//'!', 0, 0, 0, 0)

      WRITE(6,6040) (XLAB(I),I=1,NCX)
      WRITE(6,6040) (YLAB(I),I=1,NCY)

C
C     * TURN OFF NUMERIC LABELS IF XMIN>XMAX OR YMIN>YMAXSET AND IF NOT,
C     * SET UP THE LIMITS OF THE GRID.  IN BOTH CASES THE SMALL ARRAYS
C     * X AND Y MUST BE INITIALIZED SO THEY CAN BE PASSED TO AGSTUP.
C     * THE ARRAYS ONLY HOLD 2 COORDINATES - ONE IS THE LOWER LEFTMOST
C     * POINT ON THE GRID AND THE OTHER IS THE UPPER RIGHTMOST.  NOTE: THE
C     * 2 POINTS ARE IN *USER* COORDINATES NOT FRACTIONAL COORDINATES.
C
      IF (XMIN .GE. XMAX .OR. YMIN .GE. YMAX) THEN
           CALL AGSETF('LEFT/TYPE.', 0.0)
           CALL AGSETF('BOTTOM/TYPE.', 0.0)

           X(1) = 1.0
           Y(1) = 1.0
           X(2) = FLOAT(LX)
           Y(2) = FLOAT(LY)

      ELSE
           CALL AGSETF('X/MINIMUM.', XMIN)
           CALL AGSETF('X/MAXIMUM.', XMAX)
           CALL AGSETF('Y/MINIMUM.', YMIN)
           CALL AGSETF('Y/MAXIMUM.', YMAX)

           X(1) = XMIN
           Y(1) = YMIN
           X(2) = XMAX
           Y(2) = YMAX
      ENDIF


C
C     * RUN AGSTUP TO SO AUTOGRAPH CAN DETERMINE WHERE THE GRID WILL
C     * BE PLACED.
C
      NCURV = 1
      NPTS = 2
      CALL AGSTUP(X,NCURV,NPTS,NPTS,1,Y,NCURV,NPTS,NPTS,1)


C
C     * GET THE COORDINATES THAT INDICATE WHERE THE GRID LIES ON THE PAGE.
C
      CALL AGGETP('SECONDARY/CURVE.', GRIDCO, 4)

C     * MAKE A CALL TO 'SET' SO THE CONTOUR LINES AND SHADING FILL THE
C     * GRID AREA OF THE GRAPH.
C
      CALL SET(XLO, XHI, YLO, YHI, 1.0, FLOAT(LX), 1.0, FLOAT(LY), 1)
C
C     * SET BACKGROUND COLOR
C
      CALL BFCRDF(0)
C
C     * CALCULATE PT(3,3)
C
      PT = U(LX*2+3)
C
      IF (ICOSH.EQ.0 .OR. ICOSH.EQ.2) NZERO=.FALSE.
      IF (ICOSH.EQ.1 .OR. ICOSH.EQ.3) NZERO=.TRUE.
C
C     * IF LHI = 0, HIGHS AND LOWS LABELLED,
C              =-1, .. NOT LABELLED.
C
      IF (LHI.EQ.0)  CALL CPSETC('HLT',
     1               'H:B:$ZDV$:E:''L:B:$ZDV$:E:')
      IF (LHI.EQ.-1) CALL CPSETC('HLT',' '' ')
C     * Set special value SPVAL=1.0E38
      SPVAL=1.0E38
      CALL CPSETR('SPV', SPVAL)

C     * READ IN THE SHADING CARDS AND DO THE COLOUR SHADING
C     * AND DRAWING THE CONTOUR LINES.
C
      IF (ICOSH.EQ.2 .OR. ICOSH.EQ.3) THEN

C     * CARD 4 ****************************************
C     * UP TO 28 COLOURS
C
        READ(5,5056,END=901) WHITFG, NPAT, (IPAT(I),I=1,7)                      
        IF (NPAT .GT. 7) THEN
          READ(5,5055,END=901) (IPAT(I),I=8,14)                                
        ENDIF
        IF (NPAT .GT. 14) THEN
          READ(5,5055,END=901) (IPAT(I),I=15,21)                              
        ENDIF
        IF (NPAT .GT. 21) THEN
          READ(5,5055,END=901) (IPAT(I),I=22,NPAT)                              
        ENDIF

         WRITE(6,5056) WHITFG, NPAT, (IPAT(I),I=1,7)
C         WRITE(6,5055) (IPAT(I),I=8,14)
C         WRITE(6,5055) (IPAT(I),I=15,21)
C         WRITE(6,5055) (IPAT(I),I=22,NPAT)

C     * CARD 5 ****************************************
C     * UP TO 28 LEVELS
C
        IF(NPAT.LE.7) THEN
           READ(5,5057,END=901) (ZLEV(I),I=1,NPAT-1)                            
        ELSE
           READ(5,5057,END=901) (ZLEV(I),I=1,7)                              
        ENDIF
        IF (NPAT .GT. 7.AND.NPAT.LE.14) THEN
          READ(5,5057) (ZLEV(I),I=8,13)                                      
        ENDIF
        IF (NPAT .GT. 14.AND.NPAT.LE.21) THEN
          READ(5,5057) (ZLEV(I),I=8,14)                                      
          READ(5,5057) (ZLEV(I),I=15,NPAT-1)                                
        ENDIF
        IF (NPAT .GT. 21.AND.NPAT.LE.28) THEN
          READ(5,5057) (ZLEV(I),I=8,14)
          READ(5,5057) (ZLEV(I),I=15,21)
          READ(5,5057) (ZLEV(I),I=22,NPAT-1)
        ENDIF

         WRITE(6,6157) (ZLEV(I),I=1,NPAT-1)


C     Changed definition of colour plot to mean CLRPLT is true if
C     there is at least one level of colour shading.
         CLRPLT = .FALSE.
         PATPLT = .FALSE.
         DO 95 I = 1, NPAT
            IF (IPAT(I) .GE. 100 .AND. IPAT(I) .LE. 199) THEN
               CLRPLT = .TRUE.
            ELSE
               PATPLT = .TRUE.
            ENDIF
 95      CONTINUE
         IF(CLRPLT) THEN
            IF(NPAT.GT.0) THEN
               NCLRS=-NPAT
              ELSE
               NCLRS=0
            ENDIF
            CALL DFCLRS(NCLRS, HSVV,IPAT)
C            WRITE(6,*) 'IPAT ', NCLRS, IPAT
         ENDIF
C
C     * READ ARBITRARY LINE CONTOURS
C
      IF (IARBI.NE.0) THEN
         NPATS=IARBI
C     READ LEVELS
         IF(NPATS.GT.NIPATS) THEN
            WRITE(6,*) 'TOO MANY CONTOUR LEVELS, NPATS=',NPATS
            CALL                                   PXIT('HOVPLOT',-4)
         ENDIF
         DO K=1,NPATS,7
            READ(5,5027) (ZLEVS(I),I=K,K+6)
         ENDDO
C
      ENDIF
C
         CALL ARINAM(HOV_MAP,HOV_MAP_SIZE)
C
C     * SHADING
C
         CALL CONTDF(PUB)
         CALL CPRECT(U,LX,LX,LY,RWRK,LRWK,IWRK,LIWK)
         CALL CPCLAM(U,RWRK,IWRK,HOV_MAP)
         CALL GSLWSC(1.)
         CALL ARSCAM(HOV_MAP,XWRK,YWRK,NWRK,IAREA,IGRP,
     1               NOGRPS,PATTERN)
         CALL CPSETI('LBC',0)
         IF (PATPLT) THEN
           CALL CPSETI('LLB',2)
           CALL CPSETI('HLB',2)
         ENDIF
         IF (DOLGND) CALL HOV_LEGEND(POSI,CLRPLT,PATPLT)
         CALL GSLWSC(1.5)
C
C     * SET CONTOURING PARAMETERS
C
         CALL CONLS(FLO, HI, FINC, NZERO, PUB, FHIGHN, FLOWN)
         CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', ITMP)
         IF (MOD(ITMP,2) .EQ. 0) THEN
           CALL CPSETI('PAI - PARAMETER ARRAY INDEX', ITMP)
           CALL CPSETR('CLL - CONTOUR LINE LINE WIDTH',2.)
           CALL CPSETI('CLU', 3)
         ENDIF
         DO 856 I=2,ITMP-1,2
           CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
           CALL CPSETR('CLL - CONTOUR LINE LINE WIDTH',2.)
           CALL CPSETI('CLU', 3)
  856    CONTINUE
         DO 866 I=1,ITMP,2
           CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
           IF (PUB) THEN
             CALL CPSETR('CLL', 2.)
           ELSE
             CALL CPSETR('CLL', 0.5)
           ENDIF
           CALL CPSETI('CLU', 1)
  866    CONTINUE
         IF(.NOT.DOLABEL) THEN
            DO 867 I=1,ITMP
               CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
               CALL CPSETI('CLU',1)
 867        CONTINUE
         ENDIF
         IF ((INZERO .GT. 0).AND.(NZERO)) THEN
           CALL CPSETI('PAI - PARAMETER ARRAY INDEX', INZERO)
           CALL CPSETI('CLU', 0)
           CALL CPSETR('CLL', 0)
         ENDIF
C
C    * SET UP BACKGROUND
C
C         LXXX = INT((LX-1)/5)
C         LYYY = INT((LY-1)/5)
C         CALL GRIDAL(LXXX,5,LYYY,5,0,0,5,0.,0.)
C
C    * DRAWING CONTOURING LINES
C
         CALL CPLBAM(U,RWRK,IWRK,HOV_MAP)
         CALL CPCLDM(U, RWRK, IWRK, HOV_MAP, CPDRPL)
         CALL CPLBDR(U,RWRK,IWRK)
      ENDIF
C
C     * SET THE POLYLINE COLOUR INDEX AND THE TEXT COLOUR INDEX BACK TO
C     * THE FOREGROUND COLOUR FOR THE REST OF THE GRAPH.
C
C      CALL GSPLCI(1)
C      CALL GSTXCI(1)
C
C     * DO THE NON-SHADED CONTOUR GRAPHS.
C
      IF ((ICOSH.EQ.0).OR.(ICOSH.EQ.1)) THEN
C
C     * READ ARBITRARY LINE CONTOURS
C
      IF (IARBI.NE.0) THEN
         NPATS=IARBI
C     READ LEVELS
         IF(NPATS.GT.NIPATS) THEN
            WRITE(6,*) 'TOO MANY CONTOUR LEVELS, NPATS=',NPATS
            CALL                                   PXIT('HOVPLOT',-5)
         ENDIF
         DO K=1,NPATS,7
            READ(5,5027) (ZLEVS(I),I=K,K+6)
         ENDDO
C
      ENDIF
C
           CALL ARINAM(HOV_MAP,HOV_MAP_SIZE)
C
C     * SET CONTOURING PARAMETERS
C
           CALL CONTDF(PUB)
           CALL CONLS(FLO, HI, FINC, NZERO, PUB, FHIGHN, FLOWN)
           CALL CPRECT(U,LX,LX,LY,RWRK,LRWK,IWRK,LIWK)
           CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', ITMP)
           IF (MOD(ITMP,2) .EQ. 0) THEN
             CALL CPSETI('PAI - PARAMETER ARRAY INDEX', ITMP)
             CALL CPSETR('CLL - CONTOUR LINE LINE WIDTH',2.)
             CALL CPSETI('CLU', 3)
           ENDIF
           DO 870 I=2, ITMP-1, 2
             CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
             CALL CPSETR('CLL - CONTOUR LINE LINE WIDTH', 2.)
             CALL CPSETI('CLU', 3)
  870      CONTINUE
           DO 880 I=1, ITMP, 2
             CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
             IF (PUB) THEN
               CALL CPSETR('CLL', 2.)
             ELSE
               CALL CPSETR('CLL', 0.5)
             ENDIF
             CALL CPSETI('CLU', 1)
  880      CONTINUE
           IF(.NOT.DOLABEL) THEN
              WRITE(*,*) 'NO LABELS'
              DO 887 I=1,ITMP
                 CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
                 CALL CPSETI('CLU',1)
 887          CONTINUE
           ENDIF
           IF ((INZERO .GT. 0).AND.(NZERO)) THEN
            CALL CPSETI('PAI - PARAMETER ARRAY INDEX', INZERO)
            CALL CPSETI('CLU', 0)
            CALL CPSETR('CLL', 0)
           ENDIF
C
C    * SET UP BACKGROUND
C
C           LXXX = INT((LX-1)/5)
C           LYYY = INT((LY-1)/5)
C           CALL GRIDAL(LXXX,5,LYYY,5,0,0,5,0.,0.)
C
C    * DRAWING CONTOURING LINES
C
           CALL CPCLAM(U,RWRK,IWRK,HOV_MAP)
           CALL CPLBAM(U,RWRK,IWRK,HOV_MAP)
           CALL CPCLDM(U, RWRK, IWRK, HOV_MAP, CPDRPL)
           CALL CPLBDR(U,RWRK,IWRK)
      ENDIF


C
C     * LAST CHARACTER DRAWN IS AN X IN THE UPPER RIGHT CORNER.
C
      CALL SET(.01,.99,.01,.99, .01,.99,.01,.99, 1)
CCCC      CALL PCHIQU(.97,.97,'X',1,10,0,0)
C
C     * LAST LINE DRAWN
C
      IF (DOINFOLABEL) THEN
         WRITE(LINFOLABEL,7010) FLOWN,FHIGHN,FINC,PT
 7010    FORMAT("CONTOUR FROM ",F10.03,"    TO ",F10.03,
     1        "   CONTOUR INTERVAL OF ",F10.03,
     2        "   PT(3,3)= ",F10.03)
         CALL PCHIQU(.5,0.015,LINFOLABEL,.007,0.,0.)
      ENDIF


C
C     * DRAW THE BACKGROUND.
C
      CALL AGBACK


C
C     * LINE PRINTER IF REQUESTED(MS.NE.0).
C
C     CALL MOVLEV(U,GG2,LXY)
CCCC      CALL SCOPY(LXY,U,1,GG2,1)
CCCC      IF (MS .LT. 0) MS=ABS(MS+1)
CCCC      CALL FCONW2(GG2,FINC,1.,LX,LY,1,1,LX,LY,MS)
      WRITE(6,6030) IBUF
C
C-----------------------------------------------------------------------
      CALL FRAME
C      CALL RESET

      NPLOT=NPLOT+1
      GO TO 110

  901 CALL                                         PXIT('HOVPLOT',-105)
      STOP
C---------------------------------------------------------------------
 5010 FORMAT(10X,5E10.0,1X,2I2,2I5,I1,2I2)                                     
 5020 FORMAT(20A1,2E10.0,20A1,2E10.0)                                          
 5027 FORMAT(10X,7E10.0)                                                       
 5030 FORMAT(80A1)                                                             
 5055 FORMAT(15X,7I5)                                                          
 5056 FORMAT(10X,I1,I4,7I5)                                                    
 5057 FORMAT(10X,7G10.4)                                                       
 6020 FORMAT('0..EOF LOOKING FOR',I10,2X,A4,I6)
 6030 FORMAT('0', 8X,'    STEP  NAME LEVEL  LX  LY  KHEM NPACK',
     1 /' ',2X, A4,I10,2X,A4,I6,2I4,2I6)
 6040 FORMAT(' ',80A1)
 6050 FORMAT('     BEGIN PLOT NUMBER',I5)
 6060 FORMAT(' ARITHMETIC MEAN',G12.4,:/
     1       '  GEOMETRIC MEAN',G12.4)
 6090 FORMAT(/,' *** WARNING: PACKING DENSITY = ', I4,' ***',/)
 6157 FORMAT(10X,7G10.4)             
      END
