! This module is used to set various size parameters in CanDIAG fortran programs
module diag_sizes
    implicit none

    !==================
    ! CPP Replacements
    !==================
    ! number of model levels
    integer, parameter :: SIZES_ILEV = 100    ! historically $L$

    ! number of grid longitudes 
    integer, parameter :: SIZES_LON = 192      ! historically $I$ = SIZES_LON + 1

    ! number of grid latitudes
    integer, parameter :: SIZES_LAT = 96      ! historically $J$

    ! n spectral truncation wave number
    integer, parameter :: SIZES_LRT = 64      ! historically $N$ = SIZES_LRT + 1

    ! m spectral truncation wave number
    integer, parameter :: SIZES_LMT = 64      ! historically $M$ = SIZE_LMT + 1
    
    ! spectral truncation type (0 = rhomboidal, 2 = triangular)
    integer, parameter :: SIZES_TYP = 2

    ! total number of complex elements in the spectral triangle
    ! CSS NOTE: this depends on LRT, LMT, and TYP, so we may want to calculate it in the code
    !           - See dimgt.f to determine how this is calculated, along with setting LRT, and LMT
    integer, parameter :: SIZES_LA = 2145        ! historically $R$

    ! number of pressure levels
    integer, parameter :: SIZES_PLEV = 100    ! historically $PL$

    ! number of lons/lat for a subset of the program lib which 
    !   handle large record fields (1x1 grid) 
    integer, parameter :: SIZES_BLON = 360    ! historically $BI$ = SIZES_BLON + 1
    integer, parameter :: SIZES_BLAT = 181    ! includes equator - historically $BJ$

    ! max time series length
    integer, parameter :: SIZES_TSL = 18528      ! historically $TSL$

    ! number of "words" required for each number in I/O - 1 or 2
    !   - this is necessary as the files being read/written are done so in 64bit format
    !     and as a result, if a program is compiled in 32bit, two "words" will be required
    !     for each value that is read in.
    !   - 1 : for 64bit compilation
    !   - 2 : for 32bit compilation
    integer, parameter :: SIZES_NWORDIO = _PAR_NWORDIO ! historically $V$

    !=================
    ! Constant Values (set in pgmparm)
    !=================
    ! number of tracers
    !   CSS NOTE: this should be set via cpp keys, no?
    integer, parameter :: SIZES_NTRAC = 10

    ! PLID minimum value
    real, parameter :: SIZES_PTMIN = 1.E-10         ! historically $PTMIN$

    !===================
    ! Derived Variables
    !===================
    integer, parameter :: SIZES_MAXLEV  = max(SIZES_ILEV, SIZES_PLEV)
    integer, parameter :: SIZES_LONP1   = SIZES_LON + 1
    integer, parameter :: SIZES_LMTP1   = SIZES_LMT + 1
    integer, parameter :: SIZES_LRTP1   = SIZES_LRT + 1
    integer, parameter :: SIZES_NLM     = 3*(SIZES_LMTP1 - 1) + SIZES_LMTP1 ! only used in 1 plotting program.. it gets calculated there too
    integer, parameter :: SIZES_BLONP1  = SIZES_BLON + 1
    integer, parameter :: SIZES_LONP2   = SIZES_LON + 2
    
    integer, parameter :: SIZES_LONP1xLAT           = SIZES_LONP1*SIZES_LAT
    integer, parameter :: SIZES_MAXLEVxLONP1xLAT    = SIZES_MAXLEV*SIZES_LONP1xLAT
    integer, parameter :: SIZES_MAXLEVP1xLONP1xLAT  = (SIZES_MAXLEV + 1)*SIZES_LONP1xLAT
    integer, parameter :: SIZES_MAXLONP1LAT         = MAX(SIZES_LONP1,SIZES_LAT)

    integer, parameter :: SIZES_BLONP1xBLAT         = SIZES_BLONP1*SIZES_BLAT
    integer, parameter :: SIZES_MAXBLONP1BLAT       = MAX(SIZES_BLONP1,SIZES_BLAT)
    integer, parameter :: SIZES_MAXLEVxBLONP1xBLAT  = SIZES_MAXLEV*SIZES_BLONP1xBLAT 

    ! I/O buffers
    integer, parameter :: SIZES_LONP1xLATxNWORDIO   = SIZES_LONP1*SIZES_LAT*SIZES_NWORDIO
    integer, parameter :: SIZES_BLATxNWORDIO        = SIZES_BLAT*SIZES_NWORDIO 
    integer, parameter :: SIZES_BLONP1xNWORDIO      = SIZES_BLONP1*SIZES_NWORDIO ! only used in one file, but kept for consistency (with SIZES_BLATxNWORDIO)
    integer, parameter :: SIZES_BLONP1xBLATxNWORDIO = SIZES_BLONP1*SIZES_BLAT*SIZES_NWORDIO

    ! ocean params -> probably not needed, but will be kept to support legacy compilation
    integer, parameter :: SIZES_OLON    = 769   ! previously OI
    integer, parameter :: SIZES_OLAT    = 384   ! previously OJ
    integer, parameter :: SIZES_OLEV    = 200   ! previous OL


end module
