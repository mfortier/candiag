#!/bin/sh
#
#                  aodpth_volc1        KVS - Jul 17/13
#   ---------------------------------- EXTRACT AEROSOL RADIATIVE PROPERTIES
#                                      available if %aodpth is defined
#                                      (bulk and pla)
#
.     ggfiles.cdk
#
#  ----------------------------------  AEROSOL RADIATIVE PROPERTIES
#                                      AT BAND 1
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME EXB1 EXB2 EXB3 EXB4
SELECT     EXB5 EXBT ODB1 ODB2 ODB3 ODB4 ODB5 ODBT OFB1 OFB2 OFB3 OFB4 OFB5 OFBT
SELECT     ABB1 ABB2 ABB3 ABB4 ABB5 ABBT ODBV" | ccc select npakgg EXB1 EXB2 EXB3 EXB4 \
                    EXB5 EXBT ODB1 ODB2 \
                    ODB3 ODB4 ODB5 ODBT \
                    OFB1 OFB2 OFB3 OFB4 \
                    OFB5 OFBT ABB1 ABB2 \
                    ABB3 ABB4 ABB5 ABBT \
                    ODBV
#
#  ----------------------------------- AEROSOL RADIATIVE PROPERTIES
#                                      AT 550 NM
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME EXS1 EXS2 EXS3 EXS4
SELECT     EXS5 EXST ODS1 ODS2 ODS3 ODS4 ODS5 ODST OFS1 OFS2 OFS3 OFS4 OFS5 OFST
SELECT     ABS1 ABS2 ABS3 ABS4 ABS5 ABST ODSV" | ccc select npakgg EXS1 EXS2 EXS3 EXS4 \
                    EXS5 EXST ODS1 ODS2 \
                    ODS3 ODS4 ODS5 ODST \
                    OFS1 OFS2 OFS3 OFS4 \
                    OFS5 OFST ABS1 ABS2 \
                    ABS3 ABS4 ABS5 ABST \
                    ODSV
#
#  ----------------------------------- MASK OUT REGIONS WITH MISSING VALUES
#
#
echo ' FMASK            -1   -1   GT        0.' > .fmask_input_card
fmask ABST AMSK  input=.fmask_input_card
fmask ODST OMSK  input=.fmask_input_card
#
      release npakgg
#
#  ----------------------------------  SAVE SELECTED FIELDS
#
      statsav EXB1 EXB2 EXB3 EXB4 \
              EXB5 EXBT ODB1 ODB2 \
              ODB3 ODB4 ODB5 ODBT \
              OFB1 OFB2 OFB3 OFB4 \
              OFB5 OFBT ABB1 ABB2 \
              ABB3 ABB4 ABB5 ABBT \
              ODBV \
              EXS1 EXS2 EXS3 EXS4 \
              EXS5 EXST ODS1 ODS2 \
              ODS3 ODS4 ODS5 ODST \
              OFS1 OFS2 OFS3 OFS4 \
              OFS5 OFST ABS1 ABS2 \
              ABS3 ABS4 ABS5 ABST \
              ODSV AMSK OMSK \
              new_gp new_xp $stat2nd

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi
