#!/bin/sh
# jcl - May 21/2019
#   Reduce cosp4_daily.dk down to only variables requested for CMIP6.  This is
#   to avoid a huge amount of daily data being generated that was not requested.
#
#                  cosp4_daily         jcl - Jun 01/15 - jcl ------- cosp4_daily
#  ----------------------------------- update the daily COSP diagnostic deck to
#                                      match the output produced by the monthly
#                                      COSP diagnostic deck (cosp4_monthly.dk).
#                                      Also added more comments to better
#                                      documents the expected inputs and outputs.
# -------------------------------------------------------------------------

    year_offset="0"

#   ---------------------------------- check some variables

    if [ -z "$year_offset" ] ; then
      echo "ERROR in cosp4_daily_cmip6.dk: undefined variable year_offset"
      exit 1
    fi
    if [ -z "$delt" ] ; then
      echo "ERROR in cosp4_daily_cmip6.dk: undefined variable delt"
      exit 1
    fi
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`

    if [ -z "$daily_cmip5_tiers" ] ; then
#                                      set default daily tiers, if not defined
      daily_cmip5_tiers="1" # 2-D vars
    fi

#  ----------------------------------- prepare input cards

    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL       YYYYMMDD" > ic.tstep_model
    echo "C*TSTEP   $deltmin     ${yearoff}010100       ACCMODEL       YYYYMMDD" > ic.tstep_accmodel
    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model_6h
    echo "C*BINSML      4         1" > ic.binsml

# ------------------------------------ access gs file

.   ggfiles.cdk

# ----------------------------------------------------- Possible input fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# ICTP -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# MODIS related fields
#
# MDTC -> Total cloud fraction
# MDWC -> Water cloud fraction
# MDIC -> Ice cloud fraction
# MDHC -> High cloud fraction
# MDMC -> Middle cloud fraction
# MDLC -> Low cloud fraction
# MDTT -> Linear average cloud optical thickness (all clouds)
# MDWT -> Linear average cloud optical thickness (water clouds)
# MDIT -> Linear average cloud optical thickness (ice clouds)
# MDGT -> Log average cloud optical thickness (all clouds)
# MDGW -> Log average cloud optical thickness (water clouds)
# MDGI -> Log average cloud optical thickness (ice clouds)
# MDRE -> Liquid particle effective radius
# MDRI -> Ice particle effective radius
# MDPT -> Total cloud top pressure
# MDWP -> Liquid water path
# MDIP -> Ice water path
# MDTP -> Cloud top pressure versus cloud optical thickness histogram
# MDTI -> Cloud optical thikness versus ice effective radius histogram
# MDTL -> Cloud optical thikness versus liquid effective radius histogram
# MLN1 -> Cloud droplet number coincentration from algorithm
# MLN2 -> Cloud droplet number coincentration from model
# MDLQ -> Warm (>273K) clouds fore which liquid cloud particle retrieved
#         by MODIS simulator
#
# ----------------------------------------------------- Possible output fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# ICTP -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CHMK -> Mask of land topography (needed when interpolating data to height grid)
#
# MODIS related fields
#
# MDTC -> Total cloud fraction
# MDWC -> Water cloud fraction
# MDIC -> Ice cloud fraction
# MDHC -> High cloud fraction
# MDMC -> Middle cloud fraction
# MDLC -> Low cloud fraction
# MDTT -> Linear average cloud optical thickness (all clouds)
# MDWT -> Linear average cloud optical thickness (water clouds)
# MDIT -> Linear average cloud optical thickness (ice clouds)
# MDGT -> Log average cloud optical thickness (all clouds)
# MDGW -> Log average cloud optical thickness (water clouds)
# MDGI -> Log average cloud optical thickness (ice clouds)
# MDRE -> Liquid particle effective radius
# MDRI -> Ice particle effective radius
# MDPT -> Total cloud top pressure
# MDWP -> Liquid water path
# MDIP -> Ice water path
# MDTP -> Cloud top pressure versus cloud optical thickness histogram
# MDTI -> Cloud optical thikness versus ice effective radius histogram
# MDTL -> Cloud optical thikness versus liquid effective radius histogram
# MLN1 -> Cloud droplet number concentration from algorithm
# MLN2 -> Cloud droplet number concentration from model
# MDLQ -> Warm (>273K) clouds fore which liquid cloud particle retrieved
#         by MODIS simulator
#
# ---------------------------------- Notes
# There are two rather important issues to take note of for this deck
#
# 1. It only processes variables that exist in the "gs" file.  Basically, the error
#    checking is turned off, we attempt to read in the variable.
#    Therefore if the variable is not present,
#    OR the read does not work properly, the script continues onward without the variable.
#    Therefore, if you expected a particular field in the "gp" file and it
#    is not there check first if it is present in the "gs" file and the correct
#    options were set in the GCM submission job.
#
# 2. Most of the variables need to be weighted to get the proper monthly, seasonal, etc.
#    means.  Since the pooling deck have trouble with missing values the next step is
#    to average all of the weighted mean denominators and numerators.  Potentially adds
#    a lot of extra output to the diagnostic files but is necessary to get the proper
#    means at averaging times greater than a month.
#
# 3. When averaging the profile data, and when it is interpolated onto the special height grid,
#    you will need to account for the points that wind up below the height of the land.
#
# Manipulations needed to get the proper weighted means using the fields from the diagnostic
# output
#
# ICCA = ICCA/ICCF (cloud fraction weighted)
# ICCT = ICCT/ICCF (cloud fraction weighted)
# ICCP = ICCP/ICCF (cloud fraction weighted)
# ICCF = ICCF/SUNL (weighed by "observation" occurrence,
#                   i.e., solar zenith angle greater than 0.2)
# ICTP = ICTP/SUNL  (weighed by "observation" occurrence,
#                   i.e., solar zenith angle greater than 0.2)
#
# CLHC = CLHC/CLHM (weighted by "valid observation", i.e., backscatter above threshold)
# CLMC = CLMC/CLMM (weighted by "valid observation", i.e., backscatter above threshold)
# CLLC = CLLC/CLLM (weighted by "valid observation", i.e., backscatter above threshold)
# CLTC = CLTC/CLTM (weighted by "valid observation", i.e., backscatter above threshold)
# CLCP = CLCP/CLCM (weighted by "valid observation", i.e., backscatter above threshold)
#
# PL01 = PL01/ML01 (weighted by valid surface type, i.e., not over land)
# PL02 = PL02/ML02 (weighted by valid surface type, i.e., not over land)
# PL03 = PL03/ML03 (weighted by valid surface type, i.e., not over land)
# PL04 = PL04/ML04 (weighted by valid surface type, i.e., not over land)
# PL05 = PL05/ML05 (weighted by valid surface type, i.e., not over land)
#
# The topography mask (CHMK) will be needed when averaging the profiles over regions,
# i.e., zonally, when the profile has been interpolated onto the heights above mean
# sea level.
#
# For MODIS we need
#
# MDTC = MDTC/SUNL
# MDWC = MDWC/SUNL
# MDIC = MDIC/SUNL
# MDHC = MDHC/SUNL
# MDMC = MDMC/SUNL
# MDLC = MDLC/SUNL
# MDTT = MDTT/MDTC
# MDWT = MDWT/MDWC
# MDIT = MDIT/MDIC
# MDGT = MDGT/MDTC
# MDGW = MDGW/MDWC
# MDGI = MDGI/MDIC
# MDRE = MDRE/MDWC
# MDRI = MDRI/MDIC
# MDPT = MDPT/MDTC
# MDWP = MDWP/MDWC
# MDIP = MDIP/MDIC
# MDTP = MDTP/SUNL
# MDTI = MDTI/SUNL
# MDTL = MDTL/SUNL
# MLN1 = MLN1/MDLQ
# MLN2 = MLN2/MDLQ
# MDLQ = MDLQ/SUNL

# ------------------------------------ there are several daily data types:
#                                      tier 1 is for (sub)daily 2D variables extracted for all runs/periods
#                                      tier 2 is for (sub)daily 3D variables on pressure levels
#                                             extracted for some runs/periods
#                                      Also, if gssave=on, (sub)daily 3D variables on model levels are selected.
    for tier in $daily_cmip5_tiers ; do

      if [ $tier -eq 1 ] ; then

# ------------------------------------ TIER 1: 2D variables

        vars_all=''

# ----------------------------------- Select all of the ISCCP related fields at once

        vars="icca icct iccf iccp ictp sunl"
        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done


# ----------------------------------- Select CALIPSO and CloudSat cloud fraction fields at once
        vars="clhc clhm clmc clmm cllc cllm cltc cltm chmk"

        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done

# ----------------------------------- Reflectances for PARASOL
        vars="pl01 pl02 pl03 pl04 pl05 ml01 ml02 ml03 ml04 ml05"
        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done


# ----------------------------------- Select all of the MODIS related fields at once
# MODIS
        vars="mdtc mdwc mdic mdhc mdmc mdlc mdtt mdwt mdit mdgt mdgw"
        vars="$vars mdgi mdre mdri mdpt mdwp mdip mdtp mdti mdtl mln1 mln2 mdlq"

        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done


# ------------------------------------ xsave
        if [ "${vars_all}" != '' ] ; then
          vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
          rm -f old
          while [ -n "$vars0" ] ; do
            vars1=`echo "$vars0" | cut -f1-80 -d' '`
            head -160 ic.xsave > ic.xsave1
            echo vars=$vars1
            cat ic.xsave1
            xsave old $vars1 new input=ic.xsave1
            mv new old
            vars0=`echo "$vars0" | cut -f81- -d' '`
            tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
          done
#   ---------------------------------- save results.
          access olddd ${flabel}dd na
          xjoin  olddd old newdd
          save   newdd ${flabel}dd na
          delete olddd na
          release old newdd
        fi

      fi # tier=1

      if [ $tier -eq 1 ] ; then

# ------------------------------------ TIER 1: 3D variables on COSP levels

        vars_all=''

# ------------------------------------ accumulated gs variables
# CALIPSO/CLOUDSAT
        vars="clcp clcm"

        vars_u=`echo ${vars} | tr 'a-z' 'A-Z'`
        VARS=`fmtselname ${vars}`

        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg ${vars_u} || true

        for v in $vars_u ; do
          if [ -s $v ] ; then
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $v
               " >> ic.xsave
          fi
        done

# ------------------------------------ xsave
        if [ "${vars_all}" != '' ] ; then
          vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
          rm -f old
          while [ -n "$vars0" ] ; do
            vars1=`echo "$vars0" | cut -f1-80 -d' '`
            head -160 ic.xsave > ic.xsave1
            echo vars=$vars1
            cat ic.xsave1
            xsave old $vars1 new input=ic.xsave1
            mv new old
            vars0=`echo "$vars0" | cut -f81- -d' '`
            tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
          done
#   ---------------------------------- save results.

          save old ${flabel}dcosp
        fi
      fi # tier=1

    done # daily_cmip5_tiers
