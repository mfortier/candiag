#!/bin/sh
#                  xstats5             SK - Nov 19/2009 - SK -----------XSTATS5
#
#   sk: restrict MMR - pressure levels, MC - model levels.
#
#  ----------------------------------- calculate tracer statistics.
#
#   The following tracer parameters are used:
#
#      trac = (optional)  If undefined, or if $trac="ALL" then process all tracers,
#                         otherwise process only tracers listed in $trac.
#     ntrac = (mandatory) the number of all tracers.
#      itNN = (mandatory) tracer names, where NN=01,...,$ntrac.
#     advNN = (mandatory) tracer advection parameter (=1 for advective tracers).
#    xrefNN = (mandatory) xref tracer parameter (xref=0. for non-hybrid tracers),
#    xpowNN = (optional)  xpow tracer parameter (default=1.)
#
# Single level tracer diagnostics include:
#
#   VInn:                 monthly mean vertical intergrals, where nn=01,02,...
#   VInn FIRST TIME STEP: the very first time step in each month (needed for budgets)
#   XKnn/XLnn:            lowest model level concentrations.
#                         first try XKnn, and if it does not exist, use XLnn.
#   XFnn:                 surface flux.
#
#  Conservation diagnostics (mass in units of kg/m2 and corrections in kg/m2-s):
#
#   VMnn: tracer "nn" integrated mass     averaged/accumulated over month.
#   VHnn: tracer "nn" hybrid correction   averaged/accumulated over month.
#   VFnn: tracer "nn" holefill correction averaged/accumulated over month.
#   VTnn: tracer "nn" |physics tendency|  averaged/accumulated over month.
#

#   ---------------------------------- set some parameters

#                                      mr2mc parameter:
#                                      0=save mass mixing ratio (MMR) only
#                                      1=convert mass mixing ratio (MMR) to
#                                        mass concentration (MC)
    mr2mc="    1"

#                                      ml2pl parameter:
#                                      0=leave on model levels
#                                      1=interpolate MMR to pressure levels
    ml2pl="    1"                      # MMR - pressure levels, MC - model levels

#   ---------------------------------- determine list of tracers to process.

    if [ -z "$ntrac" ] ; then
#                                      ntrac must be defined.
      echo "Error: **** Number of tracer ntrac is undefined. ****"
      exit 1
    fi
#                                      check if $trac is undefined, or if trac="ALL"
    tracs=`echo "$trac" | sed 's/^ *//g' | sed 's/ *$//g'` # remove leading/trailing spaces
    if [ -z "$tracs" -o "$tracs" = "ALL" ] ; then
#                                      try to diagnose all $ntrac tracers
      trac=""
      i2=1
      while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo "0$i2" | tail -3c` # 2-digit tracer number
        eval x=\${it$i2}
        if [ -z "$x" ] ; then
          echo "Error: **** tracer name for it$i2 is undefined. ****"
          exit 1
        fi
        trac="$trac $x"
        i2=`expr $i2 + 1`
      done
      ntracf=$ntrac
    else
#                                      process only tracers specified by $trac
      ntracf=`echo $trac | wc -w`
    fi
    echo "==>Process $ntracf tracers $trac"

#   ---------------------------------- input cards for gsapl and cofagg

    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card

    echo "COFAGG.   $lon$lat    0$npg" > .cofagg_input_card

#   ---------------------------------- access beta for zonal averging

    if [ "$rcm" != "on" -a $ml2pl -ne 0 ] ; then
#                                      compute zonal averages
      lzon="    1"
#                                      try first to access beta directly
      access beta ${flabel}_gptbeta na
      if [ ! -s beta ] ; then
#                                      if not available, get it from gp file
        access oldgp ${flabel}gp
        echo "XFIND.        $d" | ccc xfind oldgp beta
      fi
    else
#                                      don't compute zonal averages
      lzon="    0"
    fi

#   ---------------------------------- access u,v,w and set parameters for gpxstat

    if [ "$stat2nd" != "off" -a $ml2pl -ne 0 ] ; then
#                                      compute 2nd order statistics and x'u' terms
      lstat="    1"
      ldt="    1"
      access u ${flabel}_gpu
      access v ${flabel}_gpv
      if [ "$wxstats" = "on" ] ; then
        lw="    1"
        access w ${flabel}_gpw
      else
        lw="    0"
      fi
    else
#                                      skip computation of 2nd order statistics
      lstat="    0"
      ldt="    0"
      lw="    0"
    fi
#                                      input card for pressure level statistics
    luvw="    0"  # don't compute wind statistics such as U"U",U"V",etc.
    lbeta="    1" # use beta for zonal averages
    echo "          $luvw$lw$ldt$lzon$lbeta$lstat$delt" > .ic_gpxstat_pl
#                                      input card for model level statistics
    luvw="   -1"  # don't compute any statistics with winds such as X"U",U"U",U"V",etc.
    lbeta="    0" # don't use beta for zonal averages
    echo "          $luvw$lw$ldt$lzon$lbeta$lstat$delt" > .ic_gpxstat_ml

#   ---------------------------------- process tracers

    xml="" # 3d tracers on model levels
    xpl="" # 3d tracers on pressure levels
    vin="" # tracer vertical integrals
    xkn="" # lowest level concentrations (XKnn or XLnn)
    xfn="" # lowest level fluxes
    vmn="" # integrated mass
    vhn="" # hybrid correction
    vfn="" # holefill correction
    vtn="" # |physics tendency|

    xtstat_getdata="y"
    i=1
    while [ $i -le $ntracf ] ; do
#                                      determine tracer name
      x=`echo $trac | cut -f$i -d' '`
#                                      determine tracer number
      i2=1
      while [ $i2 -le $ntrac ] ; do
        i2=`echo "0$i2" | tail -3c` # 2-digit tracer number
        eval xn=\${it$i2}
        if [ "$x" = "$xn" ] ; then
          break;
        fi
        i2=`expr $i2 + 1`
      done
      if [ $i2 -gt $ntrac ] ; then
        echo "Error: **** tracer number for $x is not found. ****"
        exit 1
      fi
      echo "==>Tracer number for $x is $i2."

#                                      determine tracer parameters adv,xref,xpow

      eval adv=\${adv$i2}
      if [ -z "$adv" ] ; then
        echo "Error: **** tracer parameter adv for it$i2 is undefined. ****"
        exit 1
      fi
      eval xref=\${xref$i2}
      if [ -z "$xref" ] ; then
        echo "Error: **** tracer parameter xref for it$i2 is undefined. ****"
        exit 1
      fi
      eval xpow=\${xpow$i2:=1.} # default 1.

#                                      build fixed-format strings

      NAME=`echo "    $x" | tail -5c` # right-aligned 4-character name
#                                      XREF
      xref=`echo $xref | tr -d ' '`
      XREF=`echo "          $xref" | tail -11c` # 10-character XREF
#                                      XPOW
      xpow=`echo $xpow | tr -d ' '`
      XPOW=`echo "          $xpow" | tail -11c` # 10-character XPOW

      echo "==> For tracer i=$i it=$x xref=$xref xpow=$xpow adv=$adv"

#   ---------------------------------- get model files (do this only once)

      if [ "$xtstat_getdata" = "y" ] ; then
        xtstat_getdata="n"
.       ggfiles.cdk
#                                      compute air density on model levels
#                                      spectral sigma case
        if [ "$datatype" = "specsig" ] ; then
.         spfiles.cdk
          echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME LNSP TEMP" | ccc select npaksp sslnsp sstemp
          cofagg sslnsp gslnsp input=.cofagg_input_card
          cofagg sstemp gstemp input=.cofagg_input_card
#                                      grid sigma case
        elif [ "$datatype" = "gridsig" ] ; then
          echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME LNSP TEMP" | ccc select npakgg gslnsp gstemp
        fi
#                                      calculate pressure at eta levels
        echo "GSAPRES   $coord$plid" | ccc gsapres gstemp gslnsp gspres
#                                      calculate dry air density at eta levels
        div gspres gstemp gspot
        echo "C*XLIN        287.04        0.  RHO    1" | ccc xlin gspot gsrho
      fi # xtstat_getdata

#   ---------------------------------- get the tracer on pressure levels
#                                      (process only advective tracers)

#                                      spectral-sigma and adv=1 case.

      if [ "$datatype" = "specsig" -a  $adv -eq 1 ] ; then
#                                      spectral tracer
        rm -f sstrac
	echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $NAME" | ccc select npaksp sstrac || true
        if [ ! -s sstrac ] ; then
          echo "Warning: **** spectral tracer $NAME is not found in the model ss file. ***"
        else
          if [ "$xref" = "0." ] ; then
#                                      non-hybrid tracer
            echo " GSTRACX. $coord    Q                    $ml2pl$mr2mc" > .gstracx_input_card
          else
#                                      hybrid tracer
            echo " GSTRACX. $coord $itrvar$XREF$XPOW$ml2pl$mr2mc" > .gstracx_input_card
          fi
          cat .cofagg_input_card >> .gstracx_input_card
          cat .gsapl_input_card  >> .gstracx_input_card
          gstracx sstrac ${x} gslnsp gsrho ${x}_MC input=.gstracx_input_card
          rm -f sstrac
          xpl="$xpl ${x}"
          xml="$xml ${x}_MC"
        fi

#  ----------------------------------- grid-sigma or adv=0 case.

      else
#                                      gridded tracer
        rm -f gstrac
	echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $NAME" | ccc select npakgg gstrac || true
        if [ ! -s gstrac ] ; then
          echo "Warning: **** gridded tracer $NAME is not found in the model gs file. ****"
        else
          if [ "$xref" = "0." ] ; then
#                                      non-hybrid tracer
            echo " GSTRACX. $coord    Q                    $ml2pl$mr2mc" > .gstracx_input_card
          else
#                                      hybrid tracer
            echo " GSTRACX. $coord $itrvar$XREF$XPOW$ml2pl$mr2mc" > .gstracx_input_card
          fi
          cat .gsapl_input_card >> .gstracx_input_card
          gstracx gstrac ${x} gslnsp gsrho ${x}_MC input=.gstracx_input_card
          rm -f gstrac
          xpl="$xpl ${x}"
          xml="$xml ${x}_MC"
        fi
      fi

#   ---------------------------------- single-level tracer diagnostics.
#                                      VInn - vertical integral,
#                                      XKnn/XLnn - lowest level concentration,
#                                      XFnn - surface flux.
      vinn="VI$i2"
      xfnn="XF$i2"
      xknn="XK$i2"
      xlnn="XL$i2"
      rm -f $vinn $xfnn $xlnn $xknn
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME $vinn $xfnn $xlnn $xknn" | ccc select npakgg $vinn $xfnn $xlnn $xknn || true
#                                      check for VInn
      if [ -s $vinn ] ; then
        vin="$vin $vinn"
      else
        echo "Warning: **** $vinn is not found in the model gs file. ****"
      fi
#                                      check for XFnn
      if [ -s $xfnn ] ; then
        xfn="$xfn $xfnn"
      else
        echo "Warning: **** $xfnn is not found in the model gs file. ****"
      fi
#                                      check for XKnn
      if [ -s $xknn ] ; then
        xkn="$xkn $xknn"
      else
        echo "Warning: **** $xknn is not found in the model gs file. ****"
#                                      try XLnn if XKnn is not found
        if [ -s $xlnn ] ; then
          xkn="$xkn $xlnn"
        else
          echo "Warning: **** $xlnn is not found in the model gs file. ****"
        fi
      fi

#  ----------------------------------- conservation stats (gcm15g+)

      vmnn="VM$i2"
      vhnn="VH$i2"
      vfnn="VF$i2"
      vtnn="VT$i2"
      rm -f $vmnn $vhnn $vfnn $vtnn
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME $vmnn $vhnn $vfnn $vtnn" | ccc select npakgg $vmnn $vhnn $vfnn $vtnn || true
#                                      check for VMnn
      if [ -s $vmnn ] ; then
        vmn="$vmn $vmnn"
      else
        echo "Warning: **** $vmnn is not found in the model gs file. ****"
      fi
#                                      check for VHnn
      if [ -s $vhnn ] ; then
        vhn="$vhn $vhnn"
      else
        echo "Warning: **** $vhnn is not found in the model gs file. ****"
      fi
#                                      check for VFnn
      if [ -s $vfnn ] ; then
        vfn="$vfn $vfnn"
      else
        echo "Warning: **** $vfnn is not found in the model gs file. ****"
      fi
#                                      check for VTnn
      if [ -s $vtnn ] ; then
        vtn="$vtn $vtnn"
      else
        echo "Warning: **** $vtnn is not found in the model gs file. ****"
      fi

      i=`expr $i + 1`
    done
    nxpl=`echo $xpl | wc -w`
    echo "Process $nxpl 3D tracers on pressure levels $xpl "

    nxml=`echo $xml | wc -w`
    echo "Process $nxml 3D tracers on model levels $xml "

    nvin=`echo $vin | wc -w`
    echo "Found $nvin VI diagnostics $vin"

    nxkn=`echo $xkn | wc -w`
    echo "Found $nxkn XK diagnostics $xkn"

    nxfn=`echo $xfn | wc -w`
    echo "Found $nxfn XF diagnostics $xfn"

    nvmn=`echo $vmn | wc -w`
    echo "Found $nvmn VM diagnostics $vmn"

    nvhn=`echo $vhn | wc -w`
    echo "Found $nvhn VH diagnostics $vhn"

    nvfn=`echo $vfn | wc -w`
    echo "Found $nvfn VF diagnostics $vfn"

    nvtn=`echo $vtn | wc -w`
    echo "Found $nvtn VT diagnostics $vtn"

    slx="$vin $xkn $xfn $vmn $vhn $vfn $vtn"
    nslx=`echo $slx | wc -w`
    echo "Overall, $nslx single-level diagnostics found."

#   ---------------------------------- 3d tracer statistics
#                                      on pressure levels
    if [ $nxpl -ge 1 ] ; then
      xpl0=`echo "$xpl" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      while [ -n "$xpl0" ] ; do
#                                      process maximum 80 tracers at a time
        xpl1=`echo "$xpl0" | cut -f1-80 -d' '`
        gpxstat beta u v w $xpl1 new_gp new_xp input=.ic_gpxstat_pl
        xpl0=`echo "$xpl0" | cut -f81- -d' '`
      done
    fi
#                                      on model levels
    if [ $nxml -ge 1 ] ; then
      xml0=`echo "$xml" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      while [ -n "$xml0" ] ; do
#                                      process maximum 80 tracers at a time
        xml1=`echo "$xml0" | cut -f1-80 -d' '`
        gpxstat beta u v w $xml1 new_gp new_xp input=.ic_gpxstat_ml
        xml0=`echo "$xml0" | cut -f81- -d' '`
      done
    fi
#                                      single-level statistics
    if [ $nslx -ge 1 ] ; then
      slx=`echo "$slx" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      while [ -n "$slx" ] ; do
#                                      process maximum 180 tracers at a time
        slx1=`echo "$slx" | cut -f1-180 -d' '`
        statsav $slx1 new_gp new_xp $stat2nd
        slx=`echo "$slx" | cut -f181- -d' '`
      done
    fi
#                                      save the first time step for VInn
#                                      (needed for budget calculations)
    if [ $nvin -ge 1 ] ; then
      echo "                   1         1" > ic.rcopy
      vin=`echo "$vin" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      while [ -n "$vin" ] ; do
        vin1=""
        rm -f ic.xsave
        for vi in $vin ; do
          rcopy $vi $vi.1 input=ic.rcopy
          vin1="$vin1 $vi.1"
          echo "C*XSAVE       $vi FIRST TIME STEP
" >> ic.xsave
        done
        xsave new_gp $vin1 newgp input=ic.xsave
        mv newgp new_gp
        vin=`echo "$vin" | cut -f81- -d' '`
      done
    fi

#   ---------------------------------- save results.

    if [ ! -s oldgp ] ; then
#                                      access only if not already available
      access oldgp ${flabel}gp
    fi
    xjoin  oldgp new_gp newgp
    save   newgp ${flabel}gp
    delete oldgp

    if [ "$rcm" != "on" ] ; then
      access oldxp ${flabel}xp
      xjoin  oldxp new_xp newxp
      save   newxp ${flabel}xp
      delete oldxp
    fi

#   ---------------------------------- save (sub)daily 3-d tracers, if requested

    if [ "$gpxsave" = "on" ] ; then
      for x in $xpl ; do
        save ${x} ${flabel}_gp${x}
      done
    fi
    if [ "$gsxsave" = "on" ] ; then
      save gspres ${flabel}_gspres
      save gsrho  ${flabel}_gsrho
      for x in $xml ; do
        save ${x} ${flabel}_gs${x}
      done
    fi
