#!/bin/sh
#  Convert to script - 13/DEC/2017 - DY
# ml:                      - remove: FMI, OBEG and OBWG.
# ml: based on gsstats6.dk - remove: CBGO and RES.
# ml: based on gsstats5.dk - remove: GC.
#                          - add: HSEA.
# ml: based on gsstats4.dk.- add: WSNO,ZPND,GA,GFLX,HBL,ILMO,
#                                 PET,ROFB,ROFS,UE,WTAB.
# ml: based on gsstats2.dk.- {FSAN,FLAN} for non-LTE added
#                            for CMAM.
#                          - {FSAM,FLAM} for moon-layer added.
#                          - {FSA,FLA,FSTC,FLTC} now computed
#                            diagnostically here instead of
#                            being saved model variables.
#                          - {FSR,FSRC,OLR,OLRC} are now saved
#                            model variables instead of being
#                            calculated diagnosticqlly here.
#                          - new variables {FSAC,FLAC} also
#                            now saved.
#                          - cloud forcings now defined in
#                            terms of primary fields
#                            FSR,FSRC,OLR,OLRC instead of
#                            FSAG,FSTC,FLAG,FLTC.
#                          - FDLC not needed to be calculated
#                            in this deck, since is now
#                            a primary saved model variable.
#                          - SWMX removed (not post gcm15i).
# sk: based on gsstats.dk - add a few new variables,
#                           revise treatmeant of some variables
#
#                  gsstats7            ml - mar 03/15 ----------------- gsstats7
#
#   ---------------------------------- compute statistics on fields
#                                      in gs file post gcm15i
#                                      (due to rad changes in model).
#
#  Output variables:
#
#   UFS : zonal wind stress
#  OUFS : zonal wind stress without gravity wave drag effects
#   VFS : meridional wind stress
#  OVFS : meridional wind stress without gravity wave drag effects
#  TAUA : wind stress amplitude
# OTAUA : wind stress amplitude without gravity wave drag effects
#    SU : zonal anemometer (10m) wind
#    SV : meridional anemometer (10m) wind
#   SWA : mean 10m wind amplitude - gcm15h+
#   SWX : maximum 10m wind amplitude - gcm15h+
#  SWXU : zonal component of maximum 10m wind - gcm15h+
#  SWXV : meridional component of maximum 10m wind - gcm15h+
#
#   PCP : total precipitation rate
#  PCPN : snowfall rate
#  PCPC : convective precipitation rate
#  PWAT : precipitable water
#   QFS : moisture flux
#   BWG : fresh water flux (at ice-atmosphere interface above sea-ice)
#    SQ : screen specific humidity
#   SRH : screen relative humidity
#  SRHN : screen daily-minimum relative humidity
#  SRHX : screen daily-maximum relative humidity
#
#  These are on model levels:
#
#  SCLD : cloud fraction
#   RHC : relative humidity
#  TACN : optical depth
#   CLW : cloud water content
#   CIC : cloud ice content
#  CLDT : total overlapped cloud amount
#  CLDO : total overlapped cloud amount with optical depth cutoff (for gcm16+)
#  CLWT : integrated cloud water path
#  CICT : integrated cloud ice path
#  TBND : frequency of times temperature passed to radiation out of bounds
#  EA55 : aerosol extinction at 550 nm
#  EV55 : stratospheric aerosol extinction at 550 nm
#  EV11 : stratospheric aerosol extinction at 11 microns
#   RKM : eddy viscosity coefficient for momentum
#   RKH : eddy viscosity coefficient for temperature
#   RKQ : eddy viscosity coefficient for moisture
#
#  PBLH : height of planetary boundary layer
#   TCV : top of convection
#    DR : surface drag coefficients
#
# CLASS:
#
# 1) Energy flux terms:
#
#  FSGV : Net shortwave flux into vegetation
#  FLGV : Net longwave flux into vegetation
#  HMFV : Energy used in melting/ freezing of ice/water on vegetation
#  HFCV : Heat transfer to vegetation via precipitation interception
#  HFSV : Sensible heat flux from vegetation
#  HFLV : Latent heat flux from vegetation
#  FSGN : Net shortwave flux across snow surface
#  FLGN : Net longwave flux across snow surface
#  HMFN : Energy used in melting/ freezing of ice/water in snow pack
#  HFCN : Heat transfer through snow via percolation and conduction
#  HFSN : Sensible heat flux from snow
#  HFLN : Latent heat flux from snow
#  FSGG : Net shortwave flux across soil surface
#  FLGG : Net longwave flux across soil surface
#  HFSG : Sensible heat flux from soil surface
#  HFLG : Latent heat flux from soil surface
#  HMFG : Energy used in melting/ freezing of ice/water in soil layers
#  HFCG : Heat transfer through soil layers via percolation and conduction
#  GFLX : Heat conduction between soil layers
#
# 2) Moisture flux terms:
#
#  PIVF : Frozen water input to vegetation
#  PIVL : Liquid water input to vegetation
#  QFVF : Sublimation rate of frozen water from vegetation
#  QFVL : Evaporation rate of liquid water from vegetation
#   QFN : Sublimation rate from snow surface
#   PIN : Total water input (regardless of source) falling on snow
#   PIG : Total water input (regardless of source) reaching soil surface
#   QFG : Evaporation rate from soil surface
#  ROFB : Vertical drainage at soil base
#  ROFN : Water flow from bottom of snow pack
#  ROFO : Overland runoff
#  ROFS : Lateral subsurface flow in soil
#  ROFV : Total water flow from bottom of vegetation canopy
#  ROVG : Water flow from bottom of vegetation canopy onto bare soil
#  QFVG : Water extraction from soil layers due to transpiration
#  RIVO : River runoff
#
# 3) Energy storage terms:
#
#    TV : Canopy temperature
#    TN : Snow temperature
#    GT : Ground temperature
#  TBAS : Bedrock temperature
#   FVG : Fraction of canopy over bare ground
#    FN : Snow fraction
#   FVN : Fraction of canopy over snow
#  SKYN : Sky view factor for vegetation over snow
#  SKYG : Sky view factor for vegetation over ground
#    TG : Soil layer temperature
#
# 4) Moisture storage terms:
#
#   WVF : Frozen water on vegetation
#   WVL : Liquid water on canopy
#   SNO : Snow depth (water equivalent)
#   WGF : Soil layer frozen water content
#   WGL : Soil layer liquid water content
#   WSNO: Liquid water content of the snow pack.
#   ZPND: Ponding depth in metres.
#
# 5) Misc terms:
#
#    AN : Snow albedo
#    ZN : Snow depth
#  RHON : Snow density
#  SMLT : Snow melting rate
#    MV : Instantaneous vegetation canopy mass
#    TT : Growth index for trees
#   SIC : Sea-ice mass
#  SICN : Sea-ice concentration
#  WTRN : Gains/losses to snow pack because of freezing of surface water or because of disappearance of vegetation canopy
#  WTRG : Gains/losses to surface soil water because of freezing of surface water or because of disappearance of vegetation canopy
#  WTRV : Intercepted water transferred from vegetation canopy to soil or snow because of canopy disappearance
#  GA   : Product of surface drag coefficient and wind speed
#  PET  : Potential evapotranspiration
#  WTAB : Water table depth
#
#  HBL  : Height of boundary layer top (defined if ISLFD=2)
#  ILMO : Inverse of Monin-Obukhov length (defined if ISLFD=2)
#  UE   : Friction velocity (defined if ISLFD=2)
#
#   GTA : mean surface temperature (accumulated)
#   PSA : mean surface pressure (accumulated)
#
# RADIATION:
#
#  1) Energy fluxes:
#
#      a) Atmospheric energy fluxes
#
#   FSO : all/clear sky solar flux incident on top of atmosphere
#   FSA : all-sky solar flux absorbed by atmosphere
#  FSAC : clear-sky solar flux absorbed by atmosphere
#   FLA : all-sky lw flux absorbed/emitted by atmosphere
#  FLAC : clear-sky lw flux absorbed/emitted by atmosphere
#  FSLO : solar/lw overlap (gcm15+)
#  FSAM : all-sky solar flux absorbed in moon layer
#  FLAM : all-sky lw flux absorbed in moon layer
#  FSAN : all-sky solar flux absorbed due to non-LTE
#  FLAN : all-sky lw flux absorbed due to non-LTE
#
#      b) Surface energy fluxes
#
#   FSS : flux received at the surface
#   FSG : solar heat absorbed by ground
#   FLG : net lw at the ground
#   FDL : downward atmospheric lw at sfc
#   FSV : visible flux received at the surface
#   FSD : direct beam flux received at the surface
#  FSDC : direct beam clear-sky flux received at the surface
#   HFS : surface sensible heat flux
#   HFL : surface latent heat flux
#  OFSG : absorbed s/w flux into oceans (w/m2) (gcm15+)
#  HSEA : conductive heat flux upward through snow/seaice
#         (w/m2) (gcm18+)
#
#
#   2) Net fluxes
#
# BALG : radiation budget at the ground
# BALT : radiation budget at top of atmosphere
# BALX : radiation budget at the highest full sigma model interface
#  FSR : reflected solar flux at TOA
# FSRC : clear-sky reflected solar flux at TOA
#  OLR : outgoing l/w radiation
# OLRC : clear-sky outgoing l/w radiation
#  BEG : net surface energy flux (at ice-atmosphere interface above sea-ice)
# BEGL : net surface energy flux over land tile
# BEGK : net surface energy flux over unresolved lakes tile
# BEGO : net surface energy flux over ocean open water tile
# BEGI : net surface energy flux over ocean sea ice tile
# BEGT : net surface energy flux over total ocean tile (open water + seaice tiles)
#
#   3) Near-surface quantities
#
#   ST : screen level (2m) temperature
#  STMX: mean screen level daily maximum temperature
#  STMN: mean screen level daily minimum temperature
#
#   4) Misc
#
#      a) Albedoes - calculated from monthly mean fluxes, by default, unless
#         albedo_monthly=off in which case they are computed from daily fluxes.
#
#  LPA : planetary albedo
# SALB : surface albedo
#
#      b) Clear-sky fluxes, cloud forcings and albedoes
#
# FSTC : net clear-sky solar flux at toa.
# FSGC : net clear-sky solar flux at ground.
# FSSC : incoming clear-sky solar flux at ground.
# CFST : solar cloud forcing at toa.
# CFSB : solar cloud forcing at ground.
# FLTC : net clear-sky lw flux at toa.
# FLGC : net clear-sky lw flux at ground.
# FDLC : incoming clear-sky lw flux at ground.
# CFLT : lw cloud forcing at toa.
# CFLB : lw cloud forcing at ground.
#  CFT : total cloud forcing at toa.
#  CFB : total cloud forcing at ground.
# LPAC : clear-sky planetary albedo
#
#  Moisture conservation diagnostics (mass in units of kg/m2 and corrections in kg/m2-s):
#
#  PWM : precipitable water              averaged/accumulated over month.
#  QHM : moisture hybrid correction      averaged/accumulated over month.
#  QFM : moisture holefill correction    averaged/accumulated over month.
#  QTM : moisture |physics tendency|     averaged/accumulated over month.

#  ---------------------------------- access gs files

.   ggfiles.cdk

#  ---------------------------------- Select variables
#                                     (the number of variables for one select call
#                                      must not exceed 86)

    gsvars="UFS OUFS VFS OVFS BWG \
     PCP PCPC PCPN PWAT QFS SQ SU SV \
     CLD TACN CLW CIC RH CLDT CLWT CICT PBLH TCV DR \
     FSGV FLGV HMFV HFCV HFSV HFLV FSGN FLGN HMFN HFCN HFSN HFLN FSGG FLGG HFSG HFLG HMFG HFCG \
     PIVF PIVL QFVF QFVL ROFV QFN PIN ROFN ROVG PIG QFG ROFO ROF QFVG RIVO \
     TV TN GT TBAS FVG FN FVN SKYG SKYN TG WVF WVL SNO WGF WGL"
    GSVARS=`fmtselname $gsvars`
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars
#                                      rename CLD to SCLD (for backward compatibility)
    mv CLD SCLD
#                                      rename RH to RHC (for backward compatibility)
    mv RH RHC

#                                      select another set of variables (do not exceed 86 vars!)
    gsvars="AN ZN RHON SMLT MV TT SIC SICN WTRN WTRG WTRV GTA PSA \
     FSO FSR FSRC OLR OLRC FSS FSG FLG FDL FDLC FSV FSD HFS HFL FSGC FLGC \
     BEG ST STMX STMN SRH SRHN SRHX SWA SWX SWXU SWXV FSSC CLDO FSLO \
     FSAM FLAM FSAN FLAN PWM QHM QFM QTM FSDC OFSG HSEA \
     WSNO ZPND GA GFLX HBL ILMO PET ROFB ROFS UE WTAB"
    GSVARS=`fmtselname $gsvars`
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars

#                                      select another set of variables (may not exist in older runs)
    gsvars="TBND EA55 RKM RKH RKQ"
    GSVARS=`fmtselname $gsvars`
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars || true

#                                      select another set of variables (does not exist in CMIP5)
    gsvars="EV55 EV11"
    GSVARS=`fmtselname $gsvars`
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars || true

#                                      select tiled BEGL/BEGK/BEGO/BEGI/FARE
    gsvars="BEGL BEGK BEGO BEGI FARE"
    GSVARS=`fmtselname $gsvars`
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars

    release npakgg

#  ----------------------------------- Derivative variables computation
#                                      (mainly based on eng_stat4.dk)

#  ----------------------------------- earth-atmosphere fluxes

    sub FSO  FSLO FSI
    sub FSI  FSR  FSAG
    sub FSI  FSRC FSTC
    sub FSLO OLR  FLAG
    sub FSLO OLRC FLTC

    sub FSAG FSG  FSAX
    sub FSAX FSAM FSA
    sub FSTC FSGC FSAC
    sub FLAG FLG  FLAX
    sub FLAX FLAM FLA
    sub FLTC FLGC FLAC
    rm FSAX FLAX

#  ----------------------------------- energy budgets

    add FSAG FLAG BALT
    add FSG  FLG  BALG
    rm FSAG FLAG

#   ---------------------------------- local planetary albedo.

    if [ "$albedo_monthly" != "off" ] ; then
      timavg FSR .tFSR
      timavg FSO .tFSO
      div   .tFSR .tFSO LPA
    else
      div    FSR  FSO  LPA
    fi

#   ---------------------------------- local clear-sky planetary albedo.

    if [ "$albedo_monthly" != "off" ] ; then
      timavg FSRC .tFSRC
      div  .tFSRC .tFSO LPAC
    else
      div  FSRC   FSO  LPAC
    fi

#   ---------------------------------- ground albedo. limit fsrg to
#                                      non-negative values to avoid problems
#                                      with small numerical differences
#                                      when fss is near zero.
    sub    FSS   FSG  fsrg
    if [ "$albedo_monthly" != "off" ] ; then
      timavg fsrg tfsrg
      timavg FSS  .tFSS
      div   tfsrg .tFSS SALB
    else
      div    fsrg  FSS  SALB
    fi
    rm fsrg

#   ---------------------------------- cloud forcings

    sub FSRC FSR  CFST
    sub FSG  FSGC CFSB
    sub OLRC OLR  CFLT
    sub FLG  FLGC CFLB
    add CFST CFLT CFT
    add CFSB CFLB CFB

#   ---------------------------------- surface wind stresses

    mag2d  UFS  VFS  TAUA
    mag2d OUFS OVFS OTAUA
#                                      convert K->C
#   ---------------------------------- screen temperature and daily maximum/
#                                      minimum screen temperature.

    echo ' XLIN          1.0E0 -2.7316E2' > .xlin_input_card
    for x in ST STMX STMN GT ; do
      xlin $x .$x input=.xlin_input_card
      mv .$x $x
    done

#   ---------------------------------- set FN=0,ZN=0,SNO=0 when FN is small
    echo "C*FMASK           -1 NEXT   GT     1.e-4" | ccc fmask FN FN_mask
    mlt FN FN_mask FN1 ; mv FN1 FN
    mlt ZN FN_mask ZN1 ; mv ZN1 ZN

#  ----------------------------------- compute and save statistics.

    statsav UFS  OUFS VFS  OVFS TAUA OTAUA SU SV \
            PCP  PCPC PCPN PWAT QFS  BWG  SQ \
            SCLD RHC  TACN CLW  CIC  CLDT CLWT CICT \
            PBLH TCV  DR \
            FSGV FLGV HMFV HFCV HFSV HFLV FSGN FLGN HMFN HFCN \
            HFSN HFLN FSGG FLGG HFSG HFLG HMFG HFCG \
            PIVF PIVL QFVF QFVL ROFV QFN  PIN  ROFN ROVG PIG \
            QFG  ROFO ROF  QFVG RIVO \
            TV   GT   TBAS FVG  FN   FVN  SKYN SKYG TG \
            WVF  WVL  SNO  WGF  WGL  WSNO ZPND GA   GFLX \
            HBL  ILMO PET  ROFB ROFS UE   WTAB \
            ZN   SMLT MV   TT   SIC  SICN \
            WTRN WTRG WTRV GTA  PSA \
            FSO  FSA  FLA  FSS  FSG  FLG  FDL  FSV  FSD  HFS  HFL \
            BALT BALG BEG  \
            ST   STMX STMN FSR  FSRC OLR  OLRC FSAC FLAC \
            FSTC FSGC FLTC FLGC FDLC CFST CFSB CFLT CFLB CFT  CFB \
            SRH  SRHN SRHX SWA  SWX  SWXU SWXV FSSC CLDO FSLO \
            FSAM FLAM FSAN FLAN PWM  QHM  QFM  QTM  FSDC \
            OFSG HSEA \
            new_gp new_xp $stat2nd

    if [ -s TBND ] ; then
      statsav TBND EA55 RKM RKH RKQ new_gp new_xp $stat2nd
    fi

    if [ -s EV55 ] ; then
      statsav EV55 EV11 new_gp new_xp $stat2nd
    fi

#   ---------------------------------- monthly extremes
    VARMAX="PCP STMX"
    if [ -s SWA ] ; then
      VARMAX="$VARMAX SWA"
    fi
    if [ -s SWX ] ; then
      VARMAX="$VARMAX SWX"
    fi
    if [ -s SRHX ] ; then
      VARMAX="$VARMAX SRHX"
    fi
    statsav $VARMAX new_gp new_xp timmax

    VARMIN="STMN"
    if [ -s SRHN ] ; then
      VARMIN="$VARMIN SRHN"
    fi
    statsav $VARMIN new_gp new_xp timmin

#   ---------------------------------- use snow temp. to mask out some
#                                      snow-related variables
    echo "C*FMASK           -1 NEXT   GT     1.e-4" | ccc fmask TN TN_mask
    statsav mask=TN_mask AN TN RHON new_gp new_xp off

#  ----------------------------------- compute albedos only where FSO > 5 W/m2
#                                      (~1% of max FSO) to avoid areas where
#                                      albedos are unreliable.

    echo ' FMASK            -1   -1   GT        5.' > .fmask_input_card_alb

    if [ "$albedo_monthly" != "off" ] ; then
      fmask .tFSO  MFSI input=.fmask_input_card_alb
      fmask .tFSS  MFSS input=.fmask_input_card_alb
      statsav mask=MFSI LPA LPAC new_gp new_xp off
      statsav mask=MFSS SALB     new_gp new_xp off
    else
      fmask   FSO  MFSI input=.fmask_input_card_alb
      fmask   FSS  MFSS input=.fmask_input_card_alb
      statsav mask=MFSI LPA LPAC new_gp new_xp $stat2nd
      statsav mask=MFSS SALB     new_gp new_xp $stat2nd
    fi

#  ----------------------------------- compute BEGL/BEGK/BEGI/BEGO/BEGT using FARE

#   BEG* are saved every $isbeg time steps
#   FARE is saved every min($isgg,$isbeg) time steps
    if [ $isgg -lt $isbeg ] ; then
      isfare=$isgg
    else
      isfare=$isbeg
    fi
#                                      select ocean water and sea ice fractions
    ilnd=1 # land
    iulk=2 # unresolved lakes
    irwt=3 # resolved lakes water
    iric=4 # resolved lakes sea ice
    iwat=5 # ocean open water tile
    isic=6 # ocean sea ice tile
    echo "C*SELLEV      1$ilnd" | ccc sellev FARE FAREl # fraction of land tile
    echo "C*SELLEV      1$iulk" | ccc sellev FARE FAREk # fraction of unresolved lakes tile
    echo "C*SELLEV      1$iwat" | ccc sellev FARE FAREw # fraction of ocean open water tile
    echo "C*SELLEV      1$isic" | ccc sellev FARE FAREi # fraction of ocean sea ice tile

#                                      synchronize FARE frequency with that of BEG*
    if [ $isfare -lt $isbeg ] ; then
      nspl=`expr $isbeg / $isfare`
      splt=""
      for n in `seq 1 1 $nspl` ; do
        splt="$splt dum$n"
      done
      rsplit FAREl $splt ; mv dum1 FAREl ; release dum*
      rsplit FAREk $splt ; mv dum1 FAREk ; release dum*
      rsplit FAREw $splt ; mv dum1 FAREw ; release dum*
      rsplit FAREi $splt ; mv dum1 FAREi ; release dum*
    fi
#                                      rescale fluxes over the whole grid cell
    mlt BEGL FAREl BEGLl ; mv BEGLl BEGL
    mlt BEGK FAREk BEGKk ; mv BEGKk BEGK
    mlt BEGO FAREw BEGOw ; mv BEGOw BEGO
    mlt BEGI FAREi BEGIi ; mv BEGIi BEGI
#                                      total ocean flux
    add BEGO BEGI BEGT
#                                      compute monthly means
    statsav BEGL BEGK BEGO BEGI BEGT new_gp new_xp $stat2nd

#   ---------------------------------- save results.

      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
        release oldxp
        access  oldxp ${flabel}xp
        xjoin   oldxp new_xp newxp
        save    newxp ${flabel}xp
        delete  oldxp
      fi
