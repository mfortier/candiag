#!/bin/sh
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Summary of changes.
# Initial version for CMIP6: Jason Cole, 24 January 2019.
# Made changes so it can handle 4XCO2 and Ghan forcing: Jason Cole, 14 March 2019

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Summary of function
#
# Compute the monthly means of the radiative flux profiles and instantaneous radiative
# forcing variables that have been requested for CMIP6.
#
# These variables will be generated when using the following cpp directives
# in CanAM.
#
# cpp1   ---- Set saving of radiative flux profiles (all and clear-sky, up and down)
# cpp1   ---- Note that it will also save flux profiles for diagnostics radiative
# cpp1   ---- forcing calculations as well if "radforce" defined.
# #undef rad_flux_profs
#
# cpp1   ---- Set the calculation and saving of radiative forcing fields
# #undef radforce
#
# Notes
#
#    Only variables that exist in the "gs" or "td" file are processed. Therefore if the variable is
#    not present OR the read does not work properly, the script continues onward without
#    the variable.
#
#    Therefore, if you expected a particular field in the "gp" file and it
#    is not there check first if it is present in the "gs" file and the correct
#    options were set in the GCM submission job.
#

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Output variables

# FLAU -> Upward all-sky longwave radiation flux profiles
# FSAU -> Upward all-sky shortwave radiation flux profiles
# FLAD -> Downward all-sky longwave radiation flux profiles
# FSAD -> Downward all-sky shortwave radiation flux profiles
# FLCU -> Upward clear-sky longwave radiation flux profiles
# FSCU -> Upward clear-sky shortwave radiation flux profiles
# FLCD -> Downward clear-sky longwave radiation flux profiles
# FSCD -> Downward clear-sky shortwave radiation flux profiles

# In the variables below "X" implies
# "4" - 4xCO2 atmosphere
# "G" - aerosol-free atmosphere
# "V" - stratospheric aerosol free atmosphere

# TALX -> TOA outgoing (all-sky) SW radiation in perturbed atmosphere
# TASX -> TOA outgoing (all-sky) LW radiation in perturbed atmosphere
# TCLX -> TOA outgoing clear-sky SW radiation in perturbed atmosphere
# TCSX -> TOA outgoing clear-sky LW radiation in perturbed atmosphere

# LAUX -> Upward all-sky longwave radiation flux profiles in perturbed atmosphere
# SAUX -> Upward all-sky shortwave radiation flux profiles in perturbed atmosphere
# LADX -> Downward all-sky longwave radiation flux profiles in perturbed atmosphere
# SADX -> Downward all-sky shortwave radiation flux profiles in perturbed atmosphere
# LCUX -> Upward clear-sky longwave radiation flux profiles in perturbed atmosphere
# SCUX -> Upward clear-sky shortwave radiation flux profiles in perturbed atmosphere
# LCDX -> Downward clear-sky longwave radiation flux profiles in perturbed atmosphere
# SCDX -> Downward clear-sky shortwave radiation flux profiles in perturbed atmosphere

# Special VolMIP 6-hourly output only requested at the TOA and surface
# The output is only saved if the scenario includes "VOL" and are saved to the "6h" file.
#
# Reference fluxes
# FSRR - Upward all-sky shortwave flux at TOA
# FRCR - Upward clear-sky shortwave flux at TOA
# FSOR - Downward shortwave flux at TOA
# OLRR - Upward all-sky longwave flux at TOA
# OLCR - Upward clear-sky longwave flux at TOA
# FSLR - Downward shortwave flux in the longwave range at TOA
# FSGR - Net all-sky shortwave flux at surface
# FCSR - Net clear-sky shortwave flux at surface
# FLGR - Net all-sky longwave flux at surface
# FCLR - Net clear-sky longwave flux at surface
# FSSR - Downward all-sky shortwave flux at the surface
# FDLR - Downward all-sky longwave flux at the surface
# FSCR - Downward clear-sky shortwave flux at the surface
# FLCR - Downward clear-sky longwave flux at the surface
#
# Diagnostic fluxes
# FSRV - Upward all-sky shortwave flux at TOA in stratospheric aerosol free atmosphere
# FRCV - Upward clear-sky shortwave flux at TOA in stratospheric aerosol free atmosphere
# FSOV - Downward shortwave flux at TOA in stratospheric aerosol free atmosphere
# OLRV - Upward all-sky longwave flux at TOA in stratospheric aerosol free atmosphere
# OLCV - Upward clear-sky longwave flux at TOA in stratospheric aerosol free atmosphere
# FSLV - Downward shortwave flux in the longwave range at TOA in stratospheric aerosol free atmosphere
# FGSV - Net all-sky shortwave flux at surface in stratospheric aerosol free atmosphere
# FCSV - Net clear-sky shortwave flux at surface in stratospheric aerosol free atmosphere
# FLGV - Net all-sky longwave flux at surface in stratospheric aerosol free atmosphere
# FCLV - Net clear-sky longwave flux at surface in stratospheric aerosol free atmosphere
# FSSV - Downward all-sky shortwave flux at the surface in stratospheric aerosol free atmosphere
# FDLV - Downward all-sky longwave flux at the surface in stratospheric aerosol free atmosphere
# FSCV - Downward clear-sky shortwave flux at the surface in stratospheric aerosol free atmosphere
# FLCV - Downward clear-sky longwave flux at the surface in stratospheric aerosol free atmosphere
#
# Heating rate calculations
# The 6-hourly output is only saved if the scenario includes "VOL" and are saved to the "6h" file.
# For all cases the monthly mean heating rates are saved to the "gp" files.
# The same name is used in the 6h and gp files.
#
# HRSR -> All-sky shortwave heating rate profile in unperturbed atmosphere
# HRLR -> All-sky longwave heating rate profile in unperturbed atmosphere
# HSCR -> Clear-sky shortwave heating rate profile in unperturbed atmosphere
# HLCR -> Clear-sky longwave heating rate profile in unperturbed atmosphere
#
# In the variables below "X" implies
# "4" - 4xCO2 atmosphere
# "G" - aerosol-free atmosphere
# "V" - stratospheric aerosol free atmosphere
#
# HRSX -> All-sky shortwave heating rate profile in perturbed atmosphere
# HRLX -> All-sky longwave heating rate profile in perturbed atmosphere
# HSCX -> Clear-sky shortwave heating rate profile in perturbed atmosphere
# HLCX -> Clear-sky longwave heating rate profile in perturbed atmosphere

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Input variables

# FLAU -> Upward all-sky longwave radiation flux profiles
# FSAU -> Upward all-sky shortwave radiation flux profiles
# FLAD -> Downward all-sky longwave radiation flux profiles
# FSAD -> Downward all-sky shortwave radiation flux profiles
# FLCU -> Upward clear-sky longwave radiation flux profiles
# FSCU -> Upward clear-sky shortwave radiation flux profiles
# FLCD -> Downward clear-sky longwave radiation flux profiles
# FSCD -> Downward clear-sky shortwave radiation flux profiles
#
# FSRR - Upward all-sky shortwave flux at TOA
# FRCR - Upward clear-sky shortwave flux at TOA
# FSOR - Downward shortwave flux at TOA
# OLRR - Upward all-sky longwave flux at TOA
# OLCR - Upward clear-sky longwave flux at TOA
# FSLR - Downward shortwave flux in the longwave range at TOA
# FSGR - Net all-sky shortwave flux at surface
# FCSR - Net clear-sky shortwave flux at surface
# FLGR - Net all-sky longwave flux at surface
# FCLR - Net clear-sky longwave flux at surface
# FSSR - Downward all-sky shortwave flux at the surface
# FDLR - Downward all-sky longwave flux at the surface
# FSCR - Downward clear-sky shortwave flux at the surface
# FLCR - Downward clear-sky longwave flux at the surface

# The following are perturbations between current and adjusted atmosphere.
# The "*" can be "1" or "2" but the perturbation depends on what radiative forcing
# scenario was used (RF_SCENARIO).

# LAU* -> Upward all-sky longwave radiation flux profiles in perturbed atmosphere
# SAU* -> Upward all-sky shortwave radiation flux profiles in perturbed atmosphere
# LAD* -> Downward all-sky longwave radiation flux profiles in perturbed atmosphere
# SAD* -> Downward all-sky shortwave radiation flux profiles in perturbed atmosphere
# LCU* -> Upward clear-sky longwave radiation flux profiles in perturbed atmosphere
# SCU* -> Upward clear-sky shortwave radiation flux profiles in perturbed atmosphere
# LCD* -> Downward clear-sky longwave radiation flux profiles in perturbed atmosphere
# SCD* -> Downward clear-sky shortwave radiation flux profiles in perturbed atmosphere
#
# FSR* - Upward all-sky shortwave flux at TOA in stratospheric aerosol free atmosphere
# FRC* - Upward clear-sky shortwave flux at TOA in stratospheric aerosol free atmosphere
# FSO* - Downward shortwave flux at TOA in stratospheric aerosol free atmosphere
# OLR* - Upward all-sky longwave flux at TOA in stratospheric aerosol free atmosphere
# OLC* - Upward clear-sky longwave flux at TOA in stratospheric aerosol free atmosphere
# FSL* - Downward shortwave flux in the longwave range at TOA in stratospheric aerosol free atmosphere
# FGS* - Net all-sky shortwave flux at surface in stratospheric aerosol free atmosphere
# FCS* - Net clear-sky shortwave flux at surface in stratospheric aerosol free atmosphere
# FLG* - Net all-sky longwave flux at surface in stratospheric aerosol free atmosphere
# FCL* - Net clear-sky longwave flux at surface in stratospheric aerosol free atmosphere
# FSS* - Downward all-sky shortwave flux at the surface in stratospheric aerosol free atmosphere
# FDL* - Downward all-sky longwave flux at the surface in stratospheric aerosol free atmosphere
# FSC* - Downward clear-sky shortwave flux at the surface in stratospheric aerosol free atmosphere
# FLC* - Downward clear-sky longwave flux at the surface in stratospheric aerosol free atmosphere

# Note that the heating rate fields are saved in the "gs" files.  The
# unperturbed heating rates are saved as HRSR and HRLR.
#
# HRSR -> All-sky shortwave heating rate profile in unperturbed atmosphere
# HRLR -> All-sky longwave heating rate profile in unperturbed atmosphere
# HSCR -> Clear-sky shortwave heating rate profile in unperturbed atmosphere
# HLCR -> Clear-sky longwave heating rate profile in unperturbed atmosphere
# HRS* -> All-sky shortwave heating rate profile in perturbed atmosphere
# HRL* -> All-sky longwave heating rate profile in perturbed atmosphere
# HSC* -> Clear-sky shortwave heating rate profile in perturbed atmosphere
# HLC* -> Clear-sky longwave heating rate profile in perturbed atmosphere

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Internal variables

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Begin code/script

# ===============================================================
# Access and split gs and if present the td files (for the heating rates)
# ===============================================================

.   ggfiles.cdk

# ===============================================================
# Determine the radiative forcing scenario.
# This script currently handles 4XCO2, GHAN and 4XCO2_GHAN.
# ===============================================================
echo "C*SELECT   STEP         0         0    1         0    0 NAME PARM" | ccc select npakgg parm
bin2txt parm parm.txt
RF_SCENARIO=`grep RF_SCENARIO= parm.txt | gawk -F= '{print $2}' | gawk '{$1=$1};1'`

# ===============================================================
# Read in the variables from the gs file.
# The variables and names depend on the value of RF_SCENARIO.
# ===============================================================

if [ "$RF_SCENARIO" = "4XCO2" ] ; then
    gsvars2="lau1 sau1 lad1 sad1 lcu1 scu1 lcd1 scd1"
    gsvars2o="lau4 sau4 lad4 sad4 lcu4 scu4 lcd4 scd4"
    gsvars3="lau1 sau1 lcu1 scu1"
    gsvars3o="tal4 tas4 tcl4 tcs4"
    gsvars4="hrsr hrlr hscr hlcr hrs1 hrl1 hsc1 hlc1"
    gsvars4o="hrsr hrlr hscr hlcr hrs4 hrl4 hsc4 hlc4"
elif [ "$RF_SCENARIO" = "GHAN" ] ; then
    gsvars2="lau1 sau1 lad1 sad1 lcu1 scu1 lcd1 scd1"
    gsvars2o="laug saug ladg sadg lcug scug lcdg scdg"
    gsvars3="lau1 sau1 lcu1 scu1"
    gsvars3o="talg tasg tclg tcsg"
    gsvars4="hrsr hrlr hscr hlcr hrs1 hrl1 hsc1 hlc1"
    gsvars4o="hrsr hrlr hscr hlcr hrsg hrlg hscg hlcg"
elif [ "$RF_SCENARIO" = "4XCO2_GHAN" ] ; then
    gsvars2="lau1 sau1 lad1 sad1 lcu1 scu1 lcd1 scd1 lau2 sau2 lad2 sad2 lcu2 scu2 lcd2 scd2"
    gsvars2o="lau4 sau4 lad4 sad4 lcu4 scu4 lcd4 scd4 laug saug ladg sadg lcug scug lcdg scdg"
    gsvars3="lau1 sau1 lcu1 scu1 lau2 sau2 lcu2 scu2"
    gsvars3o="tal4 tas4 tcl4 tcs4 talg tasg tclg tcsg"
    gsvars4="hrsr hrlr hscr hlcr hrs1 hrl1 hsc1 hlc1 hrs2 hrl2 hsc2 hlc2"
    gsvars4o="hrsr hrlr hscr hlcr hrs4 hrl4 hsc4 hlc4 hrsg hrlg hscg hlcg"
elif [ "$RF_SCENARIO" = "VOL" ] ; then
    gsvars2="lau1 sau1 lad1 sad1 lcu1 scu1 lcd1 scd1"
    gsvars2o="lauv sauv ladv sadv lcuv scuv lcdv scdv"
    gsvars3="lau1 sau1 lcu1 scu1"
    gsvars3o="talv tasv tclv tcsv"
# These groups below are used for the monthly and/or 6-hourly output
    gsvars4="hrsr hrlr hscr hlcr hrs1 hrl1 hsc1 hlc1"
    gsvars4o="hrsr hrlr hscr hlcr hrsv hrlv hscv hlcv"
    gsvars5="fsrr frcr fsor olrr olcr fslr fsgr fcsr flgr fclr fssr fdlr fscr flcr"
    gsvars5="$gsvars5 fsr1 frc1 fso1 olr1 olc1 fsl1 fsg1 fcs1 flg1 fcl1 fss1 fdl1 fsc1 flc1"
    gsvars5o="fsrr frcr fsor olrr olcr fslr fsgr fcsr flgr fclr fssr fdlr fscr flcr"
    gsvars5o="$gsvars5o fsrv frcv fsov olrv olcv fslv fgsv fcsv flgv fclv fssv fdlv fscv flcv"
elif [ "$RF_SCENARIO" = "TEST" ] ; then
    gsvars2="lau1 sau1 lad1 sad1 lcu1 scu1 lcd1 scd1"
    gsvars2o="laut saut ladt sadt lcut scut lcdt scdt"
    gsvars3="lau1 sau1 lcu1 scu1"
    gsvars3o="talt tast tclt tcst"
# These groups below are used for the monthly and/or 6-hourly output
    gsvars4="hrsr hrlr hscr hlcr hrs1 hrl1 hsc1 hlc1"
    gsvars4o="hrsr hrlr hscr hlcr hrst hrlt hsct hlct"
    gsvars5="fsrr frcr fsor olrr olcr fslr fsgr fcsr flgr fclr fssr fdlr fscr flcr"
    gsvars5="$gsvars5 fsr1 frc1 fso1 olr1 olc1 fsl1 fsg1 fcs1 flg1 fcl1 fss1 fdl1 fsc1 flc1"
    gsvars5o="fsrr frcr fsor olrr olcr fslr fsgr fcsr flgr fclr fssr fdlr fscr flcr"
    gsvars5o="$gsvars5o fsrt frct fsot olrt olct fslt fgst fcst flgt fclt fsst fdlt fsct flct"
fi

# Variable to hold all variables that are present
      outvars=""

# Radiative profiles
gsvars1="flau fsau flad fsad flcu fscu flcd fscd"
GSVARS1=`fmtselname $gsvars1`

echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-999999999 NAME$GSVARS1" | ccc select npakgg $gsvars1 || true

# Radiative forcing profiles
if [ ! -z "$gsvars2" ] ; then
  GSVARS2=`fmtselname $gsvars2`

  echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-999999999 NAME$GSVARS2" | ccc select npakgg $gsvars2o || true
fi

# Radiative forcing at the top of the atmosphere (upward component)
if [ ! -z "$gsvars3" ] ; then
  GSVARS3=`fmtselname $gsvars3`

  echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-1500-1500 NAME$GSVARS3" | ccc select npakgg $gsvars3o || true
fi

# Radiative heating profiles
if [ ! -z "$gsvars4" ] ; then
  GSVARS4=`fmtselname $gsvars4`

  echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-999999999 NAME$GSVARS4" | ccc select npakgg $gsvars4o || true
fi

# Check for each variable and accumulate in outvars those that exist.

for ivar in ${gsvars1} ${gsvars2o} ${gsvars3o} ${gsvars4o}; do
   if [ -s $ivar ] ; then
     outvars="$outvars $ivar"
   fi
done


# ===============================================================
# Save the results monthly.
#
# Only if output variables were generated in this deck.
# ===============================================================

if [ ! -z "${outvars}" ] ; then

  statsav ${outvars} new_gp new_xp $stat2nd

  access oldgp ${flabel}gp
  xjoin oldgp new_gp newgp
  save newgp ${flabel}gp
  delete oldgp na

  if [ "$rcm" != "on" ] ; then
    access oldxp ${flabel}xp
    xjoin oldxp new_xp newxp
    save newxp ${flabel}xp
    delete oldxp
  fi
fi

# Save the 6-hourly output only if RF_SCENARIO = "VOL" or RF_SCENARIO = "TEST"
if [ "$RF_SCENARIO" = "VOL" -o "$RF_SCENARIO" = "TEST" ] ; then
   # Prepare the input cards
   echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model_6h

   # Extract the fluxes at TOA and surface
   if [ ! -z "$gsvars5" ] ; then
      GSVARS5=`fmtselname $gsvars5`
      echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-999999999 NAME$GSVARS5" | ccc select npakgg $gsvars5o || true
   fi

   # The heating rates are already in the variable gsvars4o.  Note for VolMIP just
   # want the zonal mean heating rates.

   # Compute zonal averages, relabel the timesteps and accumulate variables to save.
   vars_all=""
   rm -f ic.xsave.6h
   for v in $gsvars4o ; do
     zonavg ${v} z${v}
     tstep z$v $v.t.6h input=ic.tstep_model_6h
     V=`echo "$v" | sed -e's/^gp//' | tr 'a-z' 'A-Z'`
     echo "C*XSAVE       ${V}
               " >> ic.xsave.6h
     vars_all="$vars_all $v.t.6h"
   done
   for v in $gsvars5o ; do
     tstep $v $v.t.6h input=ic.tstep_model_6h
     V=`echo "$v" | sed -e's/^gp//' | tr 'a-z' 'A-Z'`
     echo "C*XSAVE       ${V}
               " >> ic.xsave.6h
     vars_all="$vars_all $v.t.6h"
   done

# Save the variables to the 6-hourly file
# ------------------------------------ xsave
   vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
   rm -f old
   while [ -n "$vars0" ] ; do
     vars1=`echo "$vars0" | cut -f1-80 -d' '`
     head -160 ic.xsave.6h > ic.xsave1
     echo vars=$vars1
     cat ic.xsave1
     xsave old $vars1 new input=ic.xsave1
     mv new old
     vars0=`echo "$vars0" | cut -f81- -d' '`
     tail -n +161 ic.xsave.6h > ic.xsave1 ; mv ic.xsave1 ic.xsave.6h
   done

#   ---------------------------------- save results.
    access old6h ${flabel}6h
    xjoin  old6h old new6h
    save   new6h ${flabel}6h
    delete old6h
    release old new6h
fi
