#!/bin/sh
#                  rtdiag74            M.Lazare/S.Kharin/V.Arora - Dec 22/2015
#   ---------------------------------- Model run-time diagnostics for CanESM5 with lakes
#
#   Runtime diagnostics is run once a year assuming that model
#   files (gs,tm,etc.) are available on disk for all 12 months.
#
#
#   Switches: (determined automatically)
#
#   PhysA=on/off            physical atmosphere variables
#   CarbA=on/off            atmosphere carbon variables
#   CarbL=on/off            land carbon variables
#
#   keep_old_rtdiag=on/off  keep older rtd files for all years
#   keep_old_rtdiag_number  the number of rtdiag files for previous years that are kept on disk.
#                           Default is 3, i.e., the current year plus previous two years.
#                           If =1, only the latest rtdfile which includes the current year is kept on disk.
#   rtd2vic=on/off          transfer rtd file to Victoria in /plots/pubplots/CanESM_run_plots/rtdfiles/.

# ----------------------------------------------------------------------
#                                      runtime diagnostic deck version
    version="74"

# ----------------------------------------------------------------------
#                                      initialize list of variables to be
#                                      extracted from model files
    gsvars=""   # from gs files each month
    ssvars=""   # from ss files
    tmvars=""   # from tm files

# ---------------------------------- Local function defs

# A simple error exit routine
bail(){
  echo "${jobname}: $1"
  echo "${jobname}: $1" >> haltit
  exit 1
}

# Extract variable definitions from a shell script
pardef(){
  # usage: pardef PARM_txt var1 [var2 ...]
  # Larry Solheim ...Jan,2004

  # Extract particular variable definitions from a shell script
  # and add them to the current environment.
  # This is typically used to extract parmsub variables from a PARM record.

  # The first arg is the name of a text file containing the shell script.
  # The second and subsequent args define a list of variable names to evaluate.
  [ -z "$1" ] && bail "pardef: Missing PARM_txt file name."
  [ -z "$2" ] && bail "pardef: Missing variable name."
  loc_PARM_txt=$1
  [ -z "$loc_PARM_txt" ] && bail "pardef: loc_PARM_txt is missing"
  shift
  loc_env_list="$*"
  [ -z "$loc_env_list" ] && bail "pardef: loc_env_list is missing"

  [ ! -s $loc_PARM_txt ] && bail "pardef: $loc_PARM_txt is missing or empty"

  rm -f parmsub1 parmsub.x

  # Remove any CPP_I definition that may appear in the parmsub section
  addr1='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ss][Tt][Aa][Rr][Tt]/'
  addr2='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ee][Nn][Dd]/'
  eval sed \'${addr1},${addr2}d\' $loc_PARM_txt | grep -v end_of_cpp_i > parmsub.x

  # Remove comments and blank lines
  # Remove any lines that source an external script via ". extscript"
  # Remove " +PARM" line inserted by bin2txt, if any
  sed 's/#.*$//; /^ *[+]/d; /^ *\. .*/d; /^ *$/d' parmsub.x > parmsub1

  # Prepend a shebang line
  echo '#!/bin/sh' | cat - parmsub1 > parmsub.x

  # Ensure the resulting script ends with a blank line
  echo 'echo " "' | cat parmsub.x - > parmsub1

  # Make sure parmsub1 and parmsub.x are equivalent and executable
  cat parmsub1 > parmsub.x
  chmod u+x parmsub.x parmsub1

  [ ! -s parmsub.x ] && bail "pardef: Unable to create parmsub.x"

  echo " "
  for loc_var in $loc_env_list; do

    # Determine parameter values from the parmsub record
    echo 'echo "$'${loc_var}'"' | cat parmsub.x - > parmsub1 # added by SK to fix xemacs fontification bug"
    loc_val=`/bin/sh ./parmsub1|tail -1`

    # Create/reassign a variable of the same name in the current env
    eval ${loc_var}=\"\$loc_val\"

    [ -n "$loc_val" ] && echo "Found in $loc_PARM_txt :: ${loc_var}=$loc_val"
  done
  rm -f parmsub1 parmsub.x
}

# ------------------------------------ access PARM section to obtain values of some parameters
    mon1=`echo ${month_rtdiag_start:-1} | awk '{printf "%02d",$1}'`
    monl=`expr $mon1 + 11`
    if [ $monl -gt 12 ] ; then
      mon2=`echo $monl | awk '{printf "%02d",$1-12}'`
      yr1=`echo $year | awk '{printf "%04d", $1-1}'`;
    else
      mon2=`echo $monl | awk '{printf "%02d",$1}'`
      yr1=$year
    fi
    nmon=`expr $monl - $mon1 + 1`
    month_number=`echo "01 02 03 04 05 06 07 08 09 10 11 12 01 02 03 04 05 06 07 08 09 10 11 12" | cut -f${mon1}-${monl} -d' '`
    echo month_number=${month_number}

    month_names="JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC"
    month_lengths="31 28 31 30 31 30 31 31 30 31 30 31"
    MON1=`echo $month_names | cut -f${mon1} -d' '`
    MON2=`echo $month_names | cut -f${mon2} -d' '`
    LEN2=`echo $month_lengths | cut -f${mon2} -d' '`

    PhysA="on"  # gs file must exist
    release gs$mon1
    access  gs$mon1 ${uxxx}_${runid}_${yr1}_m${mon1}_gs na
    if [ ! -s gs$mon1 ] ; then
      echo "Error: **** ${uxxx}_${runid}_${yr1}_m${mon1}_gs is not found. ****"
      exit 1
    fi

    echo "C*SELECT   STEP         0         0    1         0    0 NAME PARM" |\
     ccc select gs$mon1 parm ||\
     bail "Unable to extract PARM record from ${uxxx}_${runid}_${yr1}_m${mon1}_gs"
    release gs$mon1
    bin2txt parm parm.txt >/dev/null
    [ ! -s parm.txt ] && bail "parm.txt is missing or empty"

    # First determine the value of ntrac to be used below
    # to create a list of tracer variable names (itNN)
    # Also get the grid size (nlat, lonsl)
    # isbeg = coupling frequency and saving frequency of variables sent to coupler. Usually isbeg=12 (3hr), or isbeg=96 (24hr).
    # israd = saving frequency of accumulated variables. Usually israd=96 (24hr)
    # isgg = saving frequency of sampled variables. Usually isgg=24 (6hrs)
    # tiled fields, SIC/SICN and FARE are saved with the same frequency min(isbeg,isgg). Usually =12 (3hr) for ESM, and =24 (6hr) for AMIP.
    pardef parm.txt ntrac nlat lonsl ilev levs delt isgg isbeg israd ntld ntlk ntwt
    [ -z "$ntrac" ] && ntrac=0
    [ -z "$nlat"  ] && bail "nlat is not defined in parm.txt"
    [ -z "$lonsl" ] && bail "lonsl is not defined in parm.txt"
    [ -z "$ilev" ] && bail "ilev is not defined in parm.txt"
    [ -z "$levs" ] && bail "levs is not defined in parm.txt"
    [ -z "$delt" ] && bail "delt is not defined in parm.txt"
    [ -z "$isgg" ] && bail "issg is not defined in parm.txt"
    [ -z "$isbeg" ] && bail "isbeg is not defined in parm.txt"
    [ -z "$israd" ] && bail "israd is not defined in parm.txt"
    [ -z "$ntld" ] && bail "ntld (number of land tiles) is not defined in parm.txt"
    [ -z "$ntlk" ] && bail "ntlk (number of lake tiles) is not defined in parm.txt"
    [ -z "$ntwt" ] && bail "ntwt (number of ocean tiles) is not defined in parm.txt"

    if [ $isgg -lt $isbeg ] ; then
      istile=$isgg
    else
      istile=$isbeg
    fi

    # number of saves per day
    nsavebeg=`echo $isbeg $delt | awk '{printf "%05d",86400/($1*$2)}'`
    nsavegg=`echo $isgg $delt | awk '{printf "%05d",86400/($1*$2)}'`
    nsavetile=`echo $istile $delt | awk '{printf "%05d",86400/($1*$2)}'`

    echo ntrac=$ntrac nlat=$nlat lonsl=$lonsl ilev=$ilev levs=$levs delt=$delt isgg=$isgg isbeg=$isbeg istile=$istile israd=$israd ntld=$ntld ntlk=$ntlk ntwt=$ntwt
    echo nsavebeg=$nsavebeg nsavegg=$nsavegg nsavetile=$nsavetile

    # ISTILE vs. ISRAD
    if [ $istile -gt $israd ] ; then
      # FARE is saved less frequently than other accumulated variables (ISTILE<ISRAD).
      # Bin variables to match FARE frequency.
      nbind=`expr $istile / $israd`
      nspld=1
    elif [ $istile -lt $israd ] ; then
      # FARE is saved more frequently than other accumulated variables  (ISTILE>ISRAD).
      # Select a subset of FARE to match variables frequency.
      nbind=1
      nspld=`expr $israd / $istile`
    else
      # FARE is saved with the same frequency as other accumulated variables (ISTILE=ISRAD).
      # No binning or splitting is needed.
      nbind=1
      nspld=1
    fi
    spltd=""
    for n in `seq 1 1 $nspld` ; do
      spltd="$spltd dumd$n"
    done
    echo israd=$israd istile=$istile nbind=$nbind nspld=$nspld spltd=$spltd

    # ISTILE vs. ISBEG
    if [ $istile -gt $isbeg ] ; then
      # FARE is saved less frequently than other accumulated variables (ISTILE<ISBEG).
      # Bin variables to match FARE frequency.
      nbinb=`expr $istile / $isbeg`
      nsplb=1
    elif [ $istile -lt $isbeg ] ; then
      # FARE is saved more frequently than other accumulated variables  (ISTILE>ISBEG).
      # Select a subset of FARE to match variables frequency.
      nbinb=1
      nsplb=`expr $isbeg / $istile`
    else
      # FARE is saved with the same frequency as other accumulated variables (ISTILE=ISBEG).
      # No binning or splitting is needed.
      nbinb=1
      nsplb=1
    fi
    spltb=""
    for n in `seq 1 1 $nsplb` ; do
      spltb="$spltb dumb$n"
    done
    echo isbeg=$isbeg istile=$istile nbinb=$nbinb nsplb=$nsplb spltb=$spltb

    # ISTILE vs. ISGG
    if [ $istile -gt $isgg ] ; then
      # FARE is saved less frequently than other accumulated variables (ISTILE<ISGG).
      # Bin variables to match FARE frequency.
      nbing=`expr $istile / $isgg`
      nsplg=1
    elif [ $istile -lt $isgg ] ; then
      # FARE is saved more frequently than other accumulated variables  (ISTILE>ISGG).
      # Select a subset of FARE to match variables frequency.
      nbing=1
      nsplg=`expr $isgg / $istile`
    else
      # FARE is saved with the same frequency as other accumulated variables (ISTILE=ISGG).
      # No binning or splitting is needed.
      nbing=1
      nsplg=1
    fi
    spltg=""
    for n in `seq 1 1 $nsplg` ; do
      spltg="$spltg dumg$n"
    done
    echo isgg=$isgg istile=$istile nbing=$nbing nsplg=$nsplg spltg=$spltg

    if [ $ntrac -gt 0 ]; then
      # Create a list of parmsub variable names whose value
      # will (should) be tracer names
      itlist=''
      nn=0
      while [ $nn -lt $ntrac ]; do
        nn=`echo $nn|awk '{printf "%2.2d",$1+1}' -`
        itlist="$itlist it$nn"
      done
      [ -z "$itlist" ] && bail "Error defining itNN variable names"

      # Add these tracer names to the current environment
      pardef parm.txt $itlist
    fi

#     # assign tiles manually (for now)
#     ilnd=1 # land
#     iwat=2 # ocean sea water
#     isic=3 # ocean sea ice
    # assign tiles manually (for now)
    ilnd=1 # land
    iulk=2 # unresolved lakes
    irwt=3 # resolved lakes water
    iric=4 # resolved lakes sea ice
    iwat=5 # ocean sea water
    isic=6 # ocean sea ice

# ocean ice density
#    denio="      913." # AGCM (from spwcon*.f)
    denio="      900." # NEMO
# lake ice density
#    denil="      913." # AGCM (from spwcon*.f)
    denil="      900." # NEMO

# ------------------------------------ Physical atmosphere variables
    if [ "$PhysA" = "on" ] ; then
      gsvars="$gsvars ST STMX STMN SQ SRH FN SNO SIC SICN LUIN LUIM LRIN LRIM GT FSR FSG FSGC FLG FLGC FDL OLR FSLO FSA FLA FSAM FLAM BEG BEGI BEGO BEGL FSGI FSGO HFLI BWGI BWGO HFS HFL BWG PCP PCPC PCPN QFS QFSL ROF RIVO FSO CLDT CLDO FSS FSSC FSD FSDC PWAT CO2 CH4 N2O WGL WGF WVL WVF WSNO ZPND TG TV RHON REF BCSN FMI RES AN TN TBEG TBEI TBES SBEG SBEI SBES TSNN TBWG TTG1 TWG1 FARE GTT SNOT ECO2 FCOO W055 W110"
      if [ "$rtd_xtrachem" = "on" ] ; then
        gsvars="$gsvars ESFS EAIS ESTS EFIS ESVC ESVE  ESFB EAIB ESTB EFIB  ESFO EAIO ESTO EFIO  WDL6 WDD6 WDS6  DD6  SLO3 SLHP SDO3 SDHP SSO3 SSHP  DOX4  WDLB WDDB WDSB  DDB  WDLO WDDO WDSO  DDO"
      fi
      if [ "$rtd_ssvars" = "on" ] ; then
        ssvars="TEMP ES TMPN ESN TMPR ESR"
      else
        ssvars=""
      fi

      #   check if CO2 (scalar CO2 concentrations) exists in gs file
      release gs${mon1}
      access  gs${mon1} ${uxxx}_${runid}_${yr1}_m${mon1}_gs na

      echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME  CO2" | ccc select gs${mon1} co2_gs || true
      if [ -s co2_gs ] ; then
        CarbA="off"
        release co2_gs
      else
        CarbA="on"
      fi

#                                      find AGCM tracers
      if [ -z "$ntrac" ] ; then
        echo "Error: **** PhysA: The number of tracer parameter ntrac is undefined. ****"
        exit 1
      fi

      PLAtracers="off"
      i2=1
      while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval x=\${it$i2}
        if [ -z "$x" ] ; then
          echo "Error: **** tracer name for it$i2 is undefined. ****"
          exit 1
        fi

        if [ "$x" != "CO2" -o "$CarbA" != "on" ] ; then
#                                      skip CO2 tracer (which is processed in CarbA section)
          gsvars="$gsvars VI$i2 VS$i2 VM$i2 VF$i2 VH$i2 VT$i2"
        fi
#                                      check for A01 name: if found then assume PLA-scheme for tracers
        if [ "$x" = "A01" ] ; then
          PLAtracers="on"
        fi
        i2=`expr $i2 + 1`
      done
    fi # PhysA


# ------------------------------------ Determine carbon switches

#   check if CarbL need to be set to on or off

    release tm$mon1
    access  tm$mon1 ${uxxx}_${runid}_${yr1}_m${mon1}_tm na

    if [ -s tm$mon1 ] ; then
      echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME CVEG" | ccc select tm$mon1 cveg_tm || true
      # CVEG must exist for CarbL=on
      if [ -s cveg_tm ] ; then
        CarbL="on"
        release cveg_tm
      else
        CarbL="off"
      fi
      release tm$mon1
    else
      CarbL="off"
    fi

    echo PhysA=$PhysA CarbA=$CarbA CarbL=$CarbL

# ------------------------------------ Atmospheric carbon variables
    if [ "$CarbA" = "on" ] ; then
#                                      find CO2 tracer
      if [ -z "$ntrac" ] ; then
        echo "Error: **** CarbA: The number of tracer parameter ntrac is undefined. ****"
        exit 1
      fi
      iCO2=""
      i2=1
      while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval x=\${it$i2}
        if [ -z "$x" ] ; then
          echo "Error: **** tracer name for it$i2 is undefined. ****"
          exit 1
        fi
        if [ "$x" = "CO2" ] ; then
          iCO2="$i2"
          gsvars="$gsvars XF$i2 XL$i2 VI$i2 VS$i2 VM$i2 VH$i2"
        fi
        i2=`expr $i2 + 1`
      done
      if [ -z "$iCO2" ] ; then
        echo "**** CO2 tracer is not found. ****"
      fi
    fi

    if [ "$CarbL" = "on" ] ; then
#                                      Land carbon variables
      # CTEM output is in tm file
      tmvars="$tmvars CVEG CDEB CHUM CFNP CFNE CFRV CFGP CFNB CFLV CFLD CFLH CFRH CFHT CFLF CFRD CFFD CFFV CBRN WFRA CH4H CH4N CW1D CW2D CLAI"
    fi

    if [ -z "$gsvars" -a -z "$ssvars" -a -z "$tmvars" ] ; then
      echo "*** There is nothing to process ***"
      exit 0
    fi

    echo "gsvars=$gsvars"
    echo "ssvars=$ssvars"
    echo "tmvars=$tmvars"

# ----------------------------------------------------------------------

#                                      starting year of rtdiag.
    if [ -z "$year_rtdiag_start" ] ; then
#                                      try $year_rdiag_start if $year_rtdiag_start is undefined
      if [ -z "$year_rdiag_start" ] ; then
        echo "*** ERROR: Start year is undefined $year_rdiag_start ***"
        exit 1
      else
        year_rtdiag_start="$year_rdiag_start"
      fi
    fi
#                                      new rtd file
    year_rtdiag_start=`echo $year_rtdiag_start | awk '{printf "%04d", $1}'`;
    year=`echo $year | awk '{printf "%04d", $1}'`;
    if [ $mon1 -eq 1 ] ; then
      yearm=$year
    else
      yearm=`echo $year | awk '{printf "%04d", $1 - 1}'`
    fi
#                                      starting year of rtdiag.
    if [ -z "${uxxx_rtd}" ] ; then
      if [ -z "${uxxx}" ] ; then
        uxxx_rtd="sc"
      else
        uxxx_rtd=`echo "$uxxx" | sed -e 's/^m/s/'`
      fi
    fi

#                                      rtd file names for the current and previous years
    keep_old_rtdiag_number=${keep_old_rtdiag_number:=3}
    echo keep_old_rtdiag_number=$keep_old_rtdiag_number

    yearm1=`echo $year | awk '{printf "%04d", $1 - 1}'`
    yearmo=`echo $year $keep_old_rtdiag_number | awk '{printf "%04d", $1 - $2}'`
    rtdfile="${uxxx_rtd}_${runid}_${year_rtdiag_start}${mon1}_${year}${mon2}_rtd074" # current year rtd file
    rtdfile1="${uxxx_rtd}_${runid}_${year_rtdiag_start}${mon1}_${yearm1}${mon2}_rtd074" # last year rtd file
    rtdfileo="${uxxx_rtd}_${runid}_${year_rtdiag_start}${mon1}_${yearmo}${mon2}_rtd074" # older rtd file to be deleted (depends on keep_old_rtdiag_number)
    echo year=$year yearm1=$yearm1 yearmo=$yearmo
    echo rtdfile=$rtdfile
    echo rtdfile1=$rtdfile1
    echo rtdfileo=$rtdfileo

#                                      some common input cards
    echo "C*GGATIM      1    1    1    1" > ic.ggatim
    echo "C*WINDOW      1    1    1    1" > ic.window

# ------------------------------------ access last-year rtd file (except for the very first year)

    if [ $yr1 -gt ${year_rtdiag_start} ] ; then
      access oldrtd $rtdfile1
    fi

# ------------------------------------ Save version and current year

    VERSTXT=`echo $version | awk '{printf "%22.15e", $1}'`
    echo " TIME      1001 VERS         1         1         1    100101        -1
$VERSTXT" > vers.txt
    YEARTXT=`echo $yearm | awk '{printf "%22.15e", $1}'`
    echo " TIME      1001 YEAR         1         1         1    100101        -1
$YEARTXT" > year.txt
    MON1TXT=`echo $mon1 | awk '{printf "%22.15e", $1}'`
    echo " TIME      1001 MON1         1         1         1    100101        -1
$MON1TXT" > mon1.txt
    chabin vers.txt vers
    chabin year.txt year
    chabin mon1.txt mon1

    echo "C*XFIND       VERSION                                      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       YEAR                                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MON1                                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind
    echo "C*XSAVE       VERSION                                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VERS
C*XSAVE       YEAR                                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     YEAR
C*XSAVE       MON1                                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     MON1" >> ic.xsave
    xsave dummy vers year mon1 newrtd input=ic.xsave

    # Nino regions weights

    # NINO3.4 (5S-5N; 170W-120W=190E-240E)
    # Gaussian 128x64 grid
    wind_nino34="   69   86   31   34"
    wght_nino34="C*MKWGHT     64    1  128    1   641 1 0    0"
    wgi1_nino34=".944444444"
    wgi2_nino34=".833333333"
    wgj1_nino34=".791536780"
    wgj2_nino34=".791536780"

    # NINO4 (5S-5N; 160E-150W =160E-210E)
    # Gaussian 128x64 grid
    wind_nino4="   58   76   31   34"
    wght_nino4="C*MKWGHT     64    1  128    1   641 1 0    0"
    wgi1_nino4=".611111111"
    wgi2_nino4=".166666667"
    wgj1_nino4=".791536780"
    wgj2_nino4=".791536780"

    # NINO3 (5S-5N; 90W-150W =210E-270E)
    # Gaussian 128x64 grid
    wind_nino3="   76   97   31   34"
    wght_nino3="C*MKWGHT     64    1  128    1   641 1 0    0"
    wgi1_nino3=".833333333"
    wgi2_nino3=".500000000"
    wgj1_nino3=".791536780"
    wgj2_nino3=".791536780"

    for ind in nino34 nino4 nino3 ; do
      eval wght=\$wght_$ind
      eval wind=\$wind_$ind
      eval wgi1=\$wgi1_$ind
      eval wgi2=\$wgi2_$ind
      eval wgj1=\$wgj1_$ind
      eval wgj2=\$wgj2_$ind

      # spatial weights
      echo "$wght" | ccc mkwght wght

      # select region
      echo "C*WINDOW  ${wind}         1" | ccc window wght nino

      # adjusting factors for partial grid-cell overlap
      i1=`echo "${wind}" | cut -c1-5`
      i2=`echo "${wind}" | cut -c6-10`
      j1=`echo "${wind}" | cut -c11-15`
      j2=`echo "${wind}" | cut -c16-20`
      echo "                  0.$wgj1" | ccc xlin nino const
      echo "C*WINDOW      1 9999$j1$j1         1    1.E+00" | ccc window const const_j1
      echo "                  0.$wgj2" | ccc xlin nino const
      echo "C*WINDOW      1 9999$j2$j2         1    1.E+00" | ccc window const const_j2
      echo "                  0.$wgi1" | ccc xlin nino const
      echo "C*WINDOW  $i1$i1    1 9999         1    1.E+00" | ccc window const const_i1
      echo "                  0.$wgi2" | ccc xlin nino const
      echo "C*WINDOW  $i2$i2    1 9999         1    1.E+00" | ccc window const const_i2
      mlt const_j1 const_j2 const_j12
      mlt const_i1 const_i2 const_i12
      mlt const_i12 const_j12 const_ij12
      mlt const_ij12 nino wght_${ind}
    done

# ------------------------------------ maximum number of variables that can be selected/xsaved

    nselmax=80
    nselmaxp1=`expr $nselmax + 1`
    nselmax2=`expr $nselmax + $nselmax`
    nselmax2p1=`expr $nselmax2 + 1`

    nvarmax=40
    nvarmaxp1=`expr $nvarmax + 1`
    nvarmax2=`expr $nvarmax + $nvarmax`
    nvarmax2p1=`expr $nvarmax2 + 1`

# ------------------------------------ month loop

    rtdlist_ann=""
    rm -f ic.xfind_ann
    rm -f ic.xsave_ann
    for m in ${month_number} ; do
      if [ $mon1 -eq 1 ] ; then
        yr="$year"
      else
        if [ $m -le $mon2 ] ; then
          yr="$year"
        else
          yr=`echo $year | awk '{printf "%04d", $1-1}'`;
        fi
      fi
      days=`echo ${month_names}   | cut -f${m} -d' '`
      lmon=`echo ${month_lengths} | cut -f${m} -d' '`

      rtdlist=""
      rm ic.xfind
      rm ic.xsave
#                                      access model files and select variables
#                                      gs file
      if [ -n "$gsvars" ] ; then
        release gs${m}
        access  gs${m} ${uxxx}_${runid}_${yr}_m${m}_gs
        if [ $m -eq $mon1 ] ; then
#                                      add additional time-invariant variables
          gsvars1="$gsvars DPTH DZG FLND FLKR FLKU"
        else
          gsvars1="$gsvars"
        fi

        varlist=`echo $gsvars1 | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        varlist0="$varlist"
        while [ -n "$varlist0" ] ; do
          varlist1=`echo "$varlist0" | cut -f1-$nselmax -d' '`
          GSVARS=`fmtselname $varlist1`
          echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$GSVARS" | ccc select gs${m} $varlist1 || true
          varlist0=`echo "$varlist0" | cut -f$nselmaxp1- -d' '`
        done
#                                      rename variables
        for x in $gsvars1 ; do
          if [ -s $x ] ; then
            mv $x gs${x}${m}
          fi
        done

#                                      select fractional mask etc. from gs file
        if [ $m -eq $mon1 ]; then
          # fractional masks
          cp gsFLND${mon1} dland_frac # dry land fraction (no unresolved or resolved lakes)
          cp gsFLKU${mon1} ulake_frac # unresolved (parameterized) lakes fraction
          cp gsFLKR${mon1} rlake_frac # resolved lakes fraction
          add dland_frac ulake_frac tland_frac # total land fraction (dry land + unresolved lakes but no resolved lakes)
          echo "C*XLIN          -1.0      1.E0" | ccc xlin tland_frac water_frac # water fraction (1-tland_frac = ocean + resolved lakes)
          sub water_frac rlake_frac ocean_frac # ocean fraction

          # glaciers/bedrock binary mask
          echo "C*FMASK           -1 NEXT   LT      -3.5                   1" | ccc fmask gsDPTH${mon1} glaci_mask # glacier binary mask (DPTH=-4)
          echo "C*FMASK           -1 NEXT   LT      -2.5                   1" | ccc fmask gsDPTH${mon1} glbed_mask # glacier & bedrock binary mask (DPTH=-3 + DPTH=-4)
          sub glbed_mask glaci_mask bedro_mask # bedrock binary mask (DPTH=-3)
          echo "C*FMASK           -1 NEXT   GT      -3.5                   1" | ccc fmask gsDPTH${mon1} nogla_mask # non-glacier land binary mask
          echo "C*FMASK           -1 NEXT   GT      -2.5                   1" | ccc fmask gsDPTH${mon1} noglb_mask # non-glacier/bedrock land binary mask
          # glaciers/bedrock fractional mask
          mlt dland_frac glaci_mask glaci_frac
          mlt dland_frac bedro_mask bedro_frac
          mlt dland_frac nogla_mask nogla_frac
          mlt dland_frac noglb_mask noglb_frac

          # globe mask (1 everywhere)
          echo "C*XLIN            0.        1." | ccc xlin dland_frac globe_frac

          # NH and SH masks
          nlath=`echo $nlat | awk '{printf "%5d",$1/2}'`
          nlatp=`echo $nlat | awk '{printf "%5d",$1/2+1}'`
          nlatnhe=`echo $nlat | awk '{printf "%5.0f",2*$1/3+1}'` # north of 30N
          nlatshe=`echo $nlat | awk '{printf "%5.0f",$1/3}'`     # south of 30S
          nlatnhem1=`echo $nlatnhe | awk '{printf "%5d",$1-1}'`  # south of 30N
          nlatshep1=`echo $nlatshe | awk '{printf "%5d",$1+1}'`  # north of 30S
          nlatnhp=`echo $nlat | awk '{printf "%5.0f",$1*5/6+2}'` # north of 60N
          nlatshp=`echo $nlat | awk '{printf "%5.0f",$1*1/6-1}'` # south of 60S
          nlatshp1=`echo $nlat | awk '{printf "%5.0f",$1*1/6}'`  # north of 60S
          echo nlath=$nlath nlatp=$nlatp nlatnhe=$nlatnhe nlatshe=$nlatshe nlatnhp=$nlatnhp nlatshp=$nlatshp
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window globe_frac globe_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window globe_frac globe_frac_nh
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window ocean_frac ocean_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window ocean_frac ocean_frac_nh
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window tland_frac tland_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window tland_frac tland_frac_nh
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window globe_frac globe_frac_nhe # globe north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window globe_frac globe_frac_she # globe south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window globe_frac globe_frac_tro # globe tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window tland_frac tland_frac_nhe # land north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window tland_frac tland_frac_she # land south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window tland_frac tland_frac_tro # land tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window dland_frac dland_frac_nhe # land north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window dland_frac dland_frac_she # land south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window dland_frac dland_frac_tro # land tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window ocean_frac ocean_frac_nhe # ocean north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window ocean_frac ocean_frac_she # ocean south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window ocean_frac ocean_frac_tro # ocean tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhp 9999         1"        | ccc window globe_frac globe_frac_nhp # north of 60N
          echo "C*WINDOW      1 9999    1$nlatshp         1"        | ccc window globe_frac globe_frac_shp # south of 60S
          echo "C*WINDOW      1 9999$nlatshp1 9999         1"       | ccc window tland_frac noant_frac # land without Antarctica

          # binary mask (=1 of fraction>0, otherwise =0)
          echo "C*FMASK           -1 NEXT   GT      0.00" | ccc fmask ocean_frac_nh ocean_mask_nh
          echo "C*FMASK           -1 NEXT   GT      0.00" | ccc fmask ocean_frac_sh ocean_mask_sh
          echo "C*FMASK           -1 NEXT   GT      0.00" | ccc fmask ulake_frac    ulake_mask
          echo "C*FMASK           -1 NEXT   GT      0.00" | ccc fmask rlake_frac    rlake_mask

          # fraction averages
          globavg globe_frac _ globe_favg
          globavg dland_frac _ dland_favg
          globavg tland_frac _ tland_favg
          globavg ocean_frac _ ocean_favg
          globavg rlake_frac _ rlake_favg
          globavg ulake_frac _ ulake_favg
          globavg nogla_frac _ nogla_favg
          globavg noglb_frac _ noglb_favg
          globavg glaci_frac _ glaci_favg
          globavg bedro_frac _ bedro_favg
          globavg globe_frac_nh _ globe_favg_nh
          globavg globe_frac_sh _ globe_favg_sh
          globavg ocean_frac_nh _ ocean_favg_nh
          globavg ocean_frac_sh _ ocean_favg_sh
          globavg tland_frac_nh _ tland_favg_nh
          globavg tland_frac_sh _ tland_favg_sh
          globavg tland_frac_nhe _ tland_favg_nhe
          globavg tland_frac_she _ tland_favg_she
          globavg tland_frac_tro _ tland_favg_tro
          globavg globe_frac_nhp _ globe_favg_nhp
          globavg globe_frac_shp _ globe_favg_shp

          # areas (m2)
          # earth_radius=6.371220E06 # AGCM
          globe_area_km2="510099699." # AGCM value
          echo "C*XLIN            0.${globe_area_km2}" | ccc xlin globe_favg globe_area_km2 # km2
          echo "C*XLIN          1.E6        0." | ccc xlin globe_area_km2 globe_area # m2
          mlt globe_area dland_favg dland_area
          mlt globe_area tland_favg tland_area
          mlt globe_area ocean_favg ocean_area
          mlt globe_area rlake_favg rlake_area
          mlt globe_area ulake_favg ulake_area
          mlt globe_area nogla_favg nogla_area
          mlt globe_area noglb_favg noglb_area
          mlt globe_area glaci_favg glaci_area
          mlt globe_area bedro_favg bedro_area
          mlt globe_area globe_favg_nh globe_area_nh
          mlt globe_area globe_favg_sh globe_area_sh
          mlt globe_area ocean_favg_nh ocean_area_nh
          mlt globe_area ocean_favg_sh ocean_area_sh
          mlt globe_area tland_favg_nh tland_area_nh
          mlt globe_area tland_favg_sh tland_area_sh
          mlt globe_area tland_favg_nhe tland_area_nhe
          mlt globe_area tland_favg_she tland_area_she
          mlt globe_area tland_favg_tro tland_area_tro
          mlt globe_area globe_favg_nhp globe_area_nhp
          mlt globe_area globe_favg_shp globe_area_shp
          # averaged soil level depth
          echo "C*BINS       -1" | ccc bins gsDZG${mon1}  gsDZGavg
        fi # m=1
      fi # gsvars

#                                      select area tiles (these are AGCM grid-cell fractions) istile=min(isbeg,isgg)
      x="FARE"
      echo "C*SELLEV      1$ilnd" | ccc sellev gs${x}${m} gs${x}l${m} # fraction of dry land
      echo "C*SELLEV      1$iwat" | ccc sellev gs${x}${m} gs${x}w${m} # fraction of ocean sea water
      echo "C*SELLEV      1$isic" | ccc sellev gs${x}${m} gs${x}i${m} # fraction of ocean sea ice
      echo "C*SELLEV      1$iulk" | ccc sellev gs${x}${m} gs${x}ul${m} # fraction over unresolved lakes
      echo "C*SELLEV      1$irwt" | ccc sellev gs${x}${m} gs${x}rw${m} # fraction of resolved lakes water
      echo "C*SELLEV      1$iric" | ccc sellev gs${x}${m} gs${x}ri${m} # fraction of resolved lakes ice
#                                      these are needed when computing ocean averages
      gdiv gs${x}w${m} ocean_frac gs${x}wo${m} #  sea water fraction (ocean portion fraction)
      gdiv gs${x}i${m} ocean_frac gs${x}io${m} #  sea ice fraction (ocean portion fraction)

      # fraction averages
      globavg gs${x}l${m} _ gs${x}l${m}_favg
      globavg gs${x}w${m} _ gs${x}w${m}_favg
      globavg gs${x}i${m} _ gs${x}i${m}_favg
      # areas (m2)
      mlt globe_area gs${x}l${m}_favg gs${x}l${m}_area
      mlt globe_area gs${x}w${m}_favg gs${x}w${m}_area
      mlt globe_area gs${x}i${m}_favg gs${x}i${m}_area

#                                      select subset of FARE to match is* frequencies
      for s in l w i ul rw ri wo io ; do
        rsplit gs${x}${s}${m} $spltd ; mv dumd1 gs${x}d${s}${m} # israd -> istile
        rsplit gs${x}${s}${m} $spltb ; mv dumb1 gs${x}b${s}${m} # isbeg -> istile
        rsplit gs${x}${s}${m} $spltg ; mv dumg1 gs${x}g${s}${m} # isgg  -> istile
      done

#                                      ss file
      if [ -n "$ssvars" ] ; then
        release ss${m}
        access  ss${m} ${uxxx}_${runid}_${yr}_m${m}_ss
        SSVARS=`fmtselname $ssvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$SSVARS" | ccc select ss${m} $ssvars || true
        for x in $ssvars ; do
          if [ -s $x ] ; then
            mv $x ss${x}${m}
          fi
        done
      fi # ssvars

#                                      tm file
      if [ -n "$tmvars" ] ; then
        release tm${m}
        access  tm${m} ${uxxx}_${runid}_${yr}_m${m}_tm
        TMVARS=`fmtselname $tmvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$TMVARS" | ccc select tm${m} $tmvars || true
        for x in $tmvars ; do
          if [ -s $x ] ; then
            mv $x tm${x}${m}
          fi
        done
      fi # tmvars

# #################################### Physical Atmosphere section

      if [ "$PhysA" = "on" ] ; then

# ------------------------------------ monthly time stamps

        x="SECS"
        gsfile=`readlink gs${m}`
        datesec=`stat -c %Y $gsfile`
        echo " TIME      1001 SECS         0         1         1    100101        -1
$datesec.E00" > datesec.txt
        chabin datesec.txt gtgs${x}${m}
        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       DATE IN SECONDS SINCE 1970                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       DATE IN SECONDS SINCE 1970                   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SECS" >> ic.xsave

# ------------------------------------ monthly ST

        x="ST"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      unresolved lakes
        globavg tgs${x}${m} _ gtgs${x}${m}u ulake_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding Antarctica
        globavg tgs${x}${m} _ gtgs${x}${m}c noant_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      NH polar cap
        globavg tgs${x}${m} _ gtgs${x}${m}nhp globe_frac_nhp
#                                      SH polar cap
        globavg tgs${x}${m} _ gtgs${x}${m}shp globe_frac_shp

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}u gtgs${x}${m}o gtgs${x}${m}n gtgs${x}${m}c gtgs${x}${m}i gtgs${x}${m}nhp gtgs${x}${m}shp"
        echo "C*XFIND       SCREEN TEMPERATURE GLOBAL (K)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE UNRESOLVED LAKES (K)      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE OCEAN (K)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE GLACIERS (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE NORTH OF 60N (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE SOUTH OF 60S (K)          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE GLOBAL (K)                (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE UNRESOLVED LAKES (K)      (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE OCEAN (K)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE GLACIERS (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE NORTH OF 60N (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE SOUTH OF 60S (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST" >> ic.xsave

# ------------------------------------ monthly STMX

        x="STMX"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN TEMPERATURE MAX GLOBAL (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX LAND (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX OCEAN (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE MAX GLOBAL (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX LAND (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX OCEAN (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave

# ------------------------------------ monthly STMX max

        x="STMX"
        timmax gs${x}${m} tgs${x}max${m}
#                                      global
        globavg tgs${x}max${m} _ gtgs${x}max${m}g
#                                      land
        globavg tgs${x}max${m} _ gtgs${x}max${m}l tland_frac
#                                      ocean
        globavg tgs${x}max${m} _ gtgs${x}max${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}max${m}g gtgs${x}max${m}l gtgs${x}max${m}o"
        echo "C*XFIND       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX LAND (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX LAND (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave

# ------------------------------------ monthly STMN

        x="STMN"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN TEMPERATURE MIN GLOBAL (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN LAND (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN OCEAN (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE MIN GLOBAL (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN LAND (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN OCEAN (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave

# ------------------------------------ monthly STMN min

        x="STMN"
        timmin gs${x}${m} tgs${x}min${m}
#                                      global
        globavg tgs${x}min${m} _ gtgs${x}min${m}g
#                                      land
        globavg tgs${x}min${m} _ gtgs${x}min${m}l tland_frac
#                                      ocean
        globavg tgs${x}min${m} _ gtgs${x}min${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}min${m}g gtgs${x}min${m}l gtgs${x}min${m}o"
        echo "C*XFIND       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN LAND (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN LAND (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave

# ------------------------------------ monthly SQ

        x="SQ"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN SPECIFIC HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY LAND                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN SPECIFIC HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY LAND                (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ" >> ic.xsave
        fi

# ------------------------------------ monthly SRH

        x="SRH"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN RELATIVE HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY LAND                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN RELATIVE HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY LAND                (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH" >> ic.xsave
        fi

# ------------------------------------ monthly GT

        x="GT"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      unresolved lakes
        globavg tgs${x}${m} _ gtgs${x}${m}u ulake_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}u gtgs${x}${m}i gtgs${x}${m}o"
        echo "C*XFIND       SKIN TEMPERATURE GLOBAL (K)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE UNRESOLVED LAKES (K)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE GLACIERS (K)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN (K)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SKIN TEMPERATURE GLOBAL (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE UNRESOLVED LAKES (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE GLACIERS (K)                (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE OCEAN (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave

# ------------------------------------ monthly GT NINO indices

        x="GT"
        for ind in nino34 nino4 nino3 ; do
          globavw tgs${x}${m} wght_${ind} _ _ gtgs${x}${m}$ind
        done
        rtdlist="$rtdlist gtgs${x}${m}nino34 gtgs${x}${m}nino4 gtgs${x}${m}nino3"
        echo "C*XFIND       SKIN TEMPERATURE NINO34 (K)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE NINO4 (K)                   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE NINO3 (K)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SKIN TEMPERATURE NINO34 (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE NINO4 (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE NINO3 (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave

# ------------------------------------ monthly tile GTT (land/sea water/sea ice tiles) (istile)

        x="GTT"
        y="FARE"
        echo "C*SELLEV      1$ilnd" | ccc sellev gs${x}${m} gs${x}l${m} # GT over land tile
        echo "C*SELLEV      1$iwat" | ccc sellev gs${x}${m} gs${x}w${m} # GT over sea water
        echo "C*SELLEV      1$isic" | ccc sellev gs${x}${m} gs${x}i${m} # GT over sea ice
        echo "C*SELLEV      1$iulk" | ccc sellev gs${x}${m} gs${x}ul${m} # GT over unresolved lakes
#                                      spread over grid cell X*f
        mlt gs${x}l${m}  gs${y}l${m}  gs${x}lg${m}
        mlt gs${x}w${m}  gs${y}w${m}  gs${x}wg${m}
        mlt gs${x}i${m}  gs${y}i${m}  gs${x}ig${m}
        mlt gs${x}ul${m} gs${y}ul${m} gs${x}ulg${m}
#                                      add sea water and sea ice tiles = ocean tiles
        add gs${y}i${m}  gs${y}w${m}   gs${y}o${m}
        add gs${x}wg${m} gs${x}ig${m}  gs${x}og${m}
#                                      daily max
        echo "C*RECMAX    $nsavebeg" | ccc recmax gs${x}lg${m}   gs${x}lgx${m}
        echo "C*RECMAX    $nsavebeg" | ccc recmax gs${x}wg${m}   gs${x}wgx${m}
        echo "C*RECMAX    $nsavebeg" | ccc recmax gs${x}ig${m}   gs${x}igx${m}
#                                      daily min
        echo "C*RECMIN    $nsavebeg" | ccc recmin gs${x}lg${m}   gs${x}lgn${m}
        echo "C*RECMIN    $nsavebeg" | ccc recmin gs${x}wg${m}   gs${x}wgn${m}
        echo "C*RECMIN    $nsavebeg" | ccc recmin gs${x}ig${m}   gs${x}ign${m}
#                                      global mean of X*f: <X*f>
        globavg gs${x}lg${m}  _ ggs${x}lg${m}
        globavg gs${x}wg${m}  _ ggs${x}wg${m}
        globavg gs${x}ig${m}  _ ggs${x}ig${m}
        globavg gs${x}og${m}  _ ggs${x}og${m}
        globavg gs${x}ulg${m} _ ggs${x}ulg${m}

        globavg gs${x}lgx${m} _ ggs${x}lgx${m}
        globavg gs${x}wgx${m} _ ggs${x}wgx${m}
        globavg gs${x}igx${m} _ ggs${x}igx${m}
        globavg gs${x}lgn${m} _ ggs${x}lgn${m}
        globavg gs${x}wgn${m} _ ggs${x}wgn${m}
        globavg gs${x}ign${m} _ ggs${x}ign${m}
#                                      global mean of f: <f>
        globavg gs${y}l${m}  _ ggs${y}l${m}
        globavg gs${y}w${m}  _ ggs${y}w${m}
        globavg gs${y}i${m}  _ ggs${y}i${m}
        globavg gs${y}o${m}  _ ggs${y}o${m}
        globavg gs${y}ul${m} _ ggs${y}ul${m}
#                                      time average of <X*f>: bar<X*f>
        timavg ggs${x}lg${m}  tggs${x}lg${m}
        timavg ggs${x}wg${m}  tggs${x}wg${m}
        timavg ggs${x}ig${m}  tggs${x}ig${m}
        timavg ggs${x}og${m}  tggs${x}og${m}
        timavg ggs${x}ulg${m} tggs${x}ulg${m}

        timavg ggs${x}lgx${m} tggs${x}lgx${m}
        timavg ggs${x}wgx${m} tggs${x}wgx${m}
        timavg ggs${x}igx${m} tggs${x}igx${m}
        timavg ggs${x}lgn${m} tggs${x}lgn${m}
        timavg ggs${x}wgn${m} tggs${x}wgn${m}
        timavg ggs${x}ign${m} tggs${x}ign${m}
#                                      time average of <f>: bar<f>
        timavg ggs${y}l${m}   tggs${y}l${m}
        timavg ggs${y}w${m}   tggs${y}w${m}
        timavg ggs${y}i${m}   tggs${y}i${m}
        timavg ggs${y}o${m}   tggs${y}o${m}
        timavg ggs${y}ul${m}  tggs${y}ul${m}
#                                       compute bar<X*f>/bar<f>
        div tggs${x}lg${m}  tggs${y}l${m}  tggs${x}l${m}
        div tggs${x}wg${m}  tggs${y}w${m}  tggs${x}w${m}
        div tggs${x}ig${m}  tggs${y}i${m}  tggs${x}i${m}
        div tggs${x}og${m}  tggs${y}o${m}  tggs${x}o${m}
        div tggs${x}ulg${m} tggs${y}ul${m} tggs${x}ul${m}

        div tggs${x}lgx${m} tggs${y}l${m}  tggs${x}lx${m}
        div tggs${x}wgx${m} tggs${y}w${m}  tggs${x}wx${m}
        div tggs${x}igx${m} tggs${y}i${m}  tggs${x}ix${m}
        div tggs${x}lgn${m} tggs${y}l${m}  tggs${x}ln${m}
        div tggs${x}wgn${m} tggs${y}w${m}  tggs${x}wn${m}
        div tggs${x}ign${m} tggs${y}i${m}  tggs${x}in${m}
#                                       set missing value when bar<f>=0 (use small epsilon instead)
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}l${m}  ftggs${x}l${m}  tggs${y}l${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}w${m}  ftggs${x}w${m}  tggs${y}w${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}i${m}  ftggs${x}i${m}  tggs${y}i${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}o${m}  ftggs${x}o${m}  tggs${y}o${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}ul${m} ftggs${x}ul${m} tggs${y}ul${m}

        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}lx${m} ftggs${x}lx${m} tggs${y}l${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}wx${m} ftggs${x}wx${m} tggs${y}w${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}ix${m} ftggs${x}ix${m} tggs${y}i${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}ln${m} ftggs${x}ln${m} tggs${y}l${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}wn${m} ftggs${x}wn${m} tggs${y}w${m}
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}in${m} ftggs${x}in${m} tggs${y}i${m}

        rtdlist="$rtdlist ftggs${x}l${m} ftggs${x}w${m} ftggs${x}i${m} ftggs${x}o${m} ftggs${x}ul${m}"
        rtdlist="$rtdlist ftggs${x}lx${m} ftggs${x}wx${m} ftggs${x}ix${m} ftggs${x}ln${m} ftggs${x}wn${m} ftggs${x}in${m}"
        echo "C*XFIND       SKIN TEMPERATURE LAND TILE (K)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OPEN WATER TILE (K)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE SEA ICE TILE (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN TILES (K)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE UNRESOLVED LAKES TILE (K)   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND TILE MAX (K)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OPEN WATER TILE MAX (K)     (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE SEA ICE TILE MAX (K)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND TILE MIN (K)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OPEN WATER TILE MIN (K)     (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE SEA ICE TILE MIN (K)        (${days} ${year_rtdiag_start}-${yearm1})
" >> ic.xfind
        echo "C*XSAVE       SKIN TEMPERATURE LAND TILE (K)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OPEN WATER TILE (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE SEA ICE TILE (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OCEAN TILES (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE UNRESOLVED LAKES TILE (K)   (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE LAND TILE MAX (K)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OPEN WATER TILE MAX (K)     (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE SEA ICE TILE MAX (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE LAND TILE MIN (K)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OPEN WATER TILE MIN (K)     (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE SEA ICE TILE MIN (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT" >> ic.xsave

# ------------------------------------ monthly PCP

        x="PCP"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      unresolved lakes
        globavg tgs${x}${m} _ gtgs${x}${m}u ulake_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}a nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac
#                                      land excluding Antarctica
        globavg tgs${x}${m} _ gtgs${x}${m}c noant_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}u gtgs${x}${m}i gtgs${x}${m}b gtgs${x}${m}a gtgs${x}${m}d gtgs${x}${m}c gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       PRECIPITATION GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION UNRESOLVED LAKES (MM/DAY)      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION GLACIERS (MM/DAY)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION BEDROCK (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC./BEDR.(MM/DAY)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PRECIPITATION GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION UNRESOLVED LAKES (MM/DAY)      (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION GLACIERS (MM/DAY)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION BEDROCK (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC./BEDR.(MM/DAY)  (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave

# ------------------------------------ monthly PCP max

        x="PCP"
        timmax gs${x}${m} tgs${x}max${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x}max${m} tgs${x}max${m}.1 ; mv tgs${x}max${m}.1 tgs${x}max${m}
#                                      global
        globavg tgs${x}max${m} _ gtgs${x}max${m}g
#                                      land
        globavg tgs${x}max${m} _ gtgs${x}max${m}l tland_frac
#                                      ocean
        globavg tgs${x}max${m} _ gtgs${x}max${m}o ocean_frac
#                                      NH extratropics
        globavg tgs${x}max${m} _ gtgs${x}max${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}max${m} _ gtgs${x}max${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}max${m} _ gtgs${x}max${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}max${m}g gtgs${x}max${m}l gtgs${x}max${m}o gtgs${x}max${m}n gtgs${x}max${m}s gtgs${x}max${m}t"
        echo "C*XFIND       MAX PRECIPITATION GLOBAL (MM/DAY)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION LAND (MM/DAY)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION OCEAN (MM/DAY)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30N-90N (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-90S (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-30N (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX PRECIPITATION GLOBAL (MM/DAY)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION LAND (MM/DAY)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION OCEAN (MM/DAY)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30N-90N (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-90S (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-30N (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave

# ------------------------------------ monthly PCPC

        x="PCPC"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       CONV. PRECIPITATION GLOBAL (MM/DAY)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION LAND (MM/DAY)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION OCEAN (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30N-90N (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-90S (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-30N (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       CONV. PRECIPITATION GLOBAL (MM/DAY)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION LAND (MM/DAY)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION OCEAN (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30N-90N (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-90S (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-30N (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC" >> ic.xsave

# ------------------------------------ monthly snowfall PCPN

        x="PCPN"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      unresolved lakes
        globavg tgs${x}${m} _ gtgs${x}${m}u ulake_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}a nogla_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}u gtgs${x}${m}i gtgs${x}${m}b gtgs${x}${m}a gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       SNOWFALL RATE GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE UNRESOLVED LAKES (MM/DAY)      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE GLACIERS (MM/DAY)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE BEDROCK (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOWFALL RATE GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE UNRESOLVED LAKES (MM/DAY)      (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE GLACIERS (MM/DAY)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE BEDROCK (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave

# ------------------------------------ monthly snowfall PCPN over tiles (israd >= istile) - not accurate over water and ice!

        x="PCPN"
        y="FARE"
#                                      spread over grid cell
        mlt gs${x}${m} gs${y}dl${m} gs${x}lg${m}
        mlt gs${x}${m} gs${y}dw${m} gs${x}wg${m}
        mlt gs${x}${m} gs${y}di${m} gs${x}ig${m}
#                                      global mean
        globavg gs${x}lg${m} _ ggs${x}lg${m}
        globavg gs${x}wg${m} _ ggs${x}wg${m}
        globavg gs${x}ig${m} _ ggs${x}ig${m}
#                                      time average
        timavg ggs${x}lg${m} tggs${x}lg${m}
        timavg ggs${x}wg${m} tggs${x}wg${m}
        timavg ggs${x}ig${m} tggs${x}ig${m}

        add tggs${x}wg${m} tggs${x}ig${m} tggs${x}og${m}
        add tggs${x}lg${m} tggs${x}og${m} tggs${x}gg${m}

        rtdlist="$rtdlist tggs${x}lg${m} tggs${x}wg${m} tggs${x}ig${m} tggs${x}og${m} tggs${x}gg${m}"
        echo "C*XFIND       GLOBAL SNOWFALL RATE LAND (KG/M2/S)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE WATER (KG/M2/S)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE SEAICE (KG/M2/S)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE OCEAN (KG/M2/S)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE GLOBE (KG/M2/S)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       GLOBAL SNOWFALL RATE LAND (KG/M2/S)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE WATER (KG/M2/S)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE SEAICE (KG/M2/S)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE OCEAN (KG/M2/S)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE GLOBE (KG/M2/S)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave

# ------------------------------------ monthly evaporation QFS

        x="QFS"
        timavg gs${x}${m}  tgs${x}${m}
        timavg gs${x}L${m} tgs${x}L${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x}${m}  tgs${x}${m}.1  ; mv tgs${x}${m}.1  tgs${x}${m}
        echo "C*XLIN        86400." | ccc xlin tgs${x}L${m} tgs${x}L${m}.1 ; mv tgs${x}L${m}.1 tgs${x}L${m}
#                                      global
        globavg tgs${x}${m}  _ gtgs${x}${m}g
#                                      dry land
        globavg tgs${x}L${m} _ gtgs${x}${m}l dland_frac
#                                      unresolved lakes
        globavg tgs${x}L${m} _ gtgs${x}${m}u ulake_frac
#                                      glaciers
        globavg tgs${x}L${m} _ gtgs${x}${m}i glaci_frac
#                                      bedrock
        globavg tgs${x}L${m} _ gtgs${x}${m}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}L${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}L${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}u gtgs${x}${m}i gtgs${x}${m}b gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       EVAPORATION GLOBAL (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND (MM/DAY)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION UNRESOLVED LAKES (MM/DAY)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION GLACIERS (MM/DAY)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION BEDROCK (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC./BEDR.(MM/DAY)    (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       EVAPORATION GLOBAL (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND (MM/DAY)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION UNRESOLVED LAKES (MM/DAY)        (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION GLACIERS (MM/DAY)                (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION BEDROCK (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC./BEDR.(MM/DAY)    (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave

# ------------------------------------ monthly ROF

        x="ROF"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}i gtgs${x}${m}b gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       RUNOFF LAND (MM/DAY)                         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF GLACIERS (MM/DAY)                     (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF BEDROCK (MM/DAY)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC./BEDR.(MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       RUNOFF LAND (MM/DAY)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF GLACIERS (MM/DAY)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF BEDROCK (MM/DAY)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC./BEDR.(MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF" >> ic.xsave

# ------------------------------------ monthly RIVO

        x="RIVO"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin gtgs${x}${m}o gtgs${x}${m}o.1 ; mv gtgs${x}${m}o.1 gtgs${x}${m}o

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       RIVER DISCHARGE (MM/DAY)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       RIVER DISCHARGE (MM/DAY)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM     RIVO" >> ic.xsave
        fi

# ------------------------------------ monthly BWGI (isbeg >= istile)

        x="BWGI" # freshwater flux P-E over sea ice
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${z}${m} ] ; then
#                                      spread over ocean portion of grid cell
        mlt gs${x}${m} gs${z}bio${m} gs${x}${m}i

        timavg gs${x}${m}i tgs${x}${m}i
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin gtgs${x}${m}io gtgs${x}${m}io.1 ; mv gtgs${x}${m}io.1 gtgs${x}${m}io

        rtdlist="$rtdlist gtgs${x}${m}io"
        echo "C*XFIND       NET SFC WATER FLUX SEAICE BWGI (MM/DAY)      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC WATER FLUX SEAICE BWGI (MM/DAY)      (${days} ${year_rtdiag_start}-${year})
NEWNAM     BWGI" >> ic.xsave
        fi

# ------------------------------------ monthly BWGO (isbeg >= istile)

        y="BWGO" # freshwater flux P-E over ocean
        z="FARE" # fractional areas
        if [ -s gs${y}${m} -a -s gs${z}${m} ] ; then
#                                      spread over ocean portion of grid cell
        mlt gs${y}${m} gs${z}bwo${m} gs${y}${m}w

        timavg gs${y}${m}w tgs${y}${m}w
#                                      ocean mean
        globavg tgs${y}${m}w _ gtgs${y}${m}wo ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin gtgs${y}${m}wo gtgs${y}${m}wo.1 ; mv gtgs${y}${m}wo.1 gtgs${y}${m}wo

        rtdlist="$rtdlist gtgs${y}${m}wo"
        echo "C*XFIND       NET SFC WATER FLUX OPEN WATER BWGO (MM/DAY)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC WATER FLUX OPEN WATER BWGO (MM/DAY)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     BWGO" >> ic.xsave
        fi

# ------------------------------------ monthly (P-E) ocean

        x1="BWGO"
        x2="BWGI"
        x="PME"
        if [ -s gtgs${x1}${m}wo -a -s gtgs${x2}${m}io ] ; then
        add gtgs${x1}${m}wo gtgs${x2}${m}io gtgs${x}${m}o  # P-E over ocean

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       OCEAN P-E (MM/DAY)                           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN P-E (MM/DAY)                           (${days} ${year_rtdiag_start}-${year})
NEWNAM      BWG" >> ic.xsave
        fi

# ------------------------------------ monthly (P-E+R) ocean

        x1="PME"
        x2="RIVO"
        x="PMER"
        if [ -s gtgs${x1}${m}o -a -s gtgs${x2}${m}o ] ; then
        add gtgs${x1}${m}o gtgs${x2}${m}o gtgs${x}${m}o # P-E+R over ocean

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       OCEAN P-E PLUS RUNOFF (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN P-E PLUS RUNOFF (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PMER" >> ic.xsave
        fi

# ------------------------------------ monthly QFS over ocean

        x1="PCP"
        x2="PME"
        x="QFS"
        if [ -s gtgs${x1}${m}o -a -s gtgs${x2}${m}o ] ; then
	sub gtgs${x1}${m}o gtgs${x2}${m}o gtgs${x}${m}o  # QFS over ocean

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       EVAPORATION OCEAN (MM/DAY)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       EVAPORATION OCEAN (MM/DAY)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave
        fi

# ------------------------------------ monthly ocean ice mass (SIC = kg)

        x="SIC"
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      convert kg/m2 to kg
        gmlt gtgs${x}${m}o ocean_area gtgs${x}m${m}o

        rtdlist="$rtdlist gtgs${x}m${m}o"
        echo "C*XFIND       OCEAN SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ unresolved lakes monthly ice mass (LUIM = kg/ice covered area)

        x="LUIM"
        y="LUIN"
        if [ -s gs${x}${m} -a  -s gs${y}${m} ] ; then
#                                      spread over lake area
        mlt gs${x}${m} gs${y}${m} gs${x}u${m}

        timavg gs${x}u${m} tgs${x}u${m}

        globavg tgs${x}u${m} _ gtgs${x}u${m}u ulake_frac
#                                      convert kg/m2 to kg
        gmlt gtgs${x}u${m}u ulake_area gtgs${x}m${m}u

        rtdlist="$rtdlist gtgs${x}m${m}u"
        echo "C*XFIND       UNRESOLVED LAKES SEAICE MASS (KG)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       UNRESOLVED LAKES SEAICE MASS (KG)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     LUIM" >> ic.xsave
        fi

# # ------------------------------------ resolved lakes monthly ice mass (LRIM = kg)

#         x="LRIM"
#         if [ -s gs${x}${m} ] ; then
#         timavg gs${x}${m} tgs${x}${m}

#         globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac
# #                                      convert kg/m2 to kg
#         gmlt gtgs${x}${m}r rlake_area gtgs${x}m${m}r

#         rtdlist="$rtdlist gtgs${x}m${m}r"
#         echo "C*XFIND       RESOLVED LAKES SEAICE MASS (KG)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
#         echo "C*XSAVE       RESOLVED LAKES SEAICE MASS (KG)              (${days} ${year_rtdiag_start}-${year})
# NEWNAM     LRIM" >> ic.xsave
#         fi

# ------------------------------------ ocean sea ice area SICN

        x="SICN"
        timavg gs${x}${m} tgs${x}${m}

        globavg tgs${x}${m} _ gtgs${x}${m}no ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}so ocean_frac_sh
#                                      convert to total area
        gmlt gtgs${x}${m}no ocean_area_nh gtgs${x}a${m}no
        gmlt gtgs${x}${m}so ocean_area_sh gtgs${x}a${m}so

        rtdlist="$rtdlist gtgs${x}a${m}no gtgs${x}a${m}so"
        echo "C*XFIND       OCEAN SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN" >> ic.xsave

# ------------------------------------ unresolved lake sea ice area LUIN

        x="LUIN"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}

        globavg tgs${x}${m} _ gtgs${x}${m}ul ulake_frac
#                                      convert to total area
        gmlt gtgs${x}${m}ul ulake_area gtgs${x}a${m}ul

        rtdlist="$rtdlist gtgs${x}a${m}ul"
        echo "C*XFIND       UNRESOLVED LAKES SEAICE AREA SICN>0 (M2)     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       UNRESOLVED LAKES SEAICE AREA SICN>0 (M2)     (${days} ${year_rtdiag_start}-${year})
NEWNAM     LUIN" >> ic.xsave
        fi

# # ------------------------------------ resolved lake sea ice area LRIN

#         x="LRIN"
#         if [ -s gs${x}${m} ] ; then
#         timavg gs${x}${m} tgs${x}${m}

#         globavg tgs${x}${m} _ gtgs${x}${m}rl rlake_frac
# #                                      convert to total area
#         gmlt gtgs${x}${m}rl rlake_area gtgs${x}a${m}rl

#         rtdlist="$rtdlist gtgs${x}a${m}rl"
#         echo "C*XFIND       RESOLVED LAKES SEAICE AREA SICN>0 (M2)       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
#         echo "C*XSAVE       RESOLVED LAKES SEAICE AREA SICN>0 (M2)       (${days} ${year_rtdiag_start}-${year})
# NEWNAM     LRIN" >> ic.xsave
#         fi

# ------------------------------------ ocean sea ice extent derived from monthly means SICN>0.15

        x="SICN"
        echo "FMASK             -1 NEXT   GT      0.15                   1                   1" | ccc fmask tgs${x}${m} tgsSICE${m}
        x="SICE"

        globavg tgs${x}${m} _ gtgs${x}${m}no ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}so ocean_frac_sh
#                                      convert to total area
        gmlt gtgs${x}${m}no ocean_area_nh gtgs${x}e${m}no
        gmlt gtgs${x}${m}so ocean_area_sh gtgs${x}e${m}so

        rtdlist="$rtdlist gtgs${x}e${m}no gtgs${x}e${m}so"
        echo "C*XFIND       OCEAN SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN" >> ic.xsave

# ------------------------------------ unresolved lake sea ice extent derived from monthly means LUIN>0.15

        x="LUIN"
        if [ -s gs${x}${m} ] ; then
        echo "FMASK             -1 NEXT   GT      0.15                   1                   1" | ccc fmask tgs${x}${m} tgsLUIE${m}
        x="LUIE"

        globavg tgs${x}${m} _ gtgs${x}${m}ul ulake_frac
#                                      convert to total area
        gmlt gtgs${x}${m}ul ulake_area gtgs${x}e${m}ul

        rtdlist="$rtdlist gtgs${x}e${m}ul"
        echo "C*XFIND       UNRESOLVED LAKES SEAICE COVER SICN>15% (M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       UNRESOLVED LAKES SEAICE COVER SICN>15% (M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     LUIN" >> ic.xsave
        fi

# # ------------------------------------ resolved lake sea ice extent derived from monthly means LRIN>0.15

#         x="LRIN"
#         if [ -s gs${x}${m} ] ; then
#         echo "FMASK             -1 NEXT   GT      0.15                   1                   1" | ccc fmask tgs${x}${m} tgsLRIE${m}
#         x="LRIE"

#         globavg tgs${x}${m} _ gtgs${x}${m}rl rlake_frac
# #                                      convert to total area
#         gmlt gtgs${x}${m}rl rlake_area gtgs${x}e${m}rl

#         rtdlist="$rtdlist gtgs${x}e${m}rl"
#         echo "C*XFIND       RESOLVED LAKES SEAICE COVER SICN>15% (M2)    (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
#         echo "C*XSAVE       RESOLVED LAKES SEAICE COVER SICN>15% (M2)    (${days} ${year_rtdiag_start}-${year})
# NEWNAM     LRIN" >> ic.xsave
#         fi

# ------------------------------------ ocean monthly SIC volume

        x="SIC"
#                                      convert to thickness (m) by dividing by ice density
        echo "C*XLIN    ${denio}      0.E0         1" | ccc xlin tgs${x}${m} tgs${x}h${m}

        globavg tgs${x}h${m} _ gtgs${x}h${m}on ocean_frac_nh
        globavg tgs${x}h${m} _ gtgs${x}h${m}os ocean_frac_sh
#                                      convert to volume (m3)
        gmlt gtgs${x}h${m}on ocean_area_nh gtgs${x}v${m}on
        gmlt gtgs${x}h${m}os ocean_area_sh gtgs${x}v${m}os

        rtdlist="$rtdlist gtgs${x}v${m}on gtgs${x}v${m}os"
        echo "C*XFIND       OCEAN SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ unresolved lake monthly LUIM volume

        x="LUIM"
        if [ -s tgs${x}u${m} ] ; then
#                                      convert to thickness (m) by dividing by ice density
        echo "C*XLIN    ${denil}      0.E0         1" | ccc xlin tgs${x}u${m} tgs${x}h${m}

        globavg tgs${x}h${m} _ gtgs${x}h${m}ul ulake_frac
#                                      convert to volume (m3)
        gmlt gtgs${x}h${m}ul ulake_area gtgs${x}v${m}ul

        rtdlist="$rtdlist gtgs${x}v${m}ul"
        echo "C*XFIND       UNRESOLVED LAKES SEAICE VOLUME (M3)          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       UNRESOLVED LAKES SEAICE VOLUME (M3)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     LUIM" >> ic.xsave
        fi

# # ------------------------------------ resolved lake monthly LRIM volume

#         x="LRIM"
#         if [ -s gs${x}${m} ] ; then
# #                                      convert to thickness (m) by dividing by ice density
#         echo "C*XLIN    ${denil}      0.E0         1" | ccc xlin tgs${x}${m} tgs${x}h${m}

#         globavg tgs${x}h${m} _ gtgs${x}h${m}rl rlake_frac
# #                                      convert to volume (m3)
#         gmlt gtgs${x}h${m}rl rlake_area gtgs${x}v${m}rl

#         rtdlist="$rtdlist gtgs${x}v${m}rl"
#         echo "C*XFIND       RESOLVED LAKES SEAICE VOLUME (M3)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
#         echo "C*XSAVE       RESOLVED LAKES SEAICE VOLUME (M3)            (${days} ${year_rtdiag_start}-${year})
# NEWNAM     LRIM" >> ic.xsave
#         fi

# ------------------------------------ ocean monthly SIC thickness

        x="SIC"
        y="SICN"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert to thickness (m) by dividing by ice denisty
        echo "C*XLIN    ${denio}      0.E0         1" | ccc xlin gs${x}${m} gs${x}h${m}
        div gs${x}h${m} gs${y}${m} gs${x}H${m}

        gmlt gs${y}${m} ocean_frac_nh mask_ocean_frac_nh
        gmlt gs${y}${m} ocean_frac_sh mask_ocean_frac_sh

        globavg gs${x}H${m} _ ggs${x}h${m}on mask_ocean_frac_nh
        globavg gs${x}H${m} _ ggs${x}h${m}os mask_ocean_frac_sh

        timavg ggs${x}h${m}on tggs${x}h${m}on
        timavg ggs${x}h${m}os tggs${x}h${m}os

        rtdlist="$rtdlist tggs${x}h${m}on tggs${x}h${m}os"
        echo "C*XFIND       OCEAN SEAICE THICKNESS NH (M)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE THICKNESS SH (M)                (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SEAICE THICKNESS NH (M)                (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE THICKNESS SH (M)                (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ unresolved lake monthly SIC thickness

        x="LUIM"
        y="LUIN"
        if [ -s gs${x}u${m} -a -s gs${y}${m} ] ; then
#                                      convert mass to lake thickness (m) by dividing by ice density
        echo "C*XLIN    ${denil}      0.E0         1" | ccc xlin gs${x}u${m} gs${x}h${m}
#                                      thickness where ice
        div gs${x}h${m} gs${y}${m} gs${x}H${m}

        gmlt gs${y}${m} ulake_frac    mask_ulake_frac

        globavg gs${x}H${m} _ ggs${x}h${m}ul mask_ulake_frac

        timavg ggs${x}h${m}ul tggs${x}h${m}ul

        rtdlist="$rtdlist tggs${x}h${m}ul"
        echo "C*XFIND       UNRESOLVED LAKES SEAICE THICKNESS (M)        (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       UNRESOLVED LAKES SEAICE THICKNESS (M)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     LUIM" >> ic.xsave
        fi

# # ------------------------------------ resolved lake monthly SIC thickness

#         x="LRIM"
#         y="LRIN"
#         if [ -s gs${x}${m} -a -s gs${y}${m} ] ; then
#         timavg gs${x}${m} tgs${x}${m}
# #                                      convert to thickness (m) by dividing by ice denisty
#         echo "C*XLIN    ${denil}      0.E0         1" | ccc xlin gs${x}${m} gs${x}h${m}
#         div gs${x}h${m} gs${y}${m} gs${x}H${m}

#         gmlt gs${y}${m} rlake_frac    mask_rlake_frac

#         globavg gs${x}H${m} _ ggs${x}h${m}rl mask_rlake_frac

#         timavg ggs${x}h${m}rl tggs${x}h${m}rl

#         rtdlist="$rtdlist tggs${x}h${m}rl"
#         echo "C*XFIND       RESOLVED LAKES SEAICE THICKNESS (M)          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
#         echo "C*XSAVE       RESOLVED LAKES SEAICE THICKNESS (M)          (${days} ${year_rtdiag_start}-${year})
# NEWNAM     LRIM" >> ic.xsave
#         fi

# ------------------------------------ ocean monthly maximum SIC thickness (derived from monthly values)

        x="SIC"

        timavg gs${x}H${m} tgs${x}H${m}

        gmlt tgs${x}H${m} ocean_mask_nh tgs${x}h${m}on
        gmlt tgs${x}H${m} ocean_mask_sh tgs${x}h${m}os

        rmax tgs${x}h${m}on tgs${x}hx${m}on
        rmax tgs${x}h${m}os tgs${x}hx${m}os

        window tgs${x}hx${m}on gtgs${x}hx${m}on input=ic.window
        window tgs${x}hx${m}os gtgs${x}hx${m}os input=ic.window

        rtdlist="$rtdlist gtgs${x}hx${m}on gtgs${x}hx${m}os"
        echo "C*XFIND       MAX OCEAN SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX OCEAN SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX OCEAN SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX OCEAN SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ unresolved monthly maximum SIC thickness (derived from monthly values)

        x="LUIM"
        if [ -s gs${x}H${m} ] ; then
        timavg gs${x}H${m} tgs${x}H${m}

        gmlt tgs${x}H${m} ulake_mask    tgs${x}h${m}ul

        rmax tgs${x}h${m}ul tgs${x}hx${m}ul

        window tgs${x}hx${m}ul gtgs${x}hx${m}ul input=ic.window

        rtdlist="$rtdlist gtgs${x}hx${m}ul"
        echo "C*XFIND       MAX UNRESOLVED LAKES SEAICE THICKNESS (M)    (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX UNRESOLVED LAKES SEAICE THICKNESS (M)    (${days} ${year_rtdiag_start}-${year})
NEWNAM     LUIM" >> ic.xsave
        fi

# # ------------------------------------ resolved lakes monthly maximum SIC thickness (derived from monthly values)

#         x="LRIM"
#         if [ -s gs${x}${m} ] ; then
#         timavg gs${x}H${m} tgs${x}H${m}

#         gmlt tgs${x}H${m} rlake_mask    tgs${x}h${m}rl

#         rmax tgs${x}h${m}rl tgs${x}hx${m}rl

#         window tgs${x}hx${m}rl gtgs${x}hx${m}rl input=ic.window

#         rtdlist="$rtdlist gtgs${x}hx${m}rl"
#         echo "C*XFIND       MAX RESOLVED LAKES SEAICE THICKNESS (M)      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
#         echo "C*XSAVE       MAX RESOLVED LAKES SEAICE THICKNESS (M)      (${days} ${year_rtdiag_start}-${year})
# NEWNAM     LRIM" >> ic.xsave
#         fi

# ------------------------------------ monthly CLDT

        x="CLDT"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDT             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDT               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDT              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDT             (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDT               (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDT              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT" >> ic.xsave
        fi

# ------------------------------------ monthly CLDO

        x="CLDO"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDO             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDO               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDO              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDO             (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDO               (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDO              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO" >> ic.xsave
        fi

# ------------------------------------ monthly radiation fluxes
#                                      compute BALT=radiation budget at top of atmosphere
#                                      compute BALX=radiation budget at top of model
#                                      compute BALP=AR5 definition of radiation budget at top of atmosphere
        if [ -s gsFSA${m} -a -s gsFSG${m} -a -s gsFLA${m} -a -s gsFLG${m} ] ; then
          # CanAM4.0
          add gsFSA${m}  gsFSG${m}  gsFSAG${m}
          add gsFLA${m}  gsFLG${m}  gsFLAG${m}
          add gsFSAG${m} gsFLAG${m} gsBALT${m}
        elif [ -s gsFSO${m} -a -s gsFSLO${m} -a -s gsFSR${m} -a -s gsOLR${m} -a -s gsFSAM${m} -a -s gsFLAM${m} ] ; then
          # CanAM4.1+
          sub gsFSO${m}  gsFSLO${m} gsFSI${m}
          sub gsFSI${m}  gsFSR${m}  gsFSAG${m}
          sub gsFSLO${m} gsOLR${m}  gsFLAG${m}
          add gsFSAG${m} gsFLAG${m} gsBALT${m}
#
          add gsFSAM${m} gsFLAM${m} gsFAM${m}
          sub gsBALT${m} gsFAM${m}  gsBALX${m}
#
          sub gsBALT${m} gsFLAM${m} gsBALP${m}
        fi

# ------------------------------------ monthly soil temperature (TG)

        x="TG"
        timavg gs${x}${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       SOIL TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SOIL TEMPERATURE EXCL.GLACIERS (K)           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SOIL TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM       TG
C*XSAVE       SOIL TEMPERATURE EXCL.GLACIERS (K)           (${days} ${year_rtdiag_start}-${year})
NEWNAM       TG" >> ic.xsave

# ------------------------------------ monthly canopy temperature (TV)

        x="TV"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only where TV>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=TV>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       CANOPY TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       CANOPY TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       TV" >> ic.xsave
        fi

# ------------------------------------ monthly liquid soil moisture (WGL)

        x="WGL"
        timavg gs${x}${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       LIQUID SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQUID SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL" >> ic.xsave

# ------------------------------------ monthly frozen soil moisture (WGF)

        x="WGF"
        timavg gs${x}${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       FROZEN SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       FROZEN SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF" >> ic.xsave

# ------------------------------------ monthly total (liquid + frozen) soil moisture (WGF+WGL)

        x="WGLF"
        add tgsWGL${m} tgsWGF${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       TOTAL SOIL MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SOIL MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF" >> ic.xsave

# ------------------------------------ instantaneous first time step total (liquid + frozen) soil moisture (WGF+WGL)

        x="WGL"
        echo "C*RCOPY            1         3" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WGF"
        echo "C*RCOPY            1         3" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WGLFi"
        add tgsWGLi${m} tgsWGFi${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       TOTAL SOIL MOISTURE FIRST LAND (KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE FIRST EXCL.GLAC.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE FIRST NO GL./BR.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SOIL MOISTURE FIRST LAND (KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE FIRST EXCL.GLAC.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE FIRST NO GL./BR.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF" >> ic.xsave

# ------------------------------------ monthly liquid veg. moisture (WVL)

        x="WVL"
        timavg gs${x}${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       LIQUID VEG. MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQUID VEG. MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVL" >> ic.xsave

# ------------------------------------ monthly frozen veg. moisture (WVF)

        x="WVF"
        timavg gs${x}${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       FROZEN VEG. MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       FROZEN VEG. MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVF" >> ic.xsave

# ------------------------------------ monthly total (liquid + frozen) veg. moisture (WVF+WVL)

        x="WVLF"
        add tgsWVL${m} tgsWVF${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       TOTAL VEG. MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL VEG. MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF" >> ic.xsave

# ------------------------------------ instantaneous first time step total (liquid + frozen) veg. moisture (WVF+WVL)

        x="WVL"
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WVF"
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WVLFi"
        add tgsWVLi${m} tgsWVFi${m} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       TOTAL VEG. MOISTURE FIRST LAND (KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE FIRST EXCL.GLAC.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE FIRST NO GL./BR.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL VEG. MOISTURE FIRST LAND (KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE FIRST EXCL.GLAC.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE FIRST NO GL./BR.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of WGL

        x="VFSL"
#                                      convert kg/m2 to m by dividing by density (1000 kg/m3)
        echo "C*XLIN         1000.       0.0         1" | ccc xlin tgsWGL${m} tgsWGL${m}d
        div tgsWGL${m}d gsDZG${mon1} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSL
C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSL" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of WGF

        x="VFSF"
#                                      convert kg/m2 to m by dividing by density (917 kg/m3)
        echo "C*XLIN          917.       0.0         1" | ccc xlin tgsWGF${m} tgsWGF${m}d
        div tgsWGF${m}d gsDZG${mon1} tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSF
C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSF" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of total soil moisture (VFSM)

        x="VFSM"
#                                      total moisture depth
        add tgsWGL${m}d tgsWGF${m}d tgsWGLF${m}d
#                                      multi-level average
        echo "C*BINS       -1" | ccc bins tgsWGLF${m}d tgsWGLF${m}davg
#                                      divide by averaged soil depth
        gdiv tgsWGLF${m}davg gsDZGavg tgs${x}${m}
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSM
C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSM" >> ic.xsave

# ------------------------------------ monthly water in snow WSNO

        x="WSNO"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       LIQ. WATER IN SNOW LAND (KG/M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW EXCL.GLAC.(KG/M2)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW EXCL.GLAC./BEDR.(KG/M2)   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQ. WATER IN SNOW LAND (KG/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW EXCL.GLAC.(KG/M2)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW EXCL.GLAC./BEDR.(KG/M2)   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO" >> ic.xsave

# ------------------------------------ instantaneous first time step liq. water in snow WSNO

        x="WSNO"
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WSNOi"
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       LIQ. WATER IN SNOW FIRST LAND (KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW FIRST EXCL.GLAC.(KG/M2)   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW FIRST NO GL./BR.(KG/M2)   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQ. WATER IN SNOW FIRST LAND (KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW FIRST EXCL.GLAC.(KG/M2)   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW FIRST NO GL./BR.(KG/M2)   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO" >> ic.xsave

# ------------------------------------ monthly ponding depth ZPND

        x="ZPND"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       PONDING DEPTH LAND (M)                       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH EXCL.GLAC.(M)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH EXCL.GLAC./BEDR.(M)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PONDING DEPTH LAND (M)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH EXCL.GLAC.(M)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH EXCL.GLAC./BEDR.(M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND" >> ic.xsave

# ------------------------------------ instantaneous first time step ponding depth ZPND

        x="ZPND"
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="ZPNDi"
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       PONDING DEPTH FIRST LAND (M)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH FIRST EXCL.GLAC.(M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH FIRST NO GL./BR.(M)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PONDING DEPTH FIRST LAND (M)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH FIRST EXCL.GLAC.(M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH FIRST NO GL./BR.(M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND" >> ic.xsave

# ------------------------------------ monthly snow mass SNO global

        x="SNO"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       SNOW MASS GLOBAL (KG)                        (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS GLOBAL (KG)                        (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO" >> ic.xsave

# ------------------------------------ instantaneous first time step snow mass SNO global

        x="SNO"
#                                      select first time step
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}${m} tgs${x}1${m}
        x="SNO1"
#                                      global
        globavg tgs${x}${m} _ gtavg
#                                      convert kg/m2 to kg
        mlt gtavg globe_area gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       SNOW MASS FIRST GLOBAL (KG)                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS FIRST GLOBAL (KG)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO" >> ic.xsave

# ------------------------------------ monthly snow mass SNOT over tiles (istile)

        x="SNOT"
        y="FARE"
        echo "C*SELLEV      1$ilnd" | ccc sellev gs${x}${m} gs${x}l${m} # snow over land
        echo "C*SELLEV      1$isic" | ccc sellev gs${x}${m} gs${x}i${m} # snow over ocean sea ice
        echo "C*SELLEV      1$iulk" | ccc sellev gs${x}${m} gs${x}ul${m} # snow over unresolved lakes
#                                      spread over grid cell
        gmlt gs${x}l${m} dland_frac   gs${x}lg${m}
        gmlt gs${x}l${m} nogla_frac   gs${x}ng${m}
        gmlt gs${x}l${m} noglb_frac   gs${x}dg${m}
        gmlt gs${x}l${m} glaci_frac   gs${x}cg${m}
        gmlt gs${x}l${m} bedro_frac   gs${x}bg${m}
        mlt gs${x}i${m}  gs${y}i${m}  gs${x}ig${m}
        mlt gs${x}ul${m} gs${y}ul${m} gs${x}ulg${m}

        timavg gs${x}lg${m}  tgs${x}lg${m}
        timavg gs${x}ng${m}  tgs${x}ng${m}
        timavg gs${x}dg${m}  tgs${x}dg${m}
        timavg gs${x}cg${m}  tgs${x}cg${m}
        timavg gs${x}bg${m}  tgs${x}bg${m}
        timavg gs${x}ig${m}  tgs${x}ig${m}
        timavg gs${x}ulg${m} tgs${x}ulg${m}
#                                      select first time step
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}lg${m}  fgs${x}lg${m}
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}ng${m}  fgs${x}ng${m}
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}dg${m}  fgs${x}dg${m}
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}cg${m}  fgs${x}cg${m}
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}bg${m}  fgs${x}bg${m}
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}ig${m}  fgs${x}ig${m}
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}ulg${m} fgs${x}ulg${m}

        globavg tgs${x}lg${m}  _ gtgs${x}lg${m}
        globavg tgs${x}ng${m}  _ gtgs${x}ng${m}
        globavg tgs${x}dg${m}  _ gtgs${x}dg${m}
        globavg tgs${x}cg${m}  _ gtgs${x}cg${m}
        globavg tgs${x}bg${m}  _ gtgs${x}bg${m}
        globavg tgs${x}ig${m}  _ gtgs${x}ig${m}
        globavg tgs${x}ulg${m} _ gtgs${x}ulg${m}

        globavg fgs${x}lg${m}  _ gfgs${x}lg${m}
        globavg fgs${x}ng${m}  _ gfgs${x}ng${m}
        globavg fgs${x}dg${m}  _ gfgs${x}dg${m}
        globavg fgs${x}cg${m}  _ gfgs${x}cg${m}
        globavg fgs${x}bg${m}  _ gfgs${x}bg${m}
        globavg fgs${x}ig${m}  _ gfgs${x}ig${m}
        globavg fgs${x}ulg${m} _ gfgs${x}ulg${m}
#                                      convert kg/m2 to kg
        mlt gtgs${x}lg${m}  globe_area gtgs${x}${m}l
        mlt gtgs${x}ng${m}  globe_area gtgs${x}${m}n
        mlt gtgs${x}dg${m}  globe_area gtgs${x}${m}d
        mlt gtgs${x}cg${m}  globe_area gtgs${x}${m}c
        mlt gtgs${x}bg${m}  globe_area gtgs${x}${m}b
        mlt gtgs${x}ig${m}  globe_area gtgs${x}${m}i
        mlt gtgs${x}ulg${m} globe_area gtgs${x}${m}ul

        mlt gfgs${x}lg${m}  globe_area gfgs${x}${m}l
        mlt gfgs${x}ng${m}  globe_area gfgs${x}${m}n
        mlt gfgs${x}dg${m}  globe_area gfgs${x}${m}d
        mlt gfgs${x}cg${m}  globe_area gfgs${x}${m}c
        mlt gfgs${x}bg${m}  globe_area gfgs${x}${m}b
        mlt gfgs${x}ig${m}  globe_area gfgs${x}${m}i
        mlt gfgs${x}ulg${m} globe_area gfgs${x}${m}ul

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}d gtgs${x}${m}c gtgs${x}${m}b gtgs${x}${m}i gtgs${x}${m}ul"
        rtdlist="$rtdlist gfgs${x}${m}l gfgs${x}${m}n gfgs${x}${m}d gfgs${x}${m}c gfgs${x}${m}b gfgs${x}${m}i gfgs${x}${m}ul"
        echo "C*XFIND       SNOW MASS LAND (KG)                          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS (KG)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS/BEDR. (KG)      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS GLACIERS (KG)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS BEDROCK (KG)                       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS OCEAN (KG)                         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS UNRESOLVED LAKES (KG)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST LAND (KG)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST LAND EXCL.GLACIERS (KG)      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST LAND EXCL.GLACIERS/BEDR. (KG)(${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST GLACIERS (KG)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST BEDROCK (KG)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST OCEAN (KG)                   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST UNRESOLVED LAKES (KG)        (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS LAND (KG)                          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS (KG)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS/BEDR. (KG)      (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS GLACIERS (KG)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS BEDROCK (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS OCEAN (KG)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS UNRESOLVED LAKES (KG)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST LAND (KG)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST LAND EXCL.GLACIERS (KG)      (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST LAND EXCL.GLACIERS/BEDR. (KG)(${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST GLACIERS (KG)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST BEDROCK (KG)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST OCEAN (KG)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST UNRESOLVED LAKES (KG)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave

# ------------------------------------ monthly hemispheric snow cover derived from snow fraction FN

        x="FN"
        timavg gs${x}${m} tgs${x}
#                                      compute averages of NH and SH land
        globavg tgs${x} _ gtgs${x}gn globe_frac_nh
        globavg tgs${x} _ gtgs${x}gs globe_frac_sh
        globavg tgs${x} _ gtgs${x}ln tland_frac_nh
        globavg tgs${x} _ gtgs${x}ls tland_frac_sh
        globavg tgs${x} _ gtgs${x}on ocean_frac_nh
        globavg tgs${x} _ gtgs${x}os ocean_frac_sh
#                                      convert to area (m^2)
        gmlt gtgs${x}gn globe_area_nh gtgs${x}${m}gna
        gmlt gtgs${x}gs globe_area_sh gtgs${x}${m}gsa
        gmlt gtgs${x}ln tland_area_nh gtgs${x}${m}lna
        gmlt gtgs${x}ls tland_area_sh gtgs${x}${m}lsa
        gmlt gtgs${x}on ocean_area_nh gtgs${x}${m}ona
        gmlt gtgs${x}os ocean_area_sh gtgs${x}${m}osa

        rtdlist="$rtdlist gtgs${x}${m}gna gtgs${x}${m}gsa gtgs${x}${m}lna gtgs${x}${m}lsa gtgs${x}${m}ona gtgs${x}${m}osa"

        echo "C*XFIND       SNOW COVER NH (M2)                           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH (M2)                           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER NH LAND (M2)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH LAND (M2)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER NH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW COVER NH (M2)                           (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH (M2)                           (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER NH LAND (M2)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH LAND (M2)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER NH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN" >> ic.xsave

# ------------------------------------ monthly aggregated snow density (RHON)

        x="RHON"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only for RHON>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=RHON>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       SNOW DENSITY (KG/M3)                         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW DENSITY (KG/M3)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM     RHON" >> ic.xsave
        fi

# ------------------------------------ monthly effective snow grain radius (REF)

        x="REF"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only for REF>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=REF>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       EFFECTIVE SNOW GRAIN RADIUS (M)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       EFFECTIVE SNOW GRAIN RADIUS (M)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      REF" >> ic.xsave
        fi

# ------------------------------------ monthly snow albedo AN

        x="AN"
        y="TN" # snow temperature is used to define where snow exists
#                                     snow mask
        echo "C*FMASK           -1 NEXT   GT     1.e-4" | ccc fmask gs${y}${m} gs${y}g${m}
        gmlt gs${y}g${m} dland_frac gs${y}l${m}
        gmlt gs${y}g${m} ocean_frac gs${y}o${m}
#
        mlt gs${x}${m} gs${y}g${m} gs${x}g${m}
        mlt gs${x}${m} gs${y}l${m} gs${x}l${m}
        mlt gs${x}${m} gs${y}o${m} gs${x}o${m}

#                                      spatial mean
        globavg gs${x}g${m} _ ggs${x}g${m}
        globavg gs${x}l${m} _ ggs${x}l${m}
        globavg gs${x}o${m} _ ggs${x}o${m}

        globavg gs${y}g${m} _ ggs${y}g${m}
        globavg gs${y}l${m} _ ggs${y}l${m}
        globavg gs${y}o${m} _ ggs${y}o${m}

        div ggs${x}g${m} ggs${y}g${m} ggs${x}g${m}.1 ; mv ggs${x}g${m}.1 ggs${x}g${m}
        div ggs${x}l${m} ggs${y}l${m} ggs${x}l${m}.1 ; mv ggs${x}l${m}.1 ggs${x}l${m}
        div ggs${x}o${m} ggs${y}o${m} ggs${x}o${m}.1 ; mv ggs${x}o${m}.1 ggs${x}o${m}
#                                      time average
        timavg ggs${x}g${m} tggs${x}g${m}
        timavg ggs${x}l${m} tggs${x}l${m}
        timavg ggs${x}o${m} tggs${x}o${m}

        rtdlist="$rtdlist tggs${x}g${m} tggs${x}l${m} tggs${x}o${m}"
        echo "
C*XFIND       SNOW ALBEDO GLOBAL                           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW ALBEDO LAND                             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW ALBEDO OCEAN                            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW ALBEDO GLOBAL                           (${days} ${year_rtdiag_start}-${year})
NEWNAM       AN
C*XSAVE       SNOW ALBEDO LAND                             (${days} ${year_rtdiag_start}-${year})
NEWNAM       AN
C*XSAVE       SNOW ALBEDO OCEAN                            (${days} ${year_rtdiag_start}-${year})
NEWNAM       AN" >> ic.xsave

# ------------------------------------ monthly global BALT (new definition)

        x="BALT"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALT (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALT (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALT" >> ic.xsave
        fi

# ------------------------------------ monthly global BALP (AR5 definition of BALT)

        x="BALP"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALP (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALP (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALP" >> ic.xsave
        fi

# ------------------------------------ monthly global BALX (radiation budget at top of model)

        x="BALX"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALX (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALX (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALX" >> ic.xsave
        fi

# ------------------------------------ monthly BEG

        x="BEG"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}o gtgs${x}${m}l"
        echo "C*XFIND       NET SFC ENERGY GLOBAL BEG (W/M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN BEG (W/M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAND BEG (W/M2)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC ENERGY GLOBAL BEG (W/M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY OCEAN BEG (W/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAND BEG (W/M2)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG" >> ic.xsave

# ------------------------------------ monthly BEGI/BEGO (isbeg >= istile)

        x="BEGI" # heat flux over sea ice
        y="BEGO" # heat flux over sea water
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${y}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${x}${m} gs${z}bio${m} gs${x}${m}i
        mlt gs${y}${m} gs${z}bwo${m} gs${y}${m}w

        timavg gs${x}${m}i tgs${x}${m}i
        timavg gs${y}${m}w tgs${y}${m}w
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
        globavg tgs${y}${m}w _ gtgs${y}${m}wo ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}io gtgs${y}${m}wo"
        echo "C*XFIND       NET SFC ENERGY SEAICE BEGI (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OPEN WATER BEGO (W/M2)        (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC ENERGY SEAICE BEGI (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BEGI
C*XSAVE       NET SFC ENERGY OPEN WATER BEGO (W/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     BEGO" >> ic.xsave
        fi

# ------------------------------------ monthly BEGL (isbeg >= istile)

        x="BEGL"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      land mean
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}l"
        echo "C*XFIND       NET SFC ENERGY LAND BEGL (W/M2)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC ENERGY LAND BEGL (W/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     BEGL" >> ic.xsave
        fi

# ------------------------------------ monthly FSGI/FSGO (isbeg >= istile)

        x="FSGI" # heat flux over sea ice
        y="FSGO" # heat flux over sea water
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${y}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${x}${m} gs${z}bio${m} gs${x}${m}i
        mlt gs${y}${m} gs${z}bwo${m} gs${y}${m}w

        timavg gs${x}${m}i tgs${x}${m}i
        timavg gs${y}${m}w tgs${y}${m}w
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
        globavg tgs${y}${m}w _ gtgs${y}${m}wo ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}io gtgs${y}${m}wo"
        echo "C*XFIND       NET SFC SOLAR FLUX SEAICE FSGI (W/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC SOLAR FLUX OPEN WATER FSGO (W/M2)    (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC SOLAR FLUX SEAICE FSGI (W/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     FSGI
C*XSAVE       NET SFC SOLAR FLUX OPEN WATER FSGO (W/M2)    (${days} ${year_rtdiag_start}-${year})
NEWNAM     FSGO" >> ic.xsave
        fi

# ------------------------------------ monthly PCP over sea water and sea ice (israd >= istile) - not accurate!

        x="PCP"
        y="FARE"
#                                      spread over ocean portion of grid cell
        mlt gs${x}${m} gs${y}dio${m} gs${x}${m}i
        mlt gs${x}${m} gs${y}dwo${m} gs${x}${m}w

        timavg gs${x}${m}i tgs${x}${m}i
        timavg gs${x}${m}w tgs${x}${m}w
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
        globavg tgs${x}${m}w _ gtgs${x}${m}wo ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin gtgs${x}${m}io gtgs${x}${m}io.1 ; mv gtgs${x}${m}io.1 gtgs${x}${m}io
        echo "C*XLIN        86400." | ccc xlin gtgs${x}${m}wo gtgs${x}${m}wo.1 ; mv gtgs${x}${m}wo.1 gtgs${x}${m}wo

        rtdlist="$rtdlist gtgs${x}${m}io gtgs${x}${m}wo"
        echo "C*XFIND       PRECIPITATION OVER SEAICE (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OVER OPEN WATER (MM/DAY)       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PRECIPITATION OVER SEAICE (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OVER OPEN WATER (MM/DAY)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave

# ------------------------------------ monthly HFLI (isbeg >= istile)

        x="HFLI" # heat flux over sea ice
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${z}${m} ] ; then
#                                      spread over ocean portion of grid cell
        mlt gs${x}${m} gs${z}bio${m} gs${x}${m}i

        timavg gs${x}${m}i tgs${x}${m}i
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}io"
        echo "C*XFIND       OCEAN SFC LATENT SUBLIM HEAT UP HFLI (W/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SFC LATENT SUBLIM HEAT UP HFLI (W/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     HFLI" >> ic.xsave
        fi

# ------------------------------------ monthly W055

        x="W055" # vertically integrated optical thickness above the tropopause at 550 nm (solar)
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global mean
        globavg tgs${x}${m} _ gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       VERT.INT.OPT.THICKNESS ABOVE TROPOP.550NM    (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VERT.INT.OPT.THICKNESS ABOVE TROPOP.550NM    (${days} ${year_rtdiag_start}-${year})
NEWNAM     W055" >> ic.xsave
        fi

# ------------------------------------ monthly W110

        x="W110" # vertically integrated optical thickness above the tropopause at 11 microns (thermal)
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global mean
        globavg tgs${x}${m} _ gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       VERT.INT.OPT.THICKNESS ABOVE TROPOP.11MICRON (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VERT.INT.OPT.THICKNESS ABOVE TROPOP.11MICRON (${days} ${year_rtdiag_start}-${year})
NEWNAM     W110" >> ic.xsave
        fi

# ------------------------------------ construct PLA tracers

        if [ "$PLAtracers" = "on" ] ; then

# ------------------------------------ ammonium sulphate burden (BAS=VI15+VI18+VI21)

        x="BAS"
        if [ -s gsVI15${m} -a -s gsVI18${m} -a -s gsVI21${m} ] ; then
          add gsVI15${m}    gsVI18${m} gsVI15p18${m}
          add gsVI15p18${m} gsVI21${m} gs${x}${m}
          release gsVI15p18${m}
        fi

# ------------------------------------ organic matter burden (BOA=VI12+VI13+VI16+VI19)

        x="BOA"
        if [ -s gsVI12${m} -a -s gsVI13${m} -a -s gsVI16${m} -a -s gsVI19${m} ] ; then
          add gsVI12${m}    gsVI13${m} gsVI12p13${m}
          add gsVI16${m}    gsVI19${m} gsVI16p19${m}
          add gsVI12p13${m} gsVI16p19${m} gs${x}${m}
          release gsVI12p13${m} gsVI16p19${m}
        fi

# ------------------------------------ black carbon burden (BBC=VI11+VI14+VI17+VI20)

        x="BBC"
        if [ -s gsVI11${m} -a -s gsVI14${m} -a -s gsVI17${m} -a -s gsVI20${m} ] ; then
          add gsVI11${m}    gsVI14${m} gsVI11p14${m}
          add gsVI17${m}    gsVI20${m} gsVI17p20${m}
          add gsVI11p14${m} gsVI17p20${m} gs${x}${m}
          release gsVI11p14${m} gsVI17p20${m}
        fi

# ------------------------------------ sea salt burden (BSS=VI07+VI08)

        x="BSS"
        if [ -s gsVI07${m} -a -s gsVI08${m} ] ; then
          add gsVI07${m} gsVI08${m} gs${x}${m}
        fi

# ------------------------------------ mineral dust burden (BMD=VI09+VI10)

        x="BMD"
        if [ -s gsVI09${m} -a -s gsVI10${m} ] ; then
          add gsVI09${m} gsVI10${m} gs${x}${m}
        fi

# ------------------------------------ number burden internally mixed aerosol (BNI=VI26+VI27+VI28+VI29+VI30)

        x="BNI"
        if [ -s gsVI26${m} -a -s gsVI27${m} -a -s gsVI28${m} -a -s gsVI29${m} -a -s gsVI30${m} ] ; then
          add gsVI26${m}    gsVI27${m}    gsVI26p27${m}
          add gsVI28${m}    gsVI29${m}    gsVI28p29${m}
          add gsVI26p27${m} gsVI28p29${m} gsVI26p29${m}
          add gsVI26p29${m} gsVI30${m}    gs${x}${m}
          release gsVI26p27${m} gsVI28p29${m} gsVI26p29${m}
        fi

# ------------------------------------ number burden externally mixed aerosol (BNE=VI22+VI23+VI24+VI25)

        x="BNE"
        if [ -s gsVI22${m} -a -s gsVI23${m} -a -s gsVI24${m} -a -s gsVI25${m} ] ; then
          add gsVI22${m}    gsVI23${m} gsVI22p23${m}
          add gsVI24${m}    gsVI25${m} gsVI24p25${m}
          add gsVI22p23${m} gsVI24p25${m} gs${x}${m}
          release gsVI22p23${m} gsVI24p25${m}
        fi
        fi # PLAtracers=on

# ------------------------------------ monthly nudging tendencies

# ------------------------------------ heat flux tendency due to difference in SST over sea water

        x="TBEG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEG" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SST over water in model, ice in obs

        x="TBEI"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEI" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SST over ice in model, water in obs

        x="TBES"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBES" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over sea water

        x="SBEG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEG" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over water in model, ice in obs

        x="SBEI"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEI" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over ice in model, water in obs

        x="SBES"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBES" >> ic.xsave
        fi

# ------------------------------------ sea ice concentration tendency

        x="TSNN"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       SEAICE CONC TENDENCY DUE TO SICN NH          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SEAICE CONC TENDENCY DUE TO SICN SH          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN NH          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TSNN
C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN SH          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TSNN" >> ic.xsave
        fi

# ------------------------------------ fresh water flux tendency due to difference in SSS

        x="TBWG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       FRESHWATER FLUX TENDENCY DUE TO SSS          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       FRESHWATER FLUX TENDENCY DUE TO SSS          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBWG" >> ic.xsave
        fi

# ------------------------------------ top soil layer temperature tendency

        x="TTG1"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}l"
        echo "C*XFIND       TG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     TTG1" >> ic.xsave
        fi

# ------------------------------------ top soil layer moisture tendency (volumetric fraction)

        x="TWG1"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}l"
        echo "C*XFIND       WG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       WG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     TWG1" >> ic.xsave
        fi

# ------------------------------------ spectral TEMP
        x="TEMP"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL                         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL                         (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES
        x="ES"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL                           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL                           (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP tendency
        x="TMPN"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL TENDENCY                (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL TENDENCY                (${days} ${year_rtdiag_start}-${year})
NEWNAM     TMPN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES tendency
        x="ESN"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL TENDENCY                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL TENDENCY                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      ESN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP reference
        x="TMPR"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL REFERENCE               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL REFERENCE               (${days} ${year_rtdiag_start}-${year})
NEWNAM     TMPN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES reference
        x="ESR"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL REFERENCE                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL REFERENCE                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      ESN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ************************************ Physical Atmosphere annual section

        if [ $m -eq $mon2 -a $nmon -eq 12 ] ; then

# ------------------------------------ annual ST

        x="ST"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      unresolved lakes
        globavg tgs${x} _ gtgs${x}u ulake_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding Antarctica
        globavg tgs${x} _ gtgs${x}c noant_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      NH polar cap
        globavg tgs${x} _ gtgs${x}nhp globe_frac_nhp
#                                      SH polar cap
        globavg tgs${x} _ gtgs${x}shp globe_frac_shp

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}u gtgs${x}o gtgs${x}n gtgs${x}c gtgs${x}i gtgs${x}nhp gtgs${x}shp"
        echo "C*XFIND       SCREEN TEMPERATURE GLOBAL (K)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE UNRESOLVED LAKES (K)      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE OCEAN (K)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE GLACIERS (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE NORTH OF 60N (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE SOUTH OF 60S (K)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE GLOBAL (K)                (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE UNRESOLVED LAKES (K)      (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE OCEAN (K)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE GLACIERS (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE NORTH OF 60N (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE SOUTH OF 60S (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST" >> ic.xsave_ann

# ------------------------------------ annual STMX

        x="STMX"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN TEMPERATURE MAX GLOBAL (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX LAND (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX OCEAN (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE MAX GLOBAL (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX LAND (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX OCEAN (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave_ann

# ------------------------------------ annual STMX max

        x="STMX"
        timmax gs${x}01 tgs${x}max gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x}max _ gtgs${x}maxg
#                                      land
        globavg tgs${x}max _ gtgs${x}maxl tland_frac
#                                      ocean
        globavg tgs${x}max _ gtgs${x}maxo ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}maxg gtgs${x}maxl gtgs${x}maxo"
        echo "C*XFIND       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX LAND (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX LAND (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave_ann

# ------------------------------------ annual STMN

        x="STMN"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN TEMPERATURE MIN GLOBAL (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN LAND (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN OCEAN (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE MIN GLOBAL (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN LAND (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN OCEAN (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave_ann

# ------------------------------------ annual STMN min

        x="STMN"
        timmin gs${x}01 tgs${x}min gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x}min _ gtgs${x}ming
#                                      land
        globavg tgs${x}min _ gtgs${x}minl tland_frac
#                                      ocean
        globavg tgs${x}min _ gtgs${x}mino ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}ming gtgs${x}minl gtgs${x}mino"
        echo "C*XFIND       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN LAND (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN LAND (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave_ann

# ------------------------------------ annual SQ

        x="SQ"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN SPECIFIC HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY LAND                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN SPECIFIC HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY LAND                (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ" >> ic.xsave_ann

# ------------------------------------ annual SRH

        x="SRH"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN RELATIVE HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY LAND                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN RELATIVE HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY LAND                (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH" >> ic.xsave_ann

# ------------------------------------ annual GT

        x="GT"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      dry land
        globavg tgs${x} _ gtgs${x}l dland_frac
#                                      unresolved lakes
        globavg tgs${x} _ gtgs${x}u ulake_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}u gtgs${x}i gtgs${x}o"
        echo "C*XFIND       SKIN TEMPERATURE GLOBAL (K)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE UNRESOLVED LAKES (K)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE GLACIERS (K)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN (K)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SKIN TEMPERATURE GLOBAL (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE UNRESOLVED LAKES (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE GLACIERS (K)                (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE OCEAN (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave_ann

# ------------------------------------ annual GT NINO indices

        x="GT"
        for ind in nino34 nino4 nino3 ; do
          globavw tgs${x} wght_${ind} _ _ gtgs${x}$ind
        done
        rtdlist_ann="$rtdlist_ann gtgs${x}nino34 gtgs${x}nino4 gtgs${x}nino3"
        echo "C*XFIND       SKIN TEMPERATURE NINO34 (K)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE NINO4 (K)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE NINO3 (K)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SKIN TEMPERATURE NINO34 (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE NINO4 (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE NINO3 (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave_ann

# ------------------------------------ annual tile GTT (land/sea water/sea ice tiles) (istile=min(isbeg,isgg))

        x="GTT"
        y="FARE"
        timavg ggs${x}lg01 tggs${x}lg _ _ ggs${x}lg02 ggs${x}lg03 ggs${x}lg04 ggs${x}lg05 ggs${x}lg06 ggs${x}lg07 ggs${x}lg08 ggs${x}lg09 ggs${x}lg10 ggs${x}lg11 ggs${x}lg12
        timavg ggs${x}wg01 tggs${x}wg _ _ ggs${x}wg02 ggs${x}wg03 ggs${x}wg04 ggs${x}wg05 ggs${x}wg06 ggs${x}wg07 ggs${x}wg08 ggs${x}wg09 ggs${x}wg10 ggs${x}wg11 ggs${x}wg12
        timavg ggs${x}ig01 tggs${x}ig _ _ ggs${x}ig02 ggs${x}ig03 ggs${x}ig04 ggs${x}ig05 ggs${x}ig06 ggs${x}ig07 ggs${x}ig08 ggs${x}ig09 ggs${x}ig10 ggs${x}ig11 ggs${x}ig12
        timavg ggs${x}og01 tggs${x}og _ _ ggs${x}og02 ggs${x}og03 ggs${x}og04 ggs${x}og05 ggs${x}og06 ggs${x}og07 ggs${x}og08 ggs${x}og09 ggs${x}og10 ggs${x}og11 ggs${x}og12
        timavg ggs${x}ulg01 tggs${x}ulg _ _ ggs${x}ulg02 ggs${x}ulg03 ggs${x}ulg04 ggs${x}ulg05 ggs${x}ulg06 ggs${x}ulg07 ggs${x}ulg08 ggs${x}ulg09 ggs${x}ulg10 ggs${x}ulg11 ggs${x}ulg12

        timavg ggs${x}lgx01 tggs${x}lgx _ _ ggs${x}lgx02 ggs${x}lgx03 ggs${x}lgx04 ggs${x}lgx05 ggs${x}lgx06 ggs${x}lgx07 ggs${x}lgx08 ggs${x}lgx09 ggs${x}lgx10 ggs${x}lgx11 ggs${x}lgx12
        timavg ggs${x}wgx01 tggs${x}wgx _ _ ggs${x}wgx02 ggs${x}wgx03 ggs${x}wgx04 ggs${x}wgx05 ggs${x}wgx06 ggs${x}wgx07 ggs${x}wgx08 ggs${x}wgx09 ggs${x}wgx10 ggs${x}wgx11 ggs${x}wgx12
        timavg ggs${x}igx01 tggs${x}igx _ _ ggs${x}igx02 ggs${x}igx03 ggs${x}igx04 ggs${x}igx05 ggs${x}igx06 ggs${x}igx07 ggs${x}igx08 ggs${x}igx09 ggs${x}igx10 ggs${x}igx11 ggs${x}igx12
        timavg ggs${x}lgn01 tggs${x}lgn _ _ ggs${x}lgn02 ggs${x}lgn03 ggs${x}lgn04 ggs${x}lgn05 ggs${x}lgn06 ggs${x}lgn07 ggs${x}lgn08 ggs${x}lgn09 ggs${x}lgn10 ggs${x}lgn11 ggs${x}lgn12
        timavg ggs${x}wgn01 tggs${x}wgn _ _ ggs${x}wgn02 ggs${x}wgn03 ggs${x}wgn04 ggs${x}wgn05 ggs${x}wgn06 ggs${x}wgn07 ggs${x}wgn08 ggs${x}wgn09 ggs${x}wgn10 ggs${x}wgn11 ggs${x}wgn12
        timavg ggs${x}ign01 tggs${x}ign _ _ ggs${x}ign02 ggs${x}ign03 ggs${x}ign04 ggs${x}ign05 ggs${x}ign06 ggs${x}ign07 ggs${x}ign08 ggs${x}ign09 ggs${x}ign10 ggs${x}ign11 ggs${x}ign12

        timavg ggs${y}l01 tggs${y}l _ _ ggs${y}l02 ggs${y}l03 ggs${y}l04 ggs${y}l05 ggs${y}l06 ggs${y}l07 ggs${y}l08 ggs${y}l09 ggs${y}l10 ggs${y}l11 ggs${y}l12
        timavg ggs${y}w01 tggs${y}w _ _ ggs${y}w02 ggs${y}w03 ggs${y}w04 ggs${y}w05 ggs${y}w06 ggs${y}w07 ggs${y}w08 ggs${y}w09 ggs${y}w10 ggs${y}w11 ggs${y}w12
        timavg ggs${y}i01 tggs${y}i _ _ ggs${y}i02 ggs${y}i03 ggs${y}i04 ggs${y}i05 ggs${y}i06 ggs${y}i07 ggs${y}i08 ggs${y}i09 ggs${y}i10 ggs${y}i11 ggs${y}i12
        timavg ggs${y}o01 tggs${y}o _ _ ggs${y}o02 ggs${y}o03 ggs${y}o04 ggs${y}o05 ggs${y}o06 ggs${y}o07 ggs${y}o08 ggs${y}o09 ggs${y}o10 ggs${y}o11 ggs${y}o12
        timavg ggs${y}ul01 tggs${y}ul _ _ ggs${y}ul02 ggs${y}ul03 ggs${y}ul04 ggs${y}ul05 ggs${y}ul06 ggs${y}ul07 ggs${y}ul08 ggs${y}ul09 ggs${y}ul10 ggs${y}ul11 ggs${y}ul12
#                                       compute bar<X*f>/bar<f>
        div tggs${x}lg  tggs${y}l  tggs${x}l
        div tggs${x}wg  tggs${y}w  tggs${x}w
        div tggs${x}ig  tggs${y}i  tggs${x}i
        div tggs${x}og  tggs${y}o  tggs${x}o
        div tggs${x}ulg tggs${y}ul tggs${x}ul

        div tggs${x}lgx  tggs${y}l  tggs${x}lx
        div tggs${x}wgx  tggs${y}w  tggs${x}wx
        div tggs${x}igx  tggs${y}i  tggs${x}ix
        div tggs${x}lgn  tggs${y}l  tggs${x}ln
        div tggs${x}wgn  tggs${y}w  tggs${x}wn
        div tggs${x}ign  tggs${y}i  tggs${x}in
#                                       set missing value when bar<f>=0 (use small epsilon instead)
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}l  ftggs${x}l  tggs${y}l
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}w  ftggs${x}w  tggs${y}w
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}i  ftggs${x}i  tggs${y}i
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}o  ftggs${x}o  tggs${y}o
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}ul ftggs${x}ul tggs${y}ul

        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}lx ftggs${x}lx tggs${y}l
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}wx ftggs${x}wx tggs${y}w
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}ix ftggs${x}ix tggs${y}i
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}ln ftggs${x}ln tggs${y}l
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}wn ftggs${x}wn tggs${y}w
        echo "C* FMSKPLT        -1 NEXT   GT     1.E-6" | ccc fmskplt tggs${x}in ftggs${x}in tggs${y}i

        rtdlist_ann="$rtdlist_ann ftggs${x}l ftggs${x}w ftggs${x}i ftggs${x}o ftggs${x}ul"
        rtdlist_ann="$rtdlist_ann ftggs${x}lx ftggs${x}wx ftggs${x}ix ftggs${x}ln ftggs${x}wn ftggs${x}in"
        echo "C*XFIND       SKIN TEMPERATURE LAND TILE (K)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OPEN WATER TILE (K)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE SEA ICE TILE (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN TILES (K)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE UNRESOLVED LAKES TILE (K)   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND TILE MAX (K)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OPEN WATER TILE MAX (K)     (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE SEA ICE TILE MAX (K)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND TILE MIN (K)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OPEN WATER TILE MIN (K)     (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE SEA ICE TILE MIN (K)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SKIN TEMPERATURE LAND TILE (K)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OPEN WATER TILE (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE SEA ICE TILE (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OCEAN TILES (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE UNRESOLVED LAKES TILE (K)   (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE LAND TILE MAX (K)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OPEN WATER TILE MAX (K)     (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE SEA ICE TILE MAX (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE LAND TILE MIN (K)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OPEN WATER TILE MIN (K)     (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE SEA ICE TILE MIN (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT" >> ic.xsave_ann

# ------------------------------------ annual PCP

        x="PCP"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      dry land
        globavg tgs${x} _ gtgs${x}l dland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      unresolved lakes
        globavg tgs${x} _ gtgs${x}u ulake_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      bedrock
        globavg tgs${x} _ gtgs${x}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}a nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac
#                                      land excluding Antarctica
        globavg tgs${x} _ gtgs${x}c noant_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}u gtgs${x}i gtgs${x}b gtgs${x}a gtgs${x}d gtgs${x}c gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       PRECIPITATION GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION UNRESOLVED LAKES (MM/DAY)      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION GLACIERS (MM/DAY)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION BEDROCK (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC./BEDR.(MM/DAY)  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PRECIPITATION GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION UNRESOLVED LAKES (MM/DAY)      (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION GLACIERS (MM/DAY)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION BEDROCK (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC./BEDR.(MM/DAY)  (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave_ann

# ------------------------------------ annual PCP max

        x="PCP"
        timmax gs${x}01 tgs${x}max gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x}max tgs${x}max.1 ; mv tgs${x}max.1 tgs${x}max
#                                      global
        globavg tgs${x}max _ gtgs${x}maxg
#                                      land
        globavg tgs${x}max _ gtgs${x}maxl tland_frac
#                                      ocean
        globavg tgs${x}max _ gtgs${x}maxo ocean_frac
#                                      NH extratropics
        globavg tgs${x}max _ gtgs${x}maxn globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}max _ gtgs${x}maxs globe_frac_she
#                                      Tropics
        globavg tgs${x}max _ gtgs${x}maxt globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}maxg gtgs${x}maxl gtgs${x}maxo gtgs${x}maxn gtgs${x}maxs gtgs${x}maxt"
        echo "C*XFIND       MAX PRECIPITATION GLOBAL (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION LAND (MM/DAY)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION OCEAN (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30N-90N (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-90S (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-30N (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX PRECIPITATION GLOBAL (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION LAND (MM/DAY)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION OCEAN (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30N-90N (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-90S (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-30N (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave_ann

# ------------------------------------ annual PCPC

        x="PCPC"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       CONV. PRECIPITATION GLOBAL (MM/DAY)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION LAND (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION OCEAN (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30N-90N (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-90S (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-30N (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CONV. PRECIPITATION GLOBAL (MM/DAY)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION LAND (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION OCEAN (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30N-90N (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-90S (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-30N (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC" >> ic.xsave_ann

# ------------------------------------ annual snowfall PCPN

        x="PCPN"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      unresolved lakes
        globavg tgs${x} _ gtgs${x}u ulake_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      bedrock
        globavg tgs${x} _ gtgs${x}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}a nogla_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}u gtgs${x}i gtgs${x}b gtgs${x}a gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       SNOWFALL RATE GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE UNRESOLVED LAKES (MM/DAY)      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE GLACIERS (MM/DAY)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE BEDROCK (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOWFALL RATE GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE UNRESOLVED LAKES (MM/DAY)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE GLACIERS (MM/DAY)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE BEDROCK (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave_ann

# ------------------------------------ annual snowfall PCPN over tiles (israd >= istile) - not accurate over water and ice!

        x="PCPN"
#                                      time average
        timavg ggs${x}lg01 tggs${x}lg _ _ ggs${x}lg02 ggs${x}lg03 ggs${x}lg04 ggs${x}lg05 ggs${x}lg06 ggs${x}lg07 ggs${x}lg08 ggs${x}lg09 ggs${x}lg10 ggs${x}lg11 ggs${x}lg12
        timavg ggs${x}wg01 tggs${x}wg _ _ ggs${x}wg02 ggs${x}wg03 ggs${x}wg04 ggs${x}wg05 ggs${x}wg06 ggs${x}wg07 ggs${x}wg08 ggs${x}wg09 ggs${x}wg10 ggs${x}wg11 ggs${x}wg12
        timavg ggs${x}ig01 tggs${x}ig _ _ ggs${x}ig02 ggs${x}ig03 ggs${x}ig04 ggs${x}ig05 ggs${x}ig06 ggs${x}ig07 ggs${x}ig08 ggs${x}ig09 ggs${x}ig10 ggs${x}ig11 ggs${x}ig12
        add tggs${x}wg tggs${x}ig tggs${x}og
        add tggs${x}lg tggs${x}og tggs${x}gg

        rtdlist_ann="$rtdlist_ann tggs${x}lg tggs${x}wg tggs${x}ig tggs${x}og tggs${x}gg"
        echo "C*XFIND       GLOBAL SNOWFALL RATE LAND (KG/M2/S)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE WATER (KG/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE SEAICE (KG/M2/S)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE OCEAN (KG/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE GLOBE (KG/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL SNOWFALL RATE LAND (KG/M2/S)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE WATER (KG/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE SEAICE (KG/M2/S)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE OCEAN (KG/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE GLOBE (KG/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave_ann

# ------------------------------------ annual evaporation QFS

        x="QFS"
        timavg gs${x}01  tgs${x}  _ _ gs${x}02  gs${x}03  gs${x}04  gs${x}05  gs${x}06  gs${x}07  gs${x}08  gs${x}09  gs${x}10  gs${x}11  gs${x}12
        timavg gs${x}L01 tgs${x}L _ _ gs${x}L02 gs${x}L03 gs${x}L04 gs${x}L05 gs${x}L06 gs${x}L07 gs${x}L08 gs${x}L09 gs${x}L10 gs${x}L11 gs${x}L12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x}  tgs${x}.1  ; mv tgs${x}.1  tgs${x}
        echo "C*XLIN        86400." | ccc xlin tgs${x}L tgs${x}L.1 ; mv tgs${x}L.1 tgs${x}L
#                                      global
        globavg tgs${x}  _ gtgs${x}g
#                                      dry land
        globavg tgs${x}L _ gtgs${x}l dland_frac
#                                      unresolved lakes
        globavg tgs${x}L _ gtgs${x}u ulake_frac
#                                      glaciers
        globavg tgs${x}L _ gtgs${x}i glaci_frac
#                                      bedrock
        globavg tgs${x}L _ gtgs${x}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}L _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}L _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}u gtgs${x}i gtgs${x}b gtgs${x}n gtgs${x}d"
        echo "C*XFIND       EVAPORATION GLOBAL (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND (MM/DAY)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION UNRESOLVED LAKES (MM/DAY)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION GLACIERS (MM/DAY)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION BEDROCK (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC./BEDR.(MM/DAY)    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EVAPORATION GLOBAL (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND (MM/DAY)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION UNRESOLVED LAKES (MM/DAY)        (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION GLACIERS (MM/DAY)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION BEDROCK (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC./BEDR.(MM/DAY)    (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave_ann

# ------------------------------------ annual ROF

        x="ROF"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      dry land
        globavg tgs${x} _ gtgs${x}l dland_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      bedrock
        globavg tgs${x} _ gtgs${x}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}i gtgs${x}b gtgs${x}n gtgs${x}d"
        echo "C*XFIND       RUNOFF LAND (MM/DAY)                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF GLACIERS (MM/DAY)                     (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF BEDROCK (MM/DAY)                      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC./BEDR.(MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RUNOFF LAND (MM/DAY)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF GLACIERS (MM/DAY)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF BEDROCK (MM/DAY)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC./BEDR.(MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF" >> ic.xsave_ann

# ------------------------------------ annual RIVO

        x="RIVO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin gtgs${x}o gtgs${x}o.1 ; mv gtgs${x}o.1 gtgs${x}o

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       RIVER DISCHARGE (MM/DAY)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RIVER DISCHARGE (MM/DAY)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     RIVO" >> ic.xsave_ann
        fi

# ------------------------------------ annual BWGI

        x="BWGI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin gtgs${x}io gtgs${x}io.1 ; mv gtgs${x}io.1 gtgs${x}io

        rtdlist_ann="$rtdlist_ann gtgs${x}io"
        echo "C*XFIND       NET SFC WATER FLUX SEAICE BWGI (MM/DAY)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC WATER FLUX SEAICE BWGI (MM/DAY)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     BWGI" >> ic.xsave_ann
        fi

# ------------------------------------ annual BWGO

        y="BWGO"
        if [ -s gs${y}01 ] ; then
        timavg gs${y}01w tgs${y}w _ _ gs${y}02w gs${y}03w gs${y}04w gs${y}05w gs${y}06w gs${y}07w gs${y}08w gs${y}09w gs${y}10w gs${y}11w gs${y}12w
#                                      ocean mean
        globavg tgs${y}w _ gtgs${y}wo ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin gtgs${y}wo gtgs${y}wo.1 ; mv gtgs${y}wo.1 gtgs${y}wo

        rtdlist_ann="$rtdlist_ann gtgs${y}wo"
        echo "C*XFIND       NET SFC WATER FLUX OPEN WATER BWGO (MM/DAY)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC WATER FLUX OPEN WATER BWGO (MM/DAY)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     BWGO" >> ic.xsave_ann
        fi

# ------------------------------------ annual (P-E) ocean

        x1="BWGO"
        x2="BWGI"
        x="PME"
        if [ -s gtgs${x1}wo -a -s gtgs${x2}io ] ; then
        add gtgs${x1}wo gtgs${x2}io gtgs${x}o  # P-E over ocean

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       OCEAN P-E (MM/DAY)                           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN P-E (MM/DAY)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG" >> ic.xsave_ann
        fi

# ------------------------------------ annual (P-E+R) ocean

        x1="PME"
        x2="RIVO"
        x="PMER"
        if [ -s gtgs${x1}o -a -s gtgs${x2}o ] ; then
        add gtgs${x1}o gtgs${x2}o gtgs${x}o # P-E+R over ocean

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       OCEAN P-E PLUS RUNOFF (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN P-E PLUS RUNOFF (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PMER" >> ic.xsave_ann
        fi

# ------------------------------------ annual QFS over ocean

        x1="PCP"
        x2="PME"
        x="QFS"
        if [ -s gtgs${x1}o -a -s gtgs${x2}o ] ; then
	sub gtgs${x1}o gtgs${x2}o gtgs${x}o  # QFS over ocean

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       EVAPORATION OCEAN (MM/DAY)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EVAPORATION OCEAN (MM/DAY)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave_ann
        fi

# ------------------------------------ annual ocean ice mass (SIC = kg)

        x="SIC"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      convert kg/m2 to kg
        gmlt gtgs${x}o ocean_area gtgs${x}mo

        rtdlist_ann="$rtdlist_ann gtgs${x}mo"
        echo "C*XFIND       OCEAN SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave_ann

# ------------------------------------ unresolved lakes annual ice mass (LUIM = kg/ice covered area)

        x="LUIM"
        if [ -s gs${x}u01 ] ; then
        timavg gs${x}u01 tgs${x}u _ _ gs${x}u02 gs${x}u03 gs${x}u04 gs${x}u05 gs${x}u06 gs${x}u07 gs${x}u08 gs${x}u09 gs${x}u10 gs${x}u11 gs${x}u12

        globavg tgs${x}u _ gtgs${x}u ulake_frac
#                                      convert kg/m2 to kg
        gmlt gtgs${x}u ulake_area gtgs${x}mu

        rtdlist_ann="$rtdlist_ann gtgs${x}mu"
        echo "C*XFIND       UNRESOLVED LAKES SEAICE MASS (KG)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       UNRESOLVED LAKES SEAICE MASS (KG)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     LUIM" >> ic.xsave_ann
        fi

# # ------------------------------------ resolved lakes annual ice mass (LRIM = kg)

#         x="LRIM"
#         if [ -s gs${x}01 ] ; then
#         timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#         globavg tgs${x} _ gtgs${x}r rlake_frac
# #                                      convert kg/m2 to kg
#         gmlt gtgs${x}r rlake_area gtgs${x}mr

#         rtdlist_ann="$rtdlist_ann gtgs${x}mr"
#         echo "C*XFIND       RESOLVED LAKES SEAICE MASS (KG)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
#         echo "C*XSAVE       RESOLVED LAKES SEAICE MASS (KG)              (ANN ${year_rtdiag_start}-${year})
# NEWNAM     LRIM" >> ic.xsave_ann
#         fi

# ------------------------------------ ocean sea ice area SICN

        x="SICN"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

        globavg tgs${x} _ gtgs${x}no ocean_frac_nh
        globavg tgs${x} _ gtgs${x}so ocean_frac_sh
#                                      convert to total area
        gmlt gtgs${x}no ocean_area_nh gtgs${x}ano
        gmlt gtgs${x}so ocean_area_sh gtgs${x}aso

        rtdlist_ann="$rtdlist_ann gtgs${x}ano gtgs${x}aso"
        echo "C*XFIND       OCEAN SEAICE AREA NH SICN>0 (M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE AREA SH SICN>0 (M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SEAICE AREA NH SICN>0 (M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE AREA SH SICN>0 (M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     SICN" >> ic.xsave_ann

# ------------------------------------ unresolved lake sea ice area LUIN

        x="LUIN"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

        globavg tgs${x} _ gtgs${x}ul ulake_frac
#                                      convert to total area
        gmlt gtgs${x}ul ulake_area gtgs${x}aul

        rtdlist_ann="$rtdlist_ann gtgs${x}aul"
        echo "C*XFIND       UNRESOLVED LAKES SEAICE AREA SICN>0 (M2)     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       UNRESOLVED LAKES SEAICE AREA SICN>0 (M2)     (ANN ${year_rtdiag_start}-${year})
NEWNAM     LUIN" >> ic.xsave_ann
        fi

# # ------------------------------------ resolved lake sea ice area LRIN

#         x="LRIN"
#         if [ -s gs${x}01 ] ; then
#         timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#         globavg tgs${x} _ gtgs${x}rl rlake_frac
# #                                      convert to total area
#         gmlt gtgs${x}rl rlake_area gtgs${x}arl

#         rtdlist_ann="$rtdlist_ann gtgs${x}arl"
#         echo "C*XFIND       RESOLVED LAKES SEAICE AREA SICN>0 (M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
#         echo "C*XSAVE       RESOLVED LAKES SEAICE AREA SICN>0 (M2)       (ANN ${year_rtdiag_start}-${year})
# NEWNAM     LRIN" >> ic.xsave_ann
#         fi

# ------------------------------------ annual soil temperature (TG)

        x="TG"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       SOIL TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SOIL TEMPERATURE EXCL.GLACIERS (K)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM       TG
C*XSAVE       SOIL TEMPERATURE EXCL.GLACIERS (K)           (ANN ${year_rtdiag_start}-${year})
NEWNAM       TG" >> ic.xsave_ann

# ------------------------------------ annual canopy temperature (TV)

        x="TV"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only where TV>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       CANOPY TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CANOPY TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       TV" >> ic.xsave_ann
        fi

# ------------------------------------ annual liquid soil moisture (WGL)

        x="WGL"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d"
        echo "C*XFIND       LIQUID SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LIQUID SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL" >> ic.xsave_ann

# ------------------------------------ annual frozen soil moisture (WGF)

        x="WGF"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d"
        echo "C*XFIND       FROZEN SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FROZEN SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF" >> ic.xsave_ann

# ------------------------------------ annual total (liquid + frozen) soil moisture (WGF+WGL)

        x="WGLF"
        add tgsWGL tgsWGF tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d"
        echo "C*XFIND       TOTAL SOIL MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOTAL SOIL MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF" >> ic.xsave_ann

# ------------------------------------ annual liquid veg. moisture (WVL)

        x="WVL"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d"
        echo "C*XFIND       LIQUID VEG. MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LIQUID VEG. MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVL" >> ic.xsave_ann

# ------------------------------------ annual frozen veg. moisture (WVF)

        x="WVF"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d"
        echo "C*XFIND       FROZEN VEG. MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FROZEN VEG. MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVF" >> ic.xsave_ann

# ------------------------------------ annual total (liquid + frozen) veg. moisture (WVF+WVL)

        x="WVLF"
        add tgsWVL tgsWVF tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d"
        echo "C*XFIND       TOTAL VEG. MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOTAL VEG. MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     WVLF" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of WGL

        x="VFSL"
#                                      convert kg/m2 to m by dividing by density (1000 kg/m3)
        echo "C*XLIN         1000.       0.0         1" | ccc xlin tgsWGL tgsWGLd
        div tgsWGLd gsDZG${mon1} tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSL
C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSL" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of WGF

        x="VFSF"
#                                      convert kg/m2 to m by dividing by density (917 kg/m3)
        echo "C*XLIN          917.       0.0         1" | ccc xlin tgsWGF tgsWGFd
        div tgsWGFd gsDZG${mon1} tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSF
C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSF" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of total soil moisture (VFSM)

        x="VFSM"
#                                      total moisture depth
        add tgsWGLd tgsWGFd tgsWGLFd
#                                      multi-level average
        echo "C*BINS       -1" | ccc bins tgsWGLFd tgsWGLFdavg
#                                      divide by averaged soil depth
        gdiv tgsWGLFdavg gsDZGavg tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSM
C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSM" >> ic.xsave_ann

# ------------------------------------ annual water in snow WSNO

        x="WSNO"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d"
        echo "C*XFIND       LIQ. WATER IN SNOW LAND (KG/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW EXCL.GLAC.(KG/M2)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW EXCL.GLAC./BEDR.(KG/M2)   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LIQ. WATER IN SNOW LAND (KG/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW EXCL.GLAC.(KG/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW EXCL.GLAC./BEDR.(KG/M2)   (ANN ${year_rtdiag_start}-${year})
NEWNAM     WSNO" >> ic.xsave_ann

# ------------------------------------ annual ponding depth ZPND

        x="ZPND"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d"
        echo "C*XFIND       PONDING DEPTH LAND (M)                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH EXCL.GLAC.(M)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH EXCL.GLAC./BEDR.(M)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PONDING DEPTH LAND (M)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH EXCL.GLAC.(M)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH EXCL.GLAC./BEDR.(M)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     ZPND" >> ic.xsave_ann

# ------------------------------------ annual snow mass SNO

        x="SNO"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       SNOW MASS GLOBAL (KG)                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW MASS GLOBAL (KG)                        (ANN ${year_rtdiag_start}-${year})
NEWNAM      SNO" >> ic.xsave_ann

# ------------------------------------ annual snow mass SNOT over tiles (istile)

        x="SNOTl"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtavg dland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg dland_area gtgs${x}l

#                                      land excluding glaciers
        globavg tgs${x} _ gtavg nogla_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}n

#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtavg noglb_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}d

#                                      glaciers
        globavg tgs${x} _ gtavg glaci_frac
#                                      convert kg/m2 to kg
        gmlt gtavg glaci_area gtgs${x}i

#                                      bedrock
        globavg tgs${x} _ gtavg bedro_frac
#                                      convert kg/m2 to kg
        gmlt gtavg bedro_area gtgs${x}b

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n gtgs${x}d gtgs${x}i gtgs${x}b"
        echo "C*XFIND       SNOW MASS LAND (KG)                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS (KG)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS/BEDR. (KG)      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS GLACIERS (KG)                      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS BEDROCK (KG)                       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW MASS LAND (KG)                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS (KG)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS/BEDR. (KG)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS GLACIERS (KG)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS BEDROCK (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave_ann

# ------------------------------------ annual snow mass SNO over sea ice

        x="SNOT"
#                                      time average
        timavg gs${x}ig01 tgs${x}ig _ _ gs${x}ig02 gs${x}ig03 gs${x}ig04 gs${x}ig05 gs${x}ig06 gs${x}ig07 gs${x}ig08 gs${x}ig09 gs${x}ig10 gs${x}ig11 gs${x}ig12
        timavg gs${x}ulg01 tgs${x}ulg _ _ gs${x}ulg02 gs${x}ulg03 gs${x}ulg04 gs${x}ulg05 gs${x}ulg06 gs${x}ulg07 gs${x}ulg08 gs${x}ulg09 gs${x}ulg10 gs${x}ulg11 gs${x}ulg12
#                                      global mean
        globavg tgs${x}ig  _ gtgs${x}ig
        globavg tgs${x}ulg _ gtgs${x}ulg
#                                      convert kg/m2 to kg
        gmlt gtgs${x}ig	 globe_area gtgs${x}ig.1  ; mv gtgs${x}ig.1  gtgs${x}ig
        gmlt gtgs${x}ulg globe_area gtgs${x}ulg.1 ; mv gtgs${x}ulg.1 gtgs${x}ulg
#                                      global mean

        rtdlist_ann="$rtdlist_ann gtgs${x}ig gtgs${x}ulg"
        echo "C*XFIND       SNOW MASS OCEAN (KG)                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS UNRESOLVED LAKES (KG)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW MASS OCEAN (KG)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS UNRESOLVED LAKES (KG)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave_ann

# ------------------------------------ annual snow density (RHON)

        x="RHON"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only for RHON>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SNOW DENSITY (KG/M3)                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW DENSITY (KG/M3)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     RHON" >> ic.xsave_ann
        fi

# ------------------------------------ annual effective snow grain radius (REF)

        x="REF"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only for REF>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       EFFECTIVE SNOW GRAIN RADIUS (M)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EFFECTIVE SNOW GRAIN RADIUS (M)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      REF" >> ic.xsave_ann
        fi

# ------------------------------------ annual snow albedo AN

        x="AN"
#                                      time average
        timavg ggs${x}g01 tggs${x}g _ _ ggs${x}g02 ggs${x}g03 ggs${x}g04 ggs${x}g05 ggs${x}g06 ggs${x}g07 ggs${x}g08 ggs${x}g09 ggs${x}g10 ggs${x}g11 ggs${x}g12
        timavg ggs${x}l01 tggs${x}l _ _ ggs${x}l02 ggs${x}l03 ggs${x}l04 ggs${x}l05 ggs${x}l06 ggs${x}l07 ggs${x}l08 ggs${x}l09 ggs${x}l10 ggs${x}l11 ggs${x}l12
        timavg ggs${x}o01 tggs${x}o _ _ ggs${x}o02 ggs${x}o03 ggs${x}o04 ggs${x}o05 ggs${x}o06 ggs${x}o07 ggs${x}o08 ggs${x}o09 ggs${x}o10 ggs${x}o11 ggs${x}o12

        rtdlist_ann="$rtdlist_ann tggs${x}g tggs${x}l tggs${x}o"
        echo "
C*XFIND       SNOW ALBEDO GLOBAL                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW ALBEDO LAND                             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW ALBEDO OCEAN                            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW ALBEDO GLOBAL                           (ANN ${year_rtdiag_start}-${year})
NEWNAM       AN
C*XSAVE       SNOW ALBEDO LAND                             (ANN ${year_rtdiag_start}-${year})
NEWNAM       AN
C*XSAVE       SNOW ALBEDO OCEAN                            (ANN ${year_rtdiag_start}-${year})
NEWNAM       AN" >> ic.xsave_ann

# ------------------------------------ annual global BALT (new definition)

        x="BALT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALT" >> ic.xsave_ann
        fi

# ------------------------------------ annual global BALP (AR5 definition of BALT)

        x="BALP"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALP (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALP (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALP" >> ic.xsave_ann
        fi

# ------------------------------------ annual global BALX (radiation budget at top of model)

        x="BALX"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALX (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALX (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALX" >> ic.xsave_ann
        fi

# ------------------------------------ annual BEG

        x="BEG"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       NET SFC ENERGY GLOBAL BEG (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN BEG (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAND BEG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL BEG (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY OCEAN BEG (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAND BEG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG" >> ic.xsave_ann

# ------------------------------------ annual BEGI/BEGO

        x="BEGI"
        y="BEGO"
        if [ -s gs${x}01 -a -s gs${y}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
        timavg gs${y}01w tgs${y}w _ _ gs${y}02w gs${y}03w gs${y}04w gs${y}05w gs${y}06w gs${y}07w gs${y}08w gs${y}09w gs${y}10w gs${y}11w gs${y}12w
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
        globavg tgs${y}w _ gtgs${y}wo ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}io gtgs${y}wo"
        echo "C*XFIND       NET SFC ENERGY SEAICE BEGI (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OPEN WATER BEGO (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY SEAICE BEGI (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BEGI
C*XSAVE       NET SFC ENERGY OPEN WATER BEGO (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     BEGO" >> ic.xsave_ann
        fi

# ------------------------------------ annual BEGL

        x="BEGL"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land mean
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l"
        echo "C*XFIND       NET SFC ENERGY LAND BEGL (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY LAND BEGL (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     BEGL" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSGI/FSGO

        x="FSGI"
        y="FSGO"
        if [ -s gs${x}01 -a -s gs${y}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
        timavg gs${y}01w tgs${y}w _ _ gs${y}02w gs${y}03w gs${y}04w gs${y}05w gs${y}06w gs${y}07w gs${y}08w gs${y}09w gs${y}10w gs${y}11w gs${y}12w
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
        globavg tgs${y}w _ gtgs${y}wo ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}io gtgs${y}wo"
        echo "C*XFIND       NET SFC SOLAR FLUX SEAICE FSGI (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC SOLAR FLUX OPEN WATER FSGO (W/M2)    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC SOLAR FLUX SEAICE FSGI (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGI
C*XSAVE       NET SFC SOLAR FLUX OPEN WATER FSGO (W/M2)    (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGO" >> ic.xsave_ann
        fi

# ------------------------------------ annual PCPO/PCPI

        x="PCP"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
        timavg gs${x}01w tgs${x}w _ _ gs${x}02w gs${x}03w gs${x}04w gs${x}05w gs${x}06w gs${x}07w gs${x}08w gs${x}09w gs${x}10w gs${x}11w gs${x}12w
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
        globavg tgs${x}w _ gtgs${x}wo ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin gtgs${x}io gtgs${x}io.1 ; mv gtgs${x}io.1 gtgs${x}io
        echo "C*XLIN        86400." | ccc xlin gtgs${x}wo gtgs${x}wo.1 ; mv gtgs${x}wo.1 gtgs${x}wo

        rtdlist_ann="$rtdlist_ann gtgs${x}io gtgs${x}wo"
        echo "C*XFIND       PRECIPITATION OVER SEAICE (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OVER OPEN WATER (MM/DAY)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PRECIPITATION OVER SEAICE (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OVER OPEN WATER (MM/DAY)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave_ann
        fi

# ------------------------------------ annual HFLI

        x="HFLI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}io"
        echo "C*XFIND       OCEAN SFC LATENT SUBLIM HEAT UP HFLI (W/M2)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SFC LATENT SUBLIM HEAT UP HFLI (W/M2)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     HFLI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FMI

        x="FMI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       HEAT TO MELT ICE GLOBAL FMI (W/M2)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT TO MELT ICE GLOBAL FMI (W/M2)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      FMI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ABEG = FSG + FLG - (HFS + LATA), where LATA=HS*PCPN + HV*(PCP-PCPN),HS=2.835E6;HV=2.501E6

        for x in FSG FLG HFS PCPN PCP ; do
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
        done
        # compute LATA
        sub gtgsPCP gtgsPCPN gtgsPCPR
        echo "C*XYLIN      2.835E6   2.501E6        0." | ccc xylin gtgsPCPN gtgsPCPR gtgsLATA
        # compute ABEG
        add gtgsFSG  gtgsFLG  gtgsFSLG
        add gtgsHFS  gtgsLATA gtgsHFSL
        sub gtgsFSLG gtgsHFSL gtgsABEG

        x="ABEG"
        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       NET SFC ENERGY GLOBAL ABEG (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL ABEG (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     ABEG" >> ic.xsave_ann

# ------------------------------------ annual global RES

        x="RES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       RESIDUAL HEAT FLUX GLOBAL RES (W/M2)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RESIDUAL HEAT FLUX GLOBAL RES (W/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      RES" >> ic.xsave_ann
        fi

# ------------------------------------ annual BWG

        x="BWG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400." | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB FRESHWATER FLUX BWG (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN FRESHWATER FLUX BWG (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND FRESHWATER FLUX BWG (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB FRESHWATER FLUX BWG (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG
C*XSAVE       OCN FRESHWATER FLUX BWG (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG
C*XSAVE       LAND FRESHWATER FLUX BWG (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSO

        x="FSO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA INC S/W GLOBAL FSO (W/M2)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA INC S/W GLOBAL FSO (W/M2)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSO" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSLO

        x="FSLO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA SOLAR/LW OVERLAP GLOBAL FSLO (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA SOLAR/LW OVERLAP GLOBAL FSLO (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSLO" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSR

        x="FSR"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA REFL S/W GLOBAL FSR (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA REFL S/W GLOBAL FSR (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSR" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSAG

        x="FSAG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA NET S/W GLOBAL FSAG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA NET S/W GLOBAL FSAG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSAG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLAG

        x="FLAG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA NET L/W GLOBAL FLAG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA NET L/W GLOBAL FLAG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLAG" >> ic.xsave_ann
        fi

# ------------------------------------ annual OLR

        x="OLR"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLOBAL OUTGOING L/W OLR (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL OUTGOING L/W OLR (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      OLR" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSS

        x="FSS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INCIDENT S/W FSS (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS
C*XSAVE       OCN SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS
C*XSAVE       LAND SFC INCIDENT S/W FSS (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSSC

        x="FSSC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INC S/W CLEAR SKY FSSC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC
C*XSAVE       OCN SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC
C*XSAVE       LAND SFC INC S/W CLEAR SKY FSSC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSG

        x="FSG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET S/W FSG (W/M2)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG
C*XSAVE       OCN SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG
C*XSAVE       LAND SFC NET S/W FSG (W/M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSGC

        x="FSGC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET S/W CLEAR SKY FSGC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC
C*XSAVE       OCN SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC
C*XSAVE       LAND SFC NET S/W CLEAR SKY FSGC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC" >> ic.xsave_ann
        fi

# ------------------------------------ annual FDL

        x="FDL"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INCIDENT L/W FDL (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL
C*XSAVE       OCN SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL
C*XSAVE       LAND SFC INCIDENT L/W FDL (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLG

        x="FLG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET L/W FLG (W/M2)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG
C*XSAVE       OCN SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG
C*XSAVE       LAND SFC NET L/W FLG (W/M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLGC

        x="FLGC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET L/W CLEAR SKY FLGC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC
C*XSAVE       OCN SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC
C*XSAVE       LAND SFC NET L/W CLEAR SKY FLGC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC" >> ic.xsave_ann
        fi

# ------------------------------------ annual HFS

        x="HFS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC SENSIBLE HEAT UP HFS (W/M2)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS
C*XSAVE       OCN SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS
C*XSAVE       LAND SFC SENSIBLE HEAT UP HFS (W/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS" >> ic.xsave_ann
        fi

# ------------------------------------ annual HFL

        x="HFL"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l"
        echo "C*XFIND       GLB SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC LATENT HEAT UP HFL (W/M2)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL
C*XSAVE       OCN SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL
C*XSAVE       LAND SFC LATENT HEAT UP HFL (W/M2)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FSD (surface diffuse downward shortwave flux)

        x="FSD"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLB SFC DIFFUSE S/W FLUX FSD (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC DIFFUSE S/W FLUX FSD (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSD" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FSDC

        x="FSDC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLB SFC DIFF.S/W C.SKY FLUX FSDC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC DIFF.S/W C.SKY FLUX FSDC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSDC" >> ic.xsave_ann
        fi

# ------------------------------------ annual CLDT

        x="CLDT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDT             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDT               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDT              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDT             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDT               (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDT              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT" >> ic.xsave_ann
        fi

# ------------------------------------ annual CLDO

        x="CLDO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDO             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDO               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDO              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDO             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDO               (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDO              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global PWAT

        x="PWAT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLOBAL PRECIPITABLE WATER PWAT (KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL PRECIPITABLE WATER PWAT (KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     PWAT" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface CO2 - this is a scalar value in runs with specified CO2 concentrations

        x="CO2"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppm
        echo "C*XLIN        1.E+06" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      CO2" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface CH4 - this is a scalar value

        x="CH4"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppb
        echo "C*XLIN        1.E+09" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CH4 CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CH4 CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      CH4" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface N2O - this is a scalar value

        x="N2O"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppb
        echo "C*XLIN        1.E+09" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE N2O CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE N2O CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      N2O" >> ic.xsave_ann
        fi

# ------------------------------------ annual W055

        x="W055" # vertically integrated optical thickness above the tropopause at 550 nm (solar)
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global mean
        globavg gtavg _ gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       VERT.INT.OPT.THICKNESS ABOVE TROPOP.550NM    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VERT.INT.OPT.THICKNESS ABOVE TROPOP.550NM    (ANN ${year_rtdiag_start}-${year})
NEWNAM     W055" >> ic.xsave_ann
        fi

# ------------------------------------ annual W110

        x="W110" # vertically integrated optical thickness above the tropopause at 11 microns (thermal)
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global mean
        globavg gtavg _ gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       VERT.INT.OPT.THICKNESS ABOVE TROPOP.11MICRON (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VERT.INT.OPT.THICKNESS ABOVE TROPOP.11MICRON (ANN ${year_rtdiag_start}-${year})
NEWNAM     W110" >> ic.xsave_ann
        fi

# ------------------------------------ annual global burdens of non-CO2 tracers

        i2=1
        while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval trac=\${it$i2}
        x="VI${i2}"
        if [ "$trac" != "CO2" -a -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        if [ "$trac" = "SO2" -o "$trac" = "SO4" -o "$trac" = "DMS" -o "$trac" = "HPO" ] ; then
          echo "C*XFIND       $trac BURDEN GLOBAL (1E6KG-S)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac BURDEN GLOBAL (1E6KG-S)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${i2}" >> ic.xsave_ann
        else
          echo "C*XFIND       $trac BURDEN GLOBAL (1E6KG)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac BURDEN GLOBAL (1E6KG)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${i2}" >> ic.xsave_ann
        fi
        fi
        i2=`expr $i2 + 1`
        done # $ntrac

# ------------------------------------ annual global mass of non-CO2 tracers

        i2=1
        while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval trac=\${it$i2}
        x="VM${i2}"
        if [ "$trac" != "CO2" -a -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        if [ "$trac" = "SO2" -o "$trac" = "SO4" -o "$trac" = "DMS" -o "$trac" = "HPO" ] ; then
          echo "C*XFIND       $trac MASS GLOBAL (KG-S/M2)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac MASS GLOBAL (KG-S/M2)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     VM${i2}" >> ic.xsave_ann
        else
          echo "C*XFIND       $trac MASS GLOBAL (KG/M2)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac MASS GLOBAL (KG/M2)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VM${i2}" >> ic.xsave_ann
        fi
        fi
        i2=`expr $i2 + 1`
        done # $ntrac

# ------------------------------------ annual global holefilling tendency of non-CO2 tracers

        i2=1
        while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval trac=\${it$i2}
        x="VF${i2}"
        if [ "$trac" != "CO2" -a -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        if [ "$trac" = "SO2" -o "$trac" = "SO4" -o "$trac" = "DMS" -o "$trac" = "HPO" ] ; then
          echo "C*XFIND       $trac HOLEFILL TEND GLOBAL (KG-S/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac HOLEFILL TEND GLOBAL (KG-S/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     VF${i2}" >> ic.xsave_ann
        else
          echo "C*XFIND       $trac HOLEFILL TEND GLOBAL (KG/M2/S)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac HOLEFILL TEND GLOBAL (KG/M2/S)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     VF${i2}" >> ic.xsave_ann
        fi
        fi
        i2=`expr $i2 + 1`
        done # $ntrac

# ------------------------------------ annual global mass fixer tendency of non-CO2 tracers

        i2=1
        while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval trac=\${it$i2}
        x="VH${i2}"
        if [ "$trac" != "CO2" -a -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        if [ "$trac" = "SO2" -o "$trac" = "SO4" -o "$trac" = "DMS" -o "$trac" = "HPO" ] ; then
          echo "C*XFIND       $trac MASS FIXER TEND GLOBAL (KG-S/M2/S)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac MASS FIXER TEND GLOBAL (KG-S/M2/S)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     VH${i2}" >> ic.xsave_ann
        else
          echo "C*XFIND       $trac MASS FIXER TEND GLOBAL (KG/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac MASS FIXER TEND GLOBAL (KG/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     VH${i2}" >> ic.xsave_ann
        fi
        fi
        i2=`expr $i2 + 1`
        done # $ntrac


# ------------------------------------ annual global absolute physics tendency of non-CO2 tracers

        i2=1
        while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval trac=\${it$i2}
        x="VT${i2}"
        if [ "$trac" != "CO2" -a -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        if [ "$trac" = "SO2" -o "$trac" = "SO4" -o "$trac" = "DMS" -o "$trac" = "HPO" ] ; then
          echo "C*XFIND       $trac ABS PHYS TEND GLOBAL (KG-S/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac ABS PHYS TEND GLOBAL (KG-S/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     VT${i2}" >> ic.xsave_ann
        else
          echo "C*XFIND       $trac ABS PHYS TEND GLOBAL (KG/M2/S)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac ABS PHYS TEND GLOBAL (KG/M2/S)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     VT${i2}" >> ic.xsave_ann
        fi
        fi
        i2=`expr $i2 + 1`
        done # $ntrac

# ------------------------------------ PLA tracers

        if [ "$PLAtracers" = "on" ] ; then

# ------------------------------------ annual global ammonium sulphate burden (BAS=VI15+VI18+VI21)

        x="BAS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       AMMONIUM SULPHATE BURDEN (1E6KG)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       AMMONIUM SULPHATE BURDEN (1E6KG)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BAS" >> ic.xsave_ann
        fi

# ------------------------------------ organic matter burden (BOA=VI12+VI13+VI16+VI19)

        x="BOA"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       ORGANIC MATTER BURDEN (1E6KG)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ORGANIC MATTER BURDEN (1E6KG)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      BOA" >> ic.xsave_ann
        fi

# ------------------------------------ black carbon burden (BBC=VI11+VI14+VI17+VI20)

        x="BBC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       BLACK CARBON BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       BLACK CARBON BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      BBC" >> ic.xsave_ann
        fi

# ------------------------------------ sea salt burden (BSS=VI07+VI08)

        x="BSS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       SEA SALT BURDEN (1E6KG)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SEA SALT BURDEN (1E6KG)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM      BSS" >> ic.xsave_ann
        fi

# ------------------------------------ mineral dust burden (BMD=VI09+VI10)

        x="BMD"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       MINERAL DUST BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MINERAL DUST BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      BMD" >> ic.xsave_ann
        fi

# ------------------------------------ number burden internally mixed aerosol (BNI=VI21+VI22+VI23+VI24)

        x="BNI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NUMBER BURDEN INTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NUMBER BURDEN INTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      BNI" >> ic.xsave_ann
        fi

# ------------------------------------ number burden externally mixed aerosol (BNE=VI16+VI17+VI18+VI19)

        x="BNE"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NUMBER BURDEN EXTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NUMBER BURDEN EXTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      BNE" >> ic.xsave_ann
        fi

        fi # PLAtracers=on

# ------------------------------------ annual nudging tendencies

# ------------------------------------ heat flux tendency due to difference in SST over sea water

        x="TBEG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEG" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SST over water in model, ice in obs

        x="TBEI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEI" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SST over ice in model, water in obs

        x="TBES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBES" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over sea water

        x="SBEG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEG" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over water in model, ice in obs

        x="SBEI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEI" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over ice in model, water in obs

        x="SBES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBES" >> ic.xsave_ann
        fi

# ------------------------------------ sea ice concentration tendency

        x="TSNN"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       SEAICE CONC TENDENCY DUE TO SICN NH          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SEAICE CONC TENDENCY DUE TO SICN SH          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN NH          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TSNN
C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN SH          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TSNN" >> ic.xsave_ann
        fi

# ------------------------------------ fresh water flux tendency due to difference in SSS

        x="TBWG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       FRESHWATER FLUX TENDENCY DUE TO SSS          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FRESHWATER FLUX TENDENCY DUE TO SSS          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBWG" >> ic.xsave_ann
        fi

# ------------------------------------ top soil layer temperature tendency

        x="TTG1"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l"
        echo "C*XFIND       TG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     TTG1" >> ic.xsave_ann
        fi

# ------------------------------------ top soil layer moisture tendency (volumetric fraction)

        x="TWG1"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l"
        echo "C*XFIND       WG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       WG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     TWG1" >> ic.xsave_ann
        fi

# ------------------------------------ spectral TEMP
        x="TEMP"
        if [ -s gss${x}01 ] ; then
        timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES
        x="ES"
        if [ -s ss${x}01 ] ; then
        timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL                           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL                           (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP tendency
        x="TMPN"
        if [ -s gss${x}01 ] ; then
        timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL TENDENCY                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL TENDENCY                (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES tendency
        x="ESN"
        if [ -s ss${x}01 ] ; then
        timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL TENDENCY                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL TENDENCY                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP reference
        x="TMPR"
        if [ -s gss${x}01 ] ; then
        timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL REFERENCE               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL REFERENCE               (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES reference
        x="ESR"
        if [ -s ss${x}01 ] ; then
        timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL REFERENCE                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL REFERENCE                 (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ Total sulfur emissions (kg S/m2/sec): TSUE = ESFS+EAIS+ESTS+EFIS+ESVC+ESVE
        x="TSUE"
        xs="ESFS EAIS ESTS EFIS ESVC ESVE"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFUR EMISSIONS (KG S/M2/S)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFUR EMISSIONS (KG S/M2/S)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total black carbon emissions (kg/m2/sec): TBCE = ESFB+EAIB+ESTB+EFIB
        x="TBCE"
        xs="ESFB EAIB ESTB EFIB"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL BLACK CARBON EMISSIONS (KG/M2/S)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL BLACK CARBON EMISSIONS (KG/M2/S)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total organic carbon emissions (kg/m2/sec): TOCE = ESFO+EAIO+ESTO+EFIO
        x="TOCE"
        xs="ESFO EAIO ESTO EFIO"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL ORGANIC CARBON EMISSIONS (KG/M2/S)     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL ORGANIC CARBON EMISSIONS (KG/M2/S)     (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total sulfate wet deposition (kg S/m2/sec): TSWD = WDL6+WDD6+WDS6
        x="TSWD"
        xs="WDL6 WDD6 WDS6"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFATE WET DEPOSITION (KG S/M2/S)     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFATE WET DEPOSITION (KG S/M2/S)     (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total sulfate dry deposition (kg S/m2/sec): DD6
        x="DD6"
        if [ -s gs${x}01 ] ; then
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFATE DRY DEPOSITION (KG S/M2/S)     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFATE DRY DEPOSITION (KG S/M2/S)     (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total sulfate in-cloud oxidation (kg S/m2/sec): TSCO = SLO3+SLHP+SDO3+SDHP+SSO3+SSHP
        x="TSCO"
        xs="SLO3 SLHP SDO3 SDHP SSO3 SSHP"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFATE IN-CLOUD OXIDATION (KG S/M2/S) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFATE IN-CLOUD OXIDATION (KG S/M2/S) (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total sulfate clear-sky oxidation (kg S/m2/sec): DOX4
        x="DOX4"
        if [ -s gs${x}01 ] ; then
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFATE CLEAR-SKY OXIDATION (KG S/M2/S)(ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFATE CLEAR-SKY OXIDATION (KG S/M2/S)(ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total black carbon wet deposition (kg/m2/sec): WDBC = WDLB+WDDB+WDSB
        x="WDBC"
        xs="WDLB WDDB WDSB"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL BLACK CARBON WET DEPOSITION (KG/M2/S)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL BLACK CARBON WET DEPOSITION (KG/M2/S)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total black carbon dry deposition (kg/m2/sec): DDB
        x="DDB"
        if [ -s gs${x}01 ] ; then
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL BLACK CARBON DRY DEPOSITION (KG/M2/S)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL BLACK CARBON DRY DEPOSITION (KG/M2/S)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total organic carbon wet deposition (kg/m2/sec): WDOC = WDLO+WDDO+WDSO
        x="WDOC"
        xs="WDLO WDDO WDSO"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL ORG. CARBON WET DEPOSITION (KG/M2/S)   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL ORG. CARBON WET DEPOSITION (KG/M2/S)   (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------  Total organic carbon dry deposition (kg/m2/sec): DDO
        x="DDO"
        if [ -s gs${x}01 ] ; then
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL ORG. CARBON DRY DEPOSITION (KG/M2/S)   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL ORG. CARBON DRY DEPOSITION (KG/M2/S)   (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

        fi # $m -eq $mon2
      fi # PhysA=on

# #################################### Atmospheric Carbon section

      if [ "$CarbA" = "on" ] ; then

# ************************************ Atmospheric Carbon annual section

        if [ $m -eq $mon1 -a $nmon -eq 12 ] ; then

# ------------------------------------ global surface CO2 MMR beginning of year

        if [ -n "$iCO2" ] ; then
        x="XL${iCO2}"
#                                      CO2 concentration at the beginning of a year
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}${m} tgs${x}day1
        globavg tgs${x}day1 _ gtgs${x}day1
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gtgs${x}day1 gtgs${x}day1.ppmv ; mv gtgs${x}day1.ppmv gtgs${x}day1

        rtdlist_ann="$rtdlist_ann gtgs${x}day1"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION $MON1 1 (PPMV)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION $MON1 1 (PPMV)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave_ann
        fi

# ------------------------------------ global CO2 beginning of year, based on daily mean burdens

        if [ -n "$iCO2" ] ; then
        x="VI${iCO2}"
#                                      CO2 burden at the beginning of a year
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}${m} gs${x}day1
        globavg gs${x}day1 _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}day1

        rtdlist_ann="$rtdlist_ann gtgs${x}day1"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN $MON1 1 (PG C)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN $MON1 1 (PG C)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${iCO2}" >> ic.xsave_ann
        fi

# ------------------------------------ global CO2 beginning of year, based on sampled burden

        if [ -n "$iCO2" ] ; then
        x="VS${iCO2}"
        if [ -s gs${x}${m} ] ; then
#                                      CO2 burden at the beginning of a year
        echo "C*RCOPY            1         1" | ccc rcopy gs${x}${m} gs${x}day1
        globavg gs${x}day1 _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}day1

        rtdlist_ann="$rtdlist_ann gtgs${x}day1"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN $MON1 1 (PG C) SAMPLED  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN $MON1 1 (PG C) SAMPLED  (ANN ${year_rtdiag_start}-${year})
NEWNAM     VS${iCO2}" >> ic.xsave_ann
        fi
        fi

        fi # $m -eq $mon1

# ------------------------------------ monthly global mass of CO2 tracer

        if [ -n "$iCO2" ] ; then
        x="VM${i2}"
        if [ -s gs${x}${m} ] ; then
#                                      global
        globavg gs${x}${m} _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN MONTHLY (PG C)        (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN MONTHLY (PG C)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     VM${iCO2}" >> ic.xsave
        fi
        fi

# ------------------------------------ monthly global surface CO2 MMR

        if [ -n "$iCO2" ] ; then
        x="XL${iCO2}"
        timavg gs${x}${m} tgs${x}${m}
        globavg tgs${x}${m} _ gtgs${x}${m}
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gtgs${x}${m} gtgs${x}${m}.ppmv ; mv gtgs${x}${m}.ppmv gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave
        fi

        if [ $m -eq $mon2 -a $nmon -eq 12 ] ; then

# ------------------------------------ global surface CO2 MMR end of year

        if [ -n "$iCO2" ] ; then
        x="XL${iCO2}"
#                                      CO2 concentration at the end of a year
        nrec=`ggstat gs${x}${m} | grep GRID | wc -l | awk '{printf "%5d", $1}'`
        echo "C*RCOPY        $nrec     $nrec" | ccc rcopy gs${x}${m} tgs${x}last
        globavg tgs${x}last _ gtgs${x}last
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gtgs${x}last gtgs${x}last.ppmv ; mv gtgs${x}last.ppmv gtgs${x}last

        rtdlist_ann="$rtdlist_ann gtgs${x}last"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION $MON2$LEN2 (PPMV)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION $MON2$LEN2 (PPMV)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave_ann
        fi

# ------------------------------------ global CO2 burden end of year, daily mean

        if [ -n "$iCO2" ] ; then
        x="VI${iCO2}"
#                                      CO2 burden at the end of a year
        echo "C*RCOPY           $LEN2        $LEN2" | ccc rcopy gs${x}${m} gs${x}last
        globavg gs${x}last _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}last

        rtdlist_ann="$rtdlist_ann gtgs${x}last"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN $MON2$LEN2 (PG C)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN $MON2$LEN2 (PG C)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${iCO2}" >> ic.xsave_ann
        fi

# ------------------------------------ global CO2 burden end of year, sampled

        if [ -n "$iCO2" ] ; then
        x="VS${iCO2}"
#                                      CO2 burden at the end of a year
        if [ -s gs${x}${m} ] ; then
        nrec=`echo $LEN2 $isgg $delt | awk '{printf "%5d", $1*86400/($2*$3)}'`
        echo "C*RCOPY        $nrec     $nrec" | ccc rcopy gs${x}${m} gs${x}last
        globavg gs${x}last _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN $MON2$LEN2 (PG C) SAMPLED  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN $MON2$LEN2 (PG C) SAMPLED  (ANN ${year_rtdiag_start}-${year})
NEWNAM     VS${iCO2}" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CO2 surface flux

        x="XF${iCO2}"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        globavg tgs${x} _ gtavg
#                                      convert CO2 FLUX from Kg CO2 m-2 sec-1 to Pg C/year
#                                      x 365 days x 86400 sec x 510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 4390152482
        echo "C*XLIN    4390152482" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       CO2 FLUX ENTERING ATMOS.(PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CO2 FLUX ENTERING ATMOS.(PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     XF${iCO2}" >> ic.xsave_ann

# ------------------------------------ annual global surface CO2 MMR->PPMV

        x="XL${iCO2}"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        globavg tgs${x} _ gtavg
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave_ann

# ------------------------------------ annual max/min global surface CO2 MMR (for monthly means)

        x="XL${iCO2}"
        timmax gtgs${x}01 gtgs${x}max gtgs${x}02 gtgs${x}03 gtgs${x}04 gtgs${x}05 gtgs${x}06 gtgs${x}07 gtgs${x}08 gtgs${x}09 gtgs${x}10 gtgs${x}11 gtgs${x}12
        timmin gtgs${x}01 gtgs${x}min gtgs${x}02 gtgs${x}03 gtgs${x}04 gtgs${x}05 gtgs${x}06 gtgs${x}07 gtgs${x}08 gtgs${x}09 gtgs${x}10 gtgs${x}11 gtgs${x}12

        rtdlist_ann="$rtdlist_ann gtgs${x}max gtgs${x}min"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (MAX ${year_rtdiag_start}-${yearm1})
C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (MIN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (MAX ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}
C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (MIN ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave_ann

        fi # $iCO2

# ------------------------------------ annual global ECO2

        x="ECO2"
        if [ -s gs${x}01 ] ; then
#                                      find annual mean and global average
        timavg gs${x}01 tavg${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        globavg tavg${x} _ gtavg
#                                      convert ECO2 from Kg CO2 m-2 sec-1 to Pg C/year
#                                      x 365 days x 86400 sec x 510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 4390152482.
        echo "C*XLIN    4390152482" | ccc xlin gtavg gtgs$x

        rtdlist_ann="$rtdlist_ann gtgs$x"
        echo "C*XFIND       ANTHROPOGENIC EMISSIONS (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ANTHROPOGENIC EMISSIONS (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     ECO2" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FCOO

        x="FCOO"
        if [ -s gs${x}01 ] ; then
#                                      find annual mean and global integral
        timavg gs${x}01 tavg${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        globavg tavg${x} _ gtavg ocean_frac
#                                      convert CO2 FLUX from Kg CO2 m-2 sec-1 to Pg C/year
#                                      x 365 days x 86400 sec x 510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 4390152482
        echo "C*XLIN    4390152482" | ccc xlin gtavg gtgs${x}
#                                      reverse sign
        echo "C*XLIN           -1." | ccc xlin gtgs${x} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
#                                      scale to the ocean area
        gmlt gtgs${x} ocean_favg gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs$x"
        echo "C*XFIND       OCEAN-ATMOS CO2 FLUX (PG C/YR)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN-ATMOS CO2 FLUX (PG C/YR)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FCOO" >> ic.xsave_ann
        fi

        fi # $m -eq $mon2
      fi # CarbA=on

# #################################### Land Carbon section

      if [ "$CarbL" = "on" ] ; then

# ************************************ Land Carbon monthly section

# ------------------------------------ monthly averaged global wetland area calculated from wetland fraction

        x="WFRA"
        if [ -s tm${x}${m} ] ; then
        timavg tm${x}${m} tavg${x}${m}
        mlt tavg${x}${m} dland_frac ltavg${x}${m}
        globavg ltavg${x}${m} _ gtavg

#                                      convert to km2
        gmlt gtavg globe_area_km2 gttm${x}${m}

        rtdlist="$rtdlist gttm${x}${m}"
        echo "C*XFIND       DYNAMIC WETLAND AREA (KM2)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       DYNAMIC WETLAND AREA (KM2)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WFRA" >> ic.xsave
        fi

# ------------------------------------ monthly averaged global wetland emissions 1 - CH4H

        x="CH4H"
        if [ -s tm${x}${m} ] ; then
        timavg tm${x}${m} tavg${x}${m}
        mlt tavg${x}${m} dland_frac ltavg${x}${m}
        globavg ltavg${x}${m} _ gttm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/month
#                                      1.0377504 x $lmon x $globe_area_km2 x 10^6 x (16/12) / 10^12
#                                      1.0377504= 12.011 x 86400 / 10^6 converts u-mol CO2-C m-2 sec-1 to g C/m2.day
#                                      $lmon is month length (defined at the beginning of the month loop)
#                                      $globe_area_km2 is global area in km^2 (defined previously)
#                                      16/12 converts C to CH4

        factor_ch4_mon=`echo $lmon ${globe_area_km2} | awk '{printf "%10.4f",12.011*86400*$1*$2*(16/12)/10^12}'`
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gttm${x}${m} ggttm${x}${m}

        rtdlist="$rtdlist ggttm${x}${m}"
        echo "C*XFIND       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CH4H" >> ic.xsave
        fi

# ------------------------------------ monthly averaged global wetland emissions 2 - CH4N

        x="CH4N"
        if [ -s tm${x}${m} ] ; then
        timavg tm${x}${m} tavg${x}${m}
        mlt tavg${x}${m} dland_frac ltavg${x}${m}
        globavg ltavg${x}${m} _ gttm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gttm${x}${m} ggttm${x}${m}

        rtdlist="$rtdlist ggttm${x}${m}"
        echo "C*XFIND       NPP WETLAND EMIS SPEC WETLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NPP WETLAND EMIS SPEC WETLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CH4N" >> ic.xsave
        fi

# ------------------------------------ monthly averaged dynamic global wetland emissions 1 - CW1D

        x="CW1D"
        if [ -s tm${x}${m} ] ; then
        timavg tm${x}${m} tavg${x}${m}
        mlt tavg${x}${m} dland_frac ltavg${x}${m}
        globavg ltavg${x}${m} _ gttm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg C/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gttm${x}${m} ggttm${x}${m}

        rtdlist="$rtdlist ggttm${x}${m}"
        echo "C*XFIND       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CW1D" >> ic.xsave
        fi

# ------------------------------------ monthly averaged dynamic global wetland emissions 2 - CW2D

        x="CW2D"
        if [ -s tm${x}${m} ] ; then
        timavg tm${x}${m} tavg${x}${m}
        mlt tavg${x}${m} dland_frac ltavg${x}${m}
        globavg ltavg${x}${m} _ gttm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg C/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gttm${x}${m} ggttm${x}${m}

        rtdlist="$rtdlist ggttm${x}${m}"
        echo "C*XFIND       NPP WETLAND EMIS DYN WETLAND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NPP WETLAND EMIS DYN WETLAND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CW2D" >> ic.xsave
        fi

# ------------------------------------ monthly averaged leaf area index CLAI

        x="CLAI"
        if [ -s tm${x}${m} ] ; then
        timavg tm${x}${m} tavg$x
        globavg tavg$x _ gttm${x}${m}    dland_frac
        globavg tavg$x _ gttm${x}nhe${m} dland_frac_nhe
        globavg tavg$x _ gttm${x}she${m} dland_frac_she
        globavg tavg$x _ gttm${x}tro${m} dland_frac_tro

        rtdlist="$rtdlist gttm${x}${m} gttm${x}nhe${m} gttm${x}she${m} gttm${x}tro${m}"
        echo "C*XFIND       LEAF AREA INDEX                              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX NORTH OF 30N                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX SOUTH OF 30S                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX 30S-30N                      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LEAF AREA INDEX                              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX NORTH OF 30N                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX SOUTH OF 30S                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX 30S-30N                      (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI" >> ic.xsave
        fi

# ************************************ Land Carbon annual section

        if [ $m -eq $mon2 -a $nmon -eq 12 ] ; then

# ------------------------------------ CVEG year end vegetation

        x="CVEG"
        echo "C*RCOPY           $LEN2        $LEN2" | ccc rcopy tm${x}${m} tavg${x}
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6(km2->m2) / 10^12=510.099699
        factor_kg2pgc=`echo ${globe_area_km2} | awk '{printf "%10.6f",$1/10^6}'`
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       VEGETATION BIOMASS (PG C)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VEGETATION BIOMASS (PG C)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     CVEG" >> ic.xsave_ann

# ------------------------------------ CDEB year end litter

        x="CDEB"
        echo "C*RCOPY           $LEN2        $LEN2" | ccc rcopy tm${x}${m} tavg${x}
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6 (km2->m2) / 10^12
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       LITTER MASS (PG C)                           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER MASS (PG C)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     CDEB" >> ic.xsave_ann

# ------------------------------------ CHUM year end soil C

        x="CHUM"
        echo "C*RCOPY           $LEN2        $LEN2" | ccc rcopy tm${x}${m} tavg${x}
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6 (km2->m2) / 10^12
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       SOIL CARBON MASS (PG C)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL CARBON MASS (PG C)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CHUM" >> ic.xsave_ann

# ------------------------------------ annual global CBRN

        x="CBRN"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
#       treat CBRN differently. I need to find global burned area.
#       multiply average annual area by 365 to get annual area burned
        echo "C*XLIN         365.0" | ccc xlin tavg${x} annual_burn_area

#       get area of grid cells
        if [ -z "$nlat" -o -z "$lonsl" ] ; then
          echo "Error: **** CarbL: nlat or lonsl are undefined. ****"
          exit 1
        fi
        echo "C*MKWGHT  $nlat    1$lonsl    1$nlat  1 0    0" | ccc mkwght grid_cell_area_fraction

        div annual_burn_area grid_cell_area_fraction fractional_area_burned
        globavg fractional_area_burned _ gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       GLOBAL BURNED AREA (KM2/YEAR)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL BURNED AREA (KM2/YEAR)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     CBRN" >> ic.xsave_ann

# ------------------------------------ annual global CFNP

        x="CFNP"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
#                                      x 12.011 / 10^6 to convert u-mol CO2-C to g C
#                                      x 365 x 86400  for 1/sec to 1/year,
#                                      x 510099699km2 x 10^6 (km2->m2) for global total in g C
#                                    / 10^15 to convert g to Pg C = 193.215001
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       NET PRIMARY PRODUCTIVITY (PG C/YR)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET PRIMARY PRODUCTIVITY (PG C/YR)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNP" >> ic.xsave_ann

# ------------------------------------ annual global CFNE

        x="CFNE"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       NET ECOSYSTEM PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET ECOSYSTEM PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNE" >> ic.xsave_ann

# ------------------------------------ annual global CFRV

        x="CFRV"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       AUTOTROPHIC RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       AUTOTROPHIC RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRV" >> ic.xsave_ann

# ------------------------------------ annual global CFGP

        x="CFGP"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       GROSS PRIMARY PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GROSS PRIMARY PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFGP" >> ic.xsave_ann

# ------------------------------------ annual global CFNB

        x="CFNB"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       NET BIOME PRODUCTIVITY (PG C/YR)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET BIOME PRODUCTIVITY (PG C/YR)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNB" >> ic.xsave_ann

# ------------------------------------ annual global CFLV

        x="CFLV"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       LUC VEG. COMBUST. EMISSION (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC VEG. COMBUST. EMISSION (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLV" >> ic.xsave_ann

# ------------------------------------ annual global CFLD

        x="CFLD"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       LUC LITTER INPUTS (PG C/YR)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC LITTER INPUTS (PG C/YR)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLD" >> ic.xsave_ann

# ------------------------------------ annual global CFLH

        x="CFLH"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       LUC SOIL CARBON INPUTS (PG C/YR)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC SOIL CARBON INPUTS (PG C/YR)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLH" >> ic.xsave_ann

# ------------------------------------ annual global CFRH

        x="CFRH"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       SOIL CARBON RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL CARBON RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRH" >> ic.xsave_ann

# ------------------------------------ annual global CFHT

        x="CFHT"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       LITTER TO SOIL C TRANSFER (PG C/YR)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER TO SOIL C TRANSFER (PG C/YR)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFHT" >> ic.xsave_ann

# ------------------------------------ annual global CFLF

        x="CFLF"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       LITTER FALL (PG C/YR)                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER FALL (PG C/YR)                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLF" >> ic.xsave_ann

# ------------------------------------ annual global CFRD

        x="CFRD"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       LITTER RESPIRATION (PG C/YR)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER RESPIRATION (PG C/YR)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRD" >> ic.xsave_ann

# ------------------------------------ annual global CFFD

        x="CFFD"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       FIRE EMISSION FROM LITTER (PG C/YR)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FIRE EMISSION FROM LITTER (PG C/YR)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFFD" >> ic.xsave_ann

# ------------------------------------ annual global CFFV

        x="CFFV"
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       FIRE EMISSION FROM VEGETATION (PG C/YR)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FIRE EMISSION FROM VEGETATION (PG C/YR)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFFV" >> ic.xsave_ann

# ------------------------------------ annual averaged global wetland area calculated from wetland fraction

        x="WFRA"
        if [ -s tm${x}01 ] ; then
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg

#                                      convert to km2
        gmlt gtavg globe_area_km2 gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       DYNAMIC WETLAND AREA (KM2)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DYNAMIC WETLAND AREA (KM2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     WFRA" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CH4H

        x="CH4H"
        if [ -s tm${x}01 ] ; then
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
#                                      1.0377504 x 365 x $globe_area_km2 x 10^6 x (16/12) / 10^12 = 257620.001
#                                      1.0377504= 12.011 x 86400 / 10^6 converts u-mol CO2-C m-2 sec-1 to g C/m2.day
#                                      16/12 converts C to CH4

        factor_ch4_ann=`echo 365 ${globe_area_km2} | awk '{printf "%10.3f",12.011*86400*$1*$2*(16/12)/10^12}'`
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CH4H" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CH4N

        x="CH4N"
        if [ -s tm${x}01 ] ; then
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       NPP WETLND EMIS SPEC WETLAND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NPP WETLND EMIS SPEC WETLAND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CH4N" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CW1D

        x="CW1D"
        if [ -s tm${x}01 ] ; then
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CW1D" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CW2D

        x="CW2D"
        if [ -s tm${x}01 ] ; then
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        mlt tavg${x} dland_frac ltavg${x}
        globavg ltavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gttm$x

        rtdlist_ann="$rtdlist_ann gttm$x"
        echo "C*XFIND       NPP WETLND EMIS DYN WETLAND AREA (TG CH4/Y)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NPP WETLND EMIS DYN WETLAND AREA (TG CH4/Y)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     CW2D" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CLAI

        x="CLAI"
        if [ -s tm${x}01 ] ; then
#                                      find annual mean and global average
        timavg tm${x}01 tavg${x} _ _ tm${x}02 tm${x}03 tm${x}04 tm${x}05 tm${x}06 tm${x}07 tm${x}08 tm${x}09 tm${x}10 tm${x}11 tm${x}12
        globavg tavg${x} _ gttm${x}    dland_frac
        globavg tavg${x} _ gttm${x}nhe dland_frac_nhe
        globavg tavg${x} _ gttm${x}she dland_frac_she
        globavg tavg${x} _ gttm${x}tro dland_frac_tro

        rtdlist_ann="$rtdlist_ann gttm$x gttm${x}nhe gttm${x}she gttm${x}tro"
        echo "C*XFIND       LEAF AREA INDEX                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX NORTH OF 30N                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX SOUTH OF 30S                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX 30S-30N                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LEAF AREA INDEX                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX NORTH OF 30N                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX SOUTH OF 30S                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX 30S-30N                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI" >> ic.xsave_ann
        fi

        fi # $m -eq $mon2
      fi # CarbL=on

# ------------------------------------ add the number of seconds since January 1, 1970 (CUT)

      if [ $m -eq $mon2 ] ; then
      x="SECS"
      datesec=`date +%s`
      echo " TIME      1001 SECS         0         1         1    100101        -1
$datesec.E00" > datesec.txt
      chabin datesec.txt gtgs$x
      rtdlist_ann="$rtdlist_ann gtgs$x"
      echo "C*XFIND       DATE IN SECONDS SINCE 1970                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
      echo "C*XSAVE       DATE IN SECONDS SINCE 1970                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SECS" >> ic.xsave_ann
      fi # $m -eq $mon2

# ------------------------------------ add ocean/land mask area fractions

      if [ $m -eq $mon2 ] ; then
      rtdlist_ann="$rtdlist_ann globe_area dland_area tland_area tland_area_nh tland_area_sh tland_area_nhe tland_area_she tland_area_tro ocean_area ocean_area_nh ocean_area_sh rlake_area ulake_area nogla_area noglb_area glaci_area bedro_area dland_favg tland_favg tland_favg_nh tland_favg_sh tland_favg_nhe tland_favg_she tland_favg_tro ocean_favg ocean_favg_nh ocean_favg_sh rlake_favg ulake_favg nogla_favg noglb_favg glaci_favg bedro_favg"
      echo "C*XFIND       GLOBAL AREA (M2)                             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       DRY LAND AREA (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA (M2)                               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA ONLY (M2)                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA NH (M2)                            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA SH (M2)                            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA NHE (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA SHE (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA TRO (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA (M2)                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA NH (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA SH (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RESOLVED LAKES AREA (M2)                     (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       UNRESOLVED LAKES AREA (M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS AREA (M2)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS/BEDROCK AREA (M2)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLACIERS AREA (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       BEDROCK AREA (M2)                            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       DRY LAND AREA FRACTION                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION NH                        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION SH                        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION NHE                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION SHE                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION TRO                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION NH                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION SH                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RESOLVED LAKES AREA FRACTION                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       UNRESOLVED LAKES AREA FRACTION               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS AREA FRACTION              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS/BEDROCK AREA FRACTION      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLACIERS AREA FRACTION                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       BEDROCK AREA FRACTION                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
      echo "C*XSAVE       GLOBAL AREA (M2)                             (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       DRY LAND AREA (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA (M2)                               (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA NH (M2)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA SH (M2)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA NHE (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA SHE (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA TRO (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA (M2)                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA NH (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA SH (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       RESOLVED LAKES AREA (M2)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       UNRESOLVED LAKES AREA (M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND W/O GLACIERS AREA (M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND W/O GLACIERS/BEDROCK AREA (M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       GLACIERS AREA (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       BEDROCK AREA (M2)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       DRY LAND AREA FRACTION                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION NH                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION SH                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION NHE                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION SHE                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION TRO                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION NH                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION SH                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       RESOLVED LAKES AREA FRACTION                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       UNRESOLVED LAKES AREA FRACTION               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND W/O GLACIERS AREA FRACTION              (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND W/O GLACIERS/BEDROCK AREA FRACTION      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       GLACIERS AREA FRACTION                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       BEDROCK AREA FRACTION                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC" >> ic.xsave_ann

      fi # $m -eq $mon2

# ------------------------------------ xsave monthly rtd statistics (split into batches of no more that $nvarmax variables)

      rtdlist=`echo $rtdlist | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      rtdlist0="$rtdlist"
      ln -sf ic.xsave ic.xsave0
      while [ -n "$rtdlist0" ] ; do
        rtdlist1=`echo "$rtdlist0" | cut -f1-$nvarmax -d' '`
        head -$nvarmax2 ic.xsave0 > ic.xsave1
#       echo $rtdlist1
#       head -$nvarmax2 ic.xsave1 | grep -v NEWNAM
        xsave newrtd $rtdlist1 newrtd1 input=ic.xsave1
        mv newrtd1 newrtd
        rtdlist0=`echo "$rtdlist0" | cut -f$nvarmaxp1- -d' '`
        tail -n +$nvarmax2p1 ic.xsave0 > ic.xsave1 ; mv ic.xsave1 ic.xsave0
      done
    done # month loop

# ------------------------------------ xsave annual rtd statistics (split into batches of no more that $nvarmax variables)

    rtdlist_ann=`echo "$rtdlist_ann" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
    rtdlist0="$rtdlist_ann"
    ln -sf ic.xsave_ann ic.xsave0
    while [ -n "$rtdlist0" ] ; do
      rtdlist1=`echo "$rtdlist0" | cut -f1-$nvarmax -d' '`
      head -$nvarmax2 ic.xsave0 > ic.xsave1
#     echo $rtdlist1
#     head -$nvarmax2 ic.xsave1 | grep -v NEWNAM
      xsave newrtd $rtdlist1 newrtd1 input=ic.xsave1
      mv newrtd1 newrtd
      rtdlist0=`echo "$rtdlist0" | cut -f$nvarmaxp1- -d' '`
      tail -n +$nvarmax2p1 ic.xsave0 > ic.xsave1 ; mv ic.xsave1 ic.xsave0
    done

#                                      relabel GRID to TIME
    echo "C*RELABL  +GRID
C*RELABL  +TIME      1001                                       100101    1" | ccc relabl newrtd newrtd1
    mv newrtd1 newrtd

# ------------------------------------ append current year to old rtd file

    if [ $yr1 -gt ${year_rtdiag_start} ] ; then
      joinrtd oldrtd newrtd newrtd2 ; mv newrtd2 newrtd
    fi

# ************************************ save new runtime diagnostics.

    save newrtd ${rtdfile}

# ************************************ delete older rtd files
    if [ "$keep_old_rtdiag" != "on" ] ; then
      release old
      access  old $rtdfileo na nocp
      delete  old na || true
    fi

# ************************************ make copy to common location

    rtd_dest_dir=${rtd_dest_dir:="$RUNPATH_ROOT/../rtdfiles"} # default location for rtd files
    rm -f        $rtd_dest_dir/$rtdfile
    cp -p newrtd $rtd_dest_dir/$rtdfile
