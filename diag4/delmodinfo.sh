#!/bin/sh
#  Convert to script - 07/DEC/2017 - DY
#  For 'Dorval' site, set default mode to "dpalist='on'".
#
#
#
#              delmodinfo              gjb,ec,sk - Jun 28/11 - fm
#   ---------------------------------- create modinfo records and compute delhat
#                                      and beta.
if [ \( "$SITE_ID" = 'Dorval'  -o "$SITE_ID" = 'DrvlSC' \) ] ; then dpalist=${dpalist:='on'} ; export dpalist ; fi
#   ---------------------------------- remove temporary "gp","gs","sp" files and
#                                      splitted variables to ensure a clean start.
.   deletgps.cdk
.   deletsp.cdk
.   delsplt.cdk

#   ---------------------------------- access model file and
#                                      select DATA DESCRIPTION records

    if [ "$datatype" = "specsig" ] ; then
      access  npaksp ${model1}ss nocp
echo "XFIND.        DATA DESCRIPTION" | ccc xfind npaksp modinfo
      release npaksp
    elif [ "$datatype" = "gridsig" ] ; then
      access  npakgg ${model1}gs nocp
echo "XFIND.        DATA DESCRIPTION" | ccc xfind npakgg modinfo
      release npakgg
    elif [ "$datatype" = "gridpr" ] ; then
      access  npakgg ${model1}gp nocp
echo "XFIND.        DATA DESCRIPTION" | ccc xfind npakgg modinfo
      release npakgg
    else
      echo " *** Error in delmodinfo.dk: invalid value for datatype. ***"
      echo " *** Use specsig, gridsig or gridpr.                     ***"
    fi

#   ---------------------------------- verify consistency of active parameters
#                                      vs. those specified in the "PARC" record

.   parcmp.cdk

#   ---------------------------------- compute beta and PS

    if [ "$datatype" = "specsig" -o "$datatype" = "gridsig" ] ; then
#                                      sigma levels case
      if [ "$datatype" = "specsig" ] ; then
.       spfiles.cdk
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME PHIS LNSP" | ccc select npaksp ssphis sslnsp
echo "COFAGG    $lon$lat    0$npg" | ccc cofagg ssphis gsphis
echo "COFAGG    $lon$lat    0$npg" | ccc cofagg sslnsp gslnsp
      else
.       ggfiles.cdk
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME PHIS LNSP" | ccc select npakgg gsphis gslnsp
      fi

      expone gslnsp sfprx

      if [ "$rcm" = "on" ] ; then
#                                      convert pressure to mb
echo "XLIN            0.01" | ccc xlin sfprx sfprx1
        mv   sfprx1 sfprx
      fi

echo "NEWNAM       PS" | ccc newnam sfprx PS
      rm sfprx

#                                      calculate del and beta

echo "BETA      $plv
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc delhat PS del

echo "BETA      $plv
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc beta PS beta

    elif [ "$datatype" = "specpr" -o "$datatype" = "gridpr" ] ; then
#                                      pressure levels case
      access gsphis $mtnfile
      if [ "$datatype" = "specpr" ] ; then
.       spfiles.cdk
echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME  PHI" | ccc select npaksp spphi
echo "COFAGG    $lon$lat    0$npg" | ccc cofagg spphi gpphi
      else
.       ggfiles.cdk
echo "SELECT    STEPS $t1 $t2 $t3 LEVS$p01$pmaxl NAME  PHI" | ccc select npakgg gpphi
      fi
      delhato gsphis gpphi del
      betao   gsphis gpphi beta PS
    else
      echo "*** Error in delmodinfo.dk: unknown or undefined datatype=$datatype"
      exit -1
    fi

#   ---------------------------------- time averages of other variables.

    timavg gsphis tphis
    timavg del    tdel
    timavg beta   tbet

    if [ "$rcm" != "on" ] ; then
      zonavg tphis  zphis
      zonavg tdel   zdel
      zonavg tbet   zbet
    fi

#   ---------------------------------- xsave modinfo, phis, beta/del

echo "XSAVE.        DATA DESCRIPTION
XSAVE.
XSAVE         PHIS
NEWNAM
XSAVE         D
NEWNAM
XSAVE         B
NEWNAM" | ccc xsave new_gp modinfo tphis tdel tbet newgp

    if [ "$rcm" != "on" ] ; then
echo "XSAVE.        DATA DESCRIPTION
XSAVE.
XSAVE         (PHIS)
NEWNAM
XSAVE         (D)
NEWNAM
XSAVE         (B)
NEWNAM" | ccc xsave new_xp modinfo zphis zdel zbet newxp
    fi

#   ---------------------------------- append PS statitsics

    statsav PS newgp newxp $stat2nd

#   ---------------------------------- save initial gp and xp files.

    save  newgp ${flabel}gp
    if [ "$rcm" != "on" ] ; then
      save  newxp ${flabel}xp
    fi

#   ---------------------------------- save PS, beta, gslnsp for future computations

    save PS ${flabel}_ps
    if [ "$rcm" != "on" ] ; then
      if [ "$d" = "B" ] ; then
        save beta ${flabel}_gpbeta
        save tbet ${flabel}_gptbeta
      elif [ "$d" = "D" ] ; then
        save del  ${flabel}_gpbeta
        save tdel ${flabel}_gptbeta
      fi
    fi
    if [ "$datatype" = "specsig" -o "$datatype" = "gridsig" ] ; then
      save gslnsp ${flabel}_gslnsp
    fi
