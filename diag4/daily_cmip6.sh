#/bin/sh
#  Phase out disabling the trap in optional processing of 
#  daily cm variables via "cmfiles.cdk" call.
#  
#
#                  daily_cmip6          sk,dy - Nov 21/13 - fm
#  -----------------------------------  extract (sub) daily data from 
#                                       the selected fields in gs and 
#                                       cm files
#
#                                       jcl - Feb 3/2019
#                                       Added extraction of daily data
#                                       for CFMIP, VolMIP
#
#  Daily data types:
#
#  tier 1 - (sub)daily 2D variables extracted for all runs/periods
#
#    input variables:
#
#      suffix=gs
#      sq stmn stmx st pcp pcpn swa rof qfs fmi flg fsg hfl hfs 
#      srh srhn srhx fn cldt pcpc su sv swx fdl fss fslo fla  olr fnla odst ufs vfs
#      wgf wgl gt sno 
#      sicn sic
#      pmsl
#      fso fsr fssc fsdc fsd fsgc fdlc flgc olrc fsrc clwt cict bcd tcd cdcb
#      cldl clln rell ctln fsdc fsd pwat w055
#      vi03 vi04 vi05 vi06 vi07 vi08 vi09 vi10 vi13
#
#      COSP related variables:
#      sunl iccf icca iccp cltc cltm cllc cllm clmc clmm clhc clhm
#      pl01 pl02 pl03 pl04 pl05 ml01 ml02 ml03 ml04 ml05
#
#    output variables:
#      suffix=dd  
#      sq stmn stmx st pcp pcpn swa rof qfs fmi flg fsg hfl hfs
#      srh srhn srhx fn cldt pcpc su sv swx fdl fss fslo fla olr fnla odst ufs vfs
#      wgfl (=wgf+wgl at 1st layer) gt sno 
#      sicn sic
#      pmsl
#      fso fsr fssc fsdc fsd fsgc fdlc flgc olrc fsrc clwt cict bcd tcd cdcb
#      cldl clln rell ctln fsdc fsd pwat w055
#      vi03 vi04 vi05 vi06 vi07 vi08 vi09 vi10 vi13
#
#      COSP related variables:
#      sunl iccf icca iccp cltc cltm cllc cllm clmc clmm clhc clhm
#      pl01 pl02 pl03 pl04 pl05 ml01 ml02 ml03 ml04 ml05 ictp mdti mdtl
#
#  tier 2 - (sub)daily 3D/zonal mean variables on pressure levels
#           extracted for some runs/periods
#
#    input variables available after gpint*.dk.
#
#    output variables:
#      suffix=dp
#      omeg phi rh shum temp u v (levels =  1000  925  850  700  600  500  400  300  250  200  150  100   70   50   30   20   10 -500 -100)
#
#      suffix=6h
#      pmsl_6h temp_6h u_6h v_6h (levels =  925  850  700  600  500  250   50)   
#
#      suffix=dx (zonal mean)
#      omeg phi rh shum temp u v
#
#  if gssave=on, (sub)daily 3D variables on model levels are selected.
#  this includes COSP 3D variables: clcp clcm
#  and other sampled variables: cld clw cic rh
#  and accumulated variables: dmc
#    
#    output variables:  
#      suffix=ds (6-hourly)
#      ps shum temp u v
#
#      suffix=dsd (24-hour mean)
#      ps shum temp u v cld clw cic rh dmc
#      clcp clcm
#
#  ----------------------------------- end of the head comments
#
#
#     if [ $daily_cmip6_tiers -eq 0 ] ; then
#       exit
#     fi

#   ---------------------------------- check some variables

    if [ -z "$year_offset" ] ; then
      echo "ERROR in daily_cmip6.dk: undefined variable year_offset"
      exit 1
    fi
    if [ -z "$delt" ] ; then
      echo "ERROR in daily_cmip6.dk: undefined variable delt"
      exit 1
    fi
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`

    if [ -z "$daily_cmip6_tiers" ] ; then

#  ----------------------------------- set default daily tiers, 
#                                      if not defined

      daily_cmip6_tiers="1" # 2-D vars
    fi

#  ----------------------------------- prepare input cards 

    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL       YYYYMMDD" > ic.tstep_model
    echo "C*TSTEP   $deltmin     ${yearoff}010100       ACCMODEL       YYYYMMDD" > ic.tstep_accmodel
    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model_6h

    bins_israd=`echo $israd $delt | awk '{printf "%05d",86400/($1*$2)}'`
    bins_isgg=`echo  $isgg  $delt | awk '{printf "%05d",86400/($1*$2)}'`
    bins_isbeg=`echo $isbeg $delt | awk '{printf "%05d",86400/($1*$2)}'`
    if [ $isgg -lt $isbeg ] ; then
      bins_istile="$bins_isgg"
    else
      bins_istile="$bins_isbeg"
    fi
    echo "C*BINSML  $bins_israd         1" > ic.binsml.israd
    echo "C*BINSML  $bins_isgg         1" > ic.binsml.isgg
    echo "C*BINSML  $bins_isbeg         1" > ic.binsml.isbeg
    echo "C*BINSML  $bins_istile         1" > ic.binsml.istile
    echo "C*BINSML      1         1" > ic.binsml.istile1

# ------------------------------------ access gs file

    if [ "$daily_cmip6_tiers" != "0" ] ; then

.     ggfiles.cdk

    fi

# ------------------------------------ there are several daily data types:
#                                      tier 1 is for (sub)daily 2D variables 
#                                             extracted for all runs/periods
#                                      tier 2 is for (sub)daily 3D variables 
#                                             on pressure levels extracted 
#                                             for some runs/periods
#                                      Also, if gssave=on, (sub)daily 3D 
#                                      variables on model levels are selected.
    for tier in $daily_cmip6_tiers ; do

      if [ $tier -eq 1 ] ; then

# ------------------------------------ TIER 1: 2D variables

        vars_all=""

# ------------------------------------ accumulated gs variables 

        # The number of variables in a single 'select' call must not exceed 86. 
        # If selecting more than 86 variables, split them in several subsets.

        vars1="sq stmn stmx st pcp pcpn swa rof qfs fmi flg fsg hfl hfs srh srhn srhx fn ufs vfs"
        vars1="${vars1} cldt pcpc su sv swx fdl fss fslo fla olr fnla odst"
        vars1="${vars1} fso fsr fssc fsdc fsd fsgc fdlc flgc olrc fsrc clwt cict bcd tcd cdcb"

        vars2="cldl clln rell ctln pwat w055"
        vars2="${vars2} vi03 vi04 vi05 vi06 vi07 vi08 vi09 vi10 vi13"
        vars2="${vars2} sunl iccf icca iccp cltc cltm cllc cllm clmc clmm clhc clhm"
        vars2="${vars2} pl01 pl02 pl03 pl04 pl05 ml01 ml02 ml03 ml04 ml05 ictp mdti mdtl"

        vars="${vars1} ${vars2}"

        VARS1=`fmtselname $vars1`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS1" | ccc select npakgg $vars1 || true
        VARS2=`fmtselname $vars2`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS2" | ccc select npakgg $vars2 || true
#                                      relabel time step
        for v in $vars ; do
          if [ -s $v ] ; then
            V=`echo $v | tr 'a-z' 'A-Z'`
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $V
               " >> ic.xsave
          fi
        done

# ------------------------------------ sampled gs variables 

        vars="wgf wgl gt sno sicn sic snot gtt fare"
        VARS=`fmtselname ${vars}`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg $vars

# ------------------------------------ access pmsl
        release pmsl
        access  pmsl ${flabel}_pmsl

# ------------------------------------ process some variables
#                                      compute first layer daily wgf+wgl
        echo "C*SELLEV      1    1" > ic.sellev1
        sellev wgf wgf1 input=ic.sellev1
        sellev wgl wgl1 input=ic.sellev1
        add wgf1 wgl1 wgfl
        echo "C*NEWNAM   WGFL" | ccc newnam wgfl wgfl.1 ; mv wgfl.1 wgfl
        release wgf1 wgl1

#                                      compute snow amount over land
        sellev snot snw   input=ic.sellev1

#                                      tile-weighted surface temperature over land
        sellev gtt  gtt1  input=ic.sellev1
        sellev fare fare1 input=ic.sellev1
	n1=`ggstat gtt1  | grep GRID | wc -l`
	n2=`ggstat fare1 | grep GRID | wc -l`
	if [ $n1 -gt $n2 ] ; then
	  nbins=`expr $n1 / $n2`
	  echo "          $nbins" | ccc bins gtt1 gtt1b
	  ln fare1 fare1b
          mlt gtt1b fare1b gts1b
	elif [ $n2 -lt $n1 ] ; then
	  nbins=`expr $n2 / $n1`
	  echo "          $nbins" | ccc bins fare1 fare1b
	  ln gtt1 gtt1b
          mlt gtt1b fare1b gts1b
	else
	  cp ic.binsml.istile ic.binsml.istile1
	  ln gtt1 gtt1b
	  ln fare1 fare1b
          mlt gtt1b fare1b gts1b
	fi

#                                      tile-weighted surface temperature over sea-ice
        echo "C*SELLEV      1    6" > ic.sellev6
        sellev gtt  gtt6  input=ic.sellev6
        sellev fare fare6 input=ic.sellev6
	n1=`ggstat gtt6  | grep GRID | wc -l`
	n2=`ggstat fare6 | grep GRID | wc -l`
	if [ $n1 -gt $n2 ] ; then
	  nbins=`expr $n1 / $n2`
	  echo "          $nbins" | ccc bins gtt6 gtt6b
	  ln fare6 fare6b
          mlt gtt6b fare6b gts6b
	elif [ $n2 -lt $n1 ] ; then
	  nbins=`expr $n2 / $n1`
	  echo "          $nbins" | ccc bins fare6 fare6b
	  ln gtt6 gtt6b
          mlt gtt6b fare6b gts6b
	else
	  cp ic.binsml.istile ic.binsml.istile1
	  ln gtt6 gtt6b
	  ln fare6 fare6b
          mlt gtt6b fare6b gts6b
	fi

#                                      tile-weighted surface temperature over land and sea-ice
        add gts1b gts6b gts16b
        add fare1b fare6b fare16b

#                                      bin daily
        vars=""
        for v in pmsl wgfl gt sno wgf wgl ; do
          binsml $v $v.d input=ic.binsml.isgg
          mv $v.d $v
          vars="$vars $v"
        done

        # daily mean of tile-weighted sea-ice & land surface temperature and tile fractions
        for v in gts16b fare16b ; do
          binsml $v $v.d input=ic.binsml.istile1
          mv $v.d $v
        done
        # normalize
        div gts16b fare16b gts
        echo "C*NEWNAM    GTS" | ccc newnam gts gts.1 ; mv gts.1 gts
        vars="$vars gts"

        for v in sicn sic snw ; do
          binsml $v $v.d input=ic.binsml.istile1
          mv $v.d $v
          vars="$vars $v"
        done
#                                      relabel time step
        for v in $vars ; do
          if [ -s $v ] ; then
            V=`echo $v | tr 'a-z' 'A-Z'`
            tstep $v gg$v input=ic.tstep_model
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $V
               " >> ic.xsave
          fi
        done

# ------------------------------------ xsave

        vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        rm -f old
        while [ -n "$vars0" ] ; do
          vars1=`echo "$vars0" | cut -f1-80 -d' '`
          head -160 ic.xsave > ic.xsave1
          echo vars=$vars1
          cat ic.xsave1
          xsave old $vars1 new input=ic.xsave1
          mv new old
          vars0=`echo "$vars0" | cut -f81- -d' '`
          tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
        done

#   ---------------------------------- save results.

        save old ${flabel}dd
      fi # tier=1

#
# ------------------------------------ TIER 2: 3D/zonal mean variables on pressure levels
#                                      (available after gpint*.dk)
      if [ $tier -eq 2 ] ; then
        vars_all=""

# ------------------------------------ 3D pressure level daily

        vars="gpt gprh gpq gpu gpv gpw gpz"
        echo -e "SELLEV       19 1000  925  850  700  600  500  400  300  250  200  150  100   70\n             50   30   20   10 -500 -100" > ic.sellev
#                                      access all variables
        for v in $vars ; do
          if [ "$v" = "gpt" ] ; then
            V="TEMP"
          elif [ "$v" = "gprh" ] ; then
            V="RH"
          elif [ "$v" = "gpq" ] ; then
            V="SHUM"
          elif [ "$v" = "gpu" ] ; then
            V="U"
          elif [ "$v" = "gpv" ] ; then
            V="V"
          elif [ "$v" = "gpw" ] ; then
            V="OMEG"
          elif [ "$v" = "gpz" ] ; then
            V="PHI"
          else
            V=`echo "$v" | sed -e's/^gp//' | tr 'a-z' 'A-Z'`
          fi
#                                      access
          release $v
          access  $v ${flabel}_$v
#                                      select levels
          sellev $v $v.l input=ic.sellev
          mv $v.l $v
#                                      bin daily
          binsml $v $v.d input=ic.binsml.isgg
#                                      relabel time step
          tstep $v.d $v.t input=ic.tstep_model
#                                      accumulate xsave input card
          echo "C*XSAVE       $V
               " >> ic.xsave
          vars_all="$vars_all $v.t"
        done
# ------------------------------------ xsave

        vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        rm -f old
        while [ -n "$vars0" ] ; do
          vars1=`echo "$vars0" | cut -f1-80 -d' '`
          head -160 ic.xsave > ic.xsave1
          echo vars=$vars1
          cat ic.xsave1
          xsave old $vars1 new input=ic.xsave1
          mv new old
          vars0=`echo "$vars0" | cut -f81- -d' '`
          tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
        done

#   ---------------------------------- save results.
        save old ${flabel}dp

# ------------------------------------ 3D pressure level 6-h variables

        rm -f ic.xsave.6h
        vars_all=""
        vars="pmsl gpu gpv gpt gpq gpw gpz gpvr" # sui svi - sampled SU/SV - not available yet
        echo "SELLEV        7  925  850  700  600  500  250   50" > ic.sellev.6h
        
        for v in $vars ; do
          if [ "$v" = "gpt" ] ; then
            V="TEMP"
          elif [ "$v" = "gprh" ] ; then
            V="RH"
          elif [ "$v" = "gpq" ] ; then
            V="SHUM"
          elif [ "$v" = "gpu" ] ; then
            V="U"
          elif [ "$v" = "gpv" ] ; then
            V="V"
          elif [ "$v" = "gpz" ] ; then
            V="PHI"
          elif [ "$v" = "gpw" ] ; then
            V="OMEG"
          elif [ "$v" = "pmsl" ] ; then
            V="PMSL"
          elif [ "$v" = "gpvr" ] ; then
            V="VORT"
          elif [ "$v" = "sui" ] ; then
            V="SUI"
          elif [ "$v" = "svi" ] ; then
            V="SVI"
          else
            V=`echo "$v" | sed -e's/^gp//' | tr 'a-z' 'A-Z'`
          fi

          if [ "$v" = "sui" ] ; then  #rsk: not available yet
            echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME  SUI" | ccc select npakgg sui
          elif [ "$v" = "svi" ] ; then
            echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME  SVI" | ccc select npakgg svi
          else
            release $v
            access $v ${flabel}_$v
#                                      select level subset
            if [ "$v" != "pmsl" ] ; then
              sellev $v $v.l input=ic.sellev.6h
              mv $v.l $v
            fi
          fi
#                                      relabel time step
          tstep $v $v.t.6h input=ic.tstep_model_6h
#                                      accumulate xsave input card
          echo "C*XSAVE       ${V}
               " >> ic.xsave.6h
             vars_all="$vars_all $v.t.6h"
        done # vars

# ------------------------------------ xsave

        vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        rm -f old
        while [ -n "$vars0" ] ; do
          vars1=`echo "$vars0" | cut -f1-80 -d' '`
          head -160 ic.xsave.6h > ic.xsave1
          echo vars=$vars1
          cat ic.xsave1
          xsave old $vars1 new input=ic.xsave1
          mv new old
          vars0=`echo "$vars0" | cut -f81- -d' '`
          tail -n +161 ic.xsave.6h > ic.xsave1 ; mv ic.xsave1 ic.xsave.6h
        done

#   ---------------------------------- save results.

        save old ${flabel}6h

# ------------------------------------ zonal mean pressure level daily
# access beta for representative zonal avg 
        access beta ${flabel}_gpbeta

        vars_all=""
        vars="gpt gprh gpq gpu gpv gpw gpz"

#                                      access all variables
        for v in $vars ; do
          if [ "$v" = "gpt" ] ; then
            V="TEMP"
          elif [ "$v" = "gprh" ] ; then
            V="RH"
          elif [ "$v" = "gpq" ] ; then
            V="SHUM"
          elif [ "$v" = "gpu" ] ; then
            V="U"
          elif [ "$v" = "gpv" ] ; then
            V="V"
          elif [ "$v" = "gpw" ] ; then
            V="OMEG"
          elif [ "$v" = "gpz" ] ; then
            V="PHI"
          else
            V=`echo "$v" | sed -e's/^gp//' | tr 'a-z' 'A-Z'`
          fi
#                                      access
          release $v
          access  $v ${flabel}_$v
#                                      bin daily
          binsml $v $v.d input=ic.binsml.isgg
#                                      regular zonal mean
          zonavg $v.d $v.dz   
#                                      relabel time step
          tstep $v.dz $v.t input=ic.tstep_model
#                                      accumulate xsave input card
          echo "C*XSAVE       $V
               " >> ic.xsave
          vars_all="$vars_all $v.t"
        done
# ------------------------------------ xsave

        vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        rm -f old
        while [ -n "$vars0" ] ; do
          vars1=`echo "$vars0" | cut -f1-80 -d' '`
          head -160 ic.xsave > ic.xsave1
          echo vars=$vars1
          cat ic.xsave1
          xsave old $vars1 new input=ic.xsave1
          mv new old
          vars0=`echo "$vars0" | cut -f81- -d' '`
          tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
        done

#   ---------------------------------- save results.
        save old ${flabel}dx

      fi # tier=2
    done # daily_cmip6_tiers

# ------------------------------------ (sub)daily on model levels

    if [ "$gssave" = "on" ] ; then
#                                      sampled 6-hourly data on model levels
#                                      requires gssave=on
      # 3D sigma levels
      gsvars="ps gsq gst gsu gsv"
      rm -f ic.xsave.gs
      for v in $gsvars ; do
        if [ "$v" = "gsq" ] ; then
          V="SHUM"
        elif [ "$v" = "gst" ] ; then
          V="TEMP"
        elif [ "$v" = "gsu" ] ; then
          V="U"
        elif [ "$v" = "gsv" ] ; then
          V="V"
        elif [ "$v" = "ps" -o "$v" = "gsps" ] ; then
          V="PS"
        fi
        release $v
        access  $v ${flabel}_$v
#                                      relable time step
        tstep $v $v.1 input=ic.tstep_model_6h ; mv $v.1 $v
#                                      accumulate xsave input card
        echo "C*XSAVE       $V
               " >> ic.xsave.gs
      done

# ------------------------------------ xsave

      vars0=`echo "$gsvars" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      rm -f old
      while [ -n "$vars0" ] ; do
        vars1=`echo "$vars0" | cut -f1-80 -d' '`
        head -160 ic.xsave.gs > ic.xsave.gs1
        xsave old $vars1 new input=ic.xsave.gs1
        mv new old
        vars0=`echo "$vars0" | cut -f81- -d' '`
        tail -n +161 ic.xsave.gs > ic.xsave.gs1 ; mv ic.xsave.gs1 ic.xsave.gs
      done

#   ---------------------------------- save results.

      save   old ${flabel}ds

# ===============================================================
# Daily mean of 3D variables on model levels
# ===============================================================

    vars_all=""

# ===============================================================
# 3D sigma level daily that has been sampled.
# ===============================================================

# List of variables we want related to the CFMIP data request.
    vars="ps gst gsq gsu gsv gsw gsz"

# Access and select the variables.

    for v in $vars ; do
      if [ "$v" = "gst" ] ; then
        V="TEMP"
      elif [ "$v" = "gsq" ] ; then
        V="SHUM"
      elif [ "$v" = "gsu" ] ; then
        V="U"
      elif [ "$v" = "gsv" ] ; then
        V="V"
      elif [ "$v" = "gsw" ] ; then
        V="OMET"
      elif [ "$v" = "gsz" ] ; then
        V="PHI"
      elif [ "$v" = "ps" -o "$v" = "gsps" ] ; then
        V="PS"
      else
        V=`echo "$v" | sed -e's/^gs//' | tr 'a-z' 'A-Z'`
      fi

      release $v
      access  $v ${flabel}_$v

# Bin the data into daily means
      binsml $v $v.d input=ic.binsml.isgg

# Relabel the timestep and accumulate list of variables that are found into vars_all and cards needed for xsave
      tstep $v.d $v.t input=ic.tstep_model

      echo "C*XSAVE       $V
           " >> ic.xsave
      vars_all="$vars_all $v.t"
    done

# List of variables we want related to the CFMIP data request.
    vars="cld clw cic rh"

# Access and select the variables.
    VARS=`fmtselname $vars`

    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg $vars || true

    for v in $vars ; do
      V=`echo "$v" | tr 'a-z' 'A-Z'`
# Bin the data into daily means
      binsml $v $v.d input=ic.binsml.isgg

# Relabel the timestep and accumulate list of variables that are found into vars_all and cards needed for xsave
      tstep $v.d $v.t input=ic.tstep_model

      echo "C*XSAVE       $V
           " >> ic.xsave

      vars_all="$vars_all $v.t"
    done

# ===============================================================
# 3D sigma level daily that has been accumulated.
# ===============================================================

# Not COSP
    vars="dmc"

# COSP
# Note that "sunl" is already handled with 2D data above.
    vars="${vars} clcp clcm"

    VARS=`fmtselname $vars`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg $vars || true

# Relabel time step and accumulate list of variables that are found into vars_all and cards needed for xsave
    for v in $vars ; do
      if [ -s $v ] ; then
        V=`echo $v | tr 'a-z' 'A-Z'`
        tstep $v gg$v input=ic.tstep_accmodel

        vars_all="$vars_all gg$v"

        echo "C*XSAVE       $V
           " >> ic.xsave
      fi
    done

# ===============================================================
# Save the 3D fields.
# ===============================================================

    vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
    rm -f old
    while [ -n "$vars0" ] ; do
      vars1=`echo "$vars0" | cut -f1-80 -d' '`
      head -160 ic.xsave > ic.xsave1
      echo vars=$vars1
      cat ic.xsave1
      xsave old $vars1 new input=ic.xsave1
      mv new old
      vars0=`echo "$vars0" | cut -f81- -d' '`
      tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
    done

# ===============================================================
# Save the resulting file.
# ===============================================================
    access olddsd ${flabel}dsd na
    if [ -s olddsd ] ; then
     xjoin  olddsd old newdsd
     save   newdsd ${flabel}dsd
     delete olddsd
     release old newdsd
    else
     save   old ${flabel}dsd
    fi

    fi # gssave=on
