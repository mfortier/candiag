#!/bin/sh
#                  gcmpak              SK - FEB 01/18 ----- ------------ gcmpak
#  ----------------------------------- Repack gcm,inv/par to gs and ss files
#                                      JC - APR 11/19
#                                      Added site history files.

# Packer executables
pakrs=${pakrs:='pakrs2'}
pakgcm=${pakgcm:='pakgcm9'}
paktnd=${paktnd:='paktnd'}

# Possibly, setup to use "float1" executables...
if [ "$float1" = 'on' ] ; then
  FLTEXTNSN='_float1'
else
  FLTEXTNSN=' '
fi

# input card for xsave
echo "C*XSAVE       DATA DESCRIPTION
C
C*XSAVE       MODEL OUTPUT
C" > ic.xsave

# last year/month in model files
mon_last12=`echo $mon $months_gcm | awk '{printf "%02d",$1+$2-1}'`
if [ $mon_last12 -gt 12 ] ; then
  year_last=`echo $year | awk '{printf "%04d",$1+1}'`
  mon_last=`echo $mon_last12 | awk '{printf "%02d",$1-12}'`
else
  year_last=$year
  mon_last=$mon_last12
fi

# access PAR
access PAR ${uxxx}_${runid}_${year_last}_m${mon_last}_par

# convert PAR to text
bin2txt PAR parm.txt >/dev/null
# extract nlat/lonsl (needed for spectral transform below)
eval `grep -m 1 " nlat=" parm.txt`
eval `grep -m 1 " lonsl=" parm.txt`
nlat=`echo $nlat | awk '{printf "%5d",$1}'`
lonsl=`echo $lonsl | awk '{printf "%5d",$1}'`
echo nlat="$nlat" lonsl="$lonsl"

# access GCM model files
access GCM ${uxxx}_${runid}_${year}_m${mon}_gcm
access INV ${uxxx}_${runid}_${year}_m${mon}_inv

# access 3HR model files
access 3HR ${uxxx}_${runid}_${year}_m${mon}_3hr na

# access CTEM model files
access CTM ${uxxx}_${runid}_${year}_m${mon}_ctm na

# access TND model files
access TND ${uxxx}_${runid}_${year}_m${mon}_tnd na

# access SITE model files
access SIT ${uxxx}_${runid}_${year}_m${mon}_sit na

# INV (time invariant) file
if [ "$parallel_io" = "on" ] ; then
  ln -s INV/OUTINV${year}${mon}_* .
  joinpio${FLTEXTNSN} OUTINV OUTINV${year}${mon}_*
else
  mv INV/OUTINV${mon} OUTINV
fi
${pakrs}${FLTEXTNSN} OUTINV inv

# merge INV and PAR records
joinup datadesc PAR inv

######################
# join GCM tiles
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  if [ "$parallel_io" = "on" ] ; then
    { ln -s GCM/OUTGCM${y}${m}_* . ; joinpio${FLTEXTNSN} GCM${y}${m} OUTGCM${y}${m}_* && status=$? || status=$? ; echo $status > exit_status_joinpio_GCM_${y}${m} ; } &
  else
    mv GCM/OUTGCM${y}${m} GCM${y}${m} && status=$? || status=$? ; echo $status > exit_status_joinpio_GCM_${y}${m}
  fi
done
wait
# check that the exit status is 0 for all processes
for f in exit_status_joinpio_GCM_* ; do
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in gcmpak.dk: $f = `cat $f`"
    exit 1
  fi
done

# repack
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  { ${pakgcm}${FLTEXTNSN} GCM${y}${m} ss${y}${m} gs${y}${m} && status=$? || status=$? ; echo $status > exit_status_pakgcm_GCM_${y}${m} ; } &
done
wait
# check that the exit status is 0 for all processes
for f in exit_status_pakgcm_GCM_* ; do
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in gcmpak.dk: $f = `cat $f`"
    exit 1
  fi
done

# merge data description and model output records
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  xsave dummy datadesc gs${y}${m} newgs${y}${m} input=ic.xsave output=xsave.output &
  xsave dummy datadesc ss${y}${m} newss${y}${m} input=ic.xsave output=xsave.output &
done # months
wait

# reset time stamp
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  if [ "$parallel_io" = "on" ] ; then
    touch -r GCM/OUTGCM${y}${m}_00 newgs${y}${m} newss${y}${m}
  else
    touch -r GCM/OUTGCM${y}${m} newgs${y}${m} newss${y}${m}
  fi

  # save gs and ss files
  save newgs${y}${m} ${uxxx}_${runid}_${y}_m${m}_gs
  save newss${y}${m} ${uxxx}_${runid}_${y}_m${m}_ss
done

######################
# 3HR files (if exist)
if [ -s 3HR ] ; then

# join 3HR tiles
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  if [ "$parallel_io" = "on" ] ; then
    { ln -s 3HR/OUT3HR${y}${m}_* . ; joinpio${FLTEXTNSN} 3HR${y}${m} OUT3HR${y}${m}_* && status=$? || status=$? ; echo $status > exit_status_joinpio_3HR_${y}${m} ; } &
  else
    mv 3HR/OUT3HR${y}${m} 3HR${y}${m} && status=$? || status=$? ; echo $status > exit_status_joinpio_3HR_${y}${m}
  fi
done
wait
# check that the exit status is 0 for all processes
for f in exit_status_joinpio_3HR_* ; do
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in gcmpak.dk: $f = `cat $f`"
    exit 1
  fi
done

# repack
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  { ${pakgcm}${FLTEXTNSN} 3HR${y}${m} ss3h${y}${m} gs3h${y}${m} && status=$? || status=$? ; echo $status > exit_status_pakgcm_3HR_${y}${m} ; } &
done
wait
# check that the exit status is 0 for all processes
for f in exit_status_pakgcm_3HR_* ; do
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in gcmpak.dk: $f = `cat $f`"
    exit 1
  fi
done

# transform ss3h to grids and merge with gs3h
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi

  if [ -s ss3h${y}${m} ] ; then
    echo "COFAGG    $lonsl$nlat    0    2" | ccc cofagg ss3h${y}${m} gss3h${y}${m}
    mkdir spltall_3h_$m
    cd spltall_3h_$m
    spltall ../gss3h${y}${m}
    if [ -s LNSP ] ; then
	# convert LNSP PS
	expone LNSP SP
	echo "C*NEWNAM     PS" | ccc newnam SP PS
	rm -f LNSP SP
	vars=`cat .VARTABLE | cut -c1-4 | sed 's/LNSP/  PS/'`
	release ../gss3h${y}${m}
	cat $vars > ../gss3h${y}${m}
    fi
    cd ..
    # merge with other gridded fields
    cat gss3h${y}${m} gs3h${y}${m} > new3h${y}${m}
  else
    mv gs3h${y}${m} new3h${y}${m}
  fi
done # months

# reset time stamp
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  if [ "$parallel_io" = "on" ] ; then
    touch -r 3HR/OUT3HR${y}${m}_00 new3h${y}${m}
  else
    touch -r 3HR/OUT3HR${y}${m} new3h${y}${m}
  fi

  # save 3h files
  save new3h${y}${m} ${uxxx}_${runid}_${y}_m${m}_3h
done
fi # ! -s 3HR

######################
# CTEM files (if exist)
if [ -s CTM ] ; then

# join CTM tiles
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  if [ "$parallel_io" = "on" ] ; then
    { ln -s CTM/OUTCTM${y}${m}_* . ; joinpio${FLTEXTNSN} CTM${y}${m} OUTCTM${y}${m}_* && status=$? || status=$? ; echo $status > exit_status_joinpio_CTM_${y}${m} ; } &
  else
    mv CTM/OUTCTM${y}${m} CTM${y}${m} && status=$? || status=$? ; echo $status > exit_status_joinpio_CTM_${y}${m}
  fi
done
wait
# check that the exit status is 0 for all processes
for f in exit_status_joinpio_CTM_* ; do
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in gcmpak.dk: $f = `cat $f`"
    exit 1
  fi
done

# repack
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  { ${pakgcm}${FLTEXTNSN} CTM${y}${m} dummyss${y}${m} newtm${y}${m} && status=$? || status=$? ; echo $status > exit_status_pakgcm_CTM_${y}${m} ; } &
done
wait
# check that the exit status is 0 for all processes
for f in exit_status_pakgcm_CTM_* ; do
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in gcmpak.dk: $f = `cat $f`"
    exit 1
  fi
done

# reset time stamp
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  if [ "$parallel_io" = "on" ] ; then
    touch -r CTM/OUTCTM${y}${m}_00 newtm${y}${m}
  else
    touch -r CTM/OUTCTM${y}${m} newtm${y}${m}
  fi

  # save tm files
  save newtm${y}${m} ${uxxx}_${runid}_${y}_m${m}_tm
done
fi # ! -s CTM

######################
# TEND files (if exist)
if [ -s TND ] ; then

# join TND tiles
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  if [ "$parallel_io" = "on" ] ; then
    { ln -s TND/OUTEND${y}${m}_* . ; joinpio${FLTEXTNSN} TND${y}${m} OUTEND${y}${m}_* && status=$? || status=$? ; echo $status > exit_status_joinpio_TND_${y}${m} ; } &
  else
    mv TND/OUTEND${y}${m} TND${y}${m} && status=$? || status=$? ; echo $status > exit_status_joinpio_TND_${y}${m}
  fi
done
wait
# check that the exit status is 0 for all processes
for f in exit_status_joinpio_TND_* ; do
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in gcmpak.dk: $f = `cat $f`"
    exit 1
  fi
done

# repack
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  { release td${y}${m} ; ${paktnd}${FLTEXTNSN} TND${y}${m} td${y}${m} && status=$? || status=$? ; echo $status > exit_status_paktnd_TND_${y}${m} ; } &
done
wait
# check that the exit status is 0 for all processes
for f in exit_status_paktnd_TND_* ; do
  if [ "`cat $f`" != "0" ] ; then
    echo "ERROR in gcmpak.dk: $f = `cat $f`"
    exit 1
  fi
done

# reset time stamp
for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  if [ "$parallel_io" = "on" ] ; then
    touch -r TND/OUTEND${y}${m}_00 td${y}${m}
  else
    touch -r TND/OUTEND${y}${m} td${y}${m}
  fi

  # save td files
  save td${y}${m} ${uxxx}_${runid}_${y}_m${m}_td
done
fi # ! -s TND

######################
# SITE files (if exist)
if [ -s SIT ] ; then

for m in `seq $mon 1 $mon_last12` ; do
  if [ $m -gt 12 ] ; then
    m=`echo $m | awk '{printf "%02d",$1-12}'`
    y=`expr $year + 1`
  else
    m=`echo $m | awk '{printf "%02d",$1}'`
    y=$year
  fi
  
  # copy file
  ln SIT/OUTSIT${y}${m} site${y}${m}

  # save site files
  save site${y}${m} ${uxxx}_${runid}_${y}_m${m}_site
done
fi # ! -s SIT

# delete GCM,INV,etc. files
delete GCM
delete INV
delete 3HR na
delete CTM na
delete TND na
delete SIT na
