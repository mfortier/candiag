#comdeck looper
#   Make looping dependent on "abort" switch setting as well as "counter".
#   no longer check joinN (N=2,3,...) if join is not defined.
#                  looper              E. Chan,dl. Jun 02/98 - fm.--------looper
#   -----------------------------------Executes line contained in previously
#                                      defined shell variable "loopline".
#
#   * This common deck should be called with lines similar to the following:
#   *
#   * loopline='access gg$counter \${model$counter}gp'
#   * . looper.cdk
#   *
#   * The variable loopline contains the line to be looped over by comdeck
#   * looper.cdk.
#   *
#   * The rules of defining a valid "loopline" is as follows:
#   *
#   * 1) The entire line to be looped must be enclosed by single quotes.
#   * 2) An optional "counter" shell variable may be used anywhere on the
#   *    line.  The "counter" variable contains the number of iterations
#   *    through the loop, beginning at 1.  It will be expanded to whatever
#   *    its value is at runtime.
#   * 3) All parmsub parmeters used on the line must be backslashed.  This
#   *    is because the line is eventually scanned twice by the Unix shell
#   *    so that the variable "counter" is expanded before any parmsub
#   *    parameter. In the example above, this permits the usage of parmsub
#   *    parameters "model1", "model2", etc., which may be defined as names
#   *    of model gp files.
#   *

#   * if 'join' is not define, it is assigned a value of 1.
      if [ -z "$join" ] ; then
         join=1
         echo '*** Parameter join undefined. It is now set to be 1. ***'
      fi

#   * Execute line contained in "loopline", "join" number of times.
#   * The use of the double "eval" ensures that the line contained
#   * in "loopline" is scanned twice by the Unix shell, allowing any
#   * non-backslashed shell variable (such as the "counter") to
#   * be expanded before backslashed variables (such as parmsub parameters).

      counter=1
      while [ "$counter" -le "$join" -a "$abort" != 'abort' ]
      do
         eval eval "$loopline"
         counter=`expr "$counter" + 1`
      done
#   ......................................................................looper
