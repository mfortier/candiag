#!/bin/sh
#                  sfctile            ml - may 24/17 ----------------- sfctile
#
#   ---------------------------------- compute statistics on tiled fields
#                                      in gs file post gcm18.
#                                      NOTE: ONLY RUN IF CPP DIRECTIVE
#                                            "define save_tiled_fields" is
#                                            activated.
#
#  Output variables:
#
#   GTT : tiled skin temperature (degK)
#  SNOT : tiled snow mass (Kg/m2)
#   ANT : tiled snow albedo (dimensionless)
#   FNT : tiled snow fraction (dimensionless)
#   TNT : tiled snow temperature (degK)
#  RHNT : tile snow density (Kg/m3)
#  BCNT : tiled snow BC
#  REFT : tiles snow grain radius (mm)

#  ---------------------------------- access gs files

.   ggfiles.cdk

#  ---------------------------------- Select variables
#                                     (the number of variables for one select call
#                                      must not exceed 86)

    gsvars="GTT SNOT ANT FNT TNT RHNT BCNT REFT"
    GSVARS=`fmtselname $gsvars`
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars

#  ----------------------------------- compute and save statistics.

    statsav GTT SNOT ANT FNT TNT RHNT BCNT REFT\
            new_gp new_xp $stat2nd

#   ---------------------------------- save results.

    release oldgp
    access  oldgp ${flabel}gp
    xjoin   oldgp new_gp newgp
    save    newgp ${flabel}gp
    delete  oldgp

    if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
    fi
