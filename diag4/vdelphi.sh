#!/bin/sh
#                  vdelphi             D Neelin,BD.,EC. oct 3/96 - dl.
#   ---------------------------------- Calculates timavg  v" dot grad phi"
#                                      and saves it as v".delgz" for ape-ke
#                                      conversions.
#   sk: rewrite for new timavg.

      access oldgp ${flabel}gp
      echo "XFIND         $d" | ccc xfind  oldgp del
#   ---------------------------------- access other files.
      access spvort ${flabel}_spvr
      access spdiv  ${flabel}_spdiv
      access spphi  ${flabel}_spz

      cwinds spvort spdiv   spbigu spbigv
      timavg spbigu tspbigu spbigup
      timavg spbigv tspbigv spbigvp
      timavg spphi  tspphi  spphip

#   ---------------------------------- compute spectral timavg  "v dot grad phi"
#                                      and look at it's zonal values.
      echo "SPVDGX    $lat$lon" | ccc spvdgx  spbigup spbigvp spphip spvdz
      timavg  spvdz   tspvdz
      echo "COFAGG    $lon$lat" | cccc cofagg  tspvdz  vdz
      rzonavg vdz     del     rzvdz
      echo "ZXINT     .101976716
  RUN $run      V+ . DEL( GZ+ ) . DAYS $days ." | ccc zxint   rzvdz
      rm spbigup spbigvp spphip tspbigu tspbigv \
         tspphi spbigu spbigv

#   ---------------------------------- save grid v".gradgz".
      echo "XSAVE         V\".DELGZ\"
NEWNAM     VDGZ" | ccc xsave oldgp vdz newgp
      save newgp ${flabel}gp
      delete oldgp
