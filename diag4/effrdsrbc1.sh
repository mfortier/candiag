#  Convert to script - 13/DEC/2017 - DY
#
#
#                 effrdsrbc1           MN - Apr 23/2014 ------------- effrdsrbc1
#   ---------------------------------- Effective Radius of Snow, BC
#                                      concentration and mixing ratio.
#                                      Based on "effrdsrbc.dk" but with some
#                                      improvements were made so that, the
#                                      results produced are correctly weighted
#                                      in the calculation of mean results.
# -------------------------------------------------------------------------
#
.   ggfiles.cdk
#
#  ------------------------------ select input fields
#
echo "SELECT          $r1 $r2 $r3         1    1       REF BCSN DEPB RHON
SELECT      SNO" | ccc select npakgg REF BCSN DEPB RHON SNO
#
#  ------------------------------ time-average
#
echo 'FMASK             -1 NEXT   GT       0.0' | ccc fmask SNO smask
timavg smask tsmask
#
mlt REF smask MREF
timavg MREF tref
div tref tsmask REFT
#
div BCSN RHON RBCN
mlt RBCN smask MRBCN
timavg MRBCN trbcn
div trbcn tsmask RBCNT
#
mlt BCSN smask MBCSN
timavg MBCSN tbcsn
div tbcsn tsmask BCSNT
#
timavg DEPB DEPBT
#
#  ------------------------------ "xsave" the fields in "new_gp"
#
echo "XSAVE.        REF
NEWNAM.    REF
XSAVE.        BCSN
NEWNAM.    BCSN
XSAVE.        RBCN
NEWNAM.    RBCN
XSAVE.        DEPB
NEWNAM.    DEPB
XSAVE.        TMSK
NEWNAM.    TMSK" | ccc xsave new_gp REFT BCSNT RBCNT DEPBT tsmask new.
      mv new. new_gp
#
#  ------------------------------ save results
#
release oldgp
access  oldgp ${flabel}gp
xjoin   oldgp new_gp newgp

save    newgp ${flabel}gp
delete  oldgp
