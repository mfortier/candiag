#deck pool3
jobname=pool3; time=$stime; memory=$memory1
. comjcl.cdk

cat > Execute_Script << 'end_of_script'

# add "pool_var" switch to control pooling of the variances.
#                  pool3               sk - May 16/05 - sk.
#   ---------------------------------- multi-year pooling of means and variances.
#                                      If pool_var=off, don't pool variances.
#                                      Pool up to 50 diagn. and/or pooled files.
#                                      Input parameters ny01,...,ny50 specify
#                                      the number of years pooled in each input
#                                      file. The value 0 is used to indicate
#                                      diagnostic files, i.e. not yet pooled
#                                      files.
#                                      When pooling pooled and diagnostic files
#                                      the 1st input file must be a pooled file.
#                                      The variable 'poolsubset' may contain
#                                      a subset of variables to be pooled.
#                                      Variable names must be separated by the
#                                      semicolon ';'. Double quotes '"' must be
#                                      protected by the backslash '\'. 
#                                      If not defined, all variables are pooled.
#                                      The variable 'poolabort' controls the
#                                      abort behaviour of the deck. If 
#                                      'poolabort=off', it will continue even if
#                                      some variables are missing in some files.
#                                      Otherwise it will abort (the default
#                                      setting).
#

#   ---------------------------------- check the number of input files
      if [ $join -gt 50 ] ; then
        echo " *** ERROR in pool3.dk: number of files must be <= 50."
        exit 1
      fi
#   ---------------------------------- check 'poolabort'
#                                      (default is strict behaviour)
      if [ "$poolabort" != "off" ] ; then
        pool_abort="  1"
      else
        pool_abort="  0"
      fi
#   ---------------------------------- set multi-year pooling type
      if [ "$pool_var" = "off" ] ; then
	pool_type=" 1"                 # pool time means only
      else
	pool_type=" 0"                 # pool time means and variances
      fi

#   ---------------------------------- access gp files
.     gpattach.cdk
#   ---------------------------------- accumulate input card for 'pool'
      pool_files=""
      n=1
      echo "          ${pool_abort}${pool_type}" > .pool_input_card
      echo "              DATA DESCRIPTION" > .modinfo_input_card
      pool_input_card="          "
      while [ $n -le $join ] ; do
        eval ny=\$ny$n
#                                      check for ny0?, if ny? is undefined
        if [ $n -le 9 -a "0$ny" = "0" ] ; then
          eval ny=\$ny0$n
        fi
        pool_input_card=${pool_input_card}`echo "     $ny" | tail -6c`
        pool_files="$pool_files gg$n"
	remnd=`expr $n % 14` || true
#                                      select DATA DESCRIPTION records from gg1
	if [ $n -eq 1 ] ; then
	  xfind gg$n modinfo input=.modinfo_input_card || true
	  if [ -s modinfo ] ; then 
	    rm empty
	    xsave empty modinfo xmodinfo input=.modinfo_input_card
	  else
	    echo " *** WARNING in pool3.dk:"
	    echo " *** There are no DATA DESCRIPTION records."
	  fi
	fi
#                                      strip off DATA DESCRIPTION records from gg?
	xdelet gg$n .gg$n input=.modinfo_input_card
	rm gg$n .gg${n}_Link ; mv .gg$n gg$n
	if [ $remnd -eq 0 -o $n -eq $join ] ; then
	  echo "$pool_input_card" >> .pool_input_card
	  pool_input_card="          "
	fi
        n=`expr $n + 1`
      done
#   ---------------------------------- select subset if 'poolsubset' is defined
      if [ "0$poolsubset" != "0" ] ; then
#                                      determine the number of variables
        nvar=`echo $poolsubset | tr ';' '\n' | wc -l`
        if [ $nvar -gt 80 ] ; then
          echo " *** ERROR in pool3.dk:"
          echo " *** number of variables in 'poolsubset' must be <= 80."
          exit 1
        fi
#                                      build input cards for xfind and xsave
        args=""
        rm .xfind_input_card .xsave_input_card
        n=1
        while [ $n -le $nvar ] ; do
          var=`echo $poolsubset|cut -f$n -d';'|sed -e's/^ *//g' -e's/ *$//g'`
#                                      skip empty names
          if [ "0$var" != "0" ] ; then
	    echo "              $var"   >> .xfind_input_card
	    echo "              $var
" >> .xsave_input_card
            args="$args v$n"
          fi
          n=`expr $n + 1`
        done
#                                      select and save requested variables
        pool_files1=""
        rm dummy
        for f in $pool_files ; do
          rm $args
          xfind $f    $args     input=.xfind_input_card
          xsave dummy $args .$f input=.xsave_input_card
          rm $args
          pool_files1="$pool_files1 .$f"
        done
        pool_files=$pool_files1
      fi
#   ---------------------------------- run 'pool' on input files:
#                                      tavg = output with time averages
#                                      stdv = output with standard deviations
#                                      covd = output with daily (co)variances
#                                      covm = output with monthly (co)variances
      cat .pool_input_card
      pool tavg stdv covd covm $pool_files input=.pool_input_card

#   ---------------------------------- save the results
      joinup newgp xmodinfo tavg stdv covd covm
      save   newgp ${flabel}gp

end_of_script

. endjcl.cdk
