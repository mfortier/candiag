#!/bin/bash
# Convert canesm timeseries data into 'cmorized' netcdf data

#=====================
# Define bail function
#=====================
  # bail is a simple error exit routine
  bail_prefix="NETCDF CONV"
  bail(){
    echo $(date)" $this_host $runid --- ${bail_prefix}: $*"
    exit 1
  }

#==================================================
# setup working directory, and setup conversion env
#==================================================
  # activate pycmor environment
  source activate ${pycmor_env}

  # get NCCONV directory
  NCCONV_DIR=${CCRNSRC}/ncconv

  # If specified by the user, checkout a specific branch/commit of pycmor,
  # else leave it as the set commit
  if [[ -n $ncconv_cmmt ]]; then 
    echo "Checking out user specified ncconv version"
    cd $NCCONV_DIR 
    git fetch origin $ncconv_cmmt 2>/dev/null || echo "Using commit instead of a branch"
    git checkout $ncconv_cmmt
    cd -
  fi

  # setup directory
  ${NCCONV_DIR}/pycmor/config-pycmor

  # source additional pycmor settings (version strings/table lists)
  . ${NCCONV_DIR}/pycmor/pycmor.cfg

  # navigate into working directory and setup additional path considerations
  WRK_DIR=$(pwd)/netcdfconv
  cd $WRK_DIR
  . path_export.sh

#=================
#   run pycmor
#=================
  # check if user provided a list of tables to convert
  if [[ -n $ncconv_tabs2conv ]]; then
    # use user defined list
    tabs2conv="$ncconv_tabs2conv"
  else
    # use default cmorlist from pycmor.cfg
    tabs2conv="$cmorlist"
  fi

  # build argument list
  if (( CANESM_CMIP == 1 )) ; then
    pycmor_opts="--prod"
  fi
  pycmor_opts="${pycmor_opts} -L --exptable $ncconv_exptab -r $runid"
  pycmor_opts="${pycmor_opts} -d $NCCONV_DIR/tables/variable_tables -k ${chunk}"
  pycmor_opts="${pycmor_opts} -v $version -n $newversion"

  # call pycmor
  # Note: the table list must be passed as a space delimited list, and thus must be within ""
  time pycmor.py $pycmor_opts -t "$tabs2conv" >> conv.log 2>&1 || echo "pycmor failed! see logs in ~/.queue"

  # check how comprehensive the conversion was 
  log_parse -c >> conv.log 2>&1
#====================
#   handle output
#====================
  output_dir=${RUNPATH}/nc_output
  tmp_nc_dir=output/CMIP6

  # confirm that at least some netcdf files were produced
  if [[ -d "$tmp_nc_dir" ]] ; then
    # output was successfully produced - store logs in output directory on RUNPATH
    log_tar_file="${output_dir}/conv_logs/${chunk}.tar.gz"
  else
    # no output produced - store logs in .queue
    log_tar_file="${HOME}/.queue/${runid}_netcdf-conv_logs_${chunk}_$(date -u +%Y%j%H%M).tar.gz"
  fi

  # make sure the storage location exists
  log_storage_dir=$(dirname $log_tar_file)
  [[ -d $log_storage_dir ]] || mkdir -p $log_storage_dir

  # bundle up log files and store them
  dir_to_be_tarrd=$(basename $log_tar_file)
  dir_to_be_tarrd=${dir_to_be_tarrd%%.*}
  mkdir $dir_to_be_tarrd

  #-- Driver logs (one for each variable table)
  if compgen -G "pycmor-driver-log_*" > /dev/null; then
    #-- only store if any were produced
    mv pycmor-driver-log_* $dir_to_be_tarrd
  else
    echo "No pycmor driver logs produced"
  fi

  #-- other single log files/direcotires
  tmp_file_list="conv.log cmor_logs pycmor_logs"
  for f in $tmp_file_list; do 
    if [[ -f $f ]] || [[ -d $f ]]; then
      mv $f $dir_to_be_tarrd
    else
      echo "No $f produced"
    fi
  done

  #-- clean old tar file
  [[ -f $log_tar_file ]] && rm -f $log_tar_file

  #-- create tar file
  tar -czvf $log_tar_file $dir_to_be_tarrd

  # store netcdf files if any were produced, bail if not
  if [[ -d "$tmp_nc_dir" ]] ; then
    # loop over rsync calls until the files are successfully transfered
    max_retries=10
    rsync_status=1
    count=0
    while [[ $rsync_status -ne 0 ]] ; do
        if (( count == max_retries )); then
            bail_msg="the ncconv job failed to transfer all output .nc files to $output_dir after $max_retries"
            bail_msg+=" retries of the rsync command! Why is it failing??"
            bail $bail_msg
        fi
        rsync -avz $(dirname $tmp_nc_dir)/ $output_dir
        rsync_status=$?
        count=$((count+1))
        sleep 30
    done
  else
    bail_msg="pycmor failed to produce ANY output! Are you sure the timeseries files are on disk?"
    bail_msg="${bail_msg} See the conversion logs at ${log_storage_dir} for additional information."
    bail $bail_msg
  fi
