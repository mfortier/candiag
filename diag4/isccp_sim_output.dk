#deck isccp_sim_output
jobname=isccp_sim_output ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<'end_of_script'

#   jcl: Compute fields related to output from ISCCP simulator
#        Add missing flags to select gridpoints.
#       isccp_sim_output               jcl - Jul 02/08 - jcl
#   ---------------------------------- module to compute monthly mean fields 
#                                      related to output created by ISCCP 
#                                      simulator.

.   ggfiles.cdk

#  ---------------------------------- Cloud optical thickness

ccc select npakgg CT01 CT02 CT03 CT04

    timavg CT01 TCT01
    timavg CT02 TCT02
    timavg CT03 TCT03
    timavg CT04 TCT04

    rm CT01 CT02 CT03 CT04

#  ---------------------------------- Total cloud fraction

ccc select npakgg CF01 CF02 CF03 CF04

    timavg CF01 TCF01
    timavg CF02 TCF02
    timavg CF03 TCF03
    timavg CF04 TCF04

    rm CF01 CF02 CF03 CF04


#  ---------------------------------- Cloud inhomogeneity

ccc select npakgg CE01 CE02 CE03 CE04

    timavg CE01 TCE01
    timavg CE02 TCE02
    timavg CE03 TCE03
    timavg CE04 TCE04

    rm CE01 CE02 CE03 CE04


#  ---------------------------------- Number of cloudy and sunlit columns

ccc select npakgg CC01 CC02 CC03 CC04

    timavg CC01 TCC01
    timavg CC02 TCC02
    timavg CC03 TCC03
    timavg CC04 TCC04

    rm CC01 CC02 CC03 CC04

#  ---------------------------------- Cloud fraction as function of cloud 
#                                     top-pressure and cloud optical 
#                                     thickness

ccc select npakgg OT01 OT02 OT03 OT04 OT05 OT06 OT07 

    timavg OT01 TOT01
    timavg OT02 TOT02
    timavg OT03 TOT03
    timavg OT04 TOT04
    timavg OT05 TOT05
    timavg OT06 TOT06
    timavg OT07 TOT07

    rm OT01 OT02 OT03 OT04 OT05 OT06 OT07 

ccc select npakgg OT08 OT09 OT10 OT11 OT12 OT13 OT14

    timavg OT08 TOT08
    timavg OT09 TOT09
    timavg OT10 TOT10
    timavg OT11 TOT11
    timavg OT12 TOT12
    timavg OT13 TOT13
    timavg OT14 TOT14

    rm OT08 OT09 OT10 OT11 OT12 OT13 OT14

ccc select npakgg OT15 OT16 OT17 OT18 OT19 OT20 OT21

    timavg OT15 TOT15
    timavg OT16 TOT16
    timavg OT17 TOT17
    timavg OT18 TOT18
    timavg OT19 TOT19
    timavg OT20 TOT20
    timavg OT21 TOT21

    rm OT15 OT16 OT17 OT18 OT19 OT20 OT21

ccc select npakgg OT22 OT23 OT24 OT25 OT26 OT27 OT28

    timavg OT22 TOT22
    timavg OT23 TOT23
    timavg OT24 TOT24
    timavg OT25 TOT25
    timavg OT26 TOT26
    timavg OT27 TOT27
    timavg OT28 TOT28

    rm OT22 OT23 OT24 OT25 OT26 OT27 OT28

ccc select npakgg OT29 OT30 OT31 OT32 OT33 OT34 OT35

    timavg OT29 TOT29
    timavg OT30 TOT30
    timavg OT31 TOT31
    timavg OT32 TOT32
    timavg OT33 TOT33
    timavg OT34 TOT34
    timavg OT35 TOT35

    rm OT29 OT30 OT31 OT32 OT33 OT34 OT35

ccc select npakgg OT36 OT37 OT38 OT39 OT40 OT41 OT42

    timavg OT36 TOT36
    timavg OT37 TOT37
    timavg OT38 TOT38
    timavg OT39 TOT39
    timavg OT40 TOT40
    timavg OT41 TOT41
    timavg OT42 TOT42

    rm OT36 OT37 OT38 OT39 OT40 OT41 OT42

ccc select npakgg OT43 OT44 OT45 OT46 OT47 OT48 OT49

    timavg OT43 TOT43
    timavg OT44 TOT44
    timavg OT45 TOT45
    timavg OT46 TOT46
    timavg OT47 TOT47
    timavg OT48 TOT48
    timavg OT49 TOT49

    rm OT43 OT44 OT45 OT46 OT47 OT48 OT49

#  ---------------------------------- Cloud top pressure

ccc select npakgg ICP

    timavg ICP TICP

    rm ICP

#  ---------------------------------- Number of sunlit columns

ccc select npakgg SUNL

    timavg SUNL TSUNL

#  ---------------------------------- Create mask for missing values
    echo ' FMASK            -1   -1   LE        0.' > .fmask_input_card

    fmask TSUNL MSK input=.fmask_input_card
    echo ' XLIN       -999.0E0     0.0E0' > .xlin_input_card
    xlin MSK TMSK input=.xlin_input_card
    rm SUNL MSK

#  ---------------------------------- Compute monthly means Cloud fractions

    div TCF01 TSUNL MTCF01
    div TCF02 TSUNL MTCF02
    div TCF03 TSUNL MTCF03
    div TCF04 TSUNL MTCF04

    rm TCF01 TCF02 TCF03 TCF04

    div TOT01 TSUNL MTOT01
    div TOT02 TSUNL MTOT02
    div TOT03 TSUNL MTOT03
    div TOT04 TSUNL MTOT04
    div TOT05 TSUNL MTOT05
    div TOT06 TSUNL MTOT06
    div TOT07 TSUNL MTOT07

    rm TOT01 TOT02 TOT03 TOT04 TOT05 TOT06 TOT07 

    div TOT08 TSUNL MTOT08
    div TOT09 TSUNL MTOT09
    div TOT10 TSUNL MTOT10
    div TOT11 TSUNL MTOT11
    div TOT12 TSUNL MTOT12
    div TOT13 TSUNL MTOT13
    div TOT14 TSUNL MTOT14

    rm TOT08 TOT09 TOT10 TOT11 TOT12 TOT13 TOT14

    div TOT15 TSUNL MTOT15
    div TOT16 TSUNL MTOT16
    div TOT17 TSUNL MTOT17
    div TOT18 TSUNL MTOT18
    div TOT19 TSUNL MTOT19
    div TOT20 TSUNL MTOT20
    div TOT21 TSUNL MTOT21

    rm TOT15 TOT16 TOT17 TOT18 TOT19 TOT20 TOT21

    div TOT22 TSUNL MTOT22
    div TOT23 TSUNL MTOT23
    div TOT24 TSUNL MTOT24
    div TOT25 TSUNL MTOT25
    div TOT26 TSUNL MTOT26
    div TOT27 TSUNL MTOT27
    div TOT28 TSUNL MTOT28

    rm TOT22 TOT23 TOT24 TOT25 TOT26 TOT27 TOT28

    div TOT29 TSUNL MTOT29
    div TOT30 TSUNL MTOT30
    div TOT31 TSUNL MTOT31
    div TOT32 TSUNL MTOT32
    div TOT33 TSUNL MTOT33
    div TOT34 TSUNL MTOT34
    div TOT35 TSUNL MTOT35

    rm TOT29 TOT30 TOT31 TOT32 TOT33 TOT34 TOT35

    div TOT36 TSUNL MTOT36
    div TOT37 TSUNL MTOT37
    div TOT38 TSUNL MTOT38
    div TOT39 TSUNL MTOT39
    div TOT40 TSUNL MTOT40
    div TOT41 TSUNL MTOT41
    div TOT42 TSUNL MTOT42

    rm TOT36 TOT37 TOT38 TOT39 TOT40 TOT41 TOT42

    div TOT43 TSUNL MTOT43
    div TOT44 TSUNL MTOT44
    div TOT45 TSUNL MTOT45
    div TOT46 TSUNL MTOT46
    div TOT47 TSUNL MTOT47
    div TOT48 TSUNL MTOT48
    div TOT49 TSUNL MTOT49

    rm TOT43 TOT44 TOT45 TOT46 TOT47 TOT48 TOT49

#  ---------------------------------- Cloud top pressure

    div TICP TCC04 MTICP

    rm TICP

#  ---------------------------------- Cloud optical thickness

    div TCT01 TCC01 MTCT01
    div TCT02 TCC02 MTCT02
    div TCT03 TCC03 MTCT03
    div TCT04 TCC04 MTCT04

    rm TCT01 TCT02 TCT03 TCT04

#  ---------------------------------- Cloud inhomogeneity as measured by 

    div TCE01 TCC01 MTCE01
    div TCE02 TCC02 MTCE02
    div TCE03 TCC03 MTCE03
    div TCE04 TCC04 MTCE04

    rm TCE01 TCE02 TCE03 TCE04

#  ---------------------------------- Join the files we want to save to file

 vars="TSUNL  TCC01  TCC02  TCC03  TCC04"
 vars="$vars MTCF01 MTCF02 MTCF03 MTCF04"
 vars="$vars MTCT01 MTCT02 MTCT03 MTCT04"
 vars="$vars MTCE01 MTCE02 MTCE03 MTCE04"
 vars="$vars MTICP"
 vars="$vars MTOT01 MTOT02 MTOT03 MTOT04 MTOT05 MTOT06 MTOT07"
 vars="$vars MTOT08 MTOT09 MTOT10 MTOT11 MTOT12 MTOT13 MTOT14"
 vars="$vars MTOT15 MTOT16 MTOT17 MTOT18 MTOT19 MTOT20 MTOT21"
 vars="$vars MTOT22 MTOT23 MTOT24 MTOT25 MTOT26 MTOT27 MTOT28"
 vars="$vars MTOT29 MTOT30 MTOT31 MTOT32 MTOT33 MTOT34 MTOT35"
 vars="$vars MTOT36 MTOT37 MTOT38 MTOT39 MTOT40 MTOT41 MTOT42"
 vars="$vars MTOT43 MTOT44 MTOT45 MTOT46 MTOT47 MTOT48 MTOT49"

#  ----------------------------------- Add missing values to fields
   for x in $vars
   do
     add TMSK $x .$x
     mv .$x $x
   done

# Save the fields to a file

    statsav $vars new_gp new_xp $stat2nd 
#  ---------------------------------- save results.

      access oldgp ${flabel}gp
      xjoin oldgp new_gp newgp
      save newgp ${flabel}gp
      delete oldgp

      if [ "$rcm" != "on" ] ; then
      access oldxp ${flabel}xp
      xjoin oldxp new_xp newxp
      save newxp ${flabel}xp
      delete oldxp
      fi

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
0
0 ISCCP_SIM_OUTPUT -------------------------------------------- ISCCP_SIM_OUTPUT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      CT01 CT02 CT03 CT04
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      CF01 CF02 CF03 CF04
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      CE01 CE02 CE03 CE04
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      CC01 CC02 CC03 CC04
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      OT01 OT02 OT03 OT04
SELECT     OT05 OT06 OT07
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      OT08 OT09 OT10 OT11
SELECT     OT12 OT13 OT14
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      OT15 OT16 OT17 OT18
SELECT     OT19 OT20 OT21
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      OT22 OT23 OT24 OT25
SELECT     OT26 OT27 OT28
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      OT29 OT30 OT31 OT32
SELECT     OT33 OT34 OT35
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      OT36 OT37 OT38 OT39
SELECT     OT40 OT41 OT42
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      OT43 OT44 OT45 OT46
SELECT     OT47 OT48 OT49
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       ICP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      SUNL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE          MEAN SUNLIT GRIDPOINTS
XSAVE      SUNL
XSAVE          MEAN HIGH-TOP CLOUDY POINTS
XSAVE      CCHI
XSAVE          MEAN MIDDLE-TOP CLOUDY POINTS
XSAVE      CCMD
XSAVE          MEAN LOW-TOP CLOUDY POINTS
XSAVE      CCLO
XSAVE          MEAN ALL-TOPS CLOUDY POINTS
XSAVE      CC00
XSAVE          MEAN HIGH-TOP CLOUD FRACTION
XSAVE      CFHI
XSAVE          MEAN MIDDLE-TOP CLOUD FRACTION
XSAVE      CFMD
XSAVE          MEAN LOW-TOP CLOUD FRACTION
XSAVE      CFLO
XSAVE          MEAN ALL-TOPS CLOUD FRACTION
XSAVE      CF00
XSAVE          MEAN HIGH-TOP CLOUD VISIBLE OPTICAL THICKNESS
XSAVE      CTHI
XSAVE          MEAN MIDDLE-TOP CLOUD VISIBLE OPTICAL THICKNESS
XSAVE      CTMD
XSAVE          MEAN LOW-TOP CLOUD VISIBLE OPTICAL THICKNESS
XSAVE      CTLO
XSAVE          MEAN ALL-TOPS CLOUD VISIBLE OPTICAL THICKNESS
XSAVE      CT00
XSAVE          MEAN HIGH-TOP CLOUD VISIBLE EPSILON PARAMETER
XSAVE      CEHI
XSAVE          MEAN MIDDLE-TOP CLOUD VISIBLE EPSILON PARAMETER
XSAVE      CEMD
XSAVE          MEAN LOW-TOP CLOUD VISIBLE EPSILON PARAMETER 
XSAVE      CELO
XSAVE          MEAN ALL-TOPS CLOUD VISIBLE EPSILON PARAMETER
XSAVE      CE00
XSAVE          MEAN CLOUD TOP PRESSURE
XSAVE       ICP
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 1 (0.0-0.3,0.0-180.0)
XSAVE      OT01
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 2 (0.3-1.3,0.0-180.0)
XSAVE      OT02
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 3 (1.3-3.6,0.0-180.0)
XSAVE      OT03
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 4 (3.6-9.4,0.0-180.0)
XSAVE      OT04
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 5 (9.4-23.0,0.0-180.0)
XSAVE      OT05
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 6 (23.0-60.0,0.0-180.0)
XSAVE      OT06
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 7 (>60.0,0.0-180.0)
XSAVE      OT07
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 8 (0.0-0.3,180.0-310.0)
XSAVE      OT08
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 9 (0.3-1.3,180.0-310.0)
XSAVE      OT09
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 10 (1.3-3.6,180.0-310.0)
XSAVE      OT10
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 11 (3.6-9.4,180.0-310.0)
XSAVE      OT11
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 12 (9.4-23.0,180.0-310.0)
XSAVE      OT12
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 13 (23.0-60.0,180.0-310.0)
XSAVE      OT13
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 14 (>60.0,180.0-310.0)
XSAVE      OT14
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 15 (0.0-0.3,310.0-440.0)
XSAVE      OT15
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 16 (0.3-1.3,310.0-440.0)
XSAVE      OT16
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 17 (1.3-3.6,310.0-440.0)
XSAVE      OT17
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 18 (3.6-9.4,310.0-440.0)
XSAVE      OT18
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 19 (9.4-23.0,310.0-440.0)
XSAVE      OT19
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 20 (23.0-60.0,310.0-440.0)
XSAVE      OT20
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 21 (>60.0,310.0-440.0)
XSAVE      OT21
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 22 (0.0-0.3,440.0-560.0)
XSAVE      OT22
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 23 (0.3-1.3,440.0-560.0)
XSAVE      OT23
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 24 (1.3-3.6,440.0-560.0)
XSAVE      OT24
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 25 (3.6-9.4,440.0-560.0)
XSAVE      OT25
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 26 (9.4-23.0,440.0-560.0)
XSAVE      OT26
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 27 (23.0-60.0,440.0-560.0)
XSAVE      OT27
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 28 (>60.0,440.0-560.0)
XSAVE      OT28
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 29 (0.0-0.3,560.0-680.0)
XSAVE      OT29
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 30 (0.3-1.3,560.0-680.0)
XSAVE      OT30
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 31 (1.3-3.6,560.0-680.0)
XSAVE      OT31
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 32 (3.6-9.4,560.0-680.0)
XSAVE      OT32
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 33 (9.4-23.0,560.0-680.0)
XSAVE      OT33
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 34 (23.0-60.0,560.0-680.0)
XSAVE      OT34
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 35 (>60.0,560.0-680.0)
XSAVE      OT35
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 36 (0.0-0.3,680.0-800.0)
XSAVE      OT36
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 37 (0.3-1.3,680.0-800.0)
XSAVE      OT37
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 38 (1.3-3.6,680.0-800.0)
XSAVE      OT38
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 39 (3.6-9.4,680.0-800.0)
XSAVE      OT39
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 40 (9.4-23.0,680.0-800.0)
XSAVE      OT40
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 41 (23.0-60.0,680.0-800.0)
XSAVE      OT41
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 42 (>60.0,680.0-800.0)
XSAVE      OT42
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 43 (0.0-0.3,>800.0)
XSAVE      OT43
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 44 (0.3-1.3,>800.0)
XSAVE      OT44
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 45 (1.3-3.6,>800.0)
XSAVE      OT45
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 46 (3.6-9.4,>800.0)
XSAVE      OT46
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 47 (9.4-23.0,>800.0) 
XSAVE      OT47
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 48 (23.0-60.0,>800.0)
XSAVE      OT48
XSAVE          MEAN CLOUD FRACTION TAU, PRES BIN 49 (>60.0,>800.0)
XSAVE      OT49
end_of_data

. endjcl.cdk
