#!/bin/sh
#
#                  aodpth_volc         Y. PENG - Apr 30/12
#   ---------------------------------- EXTRACT AEROSOL RAIATIVE PROPERTIES
#                                      available if %aodpth is defined
#                                      (bulk and pla)
#
.     ggfiles.cdk
#
#  ----------------------------------  AEROSOL RADIATIVE PROPERTIES
#                                      AT BAND 1
#
#                                      EXTINCTION
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME EXB1 EXB2 EXB3 EXB4" | ccc select npakgg EXB1 EXB2 EXB3 EXB4
#                                      OPTICAL DEPTH
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME EXB5 EXBT ODB1 ODB2" | ccc select npakgg EXB5 EXBT ODB1 ODB2
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME ODB3 ODB4 ODB5 ODBT" | ccc select npakgg ODB3 ODB4 ODB5 ODBT
#                                      FINE MODE OPTICAL DEPTH FRACTION
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME OFB1 OFB2 OFB3 OFB4" | ccc select npakgg OFB1 OFB2 OFB3 OFB4
#                                      SINGLE SCATTERING ALBEDO
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME OFB5 OFBT SAB1 SAB2" | ccc select npakgg OFB5 OFBT SAB1 SAB2
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME SAB3 SAB4 SAB5 SABT" | ccc select npakgg SAB3 SAB4 SAB5 SABT
#                                      ABSORPTION
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME ABB1 ABB2 ABB3 ABB4" | ccc select npakgg ABB1 ABB2 ABB3 ABB4
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME ABB5 ABBT ODBV"      | ccc select npakgg ABB5 ABBT ODBV
#                                      ODBV IS STRATOSPHERIC AOD
#
#  ----------------------------------- AEROSOL RADIATIVE PROPERTIES
#                                      AT 550 NM
#
#                                      EXTINCTION
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME EXS1 EXS2 EXS3 EXS4" | ccc select npakgg EXS1 EXS2 EXS3 EXS4
#                                      OPTICAL DEPTH
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME EXS5 EXST ODS1 ODS2" | ccc select npakgg EXS5 EXST ODS1 ODS2
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME ODS3 ODS4 ODS5 ODST" | ccc select npakgg ODS3 ODS4 ODS5 ODST
#                                      FINE MODE OPTICAL DEPTH FRACTION
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME OFS1 OFS2 OFS3 OFS4" | ccc select npakgg OFS1 OFS2 OFS3 OFS4
#                                      SINGLE SCATTERING ALBEDO
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME OFS5 OFST SAS1 SAS2" | ccc select npakgg OFS5 OFST SAS1 SAS2
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME SAS3 SAS4 SAS5 SAST" | ccc select npakgg SAS3 SAS4 SAS5 SAST
#                                      ABSORPTION
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME ABS1 ABS2 ABS3 ABS4" | ccc select npakgg ABS1 ABS2 ABS3 ABS4
#
echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME ABS5 ABST ODSV"      | ccc select npakgg ABS5 ABST ODSV
#                                      ODSV IS STRATOSPHERIC AOD
#
      add ODBT ODBV ODBA
      add ODST ODSV ODSA
#
      release npakgg
#
#  ----------------------------------  SAVE SELECTED FIELDS
#
      statsav EXB1 EXB2 EXB3 EXB4 \
              EXB5 EXBT ODB1 ODB2 \
              ODB3 ODB4 ODB5 ODBT \
              OFB1 OFB2 OFB3 OFB4 \
              OFB5 OFBT SAB1 SAB2 \
              SAB3 SAB4 SAB5 SABT \
              ABB1 ABB2 ABB3 ABB4 \
              ABB5 ABBT ODBV \
              EXS1 EXS2 EXS3 EXS4 \
              EXS5 EXST ODS1 ODS2 \
              ODS3 ODS4 ODS5 ODST \
              OFS1 OFS2 OFS3 OFS4 \
              OFS5 OFST SAS1 SAS2 \
              SAS3 SAS4 SAS5 SAST \
              ABS1 ABS2 ABS3 ABS4 \
              ABS5 ABST ODSV \
              new_gp new_xp $stat2nd

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp

      if [ "$rcm" != "on" ] ; then
      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save    newxp ${flabel}xp
      delete  oldxp
      fi
