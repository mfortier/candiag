#!/bin/sh
#                  rtdiag70            M.Lazare/S.Kharin/V.Arora - Dec 22/2015
#   ---------------------------------- Model run-time diagnostics
#                                      This version can be used for coupled
#                                      model runs with the old ocean.
#
#   Runtime diagnostics is run once a year assuming that model
#   files (gs,cm,tm,gz,etc.) are available on disk for all 12 months.
#
#   Changes vs. v68:
#   - add tm (terrestrial carbon) file
#   - minor fixes here and there and a few new variables.
#   - remove diagnosed annual global CO2 emissions (can be done in plot_rtdiag)
#   - remove CO2 burden change (can be done in plot_rtdiag)
#   - simplify merging (now using a binary joinrtd)
#   Changes vs. v67:
#   - switches PhysA,PhysO,CarbA,CarbO,CarbL are determined automatically based on variable availability
#   - new parameter to control the number of rtd files kept on disk (keep_old_rtdiag_number). Default is keep_old_rtdiag_number=3.
#   - new variable leaf area index (CLAI)
#   - add monthly ST over land, ocean
#   - add monthly GT over global, land, ocean and global under seaice
#   - add monthly PCP over global, land, ocean
#   - add monthly CO2 PPMV (when CO2 is a tracer)
#   - new STMX and STMN over globe, land, ocean
#   - compute ice cover separately for ocean and lakes
#   - add sea-ice area (SICN>0) as compared to sea-ice extent (SICN>15%)
#   - add global ocean nitrate
#   - fix a bug in global vertical integral for global ocean alkalinity (it was global mean instead of integral)
#   - change names for land areas and fractions
#   - add OBEI from cm files
#
#   Switches: (determined automatically)
#
#   PhysA=on/off            physical atmosphere variables
#   PhysO=on/off            physical ocean variables
#   CarbA=on/off            atmosphere carbon variables
#   CarbO=on/off            ocean carbon variables
#   CarbL=on/off            land carbon variables
#
#   keep_old_rtdiag=on/off  keep older rtd files for all years
#   keep_old_rtdiag_number  the number of rtdiag files for previous years that are kept on disk.
#                           Default is 3, i.e., the current year plus previous two years.
#                           If =1, only the latest rtdfile which includes the current year is kept on disk.
#   rtd2vic=on/off          transfer rtd file to Victoria in /plots/pubplots/CanESM_run_plots/rtdfiles/.

# ----------------------------------------------------------------------
#                                      runtime diagnostic deck version
    version="70"

# ----------------------------------------------------------------------
#                                      initialize list of variables to be
#                                      extracted from model files
    gsvars=""   # from gs files each month
    ssvars=""   # from ss files
    cmvars=""   # from cm files
    tmvars=""   # from tm files
    gzvars=""   # from gz files

    gzlevd=5  # ocean level increment to be extracted
    upper_ocean_depths="220 500 800 1000 2000 3000" # integrate to these depths

# ---------------------------------- Local function defs

# A simple error exit routine
bail(){
  echo "${jobname}: $1"
  echo "${jobname}: $1" >> haltit
  exit 1
}

# Extract variable definitions from a shell script
pardef(){
  # usage: pardef PARM_txt var1 [var2 ...]
  # Larry Solheim ...Jan,2004

  # Extract particular variable definitions from a shell script
  # and add them to the current environment.
  # This is typically used to extract parmsub variables from a PARM record.

  # The first arg is the name of a text file containing the shell script.
  # The second and subsequent args define a list of variable names to evaluate.
  [ -z "$1" ] && bail "pardef: Missing PARM_txt file name."
  [ -z "$2" ] && bail "pardef: Missing variable name."
  loc_PARM_txt=$1
  [ -z "$loc_PARM_txt" ] && bail "pardef: loc_PARM_txt is missing"
  shift
  loc_env_list="$*"
  [ -z "$loc_env_list" ] && bail "pardef: loc_env_list is missing"

  [ ! -s $loc_PARM_txt ] && bail "pardef: $loc_PARM_txt is missing or empty"

  rm -f parmsub1 parmsub.x

  # Remove any CPP_I definition that may appear in the parmsub section
  addr1='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ss][Tt][Aa][Rr][Tt]/'
  addr2='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ee][Nn][Dd]/'
  eval sed \'${addr1},${addr2}d\' $loc_PARM_txt > parmsub.x

  # Remove comments and blank lines
  # Remove any lines that source an external script via ". extscript"
  # Remove " +PARM" line inserted by bin2txt, if any
  sed 's/#.*$//; /^ *[+]/d; /^ *\. .*/d; /^ *$/d' parmsub.x > parmsub1

  # Prepend a shebang line
  echo '#!/bin/sh' | cat - parmsub1 > parmsub.x

  # Ensure the resulting script ends with a blank line
  echo 'echo " "' | cat parmsub.x - > parmsub1

  # Make sure parmsub1 and parmsub.x are equivalent and executable
  cat parmsub1 > parmsub.x
  chmod u+x parmsub.x parmsub1

  [ ! -s parmsub.x ] && bail "pardef: Unable to create parmsub.x"

  echo " "
  for loc_var in $loc_env_list; do

    # Determine parameter values from the parmsub record
    echo 'echo "$'${loc_var}'"' | cat parmsub.x - > parmsub1 # added by SK to fix xemacs fontification bug"
    loc_val=`/bin/sh ./parmsub1|tail -1`

    # Create/reassign a variable of the same name in the current env
    eval ${loc_var}=\"\$loc_val\"

    [ -n "$loc_val" ] && echo "Found in $loc_PARM_txt :: ${loc_var}=$loc_val"
  done
  rm -f parmsub1 parmsub.x
}

# ------------------------------------ access PARM section to obtain values of some parameters
    PhysA="on"  # gs file must exist
    release gs01
    access  gs01 ${uxxx}_${runid}_${year}_m01_gs na
    if [ ! -s gs01 ] ; then
      echo "Error: **** ${uxxx}_${runid}_${year}_m01_gs is not found. ****"
      exit 1
    fi

    echo "C*SELECT   STEP         0         0    1         0    0 NAME PARM" |\
     ccc select gs01 parm ||\
     bail "Unable to extract PARM record from ${uxxx}_${runid}_${year}_m01_gs"
    release gs01
    bin2txt parm parm.txt >/dev/null
    [ ! -s parm.txt ] && bail "parm.txt is missing or empty"

    # First determine the value of ntrac to be used below
    # to create a list of tracer variable names (itNN)
    # Also get the grid size (nlat, lonsl)
    pardef parm.txt ntrac nlat lonsl ilev levs delt isgg isbeg israd
    [ -z "$ntrac" ] && ntrac=0
    [ -z "$nlat"  ] && bail "nlat is not defined in parm.txt"
    [ -z "$lonsl" ] && bail "lonsl is not defined in parm.txt"
    [ -z "$ilev" ] && bail "ilev is not defined in parm.txt"
    [ -z "$levs" ] && bail "levs is not defined in parm.txt"
    [ -z "$delt" ] && bail "delt is not defined in parm.txt"
    [ -z "$isgg" ] && bail "issg is not defined in parm.txt"
    [ -z "$isbeg" ] && bail "isbeg is not defined in parm.txt"
    [ -z "$israd" ] && bail "israd is not defined in parm.txt"
    # determine isbeg to isgg ratio
    if [ $isbeg -ge $isgg ] ; then
      nisratio=`echo $isbeg $isgg |awk '{printf "%2.2d",$1/$2}'`
    else
      nisratio=`echo $isbeg $isgg |awk '{printf "%2.2d",$2/$1}'`
    fi
    echo ntrac=$ntrac nlat=$nlat lonsl=$lonsl ilev=$ilev levs=$levs delt=$delt isgg=$isgg isbeg=$isbeg israd=$israd

    if [ $ntrac -gt 0 ]; then
      # Create a list of parmsub variable names whose value
      # will (should) be tracer names
      itlist=''
      nn=0
      while [ $nn -lt $ntrac ]; do
        nn=`echo $nn|awk '{printf "%2.2d",$1+1}' -`
        itlist="$itlist it$nn"
      done
      [ -z "$itlist" ] && bail "Error defining itNN variable names"

      # Add these tracer names to the current environment
      pardef parm.txt $itlist
    fi

# ------------------------------------ Physical atmosphere variables
    if [ "$PhysA" = "on" ] ; then
      gsvars="$gsvars ST STMX STMN SQ SRH FN SNO SIC SICN GT FSR FSG FSGC FLG FLGC FDL OLR FSLO FSA FLA FSAM FLAM BEG OBEG OBEX GC HFS HFL OBWG PCP PCPC PCPN QFS ROF RIVO FSO CLDT CLDO FSS FSSC FSD FSDC PWAT CO2 CH4 N2O WGL WGF TG TV RHON REF BCSN FMI RES TBEG TBEI TBES SBEG SBEI SBES TSNN TBWG TTG1 TWG1"
      if [ "$rtd_xtrachem" = "on" ] ; then
        gsvars="$gsvars ESFS EAIS ESTS EFIS ESVC ESVE  ESFB EAIB ESTB EFIB  ESFO EAIO ESTO EFIO  WDL6 WDD6 WDS6  DD6  SLO3 SLHP SDO3 SDHP SSO3 SSHP  DOX4  WDLB WDDB WDSB  DDB  WDLO WDDO WDSO  DDO"
      fi
      if [ "$rtd_ssvars" = "on" ] ; then
        ssvars="TEMP ES TMPN ESN TMPR ESR"
      else
        ssvars=""
      fi
#                                      find AGCM tracers
      if [ -z "$ntrac" ] ; then
        echo "Error: **** PhysA: The number of tracer parameter ntrac is undefined. ****"
        exit 1
      fi

      PLAtracers="off"
      i2=1
      while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval x=\${it$i2}
        if [ -z "$x" ] ; then
          echo "Error: **** tracer name for it$i2 is undefined. ****"
          exit 1
        fi
        if [ "$x" != "CO2" ] ; then
#                                      skip CO2 tracer (which is processed in CarbA section)
          gsvars="$gsvars VI$i2"
        fi
#                                      check for A01 name: if found then assume PLA-scheme for tracers
        if [ "$x" = "A01" ] ; then
          PLAtracers="on"
        fi
        i2=`expr $i2 + 1`
      done
    fi # PhysA

# ------------------------------------ Physical ocean variables

    release gz01
    access  gz01 ${uxxx}_${runid}_${year}_m01_gz na
    if [ -s gz01 ] ; then
      PhysO="on"
    else
      PhysO="off"
    fi
    release gz01
    if [ "$PhysO" = "on" ] ; then
      gzvars="$gzvars TEMP SALT PSIB V VISO"
      cmvars="$cmvars OBET OBWA OBEI OBEX"
    fi

# ------------------------------------ Determine carbon switches (based on availibity of variables in cm files)

    release cm01
    access  cm01 ${uxxx}_${runid}_${year}_m01_cm na

#   check if CarbA and CarbO need to be set to on or off

    if [ -s cm01 ] ; then
      echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME ECO2 FCOO" | ccc select cm01 eco2 fcoo || true
      # ECO2 must exist for CarbA=on
      if [ -s eco2 ] ; then
        CarbA="on"
        release eco2
      else
        CarbA="off"
      fi
      # FCOO must exist for CarbO=on
      if [ -s fcoo -a "$PhysO" = "on" ] ; then
        CarbO="on"
        release fcoo
      else
        CarbO="off"
      fi
    else
      # if cm file does not exist, there is no ocean and atmos carbon cycle
      CarbA="off"
      CarbO="off"
      cmvars=""
    fi

#   check if CarbL need to be set to on or off

    release tm01
    access  tm01 ${uxxx}_${runid}_${year}_m01_tm na

    if [ -s cm01 -o -s tm01 ] ; then
      echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME CVEG" | ccc select cm01 cveg_cm || true

      echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME CVEG" | ccc select tm01 cveg_tm || true
      # CVEG must exist for CarbL=on
      if [ -s cveg_cm -o -s cveg_tm ] ; then
        CarbL="on"
        release cveg_cm
        release cveg_tm
      else
        CarbL="off"
      fi
    else
      CarbL="off"
    fi

#   is CTEM output thrown to tm or cm file. If tim file exists make a note of that

    if [ -s tm01 ] ; then
      tm_file_exists="yes"
    else
      tm_file_exists="no"
    fi

    release cm01
    release tm01

    echo PhysA=$PhysA PhysO=$PhysO CarbA=$CarbA CarbO=$CarbO CarbL=$CarbL

# ------------------------------------ Atmospheric carbon variables
    if [ "$CarbA" = "on" ] ; then
      cmvars="$cmvars ECO2"
#                                      find CO2 tracer
      if [ -z "$ntrac" ] ; then
        echo "Error: **** CarbA: The number of tracer parameter ntrac is undefined. ****"
        exit 1
      fi
      iCO2=""
      i2=1
      while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval x=\${it$i2}
        if [ -z "$x" ] ; then
          echo "Error: **** tracer name for it$i2 is undefined. ****"
          exit 1
        fi
        if [ "$x" = "CO2" ] ; then
          iCO2="$i2"
          gsvars="$gsvars XF$i2 XL$i2 VI$i2 VM$i2 VH$i2"
        fi
        i2=`expr $i2 + 1`
      done
      if [ -z "$iCO2" ] ; then
        echo "**** CO2 tracer is not found. ****"
      fi
    fi

# ------------------------------------ Ocean carbon variables

    if [ "$CarbO" = "on" ] ; then
#                                      Ocean carbon variables
      cmvars="$cmvars FCOO SWMX OFSG PMSL"
      gzvars="$gzvars OPCO ODPC OVIC OFNP OFXO OFXI OFNF OFSO OFSI OPH ONIT ODIC OALK OPHY OZOO ODET ALK DIC BNI"
#                                      ODIC(new)=DIC(old), ONIT(new)=BNI(old), OALK(new)=ALK(old)
    fi
    if [ "$CarbL" = "on" ] ; then
#                                      Land carbon variables
      if [ "$tm_file_exists" = "yes" ] ; then
        # CTEM output is in tm file
        tmvars="$tmvars CVEG CDEB CHUM CFNP CFNE CFRV CFGP CFNB CFLV CFLD CFLH CFRH CFHT CFLF CFRD CFFD CFFV CBRN WFRA CH4H CH4N CW1D CW2D CLAI"
      else
        # CTEM output is in cm file
        cmvars="$cmvars CVEG CDEB CHUM CFNP CFNE CFRV CFGP CFNB CFLV CFLD CFLH CFRH CFHT CFLF CFRD CFFD CFFV CBRN WFRA CH4H CH4N CW1D CW2D CLAI"
      fi
    fi

    if [ -z "$gsvars" -a -z "$ssvars" -a -z "$gzvars" -a -z "$cmvars" -a -z "$tmvars" ] ; then
      echo "*** There is nothing to process ***"
      exit 0
    fi

    echo "gsvars=$gsvars"
    echo "ssvars=$ssvars"
    echo "cmvars=$cmvars"
    echo "tmvars=$tmvars"
    echo "gzvars=$gzvars"

# ----------------------------------------------------------------------

#                                      starting year of rtdiag.
    if [ -z "$year_rtdiag_start" ] ; then
#                                      try $year_rdiag_start if $year_rtdiag_start is undefined
      if [ -z "$year_rdiag_start" ] ; then
        echo "*** ERROR: Start year is undefined $year_rdiag_start ***"
        exit 1
      else
        year_rtdiag_start="$year_rdiag_start"
      fi
    fi
#                                      new rtd file
    year_rtdiag_start=`echo $year_rtdiag_start | awk '{printf "%04d", $1}'`;
    year=`echo $year | awk '{printf "%04d", $1}'`;
#                                      starting year of rtdiag.
    if [ -z "${uxxx_rtd}" ] ; then
      if [ -z "${uxxx}" ] ; then
        uxxx_rtd="sc"
      else
        uxxx_rtd=`echo "$uxxx" | sed -e 's/^m/s/'`
      fi
    fi
    rtdfile="${uxxx_rtd}_${runid}_${year_rtdiag_start}_${year}_rtd070"

#                                      rtd file for the previous years
    keep_old_rtdiag_number=${keep_old_rtdiag_number:=3}
    echo keep_old_rtdiag_number=$keep_old_rtdiag_number

    yearm1=`echo $year | awk '{printf "%04d", $1 - 1}'`;
    yearmo=`echo $year $keep_old_rtdiag_number | awk '{printf "%04d", $1 - $2}'`
    rtdfile1="${uxxx_rtd}_${runid}_${year_rtdiag_start}_${yearm1}_rtd070" # last year rtd file
    rtdfileo="${uxxx_rtd}_${runid}_${year_rtdiag_start}_${yearmo}_rtd070" # older rtd file to be deleted (depends on keep_old_rtdiag_number)
    echo yearm1=$yearm1 yearmo=$yearmo
    echo rtdfile1=$rtdfile1 rtdfileo=$rtdfileo

#                                      some common input cards
    echo "C*GGATIM      1    1    1    1" > ic.ggatim
    echo "C*WINDOW      1    1    1    1" > ic.window

# ------------------------------------ access last-year rtd file (except for the very first year)

    if [ $year -gt ${year_rtdiag_start} ] ; then
      access oldrtd $rtdfile1
    fi

# ------------------------------------ Save version and current year

    VERS=`echo $version | awk '{printf "%22.15e", $1}'`
    echo " TIME      1001 VERS         1         1         1    100101        -1
$VERS" > vers.txt
    YEAR=`echo $year | awk '{printf "%22.15e", $1}'`
    echo " TIME      1001 YEAR         1         1         1    100101        -1
$YEAR" > year.txt
    chabin vers.txt vers
    chabin year.txt year

    echo "C*XFIND       VERSION                                      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       YEAR                                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind
    echo "C*XSAVE       VERSION                                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VERS
C*XSAVE       YEAR                                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     YEAR" >> ic.xsave
    xsave dummy vers year newrtd input=ic.xsave

    # Nino regions weights

    # NINO3.4 (5S-5N; 170W-120W=190E-240E)
    # Gaussian 128x64 grid
    wind_nino34="   69   86   31   34"
    wght_nino34="C*MKWGHT     64    1  128    1   641 1 0    0"
    wgi1_nino34=".944444444"
    wgi2_nino34=".833333333"
    wgj1_nino34=".791536780"
    wgj2_nino34=".791536780"

    # NINO4 (5S-5N; 160E-150W =160E-210E)
    # Gaussian 128x64 grid
    wind_nino4="   58   76   31   34"
    wght_nino4="C*MKWGHT     64    1  128    1   641 1 0    0"
    wgi1_nino4=".611111111"
    wgi2_nino4=".166666667"
    wgj1_nino4=".791536780"
    wgj2_nino4=".791536780"

    # NINO3 (5S-5N; 90W-150W =210E-270E)
    # Gaussian 128x64 grid
    wind_nino3="   76   97   31   34"
    wght_nino3="C*MKWGHT     64    1  128    1   641 1 0    0"
    wgi1_nino3=".833333333"
    wgi2_nino3=".500000000"
    wgj1_nino3=".791536780"
    wgj2_nino3=".791536780"

    for ind in nino34 nino4 nino3 ; do
      eval wght=\$wght_$ind
      eval wind=\$wind_$ind
      eval wgi1=\$wgi1_$ind
      eval wgi2=\$wgi2_$ind
      eval wgj1=\$wgj1_$ind
      eval wgj2=\$wgj2_$ind

      # spatial weights
      echo "$wght" | ccc mkwght wght

      # select region
      echo "C*WINDOW  ${wind}         1" | ccc window wght nino

      # adjusting factors for partial grid-cell overlap
      i1=`echo "${wind}" | cut -c1-5`
      i2=`echo "${wind}" | cut -c6-10`
      j1=`echo "${wind}" | cut -c11-15`
      j2=`echo "${wind}" | cut -c16-20`
      echo "                  0.$wgj1" | ccc xlin nino const
      echo "C*WINDOW      1 9999$j1$j1         1    1.E+00" | ccc window const const_j1
      echo "                  0.$wgj2" | ccc xlin nino const
      echo "C*WINDOW      1 9999$j2$j2         1    1.E+00" | ccc window const const_j2
      echo "                  0.$wgi1" | ccc xlin nino const
      echo "C*WINDOW  $i1$i1    1 9999         1    1.E+00" | ccc window const const_i1
      echo "                  0.$wgi2" | ccc xlin nino const
      echo "C*WINDOW  $i2$i2    1 9999         1    1.E+00" | ccc window const const_i2
      mlt const_j1 const_j2 const_j12
      mlt const_i1 const_i2 const_i12
      mlt const_i12 const_j12 const_ij12
      mlt const_ij12 nino wght_${ind}
    done

# ------------------------------------ maximum number of variables that can be selected/xsaved

    nselmax=80
    nselmaxp1=`expr $nselmax + 1`

    nvarmax=40
    nvarmaxp1=`expr $nvarmax + 1`
    nvarmax2=`expr $nvarmax + $nvarmax`
    nvarmax2p1=`expr $nvarmax2 + 1`

# ------------------------------------ month loop

    month_names="JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC"
    month_lengths="31 28 31 30 31 30 31 31 30 31 30 31"
    rtdlist_ann=""
    rm -f ic.xfind_ann
    rm -f ic.xsave_ann
    for m in 01 02 03 04 05 06 07 08 09 10 11 12 ; do
      days=`echo ${month_names}   | cut -f${m} -d' '`
      lmon=`echo ${month_lengths} | cut -f${m} -d' '`

      rtdlist=""
      rm ic.xfind
      rm ic.xsave
#                                      access model files and select variables
#                                      gs file
      if [ -n "$gsvars" ] ; then
        release gs${m}
        access  gs${m} ${uxxx}_${runid}_${year}_m${m}_gs
        if [ ${m} -eq 1 ] ; then
#                                      add additional time-invariant variables for m01
          gsvars1="$gsvars MASK DPTH DZG"
        else
          gsvars1="$gsvars"
        fi

        varlist=`echo $gsvars1 | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        varlist0="$varlist"
        while [ -n "$varlist0" ] ; do
          varlist1=`echo "$varlist0" | cut -f1-$nselmax -d' '`
          GSVARS=`fmtselname $varlist1`
          echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$GSVARS" | ccc select gs${m} $varlist1 || true
          varlist0=`echo "$varlist0" | cut -f$nselmaxp1- -d' '`
        done
#                                      rename variables
        for x in $gsvars1 ; do
          if [ -s $x ] ; then
            mv $x gs${x}${m}
          fi
        done
#                                      select MASK etc. from gs file
        if [ ${m} -eq 1 ]; then
          echo "C*FMASK           -1 NEXT   EQ        0.                   1" | ccc fmask gsMASK01 ocean_frac # 1=ocean, 0=otherwise
          echo "C*FMASK           -1 NEXT   EQ       -1.                   1" | ccc fmask gsMASK01 tland_frac # 1=land,  0=otherwise
          echo "C*FMASK           -1 NEXT   EQ        2.                   1" | ccc fmask gsMASK01 rlake_frac # 1=lakes, 0=otherwise
          echo "C*FMASK           -1 NEXT   NE       -1.                   1" | ccc fmask gsMASK01 water_frac # 1=water (ocean + lakes), 0=otherwise
          DPTHmin=`ggstat gsDPTH01 | grep GRID | cut -c73-74`
          if [ "$DPTHmin" = "-4" ] ; then
            echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gsDPTH01 nogla_frac # 1=land (no glaciers), 0=otherwise
            echo "C*FMASK           -1 NEXT   EQ       -4.                   1" | ccc fmask gsDPTH01 glaci_frac # 1=glacier, 0=otherwise
          else
            echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gsDPTH01 nogla_frac # 1=land (no glaciers), 0=otherwise
            echo "C*FMASK           -1 NEXT   EQ        0.                   1" | ccc fmask gsDPTH01 glaci_frac # 1=glacier, 0=otherwise
          fi
          # globe mask (1 everywhere)
          echo "C*XLIN            0.        1." | ccc xlin ocean_frac globe_frac
          # NH and SH masks
          nlath=`echo $nlat | awk '{printf "%5d",$1/2}'`
          nlatp=`echo $nlat | awk '{printf "%5d",$1/2+1}'`
          nlatnhe=`echo $nlat | awk '{printf "%5.0f",2*$1/3+1}'` # north of 30N
          nlatshe=`echo $nlat | awk '{printf "%5.0f",$1/3}'`     # south of 30S
          nlatnhem1=`echo $nlatnhe | awk '{printf "%5d",$1-1}'`  # south of 30N
          nlatshep1=`echo $nlatshe | awk '{printf "%5d",$1+1}'`  # north of 30S
          nlatnhp=`echo $nlat | awk '{printf "%5.0f",$1*5/6+2}'` # north of 60N
          nlatshp=`echo $nlat | awk '{printf "%5.0f",$1*1/6-1}'` # south of 60S
          nlatshp1=`echo $nlat | awk '{printf "%5.0f",$1*1/6}'`  # north of 60S
          echo nlath=$nlath nlatp=$nlatp nlatnhe=$nlatnhe nlatshe=$nlatshe nlatnhp=$nlatnhp nlatshp=$nlatshp
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window globe_frac globe_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window globe_frac globe_frac_nh
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window ocean_frac ocean_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window ocean_frac ocean_frac_nh
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window water_frac water_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window water_frac water_frac_nh
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window tland_frac tland_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window tland_frac tland_frac_nh
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window globe_frac globe_frac_nhe # globe north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window globe_frac globe_frac_she # globe south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window globe_frac globe_frac_tro # globe tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window tland_frac tland_frac_nhe # land north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window tland_frac tland_frac_she # land south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window tland_frac tland_frac_tro # land tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window ocean_frac ocean_frac_nhe # ocean north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window ocean_frac ocean_frac_she # ocean south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window ocean_frac ocean_frac_tro # ocean tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhp 9999         1"        | ccc window globe_frac globe_frac_nhp # north of 60N
          echo "C*WINDOW      1 9999    1$nlatshp         1"        | ccc window globe_frac globe_frac_shp # south of 60S
          echo "C*WINDOW      1 9999$nlatshp1 9999         1"       | ccc window tland_frac noant_frac # land without Antarctica

          # fraction averages
          globavg globe_frac _ globe_favg
          globavg tland_frac _ tland_favg
          globavg ocean_frac _ ocean_favg
          globavg rlake_frac _ rlake_favg
          globavg water_frac _ water_favg
          globavg nogla_frac _ nogla_favg
          globavg glaci_frac _ glaci_favg
          globavg globe_frac_nh _ globe_favg_nh
          globavg globe_frac_sh _ globe_favg_sh
          globavg ocean_frac_nh _ ocean_favg_nh
          globavg ocean_frac_sh _ ocean_favg_sh
          globavg water_frac_nh _ water_favg_nh
          globavg water_frac_sh _ water_favg_sh
          globavg tland_frac_nh _ tland_favg_nh
          globavg tland_frac_sh _ tland_favg_sh
          globavg tland_frac_nhe _ tland_favg_nhe
          globavg tland_frac_she _ tland_favg_she
          globavg tland_frac_tro _ tland_favg_tro
          globavg globe_frac_nhp _ globe_favg_nhp
          globavg globe_frac_shp _ globe_favg_shp

          # areas (m2)
          globe_area_km2="510099699." # AGCM value
          echo "C*XLIN            0.${globe_area_km2}" | ccc xlin globe_favg globe_area_km2 # km2
          echo "C*XLIN          1.E6        0." | ccc xlin globe_area_km2 globe_area # m2
          mlt globe_area tland_favg tland_area
          mlt globe_area ocean_favg ocean_area
          mlt globe_area rlake_favg rlake_area
          mlt globe_area water_favg water_area
          mlt globe_area nogla_favg nogla_area
          mlt globe_area glaci_favg glaci_area
          mlt globe_area globe_favg_nh globe_area_nh
          mlt globe_area globe_favg_sh globe_area_sh
          mlt globe_area ocean_favg_nh ocean_area_nh
          mlt globe_area ocean_favg_sh ocean_area_sh
          mlt globe_area water_favg_nh water_area_nh
          mlt globe_area water_favg_sh water_area_sh
          mlt globe_area tland_favg_nh tland_area_nh
          mlt globe_area tland_favg_sh tland_area_sh
          mlt globe_area tland_favg_nhe tland_area_nhe
          mlt globe_area tland_favg_she tland_area_she
          mlt globe_area tland_favg_tro tland_area_tro
          mlt globe_area globe_favg_nhp globe_area_nhp
          mlt globe_area globe_favg_shp globe_area_shp
          # averaged soil level depth
          echo "C*BINS       -1" | ccc bins gsDZG01  gsDZGavg
        fi # m=1
      fi # gsvars

#                                      ss file
      if [ -n "$ssvars" ] ; then
        release ss${m}
        access  ss${m} ${uxxx}_${runid}_${year}_m${m}_ss
        SSVARS=`fmtselname $ssvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$SSVARS" | ccc select ss${m} $ssvars || true
        for x in $ssvars ; do
          if [ -s $x ] ; then
            mv $x ss${x}${m}
          fi
        done
      fi # ssvars
#                                      cm file
      if [ -n "$cmvars" ] ; then
        release cm${m}
        access  cm${m} ${uxxx}_${runid}_${year}_m${m}_cm
        CMVARS=`fmtselname $cmvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$CMVARS" | ccc select cm${m} $cmvars || true
        for x in $cmvars ; do
          if [ -s $x ] ; then
            mv $x cm${x}${m}
          fi
        done
      fi # cmvars
#                                      tm file
      if [ -n "$tmvars" ] ; then
        release tm${m}
        access  tm${m} ${uxxx}_${runid}_${year}_m${m}_tm
        TMVARS=`fmtselname $tmvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$TMVARS" | ccc select tm${m} $tmvars || true
        for x in $tmvars ; do
          if [ -s $x ] ; then
            mv $x cm${x}${m} # not changing CTEM variable names to tm here so that we don't need to
                             # modify code further down
          fi
        done
      fi # tmvars
#                                      gz file
      if [ -n "$gzvars" ] ; then
        release gz${m}
        access  gz${m} ${uxxx}_${runid}_${year}_m${m}_gz
        GZVARS=`fmtselname $gzvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$GZVARS" | ccc select gz${m} $gzvars || true
        for x in $gzvars ; do
          if [ -s $x ] ; then
            mv $x gz${x}${m}
          fi
        done
#                                      try to select left-shifted OPH, if OPH does not exist
        if [ "$CarbO" = "on" ] ; then
          if [ ! -s gzOPH${m} ] ; then
            echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME OPH " | ccc select gz${m} gzOPH${m}
          fi
        fi

#                                      get ocean grid geometry
        if [ ${m} -eq 1 ] ; then
          grido gz${m} betao da dx dy dz
          echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME  LAT  LON" | ccc select gz${m} lato lono
#                                      area weights of the upper layer
          echo "C*RCOPY            1         1" | ccc rcopy betao betao1
          mlt betao1 da bda1
#                                      tropics, northern and southern extratropics masks
          echo "C* FMSKPLT        -1 NEXT   GT      30.0    1        0.    1" | ccc fmskplt bda1 bdanhe1 lato
          echo "C* FMSKPLT        -1 NEXT   LT     -30.0    1        0.    1" | ccc fmskplt bda1 bdashe1 lato
          echo "C* FMSKPLT        -1 NEXT   LT      30.0    1        0.    1" | ccc fmskplt bda1 bda2    lato
          echo "C* FMSKPLT        -1 NEXT   GT     -30.0    1        0.    1" | ccc fmskplt bda2 bdatro1 lato
          release bda2
#                                      relabel level to 0
          echo "
                                  0" | ccc relabl bda1 bda0
#                                      relabel level to 1
          echo "
                                  1" | ccc relabl da da1
#                                      ocean depth
          vzint betao dz betaovi
#                                      global mean depth
          globavw betaovi da1 _ _ gbetaovi gbetaovol
#         ggstat gbetaovi
#         ggstat gbetaovol
#                                      global mean depth at various levels
          for depth in $upper_ocean_depths ; do
            depth10=${depth}0
            echo "C*  VZINTV         0${depth10}" | ccc vzintv betao dz betaovi${depth}
            globavw betaovi${depth} da1 _ _ gbetaovi${depth}
          done
#                                      extract data description records
          echo "C*XFIND.      DATA DESCRIPTION" | ccc xfind gz${m} gz_data_description
          gznlev=`ggstat gz_data_description | grep 'GRID         0     Z' | cut -c'42-46'`
          gznlat=`ggstat gz_data_description | grep 'GRID         0   LAT' | cut -c'50-54'`
#                                      define nino regions masks
          echo "C*SELECT.  STEP         0         0    1         0    0 NAME  LAT  LON" | ccc select gz_data_description lat lon
          zonavg_hr lat zlat
          # NINO3:  5N-5S,  90W-150W
          # NINO4:  5N-5S, 150W-160E
          # NINO34: 5N-5S, 120W-170W
          # NINO12: 0-10S,  80W-90W

          echo "C*FMASK           -1 NEXT   GE       -5.                   1" | ccc fmask lat latm5
          echo "C*FMASK           -1 NEXT   LE        5.                   1" | ccc fmask lat latp5
          mlt latm5 latp5 latm5p5

          # nino3: 90�W-150�W and 5�S-5�N.
          echo "C*FMASK           -1 NEXT   GE      210.                   1" | ccc fmask lon lon210
          echo "C*FMASK           -1 NEXT   LE      270.                   1" | ccc fmask lon lon270
          mlt lon210 lon270 lon210to270
          mlt latm5p5 lon210to270 nino3
          mlt bda1 nino3 bda_nino3

          # nino4: 150�W-160�E and 5�S-5�N.
          echo "C*FMASK           -1 NEXT   GE      160.                   1" | ccc fmask lon lon160
          echo "C*FMASK           -1 NEXT   LE      210.                   1" | ccc fmask lon lon210
          mlt lon160 lon210 lon160to210
          mlt latm5p5 lon160to210 nino4
          mlt bda1 nino4 bda_nino4

          # nino3.4: 120�W-170�W and 5�S-5�N.
          echo "C*FMASK           -1 NEXT   GE      190.                   1" | ccc fmask lon lon190
          echo "C*FMASK           -1 NEXT   LE      240.                   1" | ccc fmask lon lon240
          mlt lon190 lon240 lon190to240
          mlt latm5p5 lon190to240 nino34
          mlt bda1 nino34 bda_nino34

          # nino1.2: 80�W-90�W and 10�S-0�N.
          echo "C*FMASK           -1 NEXT   GE      -10.                   1" | ccc fmask lat latm10
          echo "C*FMASK           -1 NEXT   LE        0.                   1" | ccc fmask lat latp10
          mlt latm10 latp10 latm10p10
          echo "C*FMASK           -1 NEXT   GE      270.                   1" | ccc fmask lon lon270
          echo "C*FMASK           -1 NEXT   LE      280.                   1" | ccc fmask lon lon280
          mlt lon270 lon280 lon270to280
          mlt latm10p10 lon270to280 nino12
          mlt bda1 nino12 bda_nino12

          # mask latitudes selecting nearest grid location to 26N and 48N
          if [ $gznlat -eq 192 ] ; then
            echo "C*FMASK           -1 NEXT   GE      25.5                   1" | ccc fmask zlat zlat25
            echo "C*FMASK           -1 NEXT   LE      26.5                   1" | ccc fmask zlat zlat26
            mlt zlat25 zlat26 zlat25to26

            echo "C*FMASK           -1 NEXT   GE      47.5                   1" | ccc fmask zlat zlat47
            echo "C*FMASK           -1 NEXT   LE      48.5                   1" | ccc fmask zlat zlat48
            mlt zlat47 zlat48 zlat47to48
          elif [ $gznlat -eq 96 ] ; then
            echo "C*FMASK           -1 NEXT   GE      25.0                   1" | ccc fmask zlat zlat25
            echo "C*FMASK           -1 NEXT   LE      26.8                   1" | ccc fmask zlat zlat26
            mlt zlat25 zlat26 zlat25to26

            echo "C*FMASK           -1 NEXT   GE      47.1                   1" | ccc fmask zlat zlat47
            echo "C*FMASK           -1 NEXT   LE      48.8                   1" | ccc fmask zlat zlat48
            mlt zlat47 zlat48 zlat47to48
          else
            echo "ERROR: invalid gznlat=$gznlat"
            exit 1
          fi
#                                      define Atlantic basin
          echo "                 ATL" | ccc masko gz_data_description atlreg
        fi # m=1
      fi # gzvars

# #################################### Physical Atmosphere section

      if [ "$PhysA" = "on" ] ; then

# ------------------------------------ monthly time stamps

        x="SECS"
        gsfile=`readlink gs${m}`
        datesec=`stat -c %Y $gsfile`
        echo " TIME      1001 SECS         0         1         1    100101        -1
$datesec.E00" > datesec.txt
        chabin datesec.txt gtgs${x}${m}
        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       DATE IN SECONDS SINCE 1970                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       DATE IN SECONDS SINCE 1970                   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SECS" >> ic.xsave

# ------------------------------------ monthly ST

        x="ST"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding Antarctica
        globavg tgs${x}${m} _ gtgs${x}${m}c noant_frac
#                                      NH polar cap
        globavg tgs${x}${m} _ gtgs${x}${m}nhp globe_frac_nhp
#                                      SH polar cap
        globavg tgs${x}${m} _ gtgs${x}${m}shp globe_frac_shp

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}n gtgs${x}${m}c gtgs${x}${m}nhp gtgs${x}${m}shp"
        echo "C*XFIND       SCREEN TEMPERATURE GLOBAL (K)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE OCEAN (K)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE NORTH OF 60N (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE SOUTH OF 60S (K)          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE GLOBAL (K)                (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE OCEAN (K)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE NORTH OF 60N (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE SOUTH OF 60S (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST" >> ic.xsave

# ------------------------------------ monthly STMX

        x="STMX"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN TEMPERATURE MAX GLOBAL (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX LAND (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX OCEAN (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE MAX GLOBAL (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX LAND (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX OCEAN (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave

# ------------------------------------ monthly STMX max

        x="STMX"
        timmax gs${x}${m} tgs${x}max${m}
#                                      global
        globavg tgs${x}max${m} _ gtgs${x}max${m}g
#                                      land
        globavg tgs${x}max${m} _ gtgs${x}max${m}l tland_frac
#                                      ocean
        globavg tgs${x}max${m} _ gtgs${x}max${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}max${m}g gtgs${x}max${m}l gtgs${x}max${m}o"
        echo "C*XFIND       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX LAND (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX LAND (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave

# ------------------------------------ monthly STMN

        x="STMN"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN TEMPERATURE MIN GLOBAL (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN LAND (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN OCEAN (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE MIN GLOBAL (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN LAND (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN OCEAN (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave

# ------------------------------------ monthly STMN min

        x="STMN"
        timmin gs${x}${m} tgs${x}min${m}
#                                      global
        globavg tgs${x}min${m} _ gtgs${x}min${m}g
#                                      land
        globavg tgs${x}min${m} _ gtgs${x}min${m}l tland_frac
#                                      ocean
        globavg tgs${x}min${m} _ gtgs${x}min${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}min${m}g gtgs${x}min${m}l gtgs${x}min${m}o"
        echo "C*XFIND       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN LAND (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN LAND (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave

# ------------------------------------ monthly SQ

        x="SQ"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN SPECIFIC HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY LAND                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN SPECIFIC HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY LAND                (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ" >> ic.xsave
        fi

# ------------------------------------ monthly SRH

        x="SRH"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN RELATIVE HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY LAND                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN RELATIVE HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY LAND                (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH" >> ic.xsave
        fi

# ------------------------------------ monthly CLDT

        x="CLDT"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDT             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDT               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDT              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDT             (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDT               (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDT              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT" >> ic.xsave
        fi

# ------------------------------------ monthly CLDO

        x="CLDO"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDO             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDO               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDO              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDO             (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDO               (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDO              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO" >> ic.xsave
        fi

# ------------------------------------ monthly GT

        x="GT"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      lakes
        globavg tgs${x}${m} _ gtgs${x}${m}k rlake_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}k"
        echo "C*XFIND       SKIN TEMPERATURE GLOBAL (K)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN (K)                   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAKES (K)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SKIN TEMPERATURE GLOBAL (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE OCEAN (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAKES (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave

# ------------------------------------ monthly ocean GTO (=GT but GTFSW=271.2 under sea ice)

        x="GTO"
#                                      modify GT to GTFSW=271.2 under sea ice (GTO)
        echo "C* FMSKPLT        -1 NEXT   NE        1.    1    271.20    1" | ccc fmskplt gsGT${m} gs${x}${m} gsGC${m}
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}og ocean_frac
#                                      tropical ocean
        globavg tgs${x}${m} _ gtgs${x}${m}ot ocean_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}og gtgs${x}${m}ot"
        echo "C*XFIND       SKIN TEMPERATURE OCEAN UNDER ICE (K)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN UNDER ICE (K) 30S-30N (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SKIN TEMPERATURE OCEAN UNDER ICE (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTO
C*XSAVE       SKIN TEMPERATURE OCEAN UNDER ICE (K) 30S-30N (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTO" >> ic.xsave

# ------------------------------------ monthly GT NINO indices

        x="GT"
        for ind in nino34 nino4 nino3 ; do
          globavw tgs${x}${m} wght_${ind} _ _ gtgs${x}${m}$ind
        done
        rtdlist="$rtdlist gtgs${x}${m}nino34 gtgs${x}${m}nino4 gtgs${x}${m}nino3"
        echo "C*XFIND       SKIN TEMPERATURE NINO34 (K)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE NINO4 (K)                   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE NINO3 (K)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SKIN TEMPERATURE NINO34 (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE NINO4 (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE NINO3 (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave

# ------------------------------------ monthly PCP

        x="PCP"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      lakes
        globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}a nogla_frac
#                                      land excluding Antarctica
        globavg tgs${x}${m} _ gtgs${x}${m}c noant_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}r gtgs${x}${m}a gtgs${x}${m}c gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       PRECIPITATION GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAKES (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PRECIPITATION GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAKES (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave

# ------------------------------------ monthly PCP max

        x="PCP"
        timmax gs${x}${m} tgs${x}min${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}min${m} tgs${x}min${m}.1 ; mv tgs${x}min${m}.1 tgs${x}min${m}
#                                      global
        globavg tgs${x}min${m} _ gtgs${x}min${m}g
#                                      land
        globavg tgs${x}min${m} _ gtgs${x}min${m}l tland_frac
#                                      ocean
        globavg tgs${x}min${m} _ gtgs${x}min${m}o ocean_frac
#                                      NH extratropics
        globavg tgs${x}min${m} _ gtgs${x}min${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}min${m} _ gtgs${x}min${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}min${m} _ gtgs${x}min${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}min${m}g gtgs${x}min${m}l gtgs${x}min${m}o gtgs${x}min${m}n gtgs${x}min${m}s gtgs${x}min${m}t"
        echo "C*XFIND       MAX PRECIPITATION GLOBAL (MM/DAY)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION LAND (MM/DAY)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION OCEAN (MM/DAY)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30N-90N (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-90S (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-30N (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX PRECIPITATION GLOBAL (MM/DAY)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION LAND (MM/DAY)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION OCEAN (MM/DAY)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30N-90N (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-90S (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-30N (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave

# ------------------------------------ monthly PCPC

        x="PCPC"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       CONV. PRECIPITATION GLOBAL (MM/DAY)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION LAND (MM/DAY)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION OCEAN (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30N-90N (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-90S (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-30N (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       CONV. PRECIPITATION GLOBAL (MM/DAY)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION LAND (MM/DAY)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION OCEAN (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30N-90N (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-90S (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-30N (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC" >> ic.xsave

# ------------------------------------ monthly snowfall PCPN

        x="PCPN"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      lakes
        globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}a nogla_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}r gtgs${x}${m}a gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       SNOWFALL RATE GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAKES (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOWFALL RATE GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAKES (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave

# ------------------------------------ monthly evaporation QFS

        x="QFS"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      lakes
        globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}r gtgs${x}${m}n"
        echo "C*XFIND       EVAPORATION GLOBAL (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND (MM/DAY)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION OCEAN (MM/DAY)                   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAKES (MM/DAY)                   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       EVAPORATION GLOBAL (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND (MM/DAY)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION OCEAN (MM/DAY)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAKES (MM/DAY)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave

# ------------------------------------ monthly ROF

        x="ROF"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       RUNOFF LAND (MM/DAY)                         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       RUNOFF LAND (MM/DAY)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF" >> ic.xsave

# ------------------------------------ monthly RIVO

        x="RIVO"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       RIVER DISCHARGE (MM/DAY)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       RIVER DISCHARGE (MM/DAY)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM     RIVO" >> ic.xsave
        fi

# ------------------------------------ monthly (P-E)(Ocean)+Runoff(Land)

        x="PMER"
        sub gtgsPCP${m}o gtgsQFS${m}o gtgsPME${m}o      # P-E over ocean
#                                      compute land runoff that goes to ocean (land runoff & P-E over lakes)
        sub gtgsPCP${m}r gtgsQFS${m}r gtgsPME${m}r      # P-E over lakes
        mlt gtgsROF${m}l tland_favg   gtgsROF${m}lt     # total runoff from land
        mlt gtgsPME${m}r rlake_favg   gtgsPME${m}rt     # total freshwater from lakes
        add gtgsROF${m}lt gtgsPME${m}rt gtgsROFt        # total freshwater flux from land runoff & P-E over lakes
        div gtgsROFt     ocean_favg   gtgsROF${m}o      # averaged freshwater flux over ocean coming from land runoff & P-E over lakes
#                                      compute P-E+R flux over ocean
        add gtgsPME${m}o gtgsROF${m}o gtgs${x}${m}o

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       OCEAN P-E PLUS RUNOFF (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN P-E PLUS RUNOFF (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PMER" >> ic.xsave

# ------------------------------------ monthly ICE mass (SIC = kg)

        x="SIC"
        timavg gs${x}${m} tgs${x}${m}
#                                      global (ocean + lakes)
        globavg tgs${x}${m} _ gtavg water_frac
#                                      convert kg/m2 to kg
        gmlt gtavg water_area gtgs${x}${m}w
#                                      ocean
        globavg tgs${x}${m} _ gtavg ocean_frac
#                                      convert kg/m2 to kg
        gmlt gtavg ocean_area gtgs${x}${m}o
#                                      lakes
        globavg tgs${x}${m} _ gtavg rlake_frac
#                                      convert kg/m2 to kg
        gmlt gtavg rlake_area gtgs${x}${m}r

        rtdlist="$rtdlist gtgs${x}${m}w gtgs${x}${m}o gtgs${x}${m}r"
        echo "C*XFIND       TOTAL SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       LAKES SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ monthly SIC volume

        x="SIC"
        timavg gs${x}${m} tgs${x}
#                                      convert to thickness (m) by dividing by DENI=913(kg/m3) (from spwcon*.f)
        echo "C*XLIN          913.      0.E0         1" | ccc xlin tgs${x} tgs${x}h
#                                      ice thickness over ocean + lakes
        globavg tgs${x}h _ gtgs${x}wn water_frac_nh
        globavg tgs${x}h _ gtgs${x}ws water_frac_sh
#                                      convert to volume (m3)
        gmlt gtgs${x}wn water_area_nh gtgs${x}w${m}nv
        gmlt gtgs${x}ws water_area_sh gtgs${x}w${m}sv
        rm gtgs${x}wn gtgs${x}ws
#                                      ice volume over ocean
        globavg tgs${x}h _ gtgs${x}on ocean_frac_nh
        globavg tgs${x}h _ gtgs${x}os ocean_frac_sh
#                                      convert to volume (m3)
        gmlt gtgs${x}on ocean_area_nh gtgs${x}o${m}nv
        gmlt gtgs${x}os ocean_area_sh gtgs${x}o${m}sv
        rm gtgs${x}on gtgs${x}os
#                                      ice volume over lakes (both hemispheres)
        globavg tgs${x}h   _ gtgs${x}r rlake_frac
#                                      convert to volume (m3)
        gmlt gtgs${x}r rlake_area gtgs${x}r${m}v
        rm gtgs${x}r

        rtdlist="$rtdlist gtgs${x}w${m}nv gtgs${x}w${m}sv gtgs${x}o${m}nv gtgs${x}o${m}sv gtgs${x}r${m}v"
        echo "C*XFIND       TOTAL SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE VOLUME (M3)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       TOTAL SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       LAKES SEAICE VOLUME (M3)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ monthly maximum hemispheric SIC thickness (derived from monthly mean SIC depth)

#                                      maximum ice thickness (ocean + lakes)
        gmlt tgs${x}h water_frac_nh tgs${x}wn
        gmlt tgs${x}h water_frac_sh tgs${x}ws
        rmax tgs${x}wn maxwnh
        rmax tgs${x}ws maxwsh
        window maxwnh gtgs${x}${m}wnx input=ic.window
        window maxwsh gtgs${x}${m}wsx input=ic.window
        rm maxwnh maxwsh
#                                      maximum ice thickness (ocean only)
        gmlt tgs${x}h ocean_frac_nh tgs${x}on
        gmlt tgs${x}h ocean_frac_sh tgs${x}os
        rmax tgs${x}on maxon
        rmax tgs${x}os maxos
        window maxon gtgs${x}${m}onx input=ic.window
        window maxos gtgs${x}${m}osx input=ic.window
        rm maxon maxos
#                                      maximum ice thickness (lakes only)
        gmlt tgs${x}h rlake_frac tgs${x}r
        rmax tgs${x}r maxk
        window maxk gtgs${x}${m}rx input=ic.window
        rm maxk

        rtdlist="$rtdlist gtgs${x}${m}wnx gtgs${x}${m}wsx gtgs${x}${m}onx gtgs${x}${m}osx gtgs${x}${m}rx"
        echo "C*XFIND       MAX TOTAL SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX TOTAL SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX OCEAN SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX OCEAN SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX LAKES SEAICE THICKNESS (M)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX TOTAL SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX TOTAL SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX OCEAN SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX OCEAN SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX LAKES SEAICE THICKNESS (M)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ ice extent in NH and SH

#                                      ice extent derived from SIC>100kg/m2 (from monthly means)
        echo "FMASK             -1 NEXT   GT      100.                   1                   1" | ccc fmask tgs${x} tgs${x}_sicn
        globavg tgs${x}_sicn _ gtgs${x}ng water_frac_nh
        globavg tgs${x}_sicn _ gtgs${x}sg water_frac_sh
        globavg tgs${x}_sicn _ gtgs${x}no ocean_frac_nh
        globavg tgs${x}_sicn _ gtgs${x}so ocean_frac_sh
        globavg tgs${x}_sicn _ gtgs${x}r  rlake_frac
#                                      convert to total (hemisphere) area
        gmlt gtgs${x}ng water_area_nh gtgs${x}${m}nag
        gmlt gtgs${x}sg water_area_sh gtgs${x}${m}sag
        gmlt gtgs${x}no ocean_area_nh gtgs${x}${m}nao
        gmlt gtgs${x}so ocean_area_sh gtgs${x}${m}sao
        gmlt gtgs${x}r  rlake_area    gtgs${x}${m}ra

        rtdlist="$rtdlist gtgs${x}${m}nag gtgs${x}${m}sag gtgs${x}${m}nao gtgs${x}${m}sao gtgs${x}${m}ra"
        echo "C*XFIND       TOTAL SEAICE COVER NH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE COVER SH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER NH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER SH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE COVER SIC>100 (M2)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE COVER NH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       TOTAL SEAICE COVER SH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE COVER NH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE COVER SH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       LAKES SEAICE COVER SIC>100 (M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

#                                      ice extent derived from SICN>0.15 (from monthly means)
        x="SICN"
        timavg gs${x}${m} tgs${x}
        echo "FMASK             -1 NEXT   GT      0.15                   1                   1" | ccc fmask tgs${x} tgs${x}_msk
        globavg tgs${x}_msk _ gtgs${x}ng water_frac_nh
        globavg tgs${x}_msk _ gtgs${x}sg water_frac_sh
        globavg tgs${x}_msk _ gtgs${x}no ocean_frac_nh
        globavg tgs${x}_msk _ gtgs${x}so ocean_frac_sh
        globavg tgs${x}_msk _ gtgs${x}r  rlake_frac
#                                      convert to total (hemisphere) area
        gmlt gtgs${x}ng water_area_nh gtgs${x}${m}nag
        gmlt gtgs${x}sg water_area_sh gtgs${x}${m}sag
        gmlt gtgs${x}no ocean_area_nh gtgs${x}${m}nao
        gmlt gtgs${x}so ocean_area_sh gtgs${x}${m}sao
        gmlt gtgs${x}r  rlake_area    gtgs${x}${m}ra

        rtdlist="$rtdlist gtgs${x}${m}nag gtgs${x}${m}sag gtgs${x}${m}nao gtgs${x}${m}sao gtgs${x}${m}ra"
        echo "C*XFIND       TOTAL SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE COVER SICN>15% (M2)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       TOTAL SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       LAKES SEAICE COVER SICN>15% (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN" >> ic.xsave

#                                      ice area derived from SICN (from monthly means)
        x="SICN"
        timavg gs${x}${m} tgs${x}
        globavg tgs${x} _ gtgs${x}ang water_frac_nh
        globavg tgs${x} _ gtgs${x}asg water_frac_sh
        globavg tgs${x} _ gtgs${x}ano ocean_frac_nh
        globavg tgs${x} _ gtgs${x}aso ocean_frac_sh
        globavg tgs${x} _ gtgs${x}ak  rlake_frac
#                                      convert to total (hemisphere) area
        gmlt gtgs${x}ang water_area_nh gtgs${x}a${m}nag
        gmlt gtgs${x}asg water_area_sh gtgs${x}a${m}sag
        gmlt gtgs${x}ano ocean_area_nh gtgs${x}a${m}nao
        gmlt gtgs${x}aso ocean_area_sh gtgs${x}a${m}sao
        gmlt gtgs${x}ak  rlake_area    gtgs${x}a${m}ra

        rtdlist="$rtdlist gtgs${x}a${m}nag gtgs${x}a${m}sag gtgs${x}a${m}nao gtgs${x}a${m}sao gtgs${x}a${m}ra"
        echo "C*XFIND       TOTAL SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE AREA SICN>0 (M2)                (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       TOTAL SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       LAKES SEAICE AREA SICN>0 (M2)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN" >> ic.xsave

#                                      ice extent derived from GC=1 (from sub-daily values)
        x="GC"
        echo "C*FMASK           -1 NEXT   EQ        1.                   1" | ccc fmask gs${x}${m} gs${x}${m}i
        timavg gs${x}${m}i tgs${x}

        globavg tgs${x} _ gtgs${x}nw water_frac_nh
        globavg tgs${x} _ gtgs${x}sw water_frac_sh
        globavg tgs${x} _ gtgs${x}no ocean_frac_nh
        globavg tgs${x} _ gtgs${x}so ocean_frac_sh
        globavg tgs${x} _ gtgs${x}r  rlake_frac
#                                      convert to total (hemisphere) area
        gmlt gtgs${x}nw water_area_nh gtgs${x}${m}naw
        gmlt gtgs${x}sw water_area_sh gtgs${x}${m}saw
        gmlt gtgs${x}no ocean_area_nh gtgs${x}${m}nao
        gmlt gtgs${x}so ocean_area_sh gtgs${x}${m}sao
        gmlt gtgs${x}r  rlake_area    gtgs${x}${m}ra

        rtdlist="$rtdlist gtgs${x}${m}naw gtgs${x}${m}saw gtgs${x}${m}nao gtgs${x}${m}sao gtgs${x}${m}ra"
        echo "C*XFIND       TOTAL SEAICE COVER NH GC=1 (M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE COVER SH GC=1 (M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER NH GC=1 (M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER SH GC=1 (M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE COVER GC=1 (M2)                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE COVER NH GC=1 (M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       TOTAL SEAICE COVER SH GC=1 (M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE COVER NH GC=1 (M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE COVER SH GC=1 (M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       LAKES SEAICE COVER GC=1 (M2)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN" >> ic.xsave

# ------------------------------------ monthly radiation fluxes
#                                      compute BALT=radiation budget at top of atmosphere
#                                      compute BALX=radiation budget at top of model
#                                      compute BALP=AR5 definition of radiation budget at top of atmosphere
        if [ -s gsFSA${m} -a -s gsFSG${m} -a -s gsFLA${m} -a -s gsFLG${m} ] ; then
          # CanAM4.0
          add gsFSA${m}  gsFSG${m}  gsFSAG${m}
          add gsFLA${m}  gsFLG${m}  gsFLAG${m}
          add gsFSAG${m} gsFLAG${m} gsBALT${m}
        elif [ -s gsFSO${m} -a -s gsFSLO${m} -a -s gsFSR${m} -a -s gsOLR${m} -a -s gsFSAM${m} -a -s gsFLAM${m} ] ; then
          # CanAM4.1+
          sub gsFSO${m}  gsFSLO${m} gsFSI${m}
          sub gsFSI${m}  gsFSR${m}  gsFSAG${m}
          sub gsFSLO${m} gsOLR${m}  gsFLAG${m}
          add gsFSAG${m} gsFLAG${m} gsBALT${m}
#
          add gsFSAM${m} gsFLAM${m} gsFAM${m}
          sub gsBALT${m} gsFAM${m}  gsBALX${m}
#
          sub gsBALT${m} gsFLAM${m} gsBALP${m}
        fi

# ------------------------------------ monthly open-water and sea-ice covered BEG(israd)/OBEG(isbeg)

#                                      use daily GC=0 as open-water criterion (for specified seaice/GT runs)
#                                      use FMI=0 as open-water criterion (for interactive ocean runs)
#                                      make GC and FMI on the same frequency
        if [ $isbeg -ge $isgg ] ; then
          y="GC"
          echo "C* BINSML $nisratio" | ccc binsml gs${y}${m} gs${y}d${m}
          y="FMI"
          cp gs${y}${m} gs${y}d${m}
        else
          y="GC"
          cp gs${y}${m} gs${y}d${m}
          y="FMI"
          echo "C* BINSML $nisratio" | ccc binsml gs${y}${m} gs${y}d${m}
        fi

        y="GC"
        echo "C*FMASK           -1 NEXT   EQ       0.0                   1" | ccc fmask gs${y}d${m} gs${y}w${m} gs${y}i${m}
#                                      FMI is packed 2:1 (4:1) and hence a small threshold ~1E-6 (~1E-2) is needed instead of FMI=0
        y="FMI"
        echo "C*FMASK           -1 NEXT   LT      1E-2                   1" | ccc fmask gs${y}d${m} gs${y}w${m}1 gs${y}i${m}1
        echo "C*FMASK           -1 NEXT   GT     -1E-2                   1" | ccc fmask gs${y}d${m} gs${y}w${m}2 gs${y}i${m}2
        mlt gs${y}w${m}1 gs${y}w${m}2 gs${y}w${m}
        add gs${y}i${m}1 gs${y}i${m}2 gs${y}i${m}
#                                      combine GC and FMI masks
        echo "C* FMSKPLT        -1 NEXT   GT       0.5    1       0.0    1" | ccc fmskplt gsFMIw${m} gsMASKw${m} gsGCw${m}
        echo "C* FMSKPLT        -1 NEXT   LT       0.5    1       1.0    1" | ccc fmskplt gsFMIi${m} gsMASKi${m} gsGCi${m}
#                                      restrict to ocean only
        gmlt gsMASKw${m} ocean_frac gsMASKwo${m} # open-water ocean mask
        gmlt gsMASKi${m} ocean_frac gsMASKio${m} # sea-ice ocean mask
#                                      mask out fluxes using open-water and seaice masks
        mlt gsBEG${m}  gsMASKwo${m} gsBEGwo${m}
        mlt gsBEG${m}  gsMASKio${m} gsBEGio${m}
        mlt gsOBEG${m} gsMASKwo${m} gsOBEGwo${m}
        mlt gsOBEG${m} gsMASKio${m} gsOBEGio${m}

# ------------------------------------ monthly global soil temperature (TG)

        x="TG"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       SOIL TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SOIL TEMPERATURE EXCL.GLACIERS (K)           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SOIL TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM       TG
C*XSAVE       SOIL TEMPERATURE EXCL.GLACIERS (K)           (${days} ${year_rtdiag_start}-${year})
NEWNAM       TG" >> ic.xsave

# ------------------------------------ monthly canopy temperature (TV)

        x="TV"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only where TV>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=TV>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       CANOPY TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       CANOPY TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       TV" >> ic.xsave
        fi

# ------------------------------------ monthly liquid soil moisture (WGL)

        x="WGL"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       LIQUID SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQUID SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL" >> ic.xsave

# ------------------------------------ monthly frozen soil moisture (WGF)

        x="WGF"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       FROZEN SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       FROZEN SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF" >> ic.xsave

# ------------------------------------ monthly total (liquid + frozen) soil moisture (WGF+WGL)

        x="WGLF"
        add tgsWGL${m} tgsWGF${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       TOTAL SOIL MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SOIL MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of WGL

        x="VFSL"
#                                      convert kg/m2 to m by dividing by density (1000 kg/m3)
        echo "C*XLIN         1000.       0.0         1" | ccc xlin tgsWGL${m} tgsWGL${m}d
        div tgsWGL${m}d gsDZG01 tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM LIQUID SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSL
C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSL" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of WGF

        x="VFSF"
#                                      convert kg/m2 to m by dividing by density (917 kg/m3)
        echo "C*XLIN          917.       0.0         1" | ccc xlin tgsWGF${m} tgsWGF${m}d
        div tgsWGF${m}d gsDZG01 tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSF
C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSF" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of total soil moisture (VFSM)

        x="VFSM"
#                                      total moisture depth
        add tgsWGL${m}d tgsWGF${m}d tgsWGLF${m}d
#                                      multi-level average
        echo "C*BINS       -1" | ccc bins tgsWGLF${m}d tgsWGLF${m}davg
#                                      divide by averaged soil depth
        gdiv tgsWGLF${m}davg gsDZGavg tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSM
C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSM" >> ic.xsave

# ------------------------------------ monthly snow mass SNO

        x="SNO"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtavg tland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg tland_area gtgs${x}${m}l
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtavg nogla_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}${m}n
#                                      ocean
        globavg tgs${x}${m} _ gtavg ocean_frac
#                                      convert kg/m2 to kg
        gmlt gtavg ocean_area gtgs${x}${m}o
#                                      lakes
        globavg tgs${x}${m} _ gtavg rlake_frac
#                                      convert kg/m2 to kg
        gmlt gtavg rlake_area gtgs${x}${m}r
#                                      glaciers
        globavg tgs${x}${m} _ gtavg glaci_frac
#                                      convert kg/m2 to kg
        gmlt gtavg glaci_area gtgs${x}${m}i

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}n gtgs${x}${m}o gtgs${x}${m}r gtgs${x}${m}i"
        echo "C*XFIND       SNOW MASS GLOBAL (KG)                        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND (KG)                          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS (KG)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS OCEAN (KG)                         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAKES (KG)                         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS GLACIERS (KG)                      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS GLOBAL (KG)                        (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS LAND (KG)                          (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS (KG)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS OCEAN (KG)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS LAKES (KG)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS GLACIERS (KG)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO" >> ic.xsave

# ------------------------------------ monthly hemispheric snow cover derived from snow fraction FN

        x="FN"
        timavg gs${x}${m} tgs${x}
#                                      compute averages of NH and SH land
        globavg tgs${x} _ gtgs${x}gn globe_frac_nh
        globavg tgs${x} _ gtgs${x}gs globe_frac_sh
        globavg tgs${x} _ gtgs${x}ln tland_frac_nh
        globavg tgs${x} _ gtgs${x}ls tland_frac_sh
        globavg tgs${x} _ gtgs${x}on ocean_frac_nh
        globavg tgs${x} _ gtgs${x}os ocean_frac_sh
#                                      convert to area (m^2)
        gmlt gtgs${x}gn globe_area_nh gtgs${x}${m}gna
        gmlt gtgs${x}gs globe_area_sh gtgs${x}${m}gsa
        gmlt gtgs${x}ln tland_area_nh gtgs${x}${m}lna
        gmlt gtgs${x}ls tland_area_sh gtgs${x}${m}lsa
        gmlt gtgs${x}on ocean_area_nh gtgs${x}${m}ona
        gmlt gtgs${x}os ocean_area_sh gtgs${x}${m}osa

        rtdlist="$rtdlist gtgs${x}${m}gna gtgs${x}${m}gsa gtgs${x}${m}lna gtgs${x}${m}lsa gtgs${x}${m}ona gtgs${x}${m}osa"

        echo "C*XFIND       SNOW COVER NH (M2)                           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH (M2)                           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER NH LAND (M2)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH LAND (M2)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER NH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW COVER NH (M2)                           (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH (M2)                           (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER NH LAND (M2)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH LAND (M2)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER NH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN" >> ic.xsave

# ------------------------------------ monthly snow density (RHON)

        x="RHON"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only for RHON>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=RHON>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       SNOW DENSITY (KG/M3)                         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW DENSITY (KG/M3)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM     RHON" >> ic.xsave
        fi

# ------------------------------------ monthly effective snow grain radius (REF)

        x="REF"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only for REF>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=REF>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       EFFECTIVE SNOW GRAIN RADIUS (M)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       EFFECTIVE SNOW GRAIN RADIUS (M)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      REF" >> ic.xsave
        fi

# # ------------------------------------ monthly black carbon mixing ratio in snow (kg/kg) RBCN=BCSN/RHON

#         x="RBCN"
#         if [ -s gsBCSN${m} -a -s gsRHON${m} ] ; then
# #                                      compute RBCN
#         div gsBCSN${m} gsRHON${m} gs${x}${m}
# #                                      masked time mean (only for RHON>0)
#         y="RHONmsk" # computed above for RHON
#         timavg gs${x}${m} tgs${x}${m}
#         timavg gs${y}${m} tgs${y}${m}

# #                                      masked spatial mean
#         globavg tgs${x}${m} _ gtgs${x}${m}
#         globavg tgs${y}${m} _ gtgs${y}${m}
#         div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

#         rtdlist="$rtdlist gtgs${x}${m}"
#         echo "C*XFIND       BLACK CARBON MIXING RATIO IN SNOW(KG/KG)     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
#         echo "C*XSAVE       BLACK CARBON MIXING RATIO IN SNOW(KG/KG)     (${days} ${year_rtdiag_start}-${year})
# NEWNAM     RBCN" >> ic.xsave
#         fi

# ------------------------------------ monthly global BALT (new definition)

        x="BALT"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALT (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALT (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALT" >> ic.xsave
        fi

# ------------------------------------ monthly global BALP (AR5 definition of BALT)

        x="BALP"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALP (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALP (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALP" >> ic.xsave
        fi

# ------------------------------------ monthly global BALX (radiation budget at top of model)

        x="BALX"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALX (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALX (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALX" >> ic.xsave
        fi

# ------------------------------------ monthly BEG

        x="BEG"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      lakes
        globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}o gtgs${x}${m}l gtgs${x}${m}r"
        echo "C*XFIND       NET SFC ENERGY GLOBAL BEG (W/M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN BEG (W/M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAND BEG (W/M2)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAKES BEG (W/M2)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC ENERGY GLOBAL BEG (W/M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY OCEAN BEG (W/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAND BEG (W/M2)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAKES BEG (W/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG" >> ic.xsave

# ------------------------------------ construct PLA tracers

        if [ "$PLAtracers" = "on" ] ; then

# ------------------------------------ ammonium sulphate burden (BAS=VI15+VI18+VI21)

        x="BAS"
        if [ -s gsVI15${m} -a -s gsVI18${m} -a -s gsVI21${m} ] ; then
          add gsVI15${m}    gsVI18${m} gsVI15p18${m}
          add gsVI15p18${m} gsVI21${m} gs${x}${m}
          release gsVI15p18${m}
        fi

# ------------------------------------ organic matter burden (BOA=VI12+VI13+VI16+VI19)

        x="BOA"
        if [ -s gsVI12${m} -a -s gsVI13${m} -a -s gsVI16${m} -a -s gsVI19${m} ] ; then
          add gsVI12${m}    gsVI13${m} gsVI12p13${m}
          add gsVI16${m}    gsVI19${m} gsVI16p19${m}
          add gsVI12p13${m} gsVI16p19${m} gs${x}${m}
          release gsVI12p13${m} gsVI16p19${m}
        fi

# ------------------------------------ black carbon burden (BBC=VI11+VI14+VI17+VI20)

        x="BBC"
        if [ -s gsVI11${m} -a -s gsVI14${m} -a -s gsVI17${m} -a -s gsVI20${m} ] ; then
          add gsVI11${m}    gsVI14${m} gsVI11p14${m}
          add gsVI17${m}    gsVI20${m} gsVI17p20${m}
          add gsVI11p14${m} gsVI17p20${m} gs${x}${m}
          release gsVI11p14${m} gsVI17p20${m}
        fi

# ------------------------------------ sea salt burden (BSS=VI07+VI08)

        x="BSS"
        if [ -s gsVI07${m} -a -s gsVI08${m} ] ; then
          add gsVI07${m} gsVI08${m} gs${x}${m}
        fi

# ------------------------------------ mineral dust burden (BMD=VI09+VI10)

        x="BMD"
        if [ -s gsVI09${m} -a -s gsVI10${m} ] ; then
          add gsVI09${m} gsVI10${m} gs${x}${m}
        fi

# ------------------------------------ number burden internally mixed aerosol (BNI=VI26+VI27+VI28+VI29+VI30)

        x="BNI"
        if [ -s gsVI26${m} -a -s gsVI27${m} -a -s gsVI28${m} -a -s gsVI29${m} -a -s gsVI30${m} ] ; then
          add gsVI26${m}    gsVI27${m}    gsVI26p27${m}
          add gsVI28${m}    gsVI29${m}    gsVI28p29${m}
          add gsVI26p27${m} gsVI28p29${m} gsVI26p29${m}
          add gsVI26p29${m} gsVI30${m}    gs${x}${m}
          release gsVI26p27${m} gsVI28p29${m} gsVI26p29${m}
        fi

# ------------------------------------ number burden externally mixed aerosol (BNE=VI22+VI23+VI24+VI25)

        x="BNE"
        if [ -s gsVI22${m} -a -s gsVI23${m} -a -s gsVI24${m} -a -s gsVI25${m} ] ; then
          add gsVI22${m}    gsVI23${m} gsVI22p23${m}
          add gsVI24${m}    gsVI25${m} gsVI24p25${m}
          add gsVI22p23${m} gsVI24p25${m} gs${x}${m}
          release gsVI22p23${m} gsVI24p25${m}
        fi
        fi # PLAtracers=on

# ------------------------------------ monthly nudging tendencies

# ------------------------------------ heat flux tendency due to difference in SST over sea water

        x="TBEG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEG" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SST over water in model, ice in obs

        x="TBEI"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEI" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SST over ice in model, water in obs

        x="TBES"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBES" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over sea water

        x="SBEG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEG" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over water in model, ice in obs

        x="SBEI"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEI" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over ice in model, water in obs

        x="SBES"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBES" >> ic.xsave
        fi

# ------------------------------------ sea ice concentration tendency

        x="TSNN"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       SEAICE CONC TENDENCY DUE TO SICN NH          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SEAICE CONC TENDENCY DUE TO SICN SH          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN NH          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TSNN
C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN SH          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TSNN" >> ic.xsave
        fi

# ------------------------------------ fresh water flux tendency due to difference in SSS

        x="TBWG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       FRESHWATER FLUX TENDENCY DUE TO SSS          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       FRESHWATER FLUX TENDENCY DUE TO SSS          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBWG" >> ic.xsave
        fi

# ------------------------------------ top soil layer temperature tendency

        x="TTG1"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}l"
        echo "C*XFIND       TG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     TTG1" >> ic.xsave
        fi

# ------------------------------------ top soil layer moisture tendency (volumetric fraction)

        x="TWG1"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}l"
        echo "C*XFIND       WG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       WG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     TWG1" >> ic.xsave
        fi

# ------------------------------------ spectral TEMP
        x="TEMP"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL                         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL                         (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES
        x="ES"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL                           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL                           (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP tendency
        x="TMPN"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL TENDENCY                (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL TENDENCY                (${days} ${year_rtdiag_start}-${year})
NEWNAM     TMPN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES tendency
        x="ESN"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL TENDENCY                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL TENDENCY                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      ESN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP reference
        x="TMPR"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL REFERENCE               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL REFERENCE               (${days} ${year_rtdiag_start}-${year})
NEWNAM     TMPN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES reference
        x="ESR"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL REFERENCE                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL REFERENCE                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      ESN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ************************************ Physical Atmosphere annual section

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ annual ST

        x="ST"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding Antarctica
        globavg tgs${x} _ gtgs${x}c noant_frac
#                                      NH polar cap
        globavg tgs${x} _ gtgs${x}nhp globe_frac_nhp
#                                      SH polar cap
        globavg tgs${x} _ gtgs${x}shp globe_frac_shp

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}n gtgs${x}c gtgs${x}nhp gtgs${x}shp"
        echo "C*XFIND       SCREEN TEMPERATURE GLOBAL (K)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE OCEAN (K)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE NORTH OF 60N (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE SOUTH OF 60S (K)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE GLOBAL (K)                (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE OCEAN (K)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE NORTH OF 60N (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE SOUTH OF 60S (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST" >> ic.xsave_ann

# ------------------------------------ annual STMX

        x="STMX"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN TEMPERATURE MAX GLOBAL (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX LAND (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX OCEAN (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE MAX GLOBAL (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX LAND (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX OCEAN (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave_ann

# ------------------------------------ annual STMX max

        x="STMX"
        timmax gs${x}01 tgs${x}max gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x}max _ gtgs${x}maxg
#                                      land
        globavg tgs${x}max _ gtgs${x}maxl tland_frac
#                                      ocean
        globavg tgs${x}max _ gtgs${x}maxo ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}maxg gtgs${x}maxl gtgs${x}maxo"
        echo "C*XFIND       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX LAND (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX LAND (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave_ann

# ------------------------------------ annual STMN

        x="STMN"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN TEMPERATURE MIN GLOBAL (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN LAND (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN OCEAN (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE MIN GLOBAL (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN LAND (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN OCEAN (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave_ann

# ------------------------------------ annual STMN min

        x="STMN"
        timmin gs${x}01 tgs${x}min gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x}min _ gtgs${x}ming
#                                      land
        globavg tgs${x}min _ gtgs${x}minl tland_frac
#                                      ocean
        globavg tgs${x}min _ gtgs${x}mino ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}ming gtgs${x}minl gtgs${x}mino"
        echo "C*XFIND       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN LAND (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN LAND (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave_ann

# ------------------------------------ annual SQ

        x="SQ"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN SPECIFIC HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY LAND                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN SPECIFIC HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY LAND                (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ" >> ic.xsave_ann

# ------------------------------------ annual SRH

        x="SRH"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN RELATIVE HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY LAND                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN RELATIVE HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY LAND                (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH" >> ic.xsave_ann

# ------------------------------------ annual GT

        x="GT"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}r"
        echo "C*XFIND       SKIN TEMPERATURE GLOBAL (K)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN (K)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAKES (K)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SKIN TEMPERATURE GLOBAL (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE OCEAN (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAKES (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave_ann

# ------------------------------------ annual ocean GTO (=GT but GTFSW=271.2 under sea ice)

        x="GTO"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean
        globavg tgs${x} _ gtgs${x}og ocean_frac
        globavg tgs${x} _ gtgs${x}ot ocean_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}og gtgs${x}ot"
        echo "C*XFIND       SKIN TEMPERATURE OCEAN UNDER ICE (K)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN UNDER ICE (K) 30S-30N (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SKIN TEMPERATURE OCEAN UNDER ICE (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTO
C*XSAVE       SKIN TEMPERATURE OCEAN UNDER ICE (K) 30S-30N (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTO" >> ic.xsave_ann

# ------------------------------------ annual GT NINO indices

        x="GT"
        for ind in nino34 nino4 nino3 ; do
          # spatial mean
          globavw tgs${x} wght_$ind _ _ gtgs${x}$ind
        done
        rtdlist_ann="$rtdlist_ann gtgs${x}nino34 gtgs${x}nino4 gtgs${x}nino3"
        echo "C*XFIND       SKIN TEMPERATURE NINO34 (K)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE NINO4 (K)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE NINO3 (K)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SKIN TEMPERATURE NINO34 (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE NINO4 (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE NINO3 (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave_ann

# ------------------------------------ annual PCP

        x="PCP"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}a nogla_frac
#                                      land excluding Antarctica
        globavg tgs${x} _ gtgs${x}c noant_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}r gtgs${x}a gtgs${x}c gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       PRECIPITATION GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAKES (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PRECIPITATION GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAKES (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave_ann

# ------------------------------------ annual PCP max

        x="PCP"
        timmax gs${x}01 tgs${x}max gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}max tgs${x}max.1 ; mv tgs${x}max.1 tgs${x}max
#                                      global
        globavg tgs${x}max _ gtgs${x}maxg
#                                      land
        globavg tgs${x}max _ gtgs${x}maxl tland_frac
#                                      ocean
        globavg tgs${x}max _ gtgs${x}maxo ocean_frac
#                                      NH extratropics
        globavg tgs${x}max _ gtgs${x}maxn globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}max _ gtgs${x}maxs globe_frac_she
#                                      Tropics
        globavg tgs${x}max _ gtgs${x}maxt globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}maxg gtgs${x}maxl gtgs${x}maxo gtgs${x}maxn gtgs${x}maxs gtgs${x}maxt"
        echo "C*XFIND       MAX PRECIPITATION GLOBAL (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION LAND (MM/DAY)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION OCEAN (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30N-90N (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-90S (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-30N (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX PRECIPITATION GLOBAL (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION LAND (MM/DAY)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION OCEAN (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30N-90N (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-90S (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-30N (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave_ann

# ------------------------------------ annual PCPC

        x="PCPC"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       CONV. PRECIPITATION GLOBAL (MM/DAY)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION LAND (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION OCEAN (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30N-90N (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-90S (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-30N (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CONV. PRECIPITATION GLOBAL (MM/DAY)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION LAND (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION OCEAN (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30N-90N (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-90S (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-30N (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC" >> ic.xsave_ann

# ------------------------------------ annual snowfall PCPN

        x="PCPN"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}a nogla_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}r gtgs${x}a gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       SNOWFALL RATE GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAKES (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOWFALL RATE GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAKES (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave_ann

# ------------------------------------ annual evaporation QFS

        x="QFS"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}r gtgs${x}n"
        echo "C*XFIND       EVAPORATION GLOBAL (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND (MM/DAY)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION OCEAN (MM/DAY)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAKES (MM/DAY)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EVAPORATION GLOBAL (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND (MM/DAY)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION OCEAN (MM/DAY)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAKES (MM/DAY)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave_ann

# ------------------------------------ annual ROF

        x="ROF"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       RUNOFF LAND (MM/DAY)                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RUNOFF LAND (MM/DAY)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF" >> ic.xsave_ann

# ------------------------------------ annual RIVO

        x="RIVO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       RIVER DISCHARGE (MM/DAY)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RIVER DISCHARGE (MM/DAY)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     RIVO" >> ic.xsave_ann
        fi

# ------------------------------------ annual (P-E)(Ocean)+Runoff(Land)

        x="PMER"
        sub gtgsPCPo gtgsQFSo gtgsPMEo        # P-E over ocean
#                                      compute land runoff that goes to ocean (land runoff & P-E over lakes)
        sub gtgsPCPr gtgsQFSr gtgsPMEr        # P-E over lakes
        mlt gtgsROFl tland_favg gtgsROFlt     # total runoff from land
        mlt gtgsPMEr rlake_favg gtgsPMErt     # total freshwater from lakes
        add gtgsROFlt gtgsPMErt gtgsROFt      # total freshwater flux from land runoff & P-E over lakes
        div gtgsROFt ocean_favg gtgsROFo      # averaged freshwater flux over ocean coming from land runoff & P-E over lakes
#                                      compute P-E+R flux over ocean
        add gtgsPMEo gtgsROFo gtgs${x}o

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       OCEAN P-E PLUS RUNOFF (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN P-E PLUS RUNOFF (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PMER" >> ic.xsave_ann

# ------------------------------------ annual ICE mass (SIC = kg)

        x="SIC"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg water_frac
#                                      convert kg/m2 to kg
        gmlt gtavg water_area gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtavg ocean_frac
#                                      convert kg/m2 to kg
        gmlt gtavg ocean_area gtgs${x}o
#                                      lakes
        globavg tgs${x} _ gtavg rlake_frac
#                                      convert kg/m2 to kg
        gmlt gtavg rlake_area gtgs${x}r

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}r"
        echo "C*XFIND       TOTAL SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOTAL SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       LAKES SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave_ann

# ------------------------------------ annual soil temperature (TG)

        x="TG"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       SOIL TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SOIL TEMPERATURE EXCL.GLACIERS (K)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM       TG
C*XSAVE       SOIL TEMPERATURE EXCL.GLACIERS (K)           (ANN ${year_rtdiag_start}-${year})
NEWNAM       TG" >> ic.xsave_ann

# ------------------------------------ annual canopy temperature (TV)

        x="TV"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only where TV>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       CANOPY TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CANOPY TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       TV" >> ic.xsave_ann
        fi

# ------------------------------------ annual liquid soil moisture (WGL)

        x="WGL"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       LIQUID SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LIQUID SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL" >> ic.xsave_ann

# ------------------------------------ annual frozen soil moisture (WGF)

        x="WGF"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       FROZEN SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FROZEN SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF" >> ic.xsave_ann

# ------------------------------------ annual total (liquid + frozen) soil moisture (WGF+WGL)

        x="WGLF"
        add tgsWGL tgsWGF tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       TOTAL SOIL MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOTAL SOIL MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of WGL

        x="VFSL"
#                                      convert kg/m2 to m by dividing by density (1000 kg/m3)
        echo "C*XLIN         1000.       0.0         1" | ccc xlin tgsWGL tgsWGLd
        div tgsWGLd gsDZG01 tgs${x}

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSL
C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSL" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of WGF

        x="VFSF"
#                                      convert kg/m2 to m by dividing by density (917 kg/m3)
        echo "C*XLIN          917.       0.0         1" | ccc xlin tgsWGF tgsWGFd
        div tgsWGFd gsDZG01 tgs${x}

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSF
C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSF" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of total soil moisture (VFSM)

        x="VFSM"
#                                      total moisture depth
        add tgsWGLd tgsWGFd tgsWGLFd
#                                      multi-level average
        echo "C*BINS       -1" | ccc bins tgsWGLFd tgsWGLFdavg
#                                      divide by averaged soil depth
        gdiv tgsWGLFdavg gsDZGavg tgs${x}

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSM
C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSM" >> ic.xsave_ann

# ------------------------------------ annual snow mass SNO

        x="SNO"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}g
#                                      land
        globavg tgs${x} _ gtavg tland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg tland_area gtgs${x}l
#                                      land excluding glaciers
        globavg tgs${x} _ gtavg nogla_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}n
#                                      ocean
        globavg tgs${x} _ gtavg ocean_frac
#                                      convert kg/m2 to kg
        gmlt gtavg ocean_area gtgs${x}o
#                                      lakes
        globavg tgs${x} _ gtavg rlake_frac
#                                      convert kg/m2 to kg
        gmlt gtavg rlake_area gtgs${x}r
#                                      glaciers
        globavg tgs${x} _ gtavg glaci_frac
#                                      convert kg/m2 to kg
        gmlt gtavg glaci_area gtgs${x}i

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}n gtgs${x}o gtgs${x}r gtgs${x}i"
        echo "C*XFIND       SNOW MASS GLOBAL (KG)                        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND (KG)                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS (KG)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS OCEAN (KG)                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAKES (KG)                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS GLACIERS (KG)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW MASS GLOBAL (KG)                        (ANN ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS LAND (KG)                          (ANN ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS (KG)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS OCEAN (KG)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS LAKES (KG)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM      SNO
C*XSAVE       SNOW MASS GLACIERS (KG)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM      SNO" >> ic.xsave_ann

# ------------------------------------ annual snow density (RHON)

        x="RHON"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only for RHON>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SNOW DENSITY (KG/M3)                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW DENSITY (KG/M3)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     RHON" >> ic.xsave_ann
        fi

# ------------------------------------ annual effective snow grain radius (REF)

        x="REF"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only for REF>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       EFFECTIVE SNOW GRAIN RADIUS (M)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EFFECTIVE SNOW GRAIN RADIUS (M)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      REF" >> ic.xsave_ann
        fi

# # ------------------------------------ annual black carbon mixing ratio in snow (kg/kg) RBCN=BCSN/RHON

#         x="RBCN"
#         if [ -s gs${x}01 ] ; then
# #                                      masked time mean (only for RHON>0)
#         y="RHONmsk" # computed above for RHON
#         timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#         timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

# #                                      masked spatial mean
#         globavg tgs${x} _ gtgs${x}
#         globavg tgs${y} _ gtgs${y}
#         div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

#         rtdlist_ann="$rtdlist_ann gtgs${x}"
#         echo "C*XFIND       BLACK CARBON MIXING RATIO IN SNOW(KG/KG)     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
#         echo "C*XSAVE       BLACK CARBON MIXING RATIO IN SNOW(KG/KG)     (ANN ${year_rtdiag_start}-${year})
# NEWNAM     RBCN" >> ic.xsave_ann
#         fi

# ------------------------------------ annual global BALT (new definition)

        x="BALT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALT" >> ic.xsave_ann
        fi

# ------------------------------------ annual global BALP (AR5 definition of BALT)

        x="BALP"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALP (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALP (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALP" >> ic.xsave_ann
        fi

# ------------------------------------ annual global BALX (radiation budget at top of model)

        x="BALX"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALX (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALX (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALX" >> ic.xsave_ann
        fi

# ------------------------------------ annual BEG

        x="BEG"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       NET SFC ENERGY GLOBAL BEG (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN BEG (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAND BEG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAKES BEG (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL BEG (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY OCEAN BEG (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAND BEG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAKES BEG (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG" >> ic.xsave_ann

# ------------------------------------ annual ocean BEG open water

        x="BEGwo"
        y="MASKwo"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       NET SFC ENERGY OPEN OCEAN BEG (W/M2)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OPEN OCEAN BEG (W/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG" >> ic.xsave_ann

# ------------------------------------ annual ocean BEG above ice

        x="BEGio"
        y="MASKio"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       NET SFC ENERGY OCEAN ICE BEG (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OCEAN ICE BEG (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG" >> ic.xsave_ann

# ------------------------------------ annual OBEG

        x="OBEG"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       NET SFC ENERGY GLOBAL OBEG (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN OBEG (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAND OBEG (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAKES OBEG (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL OBEG (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG
C*XSAVE       NET SFC ENERGY OCEAN OBEG (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG
C*XSAVE       NET SFC ENERGY LAND OBEG (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG
C*XSAVE       NET SFC ENERGY LAKES OBEG (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG" >> ic.xsave_ann

# ------------------------------------ annual global OBEX=OBEG+OBEI

        x="OBEX"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o"
        echo "C*XFIND       NET SFC ENERGY GLOBAL OBEX (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN OBEX (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL OBEX (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEX
C*XSAVE       NET SFC ENERGY OCEAN OBEX (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEX" >> ic.xsave_ann
        fi

# ------------------------------------ annual ocean OBEG open water

        x="OBEGwo"
        y="MASKwo"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       NET SFC ENERGY OPEN OCEAN OBEG (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OPEN OCEAN OBEG (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG" >> ic.xsave_ann

# ------------------------------------ annual ocean OBEG under ice

        x="OBEGio"
        y="MASKio"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       NET SFC ENERGY OCEAN ICE OBEG (W/M2)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OCEAN ICE OBEG (W/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG" >> ic.xsave_ann

# ------------------------------------ annual global FMI

        x="FMI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       HEAT TO MELT ICE GLOBAL FMI (W/M2)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT TO MELT ICE GLOBAL FMI (W/M2)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      FMI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ABEG = FSG + FLG - (HFS + LATA), where LATA=HS*PCPN + HV*(PCP-PCPN),HS=2.835E6;HV=2.501E6

        for x in FSG FLG HFS PCPN PCP ; do
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
        done
        # compute LATA
        sub gtgsPCP gtgsPCPN gtgsPCPR
        echo "C*XYLIN      2.835E6   2.501E6        0." | ccc xylin gtgsPCPN gtgsPCPR gtgsLATA
        # compute ABEG
        add gtgsFSG  gtgsFLG  gtgsFSLG
        add gtgsHFS  gtgsLATA gtgsHFSL
        sub gtgsFSLG gtgsHFSL gtgsABEG

        x="ABEG"
        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       NET SFC ENERGY GLOBAL ABEG (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL ABEG (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     ABEG" >> ic.xsave_ann

# ------------------------------------ annual global RES

        x="RES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       RESIDUAL HEAT FLUX GLOBAL RES (W/M2)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RESIDUAL HEAT FLUX GLOBAL RES (W/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      RES" >> ic.xsave_ann
        fi

# ------------------------------------ annual OBWG

        x="OBWG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}r"
        echo "C*XFIND       GLB FRESHWATER FLUX OBWG (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN FRESHWATER FLUX OBWG (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES FRESHWATER FLUX OBWG (MM/DAY)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB FRESHWATER FLUX OBWG (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWG
C*XSAVE       OCN FRESHWATER FLUX OBWG (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWG
C*XSAVE       LAKES FRESHWATER FLUX OBWG (MM/DAY)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSO

        x="FSO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA INC S/W GLOBAL FSO (W/M2)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA INC S/W GLOBAL FSO (W/M2)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSO" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSLO

        x="FSLO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA SOLAR/LW OVERLAP GLOBAL FSLO (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA SOLAR/LW OVERLAP GLOBAL FSLO (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSLO" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSR

        x="FSR"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA REFL S/W GLOBAL FSR (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA REFL S/W GLOBAL FSR (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSR" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSAG

        x="FSAG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA NET S/W GLOBAL FSAG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA NET S/W GLOBAL FSAG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSAG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLAG

        x="FLAG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA NET L/W GLOBAL FLAG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA NET L/W GLOBAL FLAG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLAG" >> ic.xsave_ann
        fi

# ------------------------------------ annual OLR

        x="OLR"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLOBAL OUTGOING L/W OLR (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL OUTGOING L/W OLR (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      OLR" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSS

        x="FSS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INCIDENT S/W FSS (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC INCIDENT S/W FSS (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS
C*XSAVE       OCN SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS
C*XSAVE       LAND SFC INCIDENT S/W FSS (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS
C*XSAVE       LAKES SFC INCIDENT S/W FSS (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSSC

        x="FSSC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INC S/W CLEAR SKY FSSC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC INC S/W CLEAR SKY FSSC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC
C*XSAVE       OCN SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC
C*XSAVE       LAND SFC INC S/W CLEAR SKY FSSC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC
C*XSAVE       LAKES SFC INC S/W CLEAR SKY FSSC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSG

        x="FSG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET S/W FSG (W/M2)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC NET S/W FSG (W/M2)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG
C*XSAVE       OCN SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG
C*XSAVE       LAND SFC NET S/W FSG (W/M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG
C*XSAVE       LAKES SFC NET S/W FSG (W/M2)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSGC

        x="FSGC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET S/W CLEAR SKY FSGC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC NET S/W CLEAR SKY FSGC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC
C*XSAVE       OCN SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC
C*XSAVE       LAND SFC NET S/W CLEAR SKY FSGC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC
C*XSAVE       LAKES SFC NET S/W CLEAR SKY FSGC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC" >> ic.xsave_ann
        fi

# ------------------------------------ annual FDL

        x="FDL"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INCIDENT L/W FDL (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC INCIDENT L/W FDL (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL
C*XSAVE       OCN SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL
C*XSAVE       LAND SFC INCIDENT L/W FDL (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL
C*XSAVE       LAKES SFC INCIDENT L/W FDL (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLG

        x="FLG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET L/W FLG (W/M2)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC NET L/W FLG (W/M2)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG
C*XSAVE       OCN SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG
C*XSAVE       LAND SFC NET L/W FLG (W/M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG
C*XSAVE       LAKES SFC NET L/W FLG (W/M2)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLGC

        x="FLGC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET L/W CLEAR SKY FLGC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC NET L/W CLEAR SKY FLGC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC
C*XSAVE       OCN SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC
C*XSAVE       LAND SFC NET L/W CLEAR SKY FLGC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC
C*XSAVE       LAKES SFC NET L/W CLEAR SKY FLGC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC" >> ic.xsave_ann
        fi

# ------------------------------------ annual HFS

        x="HFS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC SENSIBLE HEAT UP HFS (W/M2)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC SENSIBLE HEAT UP HFS (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS
C*XSAVE       OCN SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS
C*XSAVE       LAND SFC SENSIBLE HEAT UP HFS (W/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS
C*XSAVE       LAKES SFC SENSIBLE HEAT UP HFS (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS" >> ic.xsave_ann
        fi

# ------------------------------------ annual HFL

        x="HFL"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC LATENT HEAT UP HFL (W/M2)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC LATENT HEAT UP HFL (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL
C*XSAVE       OCN SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL
C*XSAVE       LAND SFC LATENT HEAT UP HFL (W/M2)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL
C*XSAVE       LAKES SFC LATENT HEAT UP HFL (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FSD (surface diffuse downward shortwave flux)

        x="FSD"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLB SFC DIFFUSE S/W FLUX FSD (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC DIFFUSE S/W FLUX FSD (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSD" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FSDC

        x="FSDC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLB SFC DIFF.S/W C.SKY FLUX FSDC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC DIFF.S/W C.SKY FLUX FSDC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSDC" >> ic.xsave_ann
        fi

# ------------------------------------ annual CLDT

        x="CLDT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDT             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDT               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDT              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDT             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDT               (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDT              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT" >> ic.xsave_ann
        fi

# ------------------------------------ annual CLDO

        x="CLDO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDO             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDO               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDO              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDO             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDO               (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDO              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global PWAT

        x="PWAT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLOBAL PRECIPITABLE WATER PWAT (KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL PRECIPITABLE WATER PWAT (KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     PWAT" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface CO2 - this is a scalar value in runs with specified CO2 concentrations

        x="CO2"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppm
        echo "C*XLIN        1.E+06" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      CO2" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface CH4 - this is a scalar value

        x="CH4"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppb
        echo "C*XLIN        1.E+09" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CH4 CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CH4 CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      CH4" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface N2O - this is a scalar value

        x="N2O"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppb
        echo "C*XLIN        1.E+09" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE N2O CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE N2O CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      N2O" >> ic.xsave_ann
        fi

# ------------------------------------ annual global burdens of non-CO2 tracers

        i2=1
        while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval trac=\${it$i2}
        x="VI${i2}"
        if [ "$trac" != "CO2" -a -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        if [ "$trac" = "SO2" -o "$trac" = "SO4" -o "$trac" = "DMS" -o "$trac" = "HPO" ] ; then
          echo "C*XFIND       $trac BURDEN GLOBAL (1E6KG-S)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac BURDEN GLOBAL (1E6KG-S)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${i2}" >> ic.xsave_ann
        else
          echo "C*XFIND       $trac BURDEN GLOBAL (1E6KG)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac BURDEN GLOBAL (1E6KG)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${i2}" >> ic.xsave_ann
        fi
        fi
        i2=`expr $i2 + 1`
        done # $ntrac

# ------------------------------------ PLA tracers

        if [ "$PLAtracers" = "on" ] ; then

# ------------------------------------ annual global ammonium sulphate burden (BAS=VI15+VI18+VI21)

        x="BAS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       AMMONIUM SULPHATE BURDEN (1E6KG)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       AMMONIUM SULPHATE BURDEN (1E6KG)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BAS" >> ic.xsave_ann
        fi

# ------------------------------------ organic matter burden (BOA=VI12+VI13+VI16+VI19)

        x="BOA"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       ORGANIC MATTER BURDEN (1E6KG)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ORGANIC MATTER BURDEN (1E6KG)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      BOA" >> ic.xsave_ann
        fi

# ------------------------------------ black carbon burden (BBC=VI11+VI14+VI17+VI20)

        x="BBC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       BLACK CARBON BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       BLACK CARBON BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      BBC" >> ic.xsave_ann
        fi

# ------------------------------------ sea salt burden (BSS=VI07+VI08)

        x="BSS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       SEA SALT BURDEN (1E6KG)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SEA SALT BURDEN (1E6KG)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM      BSS" >> ic.xsave_ann
        fi

# ------------------------------------ mineral dust burden (BMD=VI09+VI10)

        x="BMD"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       MINERAL DUST BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MINERAL DUST BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      BMD" >> ic.xsave_ann
        fi

# ------------------------------------ number burden internally mixed aerosol (BNI=VI21+VI22+VI23+VI24)

        x="BNI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NUMBER BURDEN INTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NUMBER BURDEN INTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      BNI" >> ic.xsave_ann
        fi

# ------------------------------------ number burden externally mixed aerosol (BNE=VI16+VI17+VI18+VI19)

        x="BNE"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NUMBER BURDEN EXTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NUMBER BURDEN EXTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      BNE" >> ic.xsave_ann
        fi

        fi # PLAtracers=on

# ------------------------------------ annual nudging tendencies

# ------------------------------------ heat flux tendency due to difference in SST over open water

        x="TBEG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEG" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SST over water in model, ice in obs

        x="TBEI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEI" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SST over ice in model, water in obs

        x="TBES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBES" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over open water

        x="SBEG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEG" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over water in model, ice in obs

        x="SBEI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEI" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over ice in model, water in obs

        x="SBES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBES" >> ic.xsave_ann
        fi

# ------------------------------------ seaice concentration tendency

        x="TSNN"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       SEAICE CONC TENDENCY DUE TO SICN NH          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SEAICE CONC TENDENCY DUE TO SICN SH          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN NH          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TSNN
C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN SH          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TSNN" >> ic.xsave_ann
        fi

# ------------------------------------ fresh water flux tendency due to difference in SSS

        x="TBWG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       FRESHWATER FLUX TENDENCY DUE TO SSS          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FRESHWATER FLUX TENDENCY DUE TO SSS          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBWG" >> ic.xsave_ann
        fi

# ------------------------------------ top soil layer temperature tendency

        x="TTG1"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l"
        echo "C*XFIND       TG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     TTG1" >> ic.xsave_ann
        fi

# ------------------------------------ top soil layer moisture tendency (volumetric fraction)

        x="TWG1"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l"
        echo "C*XFIND       WG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       WG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     TWG1" >> ic.xsave_ann
        fi

# ------------------------------------ spectral TEMP
        x="TEMP"
        if [ -s gss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES
        x="ES"
        if [ -s ss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL                           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL                           (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP tendency
        x="TMPN"
        if [ -s gss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL TENDENCY                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL TENDENCY                (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES tendency
        x="ESN"
        if [ -s ss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL TENDENCY                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL TENDENCY                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP reference
        x="TMPR"
        if [ -s gss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL REFERENCE               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL REFERENCE               (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES reference
        x="ESR"
        if [ -s ss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL REFERENCE                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL REFERENCE                 (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ Total sulfur emissions (kg S/m2/sec): TSUE = ESFS+EAIS+ESTS+EFIS+ESVC+ESVE
        x="TSUE"
        xs="ESFS EAIS ESTS EFIS ESVC ESVE"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFUR EMISSIONS (KG S/M2/S)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFUR EMISSIONS (KG S/M2/S)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total black carbon emissions (kg/m2/sec): TBCE = ESFB+EAIB+ESTB+EFIB
        x="TBCE"
        xs="ESFB EAIB ESTB EFIB"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL BLACK CARBON EMISSIONS (KG/M2/S)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL BLACK CARBON EMISSIONS (KG/M2/S)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total organic carbon emissions (kg/m2/sec): TOCE = ESFO+EAIO+ESTO+EFIO
        x="TOCE"
        xs="ESFO EAIO ESTO EFIO"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL ORGANIC CARBON EMISSIONS (KG/M2/S)     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL ORGANIC CARBON EMISSIONS (KG/M2/S)     (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total sulfate wet deposition (kg S/m2/sec): TSWD = WDL6+WDD6+WDS6
        x="TSWD"
        xs="WDL6 WDD6 WDS6"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFATE WET DEPOSITION (KG S/M2/S)     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFATE WET DEPOSITION (KG S/M2/S)     (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total sulfate dry deposition (kg S/m2/sec): DD6
        x="DD6"
        if [ -s gs${x}01 ] ; then
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFATE DRY DEPOSITION (KG S/M2/S)     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFATE DRY DEPOSITION (KG S/M2/S)     (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total sulfate in-cloud oxidation (kg S/m2/sec): TSCO = SLO3+SLHP+SDO3+SDHP+SSO3+SSHP
        x="TSCO"
        xs="SLO3 SLHP SDO3 SDHP SSO3 SSHP"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFATE IN-CLOUD OXIDATION (KG S/M2/S) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFATE IN-CLOUD OXIDATION (KG S/M2/S) (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total sulfate clear-sky oxidation (kg S/m2/sec): DOX4
        x="DOX4"
        if [ -s gs${x}01 ] ; then
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL SULFATE CLEAR-SKY OXIDATION (KG S/M2/S)(ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL SULFATE CLEAR-SKY OXIDATION (KG S/M2/S)(ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total black carbon wet deposition (kg/m2/sec): WDBC = WDLB+WDDB+WDSB
        x="WDBC"
        xs="WDLB WDDB WDSB"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL BLACK CARBON WET DEPOSITION (KG/M2/S)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL BLACK CARBON WET DEPOSITION (KG/M2/S)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total black carbon dry deposition (kg/m2/sec): DDB
        x="DDB"
        if [ -s gs${x}01 ] ; then
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL BLACK CARBON DRY DEPOSITION (KG/M2/S)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL BLACK CARBON DRY DEPOSITION (KG/M2/S)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------ Total organic carbon wet deposition (kg/m2/sec): WDOC = WDLO+WDDO+WDSO
        x="WDOC"
        xs="WDLO WDDO WDSO"
        x1=`echo $xs | cut -f1 -d' '`
        if [ -s gs${x1}01 ] ; then
          for f in $xs ; do
            timavg gs${f}01 tgs${f} _ _ gs${f}02 gs${f}03 gs${f}04 gs${f}05 gs${f}06 gs${f}07 gs${f}08 gs${f}09 gs${f}10 gs${f}11 gs${f}12
            globavg tgs${f} _ gtgs${f}
            if [ "$f" = "$x1" ] ; then
              mv gtgs${f} gtgs${x}
            else
              add gtgs${f} gtgs${x}  gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}
            fi
          done
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL ORG. CARBON WET DEPOSITION (KG/M2/S)   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL ORG. CARBON WET DEPOSITION (KG/M2/S)   (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

# ------------------------------------  Total organic carbon dry deposition (kg/m2/sec): DDO
        x="DDO"
        if [ -s gs${x}01 ] ; then
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
          rtdlist_ann="$rtdlist_ann gtgs${x}"
          echo "C*XFIND       TOTAL ORG. CARBON DRY DEPOSITION (KG/M2/S)   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TOTAL ORG. CARBON DRY DEPOSITION (KG/M2/S)   (ANN ${year_rtdiag_start}-${year})
NEWNAM     $x" >> ic.xsave_ann
        fi

        fi # $m=12
      fi # PhysA=on

# #################################### Atmospheric Carbon section

      if [ "$CarbA" = "on" ] ; then

# ************************************ Atmospheric Carbon annual section

        if [ ${m} -eq 01 ] ; then

# ------------------------------------ global CO2 beginning of year

        if [ -n "$iCO2" ] ; then
        x="VI${iCO2}"
#                                      CO2 burden at the beginning of a year
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}${m} gs${x}${m}.day1
        globavg gs${x}${m}.day1 _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}day1

        rtdlist_ann="$rtdlist_ann gtgs${x}day1"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN JAN 1 (PG C)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN JAN 1 (PG C)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${iCO2}" >> ic.xsave_ann
        fi
        fi # ${m} -eq 1

# ------------------------------------ monthly global surface CO2 MMR

        x="XL${iCO2}"
        timavg gs${x}${m} mgs${x}${m}
        globavg mgs${x}${m} _ gmgs${x}${m}
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gmgs${x}${m} gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ global CO2 burden end of year (Dec 31)

        if [ -n "$iCO2" ] ; then
        x="VI${iCO2}"
#                                      CO2 burden at the end of a year
        echo "C* RCOPY          31        31" | ccc rcopy gs${x}${m} gs${x}${m}.last
        globavg gs${x}${m}.last _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN DEC31 (PG C)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN DEC31 (PG C)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${iCO2}" >> ic.xsave_ann

# ------------------------------------ annual global CO2 surface flux

        x="XF${iCO2}"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        globavg tgs${x} _ gtavg
#                                      convert CO2 FLUX from Kg CO2 m-2 sec-1 to Pg C/year
#                                      x 365 days x 86400 sec x 510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 4390152482
        echo "C*XLIN    4390152482" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       CO2 FLUX ENTERING ATMOS.(PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CO2 FLUX ENTERING ATMOS.(PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     XF${iCO2}" >> ic.xsave_ann

# ------------------------------------ annual global surface CO2 MMR->PPMV

        x="XL${iCO2}"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        globavg tgs${x} _ gtavg
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave_ann

# ------------------------------------ annual max/min global surface CO2 MMR (for monthly means)

        x="XL${iCO2}"
        timmax gmgs${x}01 gtmax gmgs${x}02 gmgs${x}03 gmgs${x}04 gmgs${x}05 gmgs${x}06 gmgs${x}07 gmgs${x}08 gmgs${x}09 gmgs${x}10 gmgs${x}11 gmgs${x}12
        timmin gmgs${x}01 gtmin gmgs${x}02 gmgs${x}03 gmgs${x}04 gmgs${x}05 gmgs${x}06 gmgs${x}07 gmgs${x}08 gmgs${x}09 gmgs${x}10 gmgs${x}11 gmgs${x}12
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gtmax gtgs${x}max
        echo "C*XLIN    658409.076" | ccc xlin gtmin gtgs${x}min

        rtdlist_ann="$rtdlist_ann gtgs${x}max gtgs${x}min"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (MAX ${year_rtdiag_start}-${yearm1})
C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (MIN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (MAX ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}
C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (MIN ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave_ann

        fi # $iCO2

# ------------------------------------ annual global ECO2

        x="ECO2"
#                                      find annual mean and global average
        timavg cm${x}01 tgs${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tgs${x} _ gtavg
#                                      convert ECO2 from Kg CO2 m-2 sec-1 to Pg C/year
#                                      x 365 days x 86400 sec x 510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 4390152482.
        echo "C*XLIN    4390152482" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       ANTHROPOGENIC EMISSIONS (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ANTHROPOGENIC EMISSIONS (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     ECO2" >> ic.xsave_ann
        fi # $m=12
      fi # CarbA=on

# #################################### Physical Ocean section

      if [ "$PhysO" = "on" ] ; then

# ------------------------------------ monthly global upper layer TEMP

        x="TEMP"
        echo "  RCOPY            1         1" | ccc rcopy gz${x}${m} gz${x}1${m}
        globavw gz${x}1${m} bda1    _ _ ggz${x}1${m}
        globavw gz${x}1${m} bdatro1 _ _ ggz${x}tro1${m}
        globavw gz${x}1${m} bdanhe1 _ _ ggz${x}nhe1${m}
        globavw gz${x}1${m} bdashe1 _ _ ggz${x}she1${m}

        rtdlist="$rtdlist ggz${x}1${m} ggz${x}tro1${m} ggz${x}nhe1${m} ggz${x}she1${m}"
        echo "C*XFIND       OCEAN SFC TEMPERATURE (K)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30S-30N (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30N-90N (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30S-90S (K)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SFC TEMPERATURE (K)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30S-30N (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30N-90N (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30S-90S (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave

# ------------------------------------ NINO indices
        globavw gz${x}1${m} bda_nino3  _ _ ggz${x}nino3${m}
        globavw gz${x}1${m} bda_nino4  _ _ ggz${x}nino4${m}
        globavw gz${x}1${m} bda_nino34 _ _ ggz${x}nino34${m}
        globavw gz${x}1${m} bda_nino12 _ _ ggz${x}nino12${m}

        rtdlist="$rtdlist ggz${x}nino3${m} ggz${x}nino4${m} ggz${x}nino34${m} ggz${x}nino12${m}"
        echo "C*XFIND       OCEAN SFC TEMPERATURE NINO3 (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO4 (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO34 (K)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO12 (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SFC TEMPERATURE NINO3 (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO4 (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO34 (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO12 (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave

# ------------------------------------ monthly global upper layer SALT

        x="SALT"
        echo "  RCOPY            1         1" | ccc rcopy gz${x}${m} gz${x}1${m}
        globavw gz${x}1${m} bda1 _ _ ggz${x}1${m}
#                                      convert to psu
        echo "C*XLIN         1000." | ccc xlin ggz${x}1${m} ggz${x}1${m}.1 ; mv ggz${x}1${m}.1 ggz${x}1${m}

        rtdlist="$rtdlist ggz${x}1${m}"
        echo "C*XFIND       OCEAN SFC SALINITY (PSU)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SFC SALINITY (PSU)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave

# ************************************ Physical Ocean annual section

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ annual global top layer TEMP

        x="TEMP"
        avemongztoann gz${x}101 gz${x}102 gz${x}103 gz${x}104 gz${x}105 gz${x}106 gz${x}107 gz${x}108 gz${x}109 gz${x}110 gz${x}111 gz${x}112 gzann
        globavw gzann bda1    _ _ gtgz${x}1
        globavw gzann bdatro1 _ _ gtgz${x}tro1
        globavw gzann bdanhe1 _ _ gtgz${x}nhe1
        globavw gzann bdashe1 _ _ gtgz${x}she1

        rtdlist_ann="$rtdlist_ann gtgz${x}1 gtgz${x}tro1 gtgz${x}nhe1 gtgz${x}she1"
        echo "C*XFIND       OCEAN SFC TEMPERATURE (K)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30S-30N (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30N-90N (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30S-90S (K)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SFC TEMPERATURE (K)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30S-30N (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30N-90N (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30S-90S (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann

# ------------------------------------ NINO indices

        globavw gzann bda_nino3  _ _ ggz${x}nino3
        globavw gzann bda_nino4  _ _ ggz${x}nino4
        globavw gzann bda_nino34 _ _ ggz${x}nino34
        globavw gzann bda_nino12 _ _ ggz${x}nino12

        rtdlist_ann="$rtdlist_ann ggz${x}nino3 ggz${x}nino4 ggz${x}nino34 ggz${x}nino12"
        echo "C*XFIND       OCEAN SFC TEMPERATURE NINO3 (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO4 (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO34 (K)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO12 (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SFC TEMPERATURE NINO3 (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO4 (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO34 (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO12 (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann

# ------------------------------------ annual vertically integrated mean temperature

        x="TEMP"
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*T
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      vertically integrated global mean temperature
        globavw gzvi da1 _ _ ggzvi
        div ggzvi gbetaovi gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       OCEAN TEMPERATURE GLOBAL (K)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN TEMPERATURE GLOBAL (K)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann

# ------------------------------------ annual upper ocean vertically integrated mean temperature

        x="TEMP"
        for depth in $upper_ocean_depths ; do
        depth10=${depth}0
        depth4=`echo $depth | awk '{printf "%4i", $1}'`
        echo "C*  VZINTV         0${depth10}" | ccc vzintv gzb dz gzvi${depth}
#                                      vertically integrated global mean ocean upper layers temperature
        globavw gzvi${depth} da1 _ _ ggzvi${depth}
        div ggzvi${depth} gbetaovi${depth} gtgz${x}vi${depth}

        rtdlist_ann="$rtdlist_ann gtgz${x}vi${depth}"
        echo "C*XFIND       UPPER ${depth4}M OCEAN TEMPERATURE (K)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       UPPER ${depth4}M OCEAN TEMPERATURE (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
        done

# ------------------------------------ annual global mean ocean temperature at various depths (every 5th level)

        x="TEMP"
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy betao betao$lev
        mlt betao$lev da bda$lev
        rm betao$lev
#                                      global mean ocean temperature
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       OCEAN TEMPERATURE${LEV5}M (K)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN TEMPERATURE${LEV5}M (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done

# ------------------------------------ annual global top layer SALT

        x="SALT"
        avemongztoann gz${x}101 gz${x}102 gz${x}103 gz${x}104 gz${x}105 gz${x}106 gz${x}107 gz${x}108 gz${x}109 gz${x}110 gz${x}111 gz${x}112 gzann
        globavw gzann bda1 _ _ gtgz${x}1
#                                      convert to psu
        echo "C*XLIN         1000." | ccc xlin gtgz${x}1 gtgz${x}1.1 ; mv gtgz${x}1.1 gtgz${x}1

        rtdlist_ann="$rtdlist_ann gtgz${x}1"
        echo "C*XFIND       OCEAN SFC SALINITY (PSU)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SFC SALINITY (PSU)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave_ann

# ------------------------------------ annual vertically integrated mean salinity

        x="SALT"
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*S
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      vertically integrated global mean salinity
        globavw gzvi da1 _ _ ggzvi
        div ggzvi gbetaovi gtgz${x}vi
#                                      convert to psu
        echo "C*XLIN         1000.      0.E0 SALT" | ccc xlin gtgz${x}vi gtgz${x}vi.1 ; mv gtgz${x}vi.1 gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       OCEAN SALINITY GLOBAL (PSU)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SALINITY GLOBAL (PSU)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave_ann

# ------------------------------------ annual upper ocean vertically integrated mean salinity

        x="SALT"
        for depth in $upper_ocean_depths ; do
        depth10=${depth}0
        depth4=`echo $depth | awk '{printf "%4i", $1}'`
        echo "C*  VZINTV         0${depth10}" | ccc vzintv gzb dz gzvi${depth}
#                                      vertically integrated global mean salinity upper layers
        globavw gzvi${depth} da1 _ _ ggzvi${depth}
        div ggzvi${depth} gbetaovi${depth} gtgz${x}vi${depth}
#                                      convert to psu
        echo "C*XLIN         1000.      0.E0 SALT" | ccc xlin gtgz${x}vi${depth} gtgz${x}vi${depth}.1 ; mv gtgz${x}vi${depth}.1 gtgz${x}vi${depth}

        rtdlist_ann="$rtdlist_ann gtgz${x}vi${depth}"
        echo "C*XFIND       UPPER ${depth4}M OCEAN SALINITY (PSU)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       UPPER ${depth4}M OCEAN SALINITY (PSU)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave_ann
        done

# ------------------------------------ annual global mean salinity at various depths

        x="SALT"
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean salinity
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert to psu
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       OCEAN SALINITY${LEV5}M (PSU)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SALINITY${LEV5}M (PSU)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done

# ------------------------------------ annual global ocean OBWA (unit: kg/m2/day or mm/day)
#                                      averaged over ocean grid points

        x="OBWA"
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ gcm${x}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gcm${x} gcm${x}.1 ; mv gcm${x}.1 gcm${x}
#
        rtdlist_ann="$rtdlist_ann gcm${x}"
        echo "C*XFIND       FRESHWATER INTO OCEAN (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FRESHWATER INTO OCEAN (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWA" >> ic.xsave_ann

# ------------------------------------ annual global ocean OBET

        x="OBET"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ gcm${x}

        rtdlist_ann="$rtdlist_ann gcm${x}"
        echo "C*XFIND       NET SFC ENERGY OCEAN OBET (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OCEAN OBET (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBET" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ocean OBEI

        x="OBEI"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ gcm${x}

        rtdlist_ann="$rtdlist_ann gcm${x}"
        echo "C*XFIND       NET SFC ENERGY OCEAN OBEI (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OCEAN OBEI (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ocean OBEX=OBET+OBEI

        x="OBEX"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ gcm${x}

        rtdlist_ann="$rtdlist_ann gcm${x}"
        echo "C*XFIND       NET SFC ENERGY OCEAN OBEX CM (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OCEAN OBEX CM (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEX" >> ic.xsave_ann
        fi

# ------------------------------------ Streamfunction calculation

        for x in "PSIB" "V" "VISO" ; do
          avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gz${x}ann
        done
        joinup gzann gz_data_description gzPSIBann gzVann gzVISOann
        otransp gzann gzstat psiouta psioutg

# MXAO: north atlantic maximum streamfunction psi
# PSDP: Drake Passage transport (in Sverdrups)
# PSIN: Indonesian passage transport (in Sverdrups)
# PSBS: Bering Strait transport (in Sverdrups)
# PSGH: Cape of Good Hope transport (in Sverdrups)

        gzvars_sf="MXAO PSDP PSIN PSBS PSGH"
        GZVARS_SF=`fmtselname $gzvars_sf`
        echo "C*SELECT   STEP         0 999999999    1         099999 NAME$GZVARS_SF" | ccc select gzstat $gzvars_sf

        for x in $gzvars_sf ; do
          mv $x gz${x}
          rtdlist_ann="$rtdlist_ann gz$x"
        done
        echo "C*XFIND       MAX MERIDIONAL PSI NATL (SV)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       DRAKE PASSAGE TRANSPORT (SV)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       INDONESIAN PASSAGE TRANSPORT(SV)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       BERING STRAIT TRANSPORT (SV)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CAPE OF GOOD HOPE TRANSPORT (SV)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX MERIDIONAL PSI NATL (SV)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     MXAO
C*XSAVE       DRAKE PASSAGE TRANSPORT (SV)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PSDP
C*XSAVE       INDONESIAN PASSAGE TRANSPORT(SV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     PSIN
C*XSAVE       BERING STRAIT TRANSPORT (SV)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PSBS
C*XSAVE       CAPE OF GOOD HOPE TRANSPORT (SV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     PSGH" >> ic.xsave_ann

# ------------------------------------ North Atlantic overturning circulation indices

#                                      colocate meridional velocity
        colocat gzVann gz_data_description betao vvelcol

#                                      determine Atlantic overturning streamfunction
        gmlt_hr betao atlreg abetao
        echo "  MERPSI      1   -65.0     -35.0" | ccc merpsi vvelcol abetao dx dz lat psio psiplt

        gmlt_hr psio zlat25to26 psio25to26
        gmlt_hr psio zlat47to48 psio47to48

        echo "C*RMAXN   $gznlev" | ccc rmaxn psio       maxpsio
        echo "C*RMAXN   $gznlev" | ccc rmaxn psio25to26 maxpsio25to26
        echo "C*RMAXN   $gznlev" | ccc rmaxn psio47to48 maxpsio47to48
        ggatim maxpsio       gzMAXV   input=ic.ggatim
        ggatim maxpsio25to26 gzMAXV26 input=ic.ggatim
        ggatim maxpsio47to48 gzMAXV48 input=ic.ggatim
        echo "               1.E-6" | ccc xlin gzMAXV   gzMAXVsv
        echo "               1.E-6" | ccc xlin gzMAXV26 gzMAXV26sv
        echo "               1.E-6" | ccc xlin gzMAXV48 gzMAXV48sv

        rtdlist_ann="$rtdlist_ann gzMAXVsv gzMAXV26sv gzMAXV48sv"
        echo "C*XFIND       MAX MERIDIONAL V PSI NATL (SV)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX MERIDIONAL V PSI NATL 26N(SV)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX MERIDIONAL V PSI NATL 48N(SV)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX MERIDIONAL V PSI NATL (SV)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     MXAV
C*XSAVE       MAX MERIDIONAL V PSI NATL 26N(SV)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AV26
C*XSAVE       MAX MERIDIONAL V PSI NATL 48N(SV)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AV48" >> ic.xsave_ann
        fi # $m=12
      fi # PhysO=on

# #################################### Ocean Carbon section

      if [ "$CarbO" = "on" ] ; then

# ------------------------------------ monthly SWMX

        x="SWMX"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg${x}
#                                      global
        globavg tavg${x} _ gtcm${x}${m}g
#                                      ocean
        globavg tavg${x} _ gtcm${x}${m}o ocean_frac

        rtdlist="$rtdlist gtcm${x}${m}g gtcm${x}${m}o"
        echo "C*XFIND       SURFACE WIND SPEED GLOBAL (M/S)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SURFACE WIND SPEED OCEAN (M/S)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SURFACE WIND SPEED GLOBAL (M/S)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     SWMX
C*XSAVE       SURFACE WIND SPEED OCEAN (M/S)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     SWMX" >> ic.xsave
        fi

# ------------------------------------ monthly OFSG

        x="OFSG"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg${x}
#                                      ocean
        globavg tavg${x} _ gtcm${x}${m}o ocean_frac

        rtdlist="$rtdlist gtcm${x}${m}o"
        echo "C*XFIND       INSOLATION OCEAN (W/M2)                      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       INSOLATION OCEAN (W/M2)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM     OFSG" >> ic.xsave
        fi

# ------------------------------------ monthly global PMSL

        x="PMSL"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg${x}
#                                      global
        globavg tavg${x} _ gtcm${x}${m}g
#                                      ocean
        globavg tavg${x} _ gtcm${x}${m}o ocean_frac

        rtdlist="$rtdlist gtcm${x}${m}g gtcm${x}${m}o"
        echo "C*XFIND       SEA-LEVEL PRESSURE GLOBAL (MB)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SEA-LEVEL PRESSURE OCEAN (MB)                (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SEA-LEVEL PRESSURE GLOBAL (MB)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PMSL
C*XSAVE       SEA-LEVEL PRESSURE OCEAN (MB)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     PMSL" >> ic.xsave
        fi

# ************************************ Ocean Carbon annual section

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ annual SWMX

        x="SWMX"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
#                                      global
        globavg tavg${x} _ gtcm${x}g
#                                      ocean
        globavg tavg${x} _ gtcm${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtcm${x}g gtcm${x}o"
        echo "C*XFIND       SURFACE WIND SPEED GLOBAL (M/S)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SURFACE WIND SPEED OCEAN (M/S)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE WIND SPEED GLOBAL (M/S)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     SWMX
C*XSAVE       SURFACE WIND SPEED OCEAN (M/S)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     SWMX" >> ic.xsave_ann
        fi

# ------------------------------------ annual OFSG

        x="OFSG"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
#                                      ocean
        globavg tavg${x} _ gtcm${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtcm${x}o"
        echo "C*XFIND       INSOLATION OCEAN (W/M2)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       INSOLATION OCEAN (W/M2)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFSG" >> ic.xsave_ann
        fi

# ------------------------------------ annual PMSL

        x="PMSL"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
#                                      global
        globavg tavg${x} _ gtcm${x}g
#                                      ocean
        globavg tavg${x} _ gtcm${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtcm${x}g gtcm${x}o"
        echo "C*XFIND       SEA-LEVEL PRESSURE GLOBAL (MB)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SEA-LEVEL PRESSURE OCEAN (MB)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SEA-LEVEL PRESSURE GLOBAL (MB)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PMSL
C*XSAVE       SEA-LEVEL PRESSURE OCEAN (MB)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     PMSL" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FCOO

        x="FCOO"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global integral
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ _ gtint
#                                      convert mol C sec-1 to Pg C/year
#                                      x 365 day * 86400 sec x 12.011 / 10^15 = 3.7878E-7
#                                      x -1 to make it +ve downward
        echo "C*XLIN    -3.7878E-7" | ccc xlin gtint gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       OCEAN-ATMOS CO2 FLUX (PG C/YR)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN-ATMOS CO2 FLUX (PG C/YR)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FCOO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OPCO

        x="OPCO"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       SURFACE OCEAN PCO2 (PPMV)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE OCEAN PCO2 (PPMV)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     OPCO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ODPC

        x="ODPC"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       OCEAN-ATMOS DELTA-PCO2 (PPMV)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN-ATMOS DELTA-PCO2 (PPMV)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODPC" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OVIC

        x="OVIC"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol C sec-1 to Pg C/year
#                                      x 365 day * 86400 sec x 12.011 / 10^15 = 3.7878E-07
#                                      x -1 to make it +ve downward
        echo "C*XLIN    -3.7878E-7" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       DIC VIRTUAL FLUX (PG C/YR)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DIC VIRTUAL FLUX (PG C/YR)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     OVIC" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFNP

        x="OFNP"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol N yr-1 to Pg C/year
#                                      6.625 (C/N ratio in CMOC) x 12.011 / 10^15 = 7.9573E-14
        echo "C*XLIN    7.9573E-14" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       PRIMARY PRODUCTION (PG C/YR)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PRIMARY PRODUCTION (PG C/YR)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFNP" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFXO

        x="OFXO"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol N yr-1 to Pg C/year
#                                      6.625 (C/N ratio in CMOC) x 12.011 / 10^15 = 7.9573E-14
        echo "C*XLIN    7.9573E-14" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       EXPORT FLUX ORGANIC (PG C/YR)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EXPORT FLUX ORGANIC (PG C/YR)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFXO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFXI

        x="OFXI"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol C yr-1 to Pg C/year
#                                      12.011 / 10^15 = 12.E-15
        echo "C*XLIN    12.011E-15" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       EXPORT FLUX INORGANIC (PG C/YR)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EXPORT FLUX INORGANIC (PG C/YR)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFXI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFNF

        x="OFNF"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol N yr-1 to Tg N/year
#                                      14.0065 / 10^12 = 14.0065E-12
        echo "C*XLIN    14.007E-12" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       N2 FIXATION (TG N/YR)                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       N2 FIXATION (TG N/YR)                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFNF" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFSO

        x="OFSO"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol N yr-1 to Pg C/year
#                                      6.625 (C/N ratio in CMOC) x 12.011 / 10^15 = 7.9573E-14
        echo "C*XLIN    7.9573E-14" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       ORGANIC FLUX TO SEDS (PG C/YR)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ORGANIC FLUX TO SEDS (PG C/YR)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFSO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFSI

        x="OFSI"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol C yr-1 to Pg C/year
#                                      12.011 / 10^15 = 12.011E-15
        echo "C*XLIN    12.011E-15" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       INORGANIC FLUX TO SEDS (PG C/YR)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       INORGANIC FLUX TO SEDS (PG C/YR)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFSI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global average OPH

        x="OPH"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       SURFACE OCEAN PH                             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE OCEAN PH                             (ANN ${year_rtdiag_start}-${year})
NEWNAM      OPH" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ONIT at various levels

        x="ONIT"
        if [ ! -s gz${x}01 ] ; then
#                                      try an older name
          x="BNI"
        fi
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       NITRATE GLOBAL (MOL)                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NITRATE GLOBAL (MOL)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     ONIT" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       NITRATE${LEV5}M (MMOL/M3)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NITRATE${LEV5}M (MMOL/M3)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     ONIT" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global ODIC at various levels

        x="ODIC"
        if [ ! -s gz${x}01 ] ; then
#                                      try an older name
          x="DIC"
        fi
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       DISS.INORGANIC CARBON GLOBAL (MOL)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DISS.INORGANIC CARBON GLOBAL (MOL)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODIC" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       DISS.INORGANIC CARBON${LEV5}M (MMOL/M3)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DISS.INORGANIC CARBON${LEV5}M (MMOL/M3)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODIC" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global OALK at various levels

        x="OALK"
        if [ ! -s gz${x}01 ] ; then
#                                      try an older name
          x="ALK"
        fi
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       ALKALINITY GLOBAL (MOL)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ALKALINITY GLOBAL (MOL)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     OALK" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       ALKALINITY${LEV5}M (MMOL/M3)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ALKALINITY${LEV5}M (MMOL/M3)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     OALK" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global OPHY at various levels

        x="OPHY"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       OPHY CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OPHY CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     OPHY" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev || true
        if [ ! -s gzann$lev ] ; then
          break
        fi
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       OPHY CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OPHY CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     OPHY" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global OZOO at various levels

        x="OZOO"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       OZOO CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OZOO CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     OZOO" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev || true
        if [ ! -s gzann$lev ] ; then
          break
        fi
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       OZOO CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OZOO CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     OZOO" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global ODET at various levels

        x="ODET"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       ODET CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ODET CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODET" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       ODET CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ODET CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODET" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi
        fi # $m=12
      fi # CarbO=on

# #################################### Land Carbon section

      if [ "$CarbL" = "on" ] ; then

# ************************************ Land Carbon monthly section

# ------------------------------------ monthly averaged global wetland area calculated from wetland fraction

        x="WFRA"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtavg

#                                      convert to km2
        gmlt gtavg globe_area_km2 gtcm${x}${m}

        rtdlist="$rtdlist gtcm${x}${m}"
        echo "C*XFIND       DYNAMIC WETLAND AREA (KM2)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       DYNAMIC WETLAND AREA (KM2)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WFRA" >> ic.xsave
        fi

# ------------------------------------ monthly averaged global wetland emissions 1 - CH4H

        x="CH4H"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/month
#                                      1.0377504 x $lmon x $globe_area_km2 x 10^6 x (16/12) / 10^12
#                                      1.0377504= 12.011 x 86400 / 10^6 converts u-mol CO2-C m-2 sec-1 to g C/m2.day
#                                      $lmon is month length (defined at the beginning of the month loop)
#                                      $globe_area_km2 is global area in km^2 (defined previously)
#                                      16/12 converts C to CH4

        factor_ch4_mon=`echo $lmon ${globe_area_km2} | awk '{printf "%10.4f",12.011*86400*$1*$2*(16/12)/10^12}'`
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gtcm${x}${m} ggtcm${x}${m}

        rtdlist="$rtdlist ggtcm${x}${m}"
        echo "C*XFIND       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CH4H" >> ic.xsave
        fi

# ------------------------------------ monthly averaged global wetland emissions 2 - CH4N

        x="CH4N"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gtcm${x}${m} ggtcm${x}${m}

        rtdlist="$rtdlist ggtcm${x}${m}"
        echo "C*XFIND       NPP WETLAND EMIS SPEC WETLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NPP WETLAND EMIS SPEC WETLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CH4N" >> ic.xsave
        fi

# ------------------------------------ monthly averaged dynamic global wetland emissions 1 - CW1D

        x="CW1D"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg C/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gtcm${x}${m} ggtcm${x}${m}

        rtdlist="$rtdlist ggtcm${x}${m}"
        echo "C*XFIND       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CW1D" >> ic.xsave
        fi

# ------------------------------------ monthly averaged dynamic global wetland emissions 2 - CW2D

        x="CW2D"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg C/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gtcm${x}${m} ggtcm${x}${m}

        rtdlist="$rtdlist ggtcm${x}${m}"
        echo "C*XFIND       NPP WETLAND EMIS DYN WETLAND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NPP WETLAND EMIS DYN WETLAND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CW2D" >> ic.xsave
        fi

# ------------------------------------ monthly averaged leaf area index CLAI

        x="CLAI"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}    tland_frac
        globavg tavg _ gtcm${x}nhe${m} tland_frac_nhe
        globavg tavg _ gtcm${x}she${m} tland_frac_she
        globavg tavg _ gtcm${x}tro${m} tland_frac_tro

        rtdlist="$rtdlist gtcm${x}${m} gtcm${x}nhe${m} gtcm${x}she${m} gtcm${x}tro${m}"
        echo "C*XFIND       LEAF AREA INDEX                              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX NORTH OF 30N                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX SOUTH OF 30S                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX 30S-30N                      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LEAF AREA INDEX                              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX NORTH OF 30N                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX SOUTH OF 30S                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX 30S-30N                      (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI" >> ic.xsave
        fi

# ************************************ Land Carbon annual section

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ CVEG year end vegetation

        x="CVEG"
        echo "C*RCOPY           31        31" | ccc rcopy cm${x}${m} tavg${x}
        globavg tavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6(km2->m2) / 10^12=510.099699
        factor_kg2pgc=`echo ${globe_area_km2} | awk '{printf "%10.6f",$1/10^6}'`
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       VEGETATION BIOMASS (PG C)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VEGETATION BIOMASS (PG C)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     CVEG" >> ic.xsave_ann

# ------------------------------------ CDEB year end litter

        x="CDEB"
        echo "C*RCOPY           31        31" | ccc rcopy cm${x}${m} tavg${x}
        globavg tavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6 (km2->m2) / 10^12
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LITTER MASS (PG C)                           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER MASS (PG C)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     CDEB" >> ic.xsave_ann

# ------------------------------------ CHUM year end soil C

        x="CHUM"
        echo "C*RCOPY           31        31" | ccc rcopy cm${x}${m} tavg${x}
        globavg tavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6 (km2->m2) / 10^12
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       SOIL CARBON MASS (PG C)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL CARBON MASS (PG C)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CHUM" >> ic.xsave_ann

# ------------------------------------ annual global CBRN

        x="CBRN"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
#       treat CBRN differently. I need to find global burned area.
#       multiply average annual area by 365 to get annual area burned
        echo "C*XLIN         365.0" | ccc xlin tavg${x} annual_burn_area

#       get area of grid cells
        if [ -z "$nlat" -o -z "$lonsl" ] ; then
          echo "Error: **** CarbL: nlat or lonsl are undefined. ****"
          exit 1
        fi
        echo "C*MKWGHT  $nlat    1$lonsl    1$nlat  1 0    0" | ccc mkwght grid_cell_area_fraction

        div annual_burn_area grid_cell_area_fraction fractional_area_burned
        globavg fractional_area_burned _ gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       GLOBAL BURNED AREA (KM2/YEAR)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL BURNED AREA (KM2/YEAR)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     CBRN" >> ic.xsave_ann

# ------------------------------------ annual global CFNP

        x="CFNP"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
#                                      x 12.011 / 10^6 to convert u-mol CO2-C to g C
#                                      x 365 x 86400  for 1/sec to 1/year,
#                                      x 510099699km2 x 10^6 (km2->m2) for global total in g C
#                                    / 10^15 to convert g to Pg C = 193.215001
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NET PRIMARY PRODUCTIVITY (PG C/YR)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET PRIMARY PRODUCTIVITY (PG C/YR)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNP" >> ic.xsave_ann

# ------------------------------------ annual global CFNE

        x="CFNE"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NET ECOSYSTEM PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET ECOSYSTEM PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNE" >> ic.xsave_ann

# ------------------------------------ annual global CFRV

        x="CFRV"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       AUTOTROPHIC RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       AUTOTROPHIC RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRV" >> ic.xsave_ann

# ------------------------------------ annual global CFGP

        x="CFGP"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       GROSS PRIMARY PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GROSS PRIMARY PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFGP" >> ic.xsave_ann

# ------------------------------------ annual global CFNB

        x="CFNB"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NET BIOME PRODUCTIVITY (PG C/YR)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET BIOME PRODUCTIVITY (PG C/YR)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNB" >> ic.xsave_ann

# ------------------------------------ annual global CFLV

        x="CFLV"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LUC VEG. COMBUST. EMISSION (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC VEG. COMBUST. EMISSION (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLV" >> ic.xsave_ann

# ------------------------------------ annual global CFLD

        x="CFLD"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LUC LITTER INPUTS (PG C/YR)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC LITTER INPUTS (PG C/YR)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLD" >> ic.xsave_ann

# ------------------------------------ annual global CFLH

        x="CFLH"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LUC SOIL CARBON INPUTS (PG C/YR)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC SOIL CARBON INPUTS (PG C/YR)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLH" >> ic.xsave_ann

# ------------------------------------ annual global CFRH

        x="CFRH"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       SOIL CARBON RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL CARBON RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRH" >> ic.xsave_ann

# ------------------------------------ annual global CFHT

        x="CFHT"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LITTER TO SOIL C TRANSFER (PG C/YR)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER TO SOIL C TRANSFER (PG C/YR)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFHT" >> ic.xsave_ann

# ------------------------------------ annual global CFLF

        x="CFLF"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LITTER FALL (PG C/YR)                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER FALL (PG C/YR)                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLF" >> ic.xsave_ann

# ------------------------------------ annual global CFRD

        x="CFRD"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LITTER RESPIRATION (PG C/YR)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER RESPIRATION (PG C/YR)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRD" >> ic.xsave_ann

# ------------------------------------ annual global CFFD

        x="CFFD"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       FIRE EMISSION FROM LITTER (PG C/YR)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FIRE EMISSION FROM LITTER (PG C/YR)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFFD" >> ic.xsave_ann

# ------------------------------------ annual global CFFV

        x="CFFV"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       FIRE EMISSION FROM VEGETATION (PG C/YR)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FIRE EMISSION FROM VEGETATION (PG C/YR)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFFV" >> ic.xsave_ann

# ------------------------------------ annual averaged global wetland area calculated from wetland fraction

        x="WFRA"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg

#                                      convert to km2
        gmlt gtavg globe_area_km2 gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       DYNAMIC WETLAND AREA (KM2)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DYNAMIC WETLAND AREA (KM2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     WFRA" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CH4H

        x="CH4H"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
#                                      1.0377504 x 365 x $globe_area_km2 x 10^6 x (16/12) / 10^12 = 257620.001
#                                      1.0377504= 12.011 x 86400 / 10^6 converts u-mol CO2-C m-2 sec-1 to g C/m2.day
#                                      16/12 converts C to CH4

        factor_ch4_ann=`echo 365 ${globe_area_km2} | awk '{printf "%10.3f",12.011*86400*$1*$2*(16/12)/10^12}'`
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CH4H" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CH4N

        x="CH4N"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NPP WETLND EMIS SPEC WETLAND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NPP WETLND EMIS SPEC WETLAND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CH4N" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CW1D

        x="CW1D"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CW1D" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CW2D

        x="CW2D"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NPP WETLND EMIS DYN WETLAND AREA (TG CH4/Y)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NPP WETLND EMIS DYN WETLAND AREA (TG CH4/Y)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     CW2D" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CLAI

        x="CLAI"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtcm${x}    tland_frac
        globavg tavg _ gtcm${x}nhe tland_frac_nhe
        globavg tavg _ gtcm${x}she tland_frac_she
        globavg tavg _ gtcm${x}tro tland_frac_tro

        rtdlist_ann="$rtdlist_ann gtcm$x gtcm${x}nhe gtcm${x}she gtcm${x}tro"
        echo "C*XFIND       LEAF AREA INDEX                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX NORTH OF 30N                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX SOUTH OF 30S                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX 30S-30N                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LEAF AREA INDEX                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX NORTH OF 30N                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX SOUTH OF 30S                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX 30S-30N                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI" >> ic.xsave_ann
        fi

        fi # m=12
      fi # CarbL=on

# ------------------------------------ add the number of seconds since January 1, 1970 (CUT)

      if [ ${m} -eq 12 ] ; then
      x="SECS"
      datesec=`date +%s`
      echo " TIME      1001 SECS         0         1         1    100101        -1
$datesec.E00" > datesec.txt
      chabin datesec.txt gtgs$x
      rtdlist_ann="$rtdlist_ann gtgs$x"
      echo "C*XFIND       DATE IN SECONDS SINCE 1970                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
      echo "C*XSAVE       DATE IN SECONDS SINCE 1970                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SECS" >> ic.xsave_ann
      fi # $m=12

# ------------------------------------ add ocean/land mask area fractions

      if [ ${m} -eq 12 ] ; then
      rtdlist_ann="$rtdlist_ann globe_area tland_area tland_area_nh tland_area_sh tland_area_nhe tland_area_she tland_area_tro ocean_area ocean_area_nh ocean_area_sh rlake_area water_area nogla_area glaci_area tland_favg tland_favg_nh tland_favg_sh tland_favg_nhe tland_favg_she tland_favg_tro ocean_favg ocean_favg_nh ocean_favg_sh rlake_favg water_favg nogla_favg glaci_favg"
      echo "C*XFIND       GLOBAL AREA (M2)                             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA (M2)                               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA NH (M2)                            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA SH (M2)                            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA NHE (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA SHE (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA TRO (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA (M2)                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA NH (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA SH (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES AREA (M2)                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       WATER AREA (M2)                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS AREA (M2)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLACIERS AREA (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION NH                        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION SH                        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION NHE                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION SHE                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION TRO                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION NH                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION SH                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES AREA FRACTION                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       WATER AREA FRACTION                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS AREA FRACTION              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLACIERS AREA FRACTION                       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
      echo "C*XSAVE       GLOBAL AREA (M2)                             (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA (M2)                               (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA NH (M2)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA SH (M2)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA NHE (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA SHE (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA TRO (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA (M2)                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA NH (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA SH (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAKES AREA (M2)                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       WATER AREA (M2)                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND W/O GLACIERS AREA (M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       GLACIERS AREA (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA FRACTION                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION NH                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION SH                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION NHE                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION SHE                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION TRO                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION NH                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION SH                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAKES AREA FRACTION                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       WATER AREA FRACTION                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND W/O GLACIERS AREA FRACTION              (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       GLACIERS AREA FRACTION                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC" >> ic.xsave_ann

      if [ "$PhysO" = "on" ] ; then
      rtdlist_ann="$rtdlist_ann gbetaovi gbetaovol"
      echo "C*XFIND       OCEAN MEAN DEPTH (M)                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN VOLUME (M3)                            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
      echo "C*XSAVE       OCEAN MEAN DEPTH (M)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODPT
C*XSAVE       OCEAN VOLUME (M3)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OVOL" >> ic.xsave_ann
      fi # PhysO = on

      fi # $m=12

# ------------------------------------ xsave monthly rtd statistics (split into batches of no more that $nvarmax variables)

      rtdlist=`echo $rtdlist | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      rtdlist0="$rtdlist"
      ln -sf ic.xsave ic.xsave0
      while [ -n "$rtdlist0" ] ; do
        rtdlist1=`echo "$rtdlist0" | cut -f1-$nvarmax -d' '`
        head -$nvarmax2 ic.xsave0 > ic.xsave1
        echo $rtdlist1
        head -$nvarmax2 ic.xsave1 | grep -v NEWNAM
        xsave newrtd $rtdlist1 newrtd1 input=ic.xsave1
        mv newrtd1 newrtd
        rtdlist0=`echo "$rtdlist0" | cut -f$nvarmaxp1- -d' '`
        tail -n +$nvarmax2p1 ic.xsave0 > ic.xsave1 ; mv ic.xsave1 ic.xsave0
      done
    done # month loop

# ------------------------------------ xsave annual rtd statistics (split into batches of no more that $nvarmax variables)

    rtdlist_ann=`echo "$rtdlist_ann" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
    rtdlist0="$rtdlist_ann"
    ln -sf ic.xsave_ann ic.xsave0
    while [ -n "$rtdlist0" ] ; do
      rtdlist1=`echo "$rtdlist0" | cut -f1-$nvarmax -d' '`
      head -$nvarmax2 ic.xsave0 > ic.xsave1
#     echo $rtdlist1
#     head -$nvarmax2 ic.xsave1 | grep -v NEWNAM
      xsave newrtd $rtdlist1 newrtd1 input=ic.xsave1
      mv newrtd1 newrtd
      rtdlist0=`echo "$rtdlist0" | cut -f$nvarmaxp1- -d' '`
      tail -n +$nvarmax2p1 ic.xsave0 > ic.xsave1 ; mv ic.xsave1 ic.xsave0
    done

#                                      relabel GRID to TIME
    echo "C*RELABL  +GRID
C*RELABL  +TIME      1001                                       100101    1" | ccc relabl newrtd newrtd1
    mv newrtd1 newrtd

# ------------------------------------ append current year to old rtd file

    if [ $year -gt ${year_rtdiag_start} ] ; then
      joinrtd oldrtd newrtd newrtd2 ; mv newrtd2 newrtd
    fi

# ************************************ save new runtime diagnostics.

    save newrtd $rtdfile

# ************************************ delete older rtd files
    if [ "$keep_old_rtdiag" != "on" ] ; then
      release old
      access  old $rtdfileo na nocp
      delete  old na || true
    fi

# ************************************ make copy to common location

    rtd_dest_dir=${rtd_dest_dir:="$RUNPATH_ROOT/../rtdfiles"} # default location for rtd files
    rm -f        $rtd_dest_dir/$rtdfile
    cp -p newrtd $rtd_dest_dir/$rtdfile
