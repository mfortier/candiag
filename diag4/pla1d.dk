#deck pla1d
jobname=pla1d ; time=$gptime ; memory=$memory3
. comjcl.cdk

cat > Execute_Script <<'end_of_script'
#
#
#
#                  pla1d               KS - Dec 05/13 -------------pla1d
#   ---------------------------------- Compute diagnostic results for
#                                      PLA calculations.
#
# -------------------------------------------------------------------------
#
#  Output variables:
#
#  1) General
#
#  ALTI : Altitude
#
#  2) CDNC-related
#
#  ZCDN : Cloud droplet number concentration
#  CC02 : CCN concentration at 0.2% supersaturation
#  CC04 : CCN concentration at 0.4% supersaturation
#  CC08 : CCN concentration at 0.8% supersaturation
#  CC16 : CCN concentration at 1.6% supersaturation
#  WPAR : Parcel vertical velocity in calculation of CDNC
#  RCRI : Critical radius in calculation of CDNC
#  SUPS : Maximum supersaturation in calculation of CDNC
#  CCNE : Cloud droplet number concentration diagnosed based on aerosol 
#         mass concentrations (semi-empirical parameterization)
#  VCNE : Vertical integral of CCNE
#
#  3) Aerosol microphysics-related
#
#  VOAE : Emission flux of particulate organic matter
#  VBCE : Emission flux of black carbon
#  VASE : Emission flux of ammonium sulphate
#  VMDE : Emission flux of mineral dust
#  VSSE : Emission flux of sea salt
#  VOAG : Gas-to-particle conversion flux of particulate organic matter
#  VBCG : Gas-to-particle conversion flux of black carbon
#  VASG : Gas-to-particle conversion flux of ammonium sulphate
#  VMDG : Gas-to-particle conversion flux of mineral dust
#  VSSG : Gas-to-particle conversion flux of sea salt
#  VOAW : Stratiform cloud deposition flux of particulate organic matter
#  VBCW : Stratiform cloud deposition flux of black carbon
#  VASW : Stratiform cloud deposition flux of ammonium sulphate
#  VMDW : Stratiform cloud deposition flux of mineral dust
#  VSSW : Stratiform cloud deposition flux of sea salt
#  VOAC : Cumulus cloud deposition flux of particulate organic matter
#  VBCC : Cumulus cloud deposition flux of black carbon
#  VASC : Cumulus cloud deposition flux of ammonium sulphate
#  VMDC : Cumulus cloud deposition flux of mineral dust
#  VSSC : Cumulus cloud deposition flux of sea salt
#  VOAD : Dry deposition and graviational settling flux of particulate organic matter
#  VBCD : Dry deposition and graviational settling flux of black carbon
#  VASD : Dry deposition and graviational settling flux of ammonium sulphate
#  VMDD : Dry deposition and graviational settling flux of mineral dust
#  VSSD : Dry deposition and graviational settling flux of sea salt
#  HENR : Henry's law constant for sulphur dioxide in cloud liquid water
#  O3FR : Fraction of ozone oxidation in total in-cloud oxidation of sulphur
#  VASI : Ammonium sulphate in-cloud oxidation flux 
#  VAS1 : Stratiform cloud in-cloud scavenging flux of ammonium sulphate
#  VAS2 : Stratiform cloud below-cloud scavenging flux of ammonium sulphate
#  VAS3 : Stratiform cloud below-cloud re-evaporation flux of ammonium sulphate
#  SCND : Gas-to-particle condensation rate of ammonium sulphate 
#  VSCD : Vertical integral of SCND
#  SNCN : Gas-to-particle nucleation rate of aerosol number
#  VNCN : Vertical integral of SNCN
#  SSUN : Gas-to-particle nucleation rate of ammonium sulphate
#  VASN : Vertical integral of SSUN
#  SGSP : Gas-phase production rate of sulphuric acid
#  VGSP : Vertical integral of SGSP 
#
.   ggfiles.cdk
#
# ---------------------------------------- select input fields.
#
   varl3d='ALTI SNCN SSUN SCND SGSP CC02 CC04 CC08 CC16 WPAR'
   varl3dc='ZCDN CCNE RCRI SUPS HENR O3FR'
   varl2d='VNCN VASN VSCD VGSP VOAE VBCE VASE VMDE VSSE VOAW VBCW VASW VMDW VSSW 
           VOAD VBCD VASD VMDD VSSD VOAG VBCG VASG VMDG VSSG VOAC VBCC VASC VMDC
           VSSC VASI VAS1 VAS2 VAS3 VCNE'
#
   for VL in $varl3d ; do
     echo "SELECT          $r1 $r2 $r3     -9001 1000      $VL" | ccc select npakgg $VL
   done
   for VL in $varl3dc ; do
     echo "SELECT          $r1 $r2 $r3     -9001 1000      $VL" | ccc select npakgg $VL
   done
   for VL in $varl2d ; do
     echo "SELECT          $r1 $r2 $r3         1    1      $VL" | ccc select npakgg $VL
   done
#
# ---------------------------------------- repack fields.
#
   for VL in $varl3d ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}0
   done
   for VL in $varl3dc ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}0
   done
   for VL in $varl2d ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}0
   done
#
# ---------------------------------------- time average results.
#
   for VL in $varl3d ; do
     timavg ${VL}0 ${VL}T
   done
   for VL in $varl2d ; do
     timavg ${VL}0 ${VL}T
   done
#
# ---------------------------------------- create masks for proper averaging
#                                          of in-cloud results.
#
   for VL in $varl3dc ; do
     echo "  FMASK.             NEXT   GT        0." | ccc fmask ${VL}0 ${VL}MSK
   done
#
# ---------------------------------------- time-average in-cloud results.
#
   for VL in $varl3dc ; do
     timavg ${VL}MSK ${VL}MA
     timavg ${VL}0  ${VL}TA
     div ${VL}TA ${VL}MA ${VL}T
   done
#   
# ---------------------------------------- "xsave" results
#
   for VL in $varl3d ; do
     echo "XSAVE.        ${VL}
NEWNAM.    ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl3dc ; do
     echo "XSAVE.        ${VL}
NEWNAM.    ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl2d ; do
     echo "XSAVE.        ${VL}
NEWNAM.    ${VL}" | ccc xsaveup new_gp ${VL}T
   done
#
#  ----------------------------------- save results.
    access oldgp ${flabel}gp
    xjoin oldgp new_gp newgp
    save newgp ${flabel}gp
    delete oldgp
#    cp new_gp newgp
#    save newgp ${flabel}xt

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         CNDBEL    = $flabel
         MODEL1    = $model1
         R1        = $r1
         R2        = $r2
         R3        =      $r3
0 PLA1D ------------------------------------------------------------------ PLA1D
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
end_of_data

. endjcl.cdk
